/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './AppNavigator';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import Logger from './src/Helper/Logger';

messaging().setBackgroundMessageHandler(async remoteMessage => {
    Logger.log('index noti : => ', remoteMessage)
    // displayNotification(remoteMessage.notification.title, remoteMessage.notification.body);
});


let displayNotification = (title, message) => {
    // we display notification in alert box with title and body

    PushNotification.localNotification({
        /* Android Only Properties */
        // ticker: 'My Notification Ticker', // (optional)
        autoCancel: true, // (optional) default: true
        largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
        smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
        bigText: message, // (optional) default: "message" prop
        // subText: 'This is a subText', // (optional) default: none
        // color: 'red', // (optional) default: system default
        vibrate: true, // (optional) default: true
        vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
        tag: 'some_tag', // (optional) add tag to message
        group: 'group', // (optional) add group to message
        ongoing: false, // (optional) set whether this is an "ongoing" notification
        // actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
        invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

        /* iOS only properties */
        alertAction: 'view', // (optional) default: view
        category: '', // (optional) default: empty string

        /* iOS and Android properties */
        title: title, // (optional)
        message: message, // (required)
        // userInfo: { screen: Notifications }, // (optional) default: {} (using null throws a JSON value '<null>' error)
        playSound: true, // (optional) default: true
        soundName: 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
        number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    });
}


AppRegistry.registerComponent(appName, () => App);
