// Global Imports
import React, { Component, useEffect } from 'react'
import { LogBox } from 'react-native'
import { NavigationContainer, useNavigation } from '@react-navigation/native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'

// File Imports
import App from './App'
import selectUserType from './src/screens/selectUserType'
import Register from './src/screens/Register'
// import CustomerStack from './src/Navigation/CustomerStack'
import Loader from './src/Components/Loader'
import MainReducers from './src/ReduxStore/MainReducer'
import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import ReduxThunk from "redux-thunk";
import Login from './src/screens/Login'
import ProviderRegistration from './src/screens/ProviderRegistration'
import ForgotPassword from './src/screens/ForgotPassword'
import CustomerDrawer from './src/Navigation/CustomerDrawer'
import CustomerBottomTab, { HandleNotification } from './src/Navigation/CustomerBottomTab'
import CustomerPendingOrderDetail from './src/screens/CustomerPendingOrderDetail'
import CustomerPendingOnDemandDetail from './src/screens/CustomerPendingOnDemandDetail'
import PushNotification from 'react-native-push-notification';
import PaymentWebView from './src/screens/PaymentWebView'
import Logger from './src/Helper/Logger'


export const store = createStore(
  MainReducers,
  applyMiddleware(ReduxThunk)
);

const Stack = createStackNavigator()

const AppNavigator = (props) => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Loader />
        <Stack.Navigator
          headerMode='none'
          initialRouteName="App">
          <Stack.Screen name="App" component={App} />

          <Stack.Screen name="selectUserType" component={selectUserType} />
          {/* <Stack.Screen name="CustomerStack" component={CustomerStack} /> */}

          <Stack.Screen name='Login' component={Login} />
          <Stack.Screen name='Register' component={Register} />
          <Stack.Screen name='PaymentWebView' component={PaymentWebView} />
          <Stack.Screen name='ProviderRegistration' component={ProviderRegistration} />
          <Stack.Screen name='ForgotPassword' component={ForgotPassword} />
          <Stack.Screen name='CustomerDrawer' component={CustomerDrawer} />
          <Stack.Screen name='BottomTab' component={CustomerBottomTab} />

          <Stack.Screen name='CustomerPendingOrderDetail' component={CustomerPendingOrderDetail} />
          <Stack.Screen name='CustomerPendingOnDemandDetail' component={CustomerPendingOnDemandDetail} />

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )

}

export default AppNavigator