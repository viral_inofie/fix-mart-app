// Global imports
import React, { Fragment, useEffect } from 'react';
import { View, LogBox, StatusBar, Platform } from 'react-native'
import * as Sentry from '@sentry/react-native'
import SplashScreen from 'react-native-splash-screen'
import { CommonActions } from '@react-navigation/native'
import { useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import crashlytics from '@react-native-firebase/crashlytics';
import messaging from '@react-native-firebase/messaging';
import io from "socket.io-client";
import PushNotification from 'react-native-push-notification';


// File imports
import {
  ACCESS_TOKEN,
  CUSTOMER_ROLE,
  PREV_DEVICE_FCM_TOKEN,
  SENTRY_URL,
  SERVICE_PROVIDER,
  SOCKET_URL,
  SUPPLIER,
  USER_PROFILE
} from './src/Helper/Constants'
import { updateLoader } from './src/ReduxStore/Actions/CommonActions';
import Colors from './src/Helper/Colors';
import APICallService from './src/Api/APICallService';
import { validateResponse } from './src/Helper/Helper';
import { UpdateLoginData } from './src/ReduxStore/Actions/AuthActions';
import Logger from './src/Helper/Logger';

Sentry.init({ dsn: SENTRY_URL })

export let socket = io(SOCKET_URL, {
  transports: ["polling", "websocket"],
  autoConnect: true,
})

socket.connect();

socket.on("connect", () => {
  Logger.log("socket connect :=> ", 'socket connected...')
})

socket.on("disconnect", () => {
  Logger.log('Socket disconnect : =>', 'socker disconnected ...')
})

socket.on("error_came", (error) => {
  Logger.log('socket error : => ', error)
})
console.log("socket checking", socket.connected);
const App = ({ navigation: { replace,navigate } }) => {



  const dispatch = useDispatch();

  // debugger
  useEffect(() => {

    messaging().onMessage(async notificationOpen => {
      Logger.log("notification 1::", notificationOpen)
      const { title, body } = notificationOpen.notification;
      debugger
      displayNotification(title, body, notificationOpen.data);
    });

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage,
      );
      const { data,foreground } = remoteMessage;
      if (data.type === "6") {
        navigate('CustomerAcceptedOnDemandDetail', {
          job_id: data.job_id,
        })
      }
      if (data.type === "7") {
        navigate('CustomerAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }
      if (data.type === "23") {
        navigate('CustomerAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }
      if (data.type === "10" || data.type === "11") {
        navigate('CustomerCompleteOrderDetail', {
          job_id: data.job_id,
        })
      }
      if (data.type === '3') {
        navigate('CustomerPendingOnDemandDetail', {
          job_id: data.job_id,
          provider_name: data.user_name
        })
      }
      if (data.type === '4') { // Customer Pending Order Detail
        navigate('CustomerPendingOrderDetail', {
          job_id: data.job_id,
          provider_name: data.user_name
        })
      }

      if (data.type === '3') {
        navigate('CustomerPendingOnDemandDetail', {
          job_id: data.job_id,
          provider_name: data.user_name
        })
      }
      if (data.type === '4') {
        navigate('CustomerPendingOrderDetail', {
          job_id: data.job_id,
          provider_name: data.user_name
        })
      }
      if (data.type === "6") {
        navigate('CustomerAcceptedOnDemandDetail', {
          job_id: data.job_id,
        })
      }
      if (data.type === "7") {
        navigate('CustomerAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }
      if (data.type === "23") {
        navigate('CustomerAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }
      if (data.type === "10" || data.type === "11") {
        navigate('CustomerCompleteOrderDetail', {
          job_id: data.job_id,
        })
      }
      if (data.type === "12") {
        navigate('ProviderCompletedOrderDetail', {
          job_id: data.job_id,
        })
      }
      if (data.type === '13') {
        navigate('ProviderCompletedOrderDetail', {
          job_id: data.job_id
        })
      }

      if (data.type === "14") {
        navigate('ProviderCompletedOrderDetail', {
          job_id: data.job_id
        })
      }

      if (data.type === "15") {
        navigate('ProviderCompletedOrderDetail', {
          job_id: data.job_id
        })
      }

      if (data.type === "5" || data.type === "6" || data.type === "8") {
        navigate('ProviderAcceptedOnDemandDetail', {
          job_id: data.job_id,
        })
      }
      if (data.type === "9") {
        navigate('ProviderAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }

      if (data.type === "16") {
        navigate('ProviderAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }

      if (data.type === "17") {
        navigate('ProviderAcceptedOrderDetail', {
          job_id: data.job_id
        })
      }

      if (data.type === "18") {
        navigate('SupplierNewOrderDetail', {
          job_id: data.job_id,
        })
      }

      if (data.type === "20") {
        navigate('SupplierAcceptedOrderDetail', {
          order_id: data.job_id
        })
      }

      if (data.type === "19") {
        navigate('SupplierSubPendingOrderDetail', {
          order_id: data.job_id
        })
      }

      if (data.type === "22") {
        navigate('SupplierCompletedOrderDetail', {
          order_id: data.job_id
        })
      }

      if (data.type === "21") {
        navigate('SupplierSubAcceptedOrderDetail', {
          order_id: data.job_id
        })
      }

      if (data.type === 'supplier_deliver') {
        navigate('SupplierSubAcceptedOrderDetail', {
          order_id: data.job_id
        })
      }

      if (foreground) {
        if (data.data.type === "2") {
          navigate('ProviderJobRequestDetail', {
            job_id: data.data.job_id,
            provider_name: data.data.user_name,
            type: "offdemand"
          })
        }
        if (data.data.type === "1") {
          navigate('ProviderJobRequestDetail', {
            job_id: data.data.job_id,
            provider_name: data.data.user_name,
            type: "ondemand"
          })
        }
      }

      else {
        if (data.type === "1") {
          navigate('ProviderJobRequestDetail', {
            job_id: data.job_id,
            provider_name: data.user_name,
            type: "ondemand"
          })
        }
        if (data.type === "2") {
          navigate('ProviderJobRequestDetail', {
            job_id: data.job_id,
            provider_name: data.user_name,
            type: "offdemand"
          })
        }
      }
      // navigation.navigate(remoteMessage.data.type);
    });

    GetToken();

    crashlytics().setCrashlyticsCollectionEnabled(true);

    checkPermission();


  }, []);

  debugger
  const displayNotification = (title, message, obj) => {
    console.log(title, message, obj)
    // we display notification in alert box with title and body

    PushNotification.localNotification({
      /* Android Only Properties */
      // ticker: 'My Notification Ticker', // (optional)
      autoCancel: true, // (optional) default: true
      largeIcon: 'ic_launcher', // (optional) default: "ic_launcher"
      smallIcon: 'ic_notification', // (optional) default: "ic_notification" with fallback for "ic_launcher"
      bigText: message, // (optional) default: "message" prop
      // subText: 'This is a subText', // (optional) default: none
      // color: 'red', // (optional) default: system default
      vibrate: true, // (optional) default: true
      vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      tag: 'some_tag', // (optional) add tag to message
      group: 'group', // (optional) add group to message
      ongoing: false, // (optional) set whether this is an "ongoing" notification
      // actions: ['Yes', 'No'], // (Android only) See the doc for notification actions to know more
      invokeApp: true, // (optional) This enable click on actions to bring back the application to foreground or stay in background, default: true

      /* iOS only properties */
      alertAction: 'view', // (optional) default: view
      category: '', // (optional) default: empty string

      /* iOS and Android properties */
      id: this.lastId, // (optional) Valid unique 32 bit integer specified as string. default: Autogenerated Unique ID
      title: title, // (optional)
      message: message, // (required)
      userInfo: { data: obj }, // (optional) default: {} (using null throws a JSON value '<null>' error)
      playSound: true, // (optional) default: true
      soundName: 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
      number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
    });
  }

  const checkPermission = async () => {
    if (Platform.OS === 'android') {
      getToken();
    } else {

      const enabled = await messaging().hasPermission();
      // If Premission granted proceed towards token fetch
      if (enabled) {
        getToken();
      } else {
        // If permission hasn’t been granted to our app, request user in requestPermission method. 
        requestPermission();
      }
    }
  }

  const getToken = async () => {
    let fcmToken = await AsyncStorage.getItem(PREV_DEVICE_FCM_TOKEN);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem(PREV_DEVICE_FCM_TOKEN, fcmToken);
      }
    }
    Logger.log("Token:: ", fcmToken)
  }

  const requestPermission = async () => {
    try {
      await messaging().requestPermission();
      // User has authorised
      getToken();
    } catch (error) {
      // User has rejected permissions
      Logger.log('permission rejected');
    }
  }

  const GetToken = () => {

    AsyncStorage.getItem(ACCESS_TOKEN, (err, res) => {
      console.log(res, 'errss')
      if (err === null && res != null) {
        dispatch(updateLoader(true))
        getUserData()
      }
      else {
        dispatch(updateLoader(false))
        replace('selectUserType')
      }
    })

  }

  const getUserData = () => {
    const apiCall = new APICallService(USER_PROFILE, {});
    apiCall.callAPI().then(async (res) => {
      if (validateResponse(res)) {
        dispatch(UpdateLoginData(res.data))
        _navigateToStack(res.data.role)
      }
      else {
        dispatch(updateLoader(false))
      }
    })
      .catch(err => {
        dispatch(updateLoader(false))

      })
  }

  const _navigateToStack = (role) => {
    if (role === CUSTOMER_ROLE) {
      replace('BottomTab')
      dispatch(updateLoader(false))
    }

    if (role === SERVICE_PROVIDER) {
      replace('CustomerDrawer', {
        type: 'provider'
      })
      dispatch(updateLoader(false))
    }

    if (role === SUPPLIER) {
      replace('CustomerDrawer', {
        type: 'supplier'
      })
      dispatch(updateLoader(false))
    }
  }



  return (
    <StatusBar backgroundColor={Colors.WHITE} barStyle='dark-content' />

  )
}

export default App
