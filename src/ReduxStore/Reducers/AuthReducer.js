import { UPDATE_LOGIN_DATA, UPDATE_LOCATION, UPDATE_ADDRESS, UPDATE_SHORT_ADDRESS, UPDATE_SUBURBS } from "../Types";

export const INITIAL_STATE = {
    loginRes: '',
    latitude: '',
    longitude: '',
    address: '',
    short_address: '',
    flat_no: '',
    suburbs: '',
    landmark: ''
};

const AuthReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATE_LOGIN_DATA:
            return { ...state, loginRes: action.payload }

        case UPDATE_LOCATION:
            return { ...state, latitude: action.payload.latitude, longitude: action.payload.longitude }

        case UPDATE_ADDRESS:
            return { ...state, address: action.payload }

        case UPDATE_SHORT_ADDRESS:
            return { ...state, short_address: action.payload }

        case UPDATE_SUBURBS:
            return { ...state, flat_no: action.payload.flat_no, suburbs : action.payload.suburbs, landmark : action.payload.landmark }

        default:
            return state;
    }
}
export default AuthReducer;
