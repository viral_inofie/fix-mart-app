import { SHOW_LOADER } from '../Types'

const INITIAL_STATE = {
    isLoading: false,
}

const CommonReducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SHOW_LOADER:
            return { ...state, isLoading: action.payload }

        default:
            return state
    }
}


export default CommonReducer;