import { combineReducers } from 'redux';
import CommonReducer from './Reducers/CommonReducer';
import AuthReducer from './Reducers/AuthReducer';



const MainReducers =  combineReducers({
  CommonReducer: CommonReducer,
  AuthReducer: AuthReducer,
});



export default MainReducers;