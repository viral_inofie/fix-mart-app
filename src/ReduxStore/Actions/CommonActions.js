import { SHOW_LOADER } from "../Types";


export const updateLoader = (loader) => {
    return async (dispatch) => {
        dispatch({
            type: SHOW_LOADER,
            payload: loader
        });
    }
}
