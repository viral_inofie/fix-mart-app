import { UPDATE_LOCATION, UPDATE_LOGIN_DATA, UPDATE_ADDRESS, UPDATE_SHORT_ADDRESS, UPDATE_SUBURBS } from "../Types";


export const UpdateLoginData = (loader) => {
    return async (dispatch) => {
        dispatch({
            type: UPDATE_LOGIN_DATA,
            payload: loader
        });
    }
}

export const UpdateLocation = (data) => {
    return ({ type: UPDATE_LOCATION, payload: data })
} 

export const UpdateAddress = (data) => {
    return ({ type: UPDATE_ADDRESS, payload: data })
} 


export const UpdateShortAddress = (data) => {
    return ({ type: UPDATE_SHORT_ADDRESS, payload: data })
} 

export const flat_subrub = (data) => {
    return  ({ type : UPDATE_SUBURBS,  payload : data})
}