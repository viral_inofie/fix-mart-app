export const SHOW_LOADER = "SHOW_LOADER";

export const UPDATE_LOGIN_DATA = 'UPDATE_LOGIN_DATA';

export const UPDATE_LOCATION = 'UPDATE_LOCATION';

export const UPDATE_ADDRESS = 'UPDATE_ADDRESS';

export const UPDATE_SHORT_ADDRESS = 'UPDATE_SHORT_ADDRESS';

export const UPDATE_SUBURBS = 'UPDATE_SUBURBS'
