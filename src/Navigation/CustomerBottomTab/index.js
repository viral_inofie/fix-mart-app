//Global imports
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//File imports
import CustomerHomeStack from '../CustomerHomeStack';
import CustomerMyRequestsStack from '../CustomerMyRequestsStack';
import CustomerMessageStack from '../CustomerMessageStack';
import More from '../../screens/More';
import BottomTabComponent from '../../Components/BottomTabComponent';
import CustomerMoreStack from '../CustomerMoreStack';
import Notification from '../../screens/Notification';
import Logger from '../../Helper/Logger';
import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react';
import PushNotification from 'react-native-push-notification';
import { createStackNavigator } from '@react-navigation/stack';
import CustomerPendingOrder from '../../screens/CustomerPendingOrder';
import CustomerPendingOrderDetail from '../../screens/CustomerPendingOrderDetail';
import CustomerPendingOnDemandDetail from '../../screens/CustomerPendingOnDemandDetail';
import CustomerCertificateOnDemand from '../../screens/CustomerCertificateOnDemand';
import CustomerAcceptedOrder from '../../screens/CustomerAcceptedOrder';
import CustomerAcceptedOrderDetail from '../../screens/CustomerAcceptedOrderDetail';
import CustomerReview from '../../screens/CustomerReview';
import CustomerAcceptedOnDemandDetail from '../../screens/CustomerAcceptedOnDemandDetail';
import CustomerCompletedOrder from '../../screens/CustomerCompletedOrder';
import CustomerCompletedOrderDetail from '../../screens/CustomerCompleteOrderDetail';
import CustomerDisputedOrder from '../../screens/CustomerDisputedOrder';
import CustomerDisputeOrderDetail from '../../screens/CustomerDisputedOrderDetail';
import CustomerSubmitNoteDispute from '../../screens/CustomerSubmitNoteDispute';
import messaging from '@react-native-firebase/messaging';

const BottomTabComp = (props) => <BottomTabComponent {...props} />


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();


function CustomerPendingOrderStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerPendingOrder'>
            <Stack.Screen name='CustomerPendingOrder' component={CustomerPendingOrder} />
            <Stack.Screen name='CustomerPendingOrderDetail' component={CustomerPendingOrderDetail} />
            <Stack.Screen name='CustomerPendingOnDemandDetail' component={CustomerPendingOnDemandDetail} />
            <Stack.Screen name='CustomerCertificateOnDemand' component={CustomerCertificateOnDemand} />
        </Stack.Navigator>
    );
}

function CustomerAcceptedOrderStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerAcceptedOrder'>
            <Stack.Screen name='CustomerAcceptedOrder' component={CustomerAcceptedOrder} />
            <Stack.Screen name='CustomerAcceptedOrderDetail' component={CustomerAcceptedOrderDetail} />
            <Stack.Screen name='CustomerReview' component={CustomerReview} />
            <Stack.Screen name='CustomerAcceptedOnDemandDetail' component={CustomerAcceptedOnDemandDetail} />
        </Stack.Navigator>
    );
}


function CustomerCompleteOrderStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerCompletedOrder'>
            <Stack.Screen name='CustomerCompletedOrder' component={CustomerCompletedOrder} />
            <Stack.Screen name='CustomerCompleteOrderDetail' component={CustomerCompletedOrderDetail} />
            <Stack.Screen name='CustomerReview' component={CustomerReview} />
        </Stack.Navigator>
    );
}

function CustomerDisputedOrderStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerDisputedOrder'>
            <Stack.Screen name='CustomerDisputedOrder' component={CustomerDisputedOrder} />
            <Stack.Screen name='CustomerDisputeOrderDetail' component={CustomerDisputeOrderDetail} />
            <Stack.Screen name='CustomerSubmitNoteDispute' component={CustomerSubmitNoteDispute} />
        </Stack.Navigator>
    );
}


function CustomerBottomTab() {
    const navigation = useNavigation();

    useEffect(() => {
        PushNotification.configure({
            onAction: function (notification) {
                console.log("customer ACTION:", notification.action);
                console.log("customer NOTIFICATION:", notification);
            
                // process the action
              },
            onNotification: function (notification) {
                setTimeout(() => {
                    Logger.log("onNotification :", notification)
                const { data } = notification;
                if (data.type === "6") {
                    navigation.navigate('CustomerAcceptedOnDemandDetail', {
                        job_id: data.job_id,
                    })
                }
                if (data.type === "7") {
                    navigation.navigate('CustomerAcceptedOrderDetail', {
                        job_id: data.job_id
                    })
                }
                if (data.type === "23") {
                    navigation.navigate('CustomerAcceptedOrderDetail', {
                        job_id: data.job_id
                    })
                }
                if (data.type === "10" || data.type === "11") {
                    navigation.navigate('CustomerCompleteOrderDetail', {
                        job_id: data.job_id,
                    })
                }
                if (data.type === '3') { 
                    navigation.navigate('CustomerPendingOnDemandDetail', {
                        job_id: data.job_id,
                        provider_name: data.user_name
                    })
                }
                if (data.type === '4') { // Customer Pending Order Detail
                    navigation.navigate('CustomerPendingOrderDetail', {
                        job_id: data.job_id,
                        provider_name: data.user_name
                    })
                }
                }, 3000);
                
            }
        })

    }, [])

    return (
        <Tab.Navigator tabBar={BottomTabComp}>
            <Tab.Screen name="Home" component={CustomerHomeStack} />
            
            <Tab.Screen name="Job Request" component={CustomerMyRequestsStack} />
            <Tab.Screen name="Orders" component={() => { }} />
            <Tab.Screen name="Notification" component={Notification} />
            <Tab.Screen name="More" component={CustomerMoreStack} />

            <Tab.Screen name="CustomerPendingOrderStack" component={CustomerPendingOrderStack} />
            <Tab.Screen name="CustomerAcceptedOrderStack" component={CustomerAcceptedOrderStack} />
            <Tab.Screen name="CustomerCompleteOrderStack" component={CustomerCompleteOrderStack} />
            <Tab.Screen name="CustomerDisputedOrderStack" component={CustomerDisputedOrderStack} />

            <Tab.Screen name='CustomerPendingOrderDetail' component={CustomerPendingOrderDetail} />
            <Tab.Screen name='CustomerPendingOnDemandDetail' component={CustomerPendingOnDemandDetail} />

            <Tab.Screen name='CustomerAcceptedOrderDetail' component={CustomerAcceptedOrderDetail} />
            <Tab.Screen name='CustomerAcceptedOnDemandDetail' component={CustomerAcceptedOnDemandDetail} />

            <Tab.Screen name='CustomerCompleteOrderDetail' component={CustomerCompletedOrderDetail} />
            <Tab.Screen name='CustomerReview' component={CustomerReview} />

        </Tab.Navigator>
    );
}


export default CustomerBottomTab;