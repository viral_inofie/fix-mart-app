//Global imports
import React ,{ useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';


//File imports
import CustomerCompleteOrderDetail from '../../screens/CustomerCompleteOrderDetail';
import CustomerCompletedOrder from '../../screens/CustomerCompletedOrder';
import CustomerReview from '../../screens/CustomerReview';
import { useNavigation } from '@react-navigation/native';

const Stack = createStackNavigator();

function CustomerCompleteOrderStack() {
    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if(remoteMessage.data.type === "10" || remoteMessage.data.type === "11"){
                navigation.navigate('CustomerCompleteOrderDetail',{
                    job_id : remoteMessage.data.job_id,
                })
            }
        });

    }, [])
    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerCompletedOrder'>
            <Stack.Screen name='CustomerCompletedOrder' component={CustomerCompletedOrder} />
            <Stack.Screen name='CustomerCompleteOrderDetail' component={CustomerCompleteOrderDetail} />
            <Stack.Screen name='CustomerReview' component={CustomerReview} />

        </Stack.Navigator>
    );
}

export default CustomerCompleteOrderStack;