//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import SupplierSubCompletedOrder from '../../screens/SupplierSubCompletedOrder';
import SupplierSubCompletedOrderDetail from '../../screens/SupplierSubCompletedOrderDetail';


const Stack = createStackNavigator();

function SupplierSubCompletedOrderStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='SupplierSubCompletedOrder'>
            <Stack.Screen  name='SupplierSubCompletedOrder' component={SupplierSubCompletedOrder} />
            <Stack.Screen name='SupplierSubCompletedOrderDetail' component={SupplierSubCompletedOrderDetail} />
        </Stack.Navigator>
    );
}

export default SupplierSubCompletedOrderStack;