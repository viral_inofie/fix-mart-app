//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import CustomerProfile from '../../screens/CustomerProfile';
import CustomerEditProfile from '../../screens/CustomerEditProfile';

const Stack = createStackNavigator();

function CustomerProfileStack({ route: { params: { type } } }) {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerProfile'>
            <Stack.Screen initialParams={{ user_type: type }} name='CustomerProfile' component={CustomerProfile} />
            <Stack.Screen name='CustomerEditProfile' component={CustomerEditProfile} />
        </Stack.Navigator>
    );
}

export default CustomerProfileStack;