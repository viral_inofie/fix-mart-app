//Global imports
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

//File imports
import ProviderAcceptedOrder from '../../screens/ProviderAcceptedOrder';
import ProviderAcceptedOrderDetail from '../../screens/ProviderAcceptedOrderDetail';
import ProviderAcceptedOnDemandDetail from '../../screens/ProviderAcceptedOnDemandDetail';
import { useNavigation } from '@react-navigation/native';


const Stack = createStackNavigator();

function ProviderAcceptedOrderStack() {

    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if(remoteMessage.data.type === "5" || remoteMessage.data.type === "6" || remoteMessage.data.type === "8"){
                navigation.navigate('ProviderAcceptedOnDemandDetail',{
                    job_id : remoteMessage.data.job_id,
                })
            }
            if(remoteMessage.data.type === "9"){
                navigation.navigate('ProviderAcceptedOrderDetail',{
                    job_id : remoteMessage.data.job_id
                })
            }

            if(remoteMessage.data.type === "16"){
                navigation.navigate('ProviderAcceptedOrderDetail',{
                    job_id : remoteMessage.data.job_id
                })
            }

            if(remoteMessage.data.type === "17"){
                navigation.navigate('ProviderAcceptedOrderDetail',{
                    job_id : remoteMessage.data.job_id
                })
            }
            
        });

    }, [])

    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='ProviderAcceptedOrder'>
            <Stack.Screen  name='ProviderAcceptedOrder' component={ProviderAcceptedOrder} />
            <Stack.Screen  name='ProviderAcceptedOrderDetail' component={ProviderAcceptedOrderDetail} />
            <Stack.Screen  name='ProviderAcceptedOnDemandDetail' component={ProviderAcceptedOnDemandDetail} />
        </Stack.Navigator>
    );
}

export default ProviderAcceptedOrderStack;