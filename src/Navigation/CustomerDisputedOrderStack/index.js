//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import CustomerDisputedOrder from '../../screens/CustomerDisputedOrder';
import CustomerDisputeOrderDetail from '../../screens/CustomerDisputedOrderDetail';
import CustomerSubmitNoteDispute from '../../screens/CustomerSubmitNoteDispute';

const Stack = createStackNavigator();

function CustomerDisputedOrderStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerDisputedOrder'>
            <Stack.Screen name='CustomerDisputedOrder' component={CustomerDisputedOrder} />
            <Stack.Screen name='CustomerDisputeOrderDetail' component={CustomerDisputeOrderDetail} />
            <Stack.Screen name='CustomerSubmitNoteDispute' component={CustomerSubmitNoteDispute} />
        </Stack.Navigator>
    );
}

export default CustomerDisputedOrderStack;