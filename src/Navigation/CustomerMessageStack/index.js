//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import CustomerMessages from '../../screens/CustomerMessages';
import CustomerConversation from '../../screens/CustomerConversation';

const Stack = createStackNavigator();

function CustomerMessageStack({ route: { params: { type } } }) {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerConversation'>
            <Stack.Screen name='CustomerConversation' initialParams={{ user_type: type }}  component={CustomerConversation} />
            <Stack.Screen name='CustomerMessages' component={CustomerMessages} />
        </Stack.Navigator>
    );
}

export default CustomerMessageStack;