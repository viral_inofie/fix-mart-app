//Global imports
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import ProviderPedingOrder from '../../screens/ProviderPedingOrder';
import ProviderPendingOrderDetail from '../../screens/ProviderPendingOrderDetail';
import ProviderPendingOnDemandDetail from '../../screens/ProviderPendingOnDemandDetail';
import ProviderOnDemandHelp from '../../screens/ProviderOnDemadHelp';
import CustomerCertificateOnDemand from '../../screens/CustomerCertificateOnDemand';
import { useNavigation } from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';

const Stack = createStackNavigator();

function ProviderPendingOrderStack() {

    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='ProviderPedingOrder'>
            <Stack.Screen  name='ProviderPedingOrder' component={ProviderPedingOrder} />
            <Stack.Screen  name='ProviderPendingOrderDetail' component={ProviderPendingOrderDetail} />
            <Stack.Screen  name='ProviderPendingOnDemandDetail' component={ProviderPendingOnDemandDetail} />
            <Stack.Screen  name='ProviderOnDemandHelp' component={ProviderOnDemandHelp} />

        </Stack.Navigator>
    );
}

export default ProviderPendingOrderStack;