//Global imports
import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

//File imports
import CustomerPendingOrder from '../../screens/CustomerPendingOrder';
import CustomerPendingOrderDetail from '../../screens/CustomerPendingOrderDetail';
import CustomerPendingOnDemandDetail from '../../screens/CustomerPendingOnDemandDetail';
import CustomerCertificateOnDemand from '../../screens/CustomerCertificateOnDemand';
import { useNavigation } from '@react-navigation/native';
import PushNotification from 'react-native-push-notification';

const Stack = createStackNavigator();


function CustomerPendingOrderStack() {

    const navigation = useNavigation();


    useEffect(() => {



        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if (remoteMessage.data.type === '3') {
                navigation.navigate('CustomerPendingOnDemandDetail', {
                    job_id: remoteMessage.data.job_id,
                    provider_name: remoteMessage.data.user_name
                })
            }
            if (remoteMessage.data.type === '4') {
                navigation.navigate('CustomerPendingOrderDetail', {
                    job_id: remoteMessage.data.job_id,
                    provider_name: remoteMessage.data.user_name
                })
            }
        });

    }, [])

    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerPendingOrder'>
            <Stack.Screen name='CustomerPendingOrder' component={CustomerPendingOrder} />
            <Stack.Screen name='CustomerPendingOrderDetail' component={CustomerPendingOrderDetail} />
            <Stack.Screen name='CustomerPendingOnDemandDetail' component={CustomerPendingOnDemandDetail} />
            <Stack.Screen name='CustomerCertificateOnDemand' component={CustomerCertificateOnDemand} />
        </Stack.Navigator>
    );
}

export default CustomerPendingOrderStack;