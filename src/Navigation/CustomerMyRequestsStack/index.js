import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import CustomerMyRequests from '../../screens/CustomerMyRequests';
import CustomerMyRequestsDetail from '../../screens/CustomerMyRequestsDetail';

const Stack = createStackNavigator();

function CustomerMyRequestsStack({ route: { params : type } }) {
   console.log("new job request",type)
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerMyRequests'>
            <Stack.Screen name='CustomerMyRequests' initialParams={{ user_type: type }} component={CustomerMyRequests} />
            <Stack.Screen name='CustomerMyRequestsDetail' component={CustomerMyRequestsDetail} />
        </Stack.Navigator>
    );
}

export default CustomerMyRequestsStack;