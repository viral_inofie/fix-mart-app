//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import SupplierHome from '../../screens/SupplierHome';
import SupplierNewOrderDetail from '../../screens/SupplierNewOrderDetail';
import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react';

import messaging from '@react-native-firebase/messaging';

const Stack = createStackNavigator();

function SupplierHomeStack() {

    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if(remoteMessage.data.type === "18"){
                navigation.navigate('SupplierNewOrderDetail',{
                    job_id : remoteMessage.data.job_id,
                })
            }
        });

    }, [])
    
    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='SupplierHome'>
            <Stack.Screen name='SupplierHome' component={SupplierHome} />
            <Stack.Screen name='SupplierNewOrderDetail' component={SupplierNewOrderDetail} />
        </Stack.Navigator>
    );
}

export default SupplierHomeStack;