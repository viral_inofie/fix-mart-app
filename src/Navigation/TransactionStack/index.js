//Global imports
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import TransactionHistory from '../../screens/TransactionHistory';
import CustomerTransactionHistoryDetail from '../../screens/CustomerTransactionHistoryDetail';


const Stack = createStackNavigator();

function TransactionStack({ route: { params: { type } } }) {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='TransactionHistory'>
            <Stack.Screen initialParams={{ user_type: type }} name='TransactionHistory' component={TransactionHistory} />
            <Stack.Screen name='CustomerTransactionHistoryDetail' component={CustomerTransactionHistoryDetail} />
        </Stack.Navigator>
    );
}

export default TransactionStack;