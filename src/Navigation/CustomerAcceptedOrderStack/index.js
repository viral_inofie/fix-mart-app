import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

import CustomerAcceptedOrder from '../../screens/CustomerAcceptedOrder';
import CustomerAcceptedOrderDetail from '../../screens/CustomerAcceptedOrderDetail';
import CustomerReview from '../../screens/CustomerReview';
import CustomerAcceptedOnDemandDetail from '../../screens/CustomerAcceptedOnDemandDetail';
import { useNavigation } from '@react-navigation/native';
import PushNotification from 'react-native-push-notification';


const Stack = createStackNavigator();

function CustomerAcceptedOrderStack() {
    const navigation = useNavigation();

    useEffect(() => {


        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if (remoteMessage.data.type === "6") {
                navigation.navigate('CustomerAcceptedOnDemandDetail', {
                    job_id: remoteMessage.data.job_id,
                })
            }
            if (remoteMessage.data.type === "7") {
                navigation.navigate('CustomerAcceptedOrderDetail', {
                    job_id: remoteMessage.data.job_id
                })
            }
            if (remoteMessage.data.type === "23") {
                navigation.navigate('CustomerAcceptedOrderDetail', {
                    job_id: remoteMessage.data.job_id
                })
            }
        });

    }, [])
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerAcceptedOrder'>
            <Stack.Screen name='CustomerAcceptedOrder' component={CustomerAcceptedOrder} />
            <Stack.Screen name='CustomerAcceptedOrderDetail' component={CustomerAcceptedOrderDetail} />
            <Stack.Screen name='CustomerReview' component={CustomerReview} />
            <Stack.Screen name='CustomerAcceptedOnDemandDetail' component={CustomerAcceptedOnDemandDetail} />
        </Stack.Navigator>
    );
}

export default CustomerAcceptedOrderStack;