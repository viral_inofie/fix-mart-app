//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import CustomerHome from '../../screens/CustomerHome';
import CustomerService from '../../screens/CustomerService';
import CustomerCategory from '../../screens/CustomerCategory';
import CustomerSubCategory from '../../screens/CustomerSubCategory';
import CustomerRequestDetail from '../../screens/CustomerRequestDetail';
import CustomerRequestDetailConfirm from '../../screens/CustomerRequestDetailConfirm';
import Notification from '../../screens/Notification';
import SearchAddress from '../SearchAddress';

const Stack = createStackNavigator();

const NotificationConst = () => <Notification UserType='customer' />

function CustomerHomeStack(props) {
    console.log(props,'homestack')
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='CustomerHome'>
            <Stack.Screen name='CustomerHome' component={CustomerHome} />
            <Stack.Screen name='CustomerService' component={CustomerService} />
            <Stack.Screen name='CustomerCategory' component={CustomerCategory} />
            <Stack.Screen name='CustomerSubCategory' component={CustomerSubCategory} />
            <Stack.Screen name='CustomerRequestDetail' component={CustomerRequestDetail} />
            <Stack.Screen name='CustomerRequestDetailConfirm' component={CustomerRequestDetailConfirm} />
            <Stack.Screen name='Notification' component={NotificationConst} />
            <Stack.Screen name='SearchAddress' component={SearchAddress} />
        </Stack.Navigator>
    );
}

export default CustomerHomeStack;