import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Register from '../../screens/Register'
import ProviderRegistration from '../../screens/ProviderRegistration'
import Login from '../../screens/Login'
import ForgotPassword from '../../screens/ForgotPassword'
import CustomerDrawer from '../CustomerDrawer'
import Logger from '../../Helper/Logger'


const Stack = createStackNavigator()

function CustomerStack(props) {
  const { navigation, route: { params } } = props
  Logger.log('props => ', props)
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName={params.type === 'AutoLogin' ? 'CustomerDrawer' : 'Login'}>
      <Stack.Screen name='Login' component={Login} initialParams={{ type: props.route.params.type }} />
      <Stack.Screen name='Register' component={Register} initialParams={{ type: props.route.params.type }} />
      <Stack.Screen name='ProviderRegistration' component={ProviderRegistration} initialParams={{ type: props.route.params.type }} />
      <Stack.Screen name='ForgotPassword' component={ForgotPassword} />
      <Stack.Screen name='CustomerDrawer' component={() => <CustomerDrawer type={params.user} />} />
    </Stack.Navigator>
  )
}

export default CustomerStack