//Global imports
import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

//File imports
import SupplierSubPendingOrder from '../../screens/SupplierSubPendingOrder';
import SupplierSubPendingOrderDetail from '../../screens/SupplierSubPendingOrderDetail';
import { useNavigation } from '@react-navigation/native';


const Stack = createStackNavigator();

function SupplierSubPendingOrderStack() {

    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            
            if(remoteMessage.data.type === "19"){
                navigation.navigate('SupplierSubPendingOrderDetail',{
                    order_id : remoteMessage.data.job_id
                })
            }
        });

    }, [])
    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='SupplierSubPendingOrder'>
            <Stack.Screen  name='SupplierSubPendingOrder' component={SupplierSubPendingOrder} />
            <Stack.Screen name='SupplierSubPendingOrderDetail' component={SupplierSubPendingOrderDetail} />
        </Stack.Navigator>
    );
}

export default SupplierSubPendingOrderStack;