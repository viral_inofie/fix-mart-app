//Global imports
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

//File imports
import More from '../../screens/More';
import CustomerReportToAdmin from '../../screens/CustomerReportToAdmin';
import TransactionStack from '../TransactionStack';
import CustomerEditProfile from '../../screens/CustomerEditProfile';
import ChangePassword from '../../screens/ChangePassword';

const Stack = createStackNavigator();

function CustomerMoreStack() {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='More'>
            <Stack.Screen name='More' component={More} />
            <Stack.Screen name='TransactionStack' component={TransactionStack} />
            <Stack.Screen name='CustomerReportToAdmin' component={CustomerReportToAdmin} />
            <Stack.Screen name='CustomerEditProfile' component={CustomerEditProfile} />
            <Stack.Screen name='ChangePassword' component={ChangePassword} />

        </Stack.Navigator>
    );
}

export default CustomerMoreStack;