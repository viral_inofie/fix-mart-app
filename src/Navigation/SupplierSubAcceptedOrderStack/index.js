//Global imports
import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

//File imports
import SupplierSubAcceptedOrder from '../../screens/SupplierSubAcceptedOrder';
import SupplierSubAcceptedOrderDetail from '../../screens/SupplierSubAcceptedOrderDetail';
import { useNavigation } from '@react-navigation/native';



const Stack = createStackNavigator();

function SupplierSubAcceptedOrderStack() {
    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            
            if(remoteMessage.data.type === "21"){
                navigation.navigate('SupplierSubAcceptedOrderDetail',{
                    order_id : remoteMessage.data.job_id
                })
            }

            if(remoteMessage.data.type === 'supplier_deliver'){
                navigation.navigate('SupplierSubAcceptedOrderDetail',{
                    order_id : remoteMessage.data.job_id
                })
            }
        });

    }, [])
    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='SupplierSubAcceptedOrder'>
            <Stack.Screen  name='SupplierSubAcceptedOrder' component={SupplierSubAcceptedOrder} />
            <Stack.Screen name='SupplierSubAcceptedOrderDetail' component={SupplierSubAcceptedOrderDetail} />
        </Stack.Navigator>
    );
}

export default SupplierSubAcceptedOrderStack;