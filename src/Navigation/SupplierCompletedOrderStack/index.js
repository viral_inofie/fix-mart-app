//Global imports
import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

//File imports
import SupplierCompletedOrder from '../../screens/SupplierCompletedOrder';
import SupplierCompletedOrderDetail from '../../screens/SupplierCompleteOrderDetail';
import { useNavigation } from '@react-navigation/native';


const Stack = createStackNavigator();

function SupplierCompletedOrderStack() {

    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            
            if(remoteMessage.data.type === "22"){
                navigation.navigate('SupplierCompletedOrderDetail',{
                    order_id : remoteMessage.data.job_id
                })
            }

        });

    }, [])
    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='SupplierCompletedOrder'>
            <Stack.Screen name='SupplierCompletedOrder' component={SupplierCompletedOrder} />
            <Stack.Screen name='SupplierCompletedOrderDetail' component={SupplierCompletedOrderDetail} />
        </Stack.Navigator>
    );
}

export default SupplierCompletedOrderStack;