//Global imports
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';
//File imports
import ProviderCompletedOrder from '../../screens/ProviderCompletedOrder';
import ProviderCompletedOrderDetail from '../../screens/ProviderCompleteOrderDetail';
import { useNavigation } from '@react-navigation/native';

const Stack = createStackNavigator();

function ProviderCompletedOrderStack() {
    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if (remoteMessage.data.type === "12") {
                navigation.navigate('ProviderCompletedOrderDetail', {
                    job_id: remoteMessage.data.job_id,
                })
            }
            if(remoteMessage.data.type === '13') {
                navigation.navigate('ProviderCompletedOrderDetail', {
                    job_id: remoteMessage.data.job_id
                })
            }

            if(remoteMessage.data.type === "14") {
                navigation.navigate('ProviderCompletedOrderDetail', {
                    job_id: remoteMessage.data.job_id
                })
            }

            if(remoteMessage.data.type === "15") {
                navigation.navigate('ProviderCompletedOrderDetail', {
                    job_id: remoteMessage.data.job_id
                })
            }

        });

    }, [])
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='ProviderCompletedOrder'>
            <Stack.Screen name='ProviderCompletedOrder' component={ProviderCompletedOrder} />
            <Stack.Screen name='ProviderCompletedOrderDetail' component={ProviderCompletedOrderDetail} />
        </Stack.Navigator>
    );
}

export default ProviderCompletedOrderStack;