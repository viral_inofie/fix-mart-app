//Global imports
import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

//File imports
import CustomerDrawerComponent from '../../Components/CustomerDrawerComponent';
import Colors from '../../Helper/Colors';
import CustomerReportToAdmin from '../../screens/CustomerReportToAdmin';
import ProviderPedingOrder from '../../screens/ProviderPedingOrder'
import TransactionStack from '../TransactionStack';
import CustomerMessageStack from '../CustomerMessageStack/index';
import CustomerMyRequestsStack from '../CustomerMyRequestsStack';
import CustomerProfileStack from '../CustomerProfileStack';
import Notification from '../../screens/Notification';
import SupplierSubCompletedOrderStack from '../SupplierSubCompletedOrderStack';
import CustomerBottomTab from '../CustomerBottomTab';
import { useEffect } from 'react';
import PushNotification from 'react-native-push-notification';
import Logger from '../../Helper/Logger';
import { createStackNavigator } from '@react-navigation/stack';
import CustomerHome from '../../screens/CustomerHome';
import CustomerService from '../../screens/CustomerService';
import CustomerCategory from '../../screens/CustomerCategory';
import CustomerSubCategory from '../../screens/CustomerSubCategory';
import CustomerRequestDetail from '../../screens/CustomerRequestDetail';
import CustomerRequestDetailConfirm from '../../screens/CustomerRequestDetailConfirm';
import SearchAddress from '../SearchAddress';
import CustomerPendingOrder from '../../screens/CustomerPendingOrder';
import CustomerPendingOrderDetail from '../../screens/CustomerPendingOrderDetail';
import CustomerPendingOnDemandDetail from '../../screens/CustomerPendingOnDemandDetail';
import CustomerCertificateOnDemand from '../../screens/CustomerCertificateOnDemand';
import CustomerAcceptedOrder from '../../screens/CustomerAcceptedOrder';
import CustomerAcceptedOrderDetail from '../../screens/CustomerAcceptedOrderDetail';
import CustomerReview from '../../screens/CustomerReview';
import CustomerAcceptedOnDemandDetail from '../../screens/CustomerAcceptedOnDemandDetail';
import CustomerDisputedOrder from '../../screens/CustomerDisputedOrder';
import CustomerDisputeOrderDetail from '../../screens/CustomerDisputedOrderDetail';
import CustomerSubmitNoteDispute from '../../screens/CustomerSubmitNoteDispute';
import CustomerCompletedOrder from '../../screens/CustomerCompletedOrder';
import CustomerCompletedOrderDetail from '../../screens/CustomerCompleteOrderDetail';
import ProviderCompletedOrder from '../../screens/ProviderCompletedOrder';
import ProviderCompletedOrderDetail from '../../screens/ProviderCompleteOrderDetail';
import ProviderAcceptedOrder from '../../screens/ProviderAcceptedOrder';
import ProviderAcceptedOrderDetail from '../../screens/ProviderAcceptedOrderDetail';
import ProviderAcceptedOnDemandDetail from '../../screens/ProviderAcceptedOnDemandDetail';
import ProviderPendingOrderDetail from '../../screens/ProviderPendingOrderDetail';
import ProviderPendingOnDemandDetail from '../../screens/ProviderPendingOnDemandDetail';
import ProviderOnDemandHelp from '../../screens/ProviderOnDemadHelp';
import SupplierAcceptedOrder from '../../screens/SupplierAcceptedOrder';
import SupplierAcceptedOrderDetail from '../../screens/SupplierAcceptedOrderDetail';
import ProviderJobRequest from '../../screens/ProviderJobRequest';
import ProviderListOfProducts from '../../screens/ProviderListOfProducts';
import ProvideProductDetail from '../../screens/ProviderProductDetail';
import CustomerMessages from '../../screens/CustomerMessages';
import ProviderJobRequestDetail from "../../screens/ProviderJobRequestDetail";
import { useNavigation } from '@react-navigation/native';
import SupplierSubPendingOrder from '../../screens/SupplierSubPendingOrder';
import SupplierSubPendingOrderDetail from '../../screens/SupplierSubPendingOrderDetail';
import SupplierCompletedOrder from '../../screens/SupplierCompletedOrder';
import SupplierCompletedOrderDetail from '../../screens/SupplierCompleteOrderDetail';
import SupplierSubAcceptedOrder from '../../screens/SupplierSubAcceptedOrder';
import SupplierSubAcceptedOrderDetail from '../../screens/SupplierSubAcceptedOrderDetail';
import SupplierNewOrderDetail from "../../screens/SupplierNewOrderDetail";
import SupplierHome from "../../screens/SupplierHome";

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const NotificationConst = () => <Notification UserType='customer' />

function CustomerHomeStack(props) {
  console.log(props, 'homestack')
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='CustomerHome'>
      <Stack.Screen name='CustomerHome' component={CustomerHome} />
      <Stack.Screen name='CustomerService' component={CustomerService} />
      <Stack.Screen name='CustomerCategory' component={CustomerCategory} />
      <Stack.Screen name='CustomerSubCategory' component={CustomerSubCategory} />
      <Stack.Screen name='CustomerRequestDetail' component={CustomerRequestDetail} />
      <Stack.Screen name='CustomerRequestDetailConfirm' component={CustomerRequestDetailConfirm} />
      <Stack.Screen name='Notification' component={NotificationConst} />
      <Stack.Screen name='SearchAddress' component={SearchAddress} />
    </Stack.Navigator>
  );
}

function CustomerPendingOrderStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='CustomerPendingOrder'>
      <Stack.Screen name='CustomerPendingOrder' component={CustomerPendingOrder} />
      <Stack.Screen name='CustomerPendingOrderDetail' component={CustomerPendingOrderDetail} />
      <Stack.Screen name='CustomerPendingOnDemandDetail' component={CustomerPendingOnDemandDetail} />
      <Stack.Screen name='CustomerCertificateOnDemand' component={CustomerCertificateOnDemand} />
    </Stack.Navigator>
  );
}

function CustomerAcceptedOrderStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='CustomerAcceptedOrder'>
      <Stack.Screen name='CustomerAcceptedOrder' component={CustomerAcceptedOrder} />
      <Stack.Screen name='CustomerAcceptedOrderDetail' component={CustomerAcceptedOrderDetail} />
      <Stack.Screen name='CustomerReview' component={CustomerReview} />
      <Stack.Screen name='CustomerAcceptedOnDemandDetail' component={CustomerAcceptedOnDemandDetail} />
    </Stack.Navigator>
  );
}

function CustomerDisputedOrderStack() {
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='CustomerDisputedOrder'>
      <Stack.Screen name='CustomerDisputedOrder' component={CustomerDisputedOrder} />
      <Stack.Screen name='CustomerDisputeOrderDetail' component={CustomerDisputeOrderDetail} />
      <Stack.Screen name='CustomerSubmitNoteDispute' component={CustomerSubmitNoteDispute} />
    </Stack.Navigator>
  );
}

function CustomerCompleteOrderStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='CustomerCompletedOrder'>
      <Stack.Screen name='CustomerCompletedOrder' component={CustomerCompletedOrder} />
      <Stack.Screen name='CustomerCompleteOrderDetail' component={CustomerCompletedOrderDetail} />
      <Stack.Screen name='CustomerReview' component={CustomerReview} />

    </Stack.Navigator>
  );
}

function ProviderCompletedOrderStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='ProviderCompletedOrder'>
      <Stack.Screen name='ProviderCompletedOrder' component={ProviderCompletedOrder} />
      <Stack.Screen name='ProviderCompletedOrderDetail' component={ProviderCompletedOrderDetail} />
    </Stack.Navigator>
  );
}

function ProviderAcceptedOrderStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='ProviderAcceptedOrder'>
      <Stack.Screen name='ProviderAcceptedOrder' component={ProviderAcceptedOrder} />
      <Stack.Screen name='ProviderAcceptedOrderDetail' component={ProviderAcceptedOrderDetail} />
      <Stack.Screen name='ProviderAcceptedOnDemandDetail' component={ProviderAcceptedOnDemandDetail} />
    </Stack.Navigator>
  );
}


function ProviderPendingOrderStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='ProviderPedingOrder'>
      <Stack.Screen name='ProviderPedingOrder' component={ProviderPedingOrder} />
      <Stack.Screen name='ProviderPendingOrderDetail' component={ProviderPendingOrderDetail} />
      <Stack.Screen name='ProviderPendingOnDemandDetail' component={ProviderPendingOnDemandDetail} />
      <Stack.Screen name='ProviderOnDemandHelp' component={ProviderOnDemandHelp} />

    </Stack.Navigator>
  );
}

function SupplierHomeStack() {

  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='SupplierHome'>
      <Stack.Screen name='SupplierHome' component={SupplierHome} />
      <Stack.Screen name='SupplierNewOrderDetail' component={SupplierNewOrderDetail} />
    </Stack.Navigator>
  );
}

function SupplierAcceptedOrderStack() {
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='SupplierAcceptedOrder'>
      <Stack.Screen name='SupplierAcceptedOrder' component={SupplierAcceptedOrder} />
      <Stack.Screen name='SupplierAcceptedOrderDetail' component={SupplierAcceptedOrderDetail} />
    </Stack.Navigator>
  );
}

function ProviderJobRequestStack() {
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='ProviderJobRequest'>
      <Stack.Screen name='ProviderJobRequest' component={ProviderJobRequest} />
      <Stack.Screen name='ProviderJobRequestDetail' component={ProviderJobRequestDetail} />
      <Stack.Screen name='ProviderListOfProducts' component={ProviderListOfProducts} />
      <Stack.Screen name='ProvideProductDetail' component={ProvideProductDetail} />
      <Stack.Screen name='CustomerMessages' component={CustomerMessages} />
      <Stack.Screen name='ProviderOnDemandHelp' component={ProviderOnDemandHelp} />
    </Stack.Navigator>
  );
}

function SupplierSubPendingOrderStack() {
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='SupplierSubPendingOrder'>
      <Stack.Screen name='SupplierSubPendingOrder' component={SupplierSubPendingOrder} />
      <Stack.Screen name='SupplierSubPendingOrderDetail' component={SupplierSubPendingOrderDetail} />
    </Stack.Navigator>
  );
}

function SupplierCompletedOrderStack() {
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='SupplierCompletedOrder'>
      <Stack.Screen name='SupplierCompletedOrder' component={SupplierCompletedOrder} />
      <Stack.Screen name='SupplierCompletedOrderDetail' component={SupplierCompletedOrderDetail} />
    </Stack.Navigator>
  );
}

function SupplierSubAcceptedOrderStack() {
  return (
    <Stack.Navigator
      headerMode='none'
      initialRouteName='SupplierSubAcceptedOrder'>
      <Stack.Screen name='SupplierSubAcceptedOrder' component={SupplierSubAcceptedOrder} />
      <Stack.Screen name='SupplierSubAcceptedOrderDetail' component={SupplierSubAcceptedOrderDetail} />
    </Stack.Navigator>
  );
}

const CustomComp = (props, newProps) => <CustomerDrawerComponent {...props} value={newProps.route.params.type} />

function CustomerDrawer(props) {

  const navigation = useNavigation();
  useEffect(() => {
    PushNotification.configure({
      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);
    
        // process the action
      },
      onNotification: function (notification) {
        setTimeout(()=>{
          Logger.log("onNotificationDrawer :", notification)
          const { data, foreground } = notification;
          if (data.type === '3') {
            navigation.navigate('CustomerPendingOnDemandDetail', {
              job_id: data.job_id,
              provider_name: data.user_name
            })
          }
          if (data.type === '4') {
            navigation.navigate('CustomerPendingOrderDetail', {
              job_id: data.job_id,
              provider_name: data.user_name
            })
          }
          if (data.type === "6") {
            navigation.navigate('CustomerAcceptedOnDemandDetail', {
              job_id: data.job_id,
            })
          }
          if (data.type === "7") {
            navigation.navigate('CustomerAcceptedOrderDetail', {
              job_id: data.job_id
            })
          }
          if (data.type === "23") {
            navigation.navigate('CustomerAcceptedOrderDetail', {
              job_id: data.job_id
            })
          }
          if (data.type === "10" || data.type === "11") {
            navigation.navigate('CustomerCompleteOrderDetail', {
              job_id: data.job_id,
            })
          }
          if (data.type === "12") {
            navigation.navigate('ProviderCompletedOrderDetail', {
              job_id: data.job_id,
            })
          }
          if (data.type === '13') {
            navigation.navigate('ProviderCompletedOrderDetail', {
              job_id: data.job_id
            })
          }
  
          if (data.type === "14") {
            navigation.navigate('ProviderCompletedOrderDetail', {
              job_id: data.job_id
            })
          }
  
          if (data.type === "15") {
            navigation.navigate('ProviderCompletedOrderDetail', {
              job_id: data.job_id
            })
          }
  
          if (data.type === "5" || data.type === "6" || data.type === "8") {
            navigation.navigate('ProviderAcceptedOnDemandDetail', {
              job_id: data.job_id,
            })
          }
          if (data.type === "9") {
            navigation.navigate('ProviderAcceptedOrderDetail', {
              job_id: data.job_id
            })
          }
  
          if (data.type === "16") {
            navigation.navigate('ProviderAcceptedOrderDetail', {
              job_id: data.job_id
            })
          }
  
          if (data.type === "17") {
            navigation.navigate('ProviderAcceptedOrderDetail', {
              job_id: data.job_id
            })
          }
  
          if (data.type === "18") {
            navigation.navigate('SupplierNewOrderDetail', {
              job_id: data.job_id,
            })
          }
  
          if (data.type === "20") {
            navigation.navigate('SupplierAcceptedOrderDetail', {
              order_id: data.job_id
            })
          }
  
          if (data.type === "19") {
            navigation.navigate('SupplierSubPendingOrderDetail', {
              order_id: data.job_id
            })
          }
  
          if (data.type === "22") {
            navigation.navigate('SupplierCompletedOrderDetail', {
              order_id: data.job_id
            })
          }
  
          if (data.type === "21") {
            navigation.navigate('SupplierSubAcceptedOrderDetail', {
              order_id: data.job_id
            })
          }
  
          if (data.type === 'supplier_deliver') {
            navigation.navigate('SupplierSubAcceptedOrderDetail', {
              order_id: data.job_id
            })
          }
  
          if (foreground) {
            if (data.data.type === "2") {
              navigation.navigate('ProviderJobRequestDetail', {
                job_id: data.data.job_id,
                provider_name: data.data.user_name,
                type: "offdemand"
              })
            }
            if (data.data.type === "1") {
              navigation.navigate('ProviderJobRequestDetail', {
                job_id: data.data.job_id,
                provider_name: data.data.user_name,
                type: "ondemand"
              })
            }
          }
  
          else {
            if (data.type === "1") {
              navigation.navigate('ProviderJobRequestDetail', {
                job_id: data.job_id,
                provider_name: data.user_name,
                type: "ondemand"
              })
            }
            if (data.type === "2") {
              navigation.navigate('ProviderJobRequestDetail', {
                job_id: data.job_id,
                provider_name: data.user_name,
                type: "offdemand"
              })
            }
          }
        },3000)
        
      }
    })
  }, [navigation,props]);

  const UserType = props.route.params.type;
  console.log(UserType, 'UserType')

  return (
    <Drawer.Navigator
      drawerType='slide'
      overlayColor='#00000080'
      drawerStyle={{
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        width: '80%'
      }}

      initialRouteName={UserType === 'supplier' ? 'SupplierHomeStack' : UserType === 'provider' ? 'ProviderJobRequestStack' : 'CustomerHome'}
      drawerContent={(val) => CustomComp(val, props)} >
      <Drawer.Screen name="Home" initialParams={{ UserType: UserType }} component={CustomerHomeStack} />
      <Drawer.Screen name="Profile" initialParams={{ type: UserType }} component={CustomerProfileStack} />
      <Drawer.Screen name="New Job Requests" initialParams={{ type: UserType }} component={ProviderJobRequest} />
      <Drawer.Screen name="Order" component={() => { }} />
      <Drawer.Screen name="CustomerPendingOrder" component={CustomerPendingOrderStack} />
      <Drawer.Screen name="CustomerAcceptedOrder" component={CustomerAcceptedOrderStack} />
      <Drawer.Screen name="CustomerDisputedOrder" component={CustomerDisputedOrderStack} />
      <Drawer.Screen name="CustomerCompletedOrder" component={CustomerCompleteOrderStack} />
      <Drawer.Screen name="Transaction History" initialParams={{ type: UserType }} component={TransactionStack} />
      {props.route.params.type !== 'customer'
        && <Drawer.Screen name="Supplier Order" component={() => { }} />
      }
      <Drawer.Screen name="ProviderCompletedOrder" component={ProviderCompletedOrderStack} />
      <Drawer.Screen name="ProviderAcceptedOrder" component={ProviderAcceptedOrderStack} />
      <Drawer.Screen name="ProviderPedingOrder" component={ProviderPendingOrderStack} />
      <Drawer.Screen name="Messages" initialParams={{ type: UserType }} component={CustomerMessageStack} />
      <Drawer.Screen initialParams={{ UserType: UserType }} name="Notifications" component={Notification} />

      <Drawer.Screen name="Report To Admin" initialParams={{ type: UserType }} component={CustomerReportToAdmin} />
      <Drawer.Screen name="Logout" component={() => { }} />

      <Drawer.Screen name="SupplierHomeStack" component={SupplierHomeStack} />
      <Drawer.Screen name="SupplierAcceptedOrder" component={SupplierAcceptedOrderStack} />

      <Drawer.Screen name="ProviderJobRequestStack" component={ProviderJobRequestStack} />
      <Drawer.Screen name="SupplierSubPendingOrderStack" component={SupplierSubPendingOrderStack} />
      <Drawer.Screen name="SupplierCompletedOrderStack" component={SupplierCompletedOrderStack} />
      <Drawer.Screen name="SupplierSubAcceptedOrderStack" component={SupplierSubAcceptedOrderStack} />

      <Drawer.Screen name="SupplierSubCompletedOrderStack" component={SupplierSubCompletedOrderStack} />

      <Drawer.Screen name='ProviderListOfProducts' component={ProviderListOfProducts} />
      <Drawer.Screen name='ProvideProductDetail' component={ProvideProductDetail} />
      <Drawer.Screen name='CustomerMessages' component={CustomerMessages} />
      <Drawer.Screen name='ProviderOnDemandHelp' component={ProviderOnDemandHelp} />

      <Drawer.Screen name='ProviderAcceptedOrderDetail' component={ProviderAcceptedOrderDetail} />
      <Drawer.Screen name='ProviderJobRequestDetail' component={ProviderJobRequestDetail} />
      <Drawer.Screen name='CustomerPendingOnDemandDetail' component={CustomerPendingOnDemandDetail} />
      <Drawer.Screen name='CustomerPendingOrderDetail' component={CustomerPendingOrderDetail} />
      <Drawer.Screen name='CustomerAcceptedOnDemandDetail' component={CustomerAcceptedOnDemandDetail} />
      <Drawer.Screen name='CustomerAcceptedOrderDetail' component={CustomerAcceptedOrderDetail} />
      <Drawer.Screen name='CustomerCompleteOrderDetail' component={CustomerCompletedOrderDetail} />
      <Drawer.Screen name='ProviderCompletedOrderDetail' component={ProviderCompletedOrderDetail} />
      <Drawer.Screen name='ProviderAcceptedOnDemandDetail' component={ProviderAcceptedOnDemandDetail} />
      <Drawer.Screen name='SupplierNewOrderDetail' component={SupplierNewOrderDetail} />
      <Drawer.Screen name='SupplierAcceptedOrderDetail' component={SupplierAcceptedOrderDetail} />
      <Drawer.Screen name='SupplierSubPendingOrderDetail' component={SupplierSubPendingOrderDetail} />
      <Drawer.Screen name='SupplierCompletedOrderDetail' component={SupplierCompletedOrderDetail} />
      <Drawer.Screen name='SupplierSubAcceptedOrderDetail' component={SupplierSubAcceptedOrderDetail} />


    </Drawer.Navigator>
  );
}


export default CustomerDrawer;