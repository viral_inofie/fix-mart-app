//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, ImageBackground, ScrollView, Platform } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { RNToasty } from 'react-native-toasty';


//File imports
import Colors from '../../Helper/Colors';
import BackArrow from '../../../assets/images/backarrow.png';
import Styles from './Styles';
import BlackPin from '../../../assets/images/blackpin.png';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomTextField from '../../Components/TextField';
import CustomButton from '../../Components/Button';
import { useDispatch, useSelector } from 'react-redux';
import { height, width } from 'react-native-dimension';
import { RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { flat_subrub, UpdateAddress, UpdateLocation } from '../../ReduxStore/Actions/AuthActions';
import { add } from 'react-native-reanimated';
import Fonts from '../../Helper/Fonts';

const { InputField, saveButton, addressStyle, validationText } = Styles

const SearchAddress = ({ navigation: { pop, goBack, navigate } }) => {

    const [Data, SetData] = useState({
        flatNo: '',
        landmark: '',
        Suburb: ''
    })
    const [address, setAddress] = useState('');

    const dispatch = useDispatch();


    const { latitude, longitude, flat_no, suburb, landmark } = useSelector(state => ({
        latitude: state.AuthReducer.latitude,
        longitude: state.AuthReducer.longitude,
        flat_no: state.AuthReducer.flat_no,
        suburb: state.AuthReducer.suburbs,
        landmark: state.AuthReducer.landmark
    }))

    useEffect(() => {
        SetData({ ...Data, flatNo: flat_no, landmark: landmark, Suburb: suburb })
        dispatch(updateLoader(true))
        onSelectRegion({ latitude: latitude, longitude: longitude });
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToBack = () => {
        goBack()
    }

    const _navigateToHome = () => {
        let addStr = ''
        if (Data.flatNo != '') {
            addStr = addStr + Data.flatNo + ", "
        }
        if (Data.landmark != '') {
            addStr = addStr + Data.landmark + ", "
        }
        if (Data.Suburb != '') {
            addStr = addStr + Data.Suburb + ", "
        }

        addStr = addStr + address
        dispatch(UpdateAddress(addStr))
        dispatch(flat_subrub({
            flat_no: Data.flatNo,
            suburbs: Data.Suburb,
            landmark: Data.landmark
        }))
        pop();
        RNToasty.Success({
            title: 'Address saved successfully...!',
            withIcon: false,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const initialRegion = () => {
        // let {
        //   longitude,
        //   latitude
        // } = LatLong;
        if (latitude != "") {
            // initialAddress(LatLong.longitude,LatLong.latitude);
            return {
                longitude: longitude,
                latitude: latitude,
                longitudeDelta: 0.5,
                latitudeDelta: 0.5
            }
        }
        else {
            return null;
        }

    };

    const onDragEnd = event => {
        let {
            nativeEvent: {
                coordinate: { longitude, latitude },
            },
        } = event;
        dispatch(UpdateLocation({ latitude: latitude, longitude: longitude }))
        onSelectRegion({ latitude: latitude, longitude: longitude });
    };

    const userMarker = () => {

        if (latitude != '') {
            return (
                <Marker
                    onDragEnd={event => {
                        onDragEnd(event);
                    }}
                    draggable
                    coordinate={{ latitude, longitude }}
                    style={{ backgroundColor: 'transparent', height: 100, width: 100 }}
                    pinColor={Colors.APP_PRIMARY}
                >
                </Marker>
            );
        }
    };

    const onSelectRegion = async (data) => {
        let { latitude, longitude } = data;

        let key = "AIzaSyDOVCWIQrQbXmfaIpEhCxGNKlMqPpqELeo";

        fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${key}`)
            .then((resp) => resp.json())
            .then(async (dt) => {
                const {
                    results
                } = dt;
                console.log(dt, 'address')
                if (results.length > 0) {
                    dispatch(updateLoader(false))
                    setAddress(results[0].formatted_address)
                }
            })
            .catch((error) => {
                console.log('err', error)
                dispatch(updateLoader(false))

            });
    }

    const [BadFlat, set_BadFlat] = useState(false)
    const [BadSub, set_BadSub] = useState(false)

    const _validate = () => {

        const { flatNo, Suburb } = Data;

        let valid = true;

        if (flatNo === '') {
            set_BadFlat(true);
            valid = false
        }
        else {
            set_BadFlat(false);
        }

        if (Suburb === '') {
            set_BadSub(true);
            valid = false
        }
        else {
            set_BadSub(false)
        }

        return valid
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Search Address'
            LeftIcon={BackArrow}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderMapView = () => {
        return <View style={{ flex: 1 }}>
            <MapView
                style={{
                    height: height(45),
                    width: '100%',
                }}
                maxZoomLevel={20}
                minZoomLevel={12}
                provider={PROVIDER_GOOGLE}
                initialRegion={initialRegion()}
                loadingEnabled={false}
                loadingIndicatorColor={Colors.APP_PRIMARY}
                showsCompass={true}
                showsUserLocation={true}
                key={latitude}
            >
                {userMarker()}
            </MapView>
        </View>
    }

    const renderAddressField = () => (
        <View style={{ flex: 1, paddingHorizontal: width(10), paddingTop: height(2) }}>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', paddingHorizontal: width(3) }}>
                <ImageBackground source={BlackPin} resizeMode='contain' style={{ height: 18, width: 18 }} />

                <Text style={addressStyle}>{address}</Text>
            </View>

            <CustomTextField
                containerView={InputField}
                placeholder='Flat no/House no/Building no'
                value={Data.flatNo}
                onChangeText={(text) => SetData({ ...Data, flatNo: text })}
            />
            {
                BadFlat && (
                    <Text style={validationText}>Enter Flat no</Text>
                )
            }

            <CustomTextField
                containerView={InputField}
                placeholder='Suburb'
                value={Data.Suburb}
                onChangeText={(text) => SetData({ ...Data, Suburb: text })}
            />
            {
                BadSub && (
                    <Text style={validationText}>Enter Suburb</Text>
                )
            }

            <CustomTextField
                containerView={InputField}
                placeholder='Landmark'
                value={Data.landmark}
                onChangeText={(text) => SetData({ ...Data, landmark: text })}
            />



            <CustomButton
                title={'Save'}
                buttonAction={() => {
                    if (_validate()) {
                        _navigateToHome()
                    }
                }}
                btnStyle={saveButton}
            />
        </View>
    )

    return (
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.WHITE }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <ScrollView scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }} contentContainerStyle={{ paddingBottom: 20 }}>
                {renderHeader()}

                {renderMapView()}

                {renderAddressField()}
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default SearchAddress;