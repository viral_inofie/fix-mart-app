//Global imports
import { StyleSheet } from 'react-native';
import { height, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

const Styles = StyleSheet.create({
    InputField:{
        height:height(7),
        width:width(80),
        borderRadius:height(3.5),
        borderWidth:1,
        borderColor:Colors.INDEX_GREY,
        // paddingHorizontal:width(5),
        flexDirection:'row',
        alignItems:'center',
        marginTop:height(2),
    },


    saveButton:{
        height:height(7),
        width:width(80),
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.APP_PRIMARY,
        borderRadius:height(3.5),
        marginTop:height(2)
    },

    addressStyle : {
        fontSize : 14,
        color : Colors.BLACK06,
        fontFamily : Fonts.POPPINS_REGULAR,
        marginLeft : width(2)
    },

    validationText : {
        fontFamily : Fonts.POPPINS_REGULAR,
        color : 'red',
        fontSize : 14,
        marginLeft :20,
        marginTop : 5
    }
})

export default Styles;