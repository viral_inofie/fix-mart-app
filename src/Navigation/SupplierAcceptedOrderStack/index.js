//Global imports
import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

//File imports
import SupplierAcceptedOrder from '../../screens/SupplierAcceptedOrder';
import SupplierAcceptedOrderDetail from '../../screens/SupplierAcceptedOrderDetail';
import { useNavigation } from '@react-navigation/native';

const Stack = createStackNavigator();

function SupplierAcceptedOrderStack() {

    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            
            if(remoteMessage.data.type === "20"){
                navigation.navigate('SupplierAcceptedOrderDetail',{
                    order_id : remoteMessage.data.job_id
                })
            }
        });

    }, [])
    
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='SupplierAcceptedOrder'>
            <Stack.Screen name='SupplierAcceptedOrder' component={SupplierAcceptedOrder} />
            <Stack.Screen name='SupplierAcceptedOrderDetail' component={SupplierAcceptedOrderDetail} />
        </Stack.Navigator>
    );
}

export default SupplierAcceptedOrderStack;