//Global imports
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';


//File imports
import ProviderJobRequest from '../../screens/ProviderJobRequest';
import ProviderJobRequestDetail from '../../screens/ProviderJobRequestDetail';
import ProviderListOfProducts from '../../screens/ProviderListOfProducts';
import ProvideProductDetail from '../../screens/ProviderProductDetail';
import CustomerMessages from '../../screens/CustomerMessages';
import ProviderOnDemandHelp from '../../screens/ProviderOnDemadHelp';
import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react';

const Stack = createStackNavigator();

function ProviderJobRequestStack() {

    const navigation = useNavigation();

    useEffect(() => {
        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            if(remoteMessage.data.type === '1' || remoteMessage.data.type === '2' ){
                navigation.navigate('ProviderJobRequest',{
                    job_id : remoteMessage.data.job_id,
                })
            }
        });

    }, [])

    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName='ProviderJobRequest'>
            <Stack.Screen  name='ProviderJobRequest' component={ProviderJobRequest} />
            <Stack.Screen  name='ProviderJobRequestDetail' component={ProviderJobRequestDetail} />
            <Stack.Screen  name='ProviderListOfProducts' component={ProviderListOfProducts} />
            <Stack.Screen  name='ProvideProductDetail' component={ProvideProductDetail} />
            <Stack.Screen  name='CustomerMessages' component={CustomerMessages} />
            <Stack.Screen  name='ProviderOnDemandHelp' component={ProviderOnDemandHelp} />

        </Stack.Navigator>
    );
}

export default ProviderJobRequestStack;