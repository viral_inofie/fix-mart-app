//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList } from 'react-native';
import { useDispatch } from 'react-redux';


//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, GET_SUB_CATEGORY, REPAIRING, INSTALLATION, SUPPLY_AND_INSTALLTION } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import DrawerIcon from '../../../assets/images/Menu.png';
import BackArrow from '../../../assets/images/backarrow.png';
import CustomerSubCategoryList from '../../Components/CustomerSubCategoryList';
import { serviceParam } from '../../Api/APIJson';


const { Container } = Styles;

const CustomerSubCategory = ({ navigation: { navigate, toggleDrawer, goBack }, route: { params } }) => {
    const dispatch = useDispatch();
    console.log(params,'paramsparmas')
    useEffect(() => {
        dispatch(updateLoader(true))
        getSubCategory()
    }, [])

    const [SubCategoryData, SetSubCategoryData] = useState([])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _toggleDrawer = () => {
        goBack()
    }

    const _navigateToRequestDetail = (image, name, sub_category_id) => {
        navigate('CustomerRequestDetail', {
            job_type: params.job_type,
            sub_cat_name: name,
            image: image,
            sub_job_type: params.sub_job_type,
            sub_category_id: sub_category_id,
            category_id : params.category_id,
            address : params.address
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _renderItem = (item, index) => {
        return <CustomerSubCategoryList
            _navigateToRequestDetail={(image, name, sub_category_id) => _navigateToRequestDetail(image, name, sub_category_id)}
            item={item}
            time={100 * index}
        />
    }

    const getSubCategory = () => {
        const apiClass = new APICallService(GET_SUB_CATEGORY, serviceParam(params.category_id, params.sub_job_type ))

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    SetSubCategoryData(res.data)
                    dispatch(updateLoader(false))
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title={params.sub_job_type === 1 ? 'Plumbing Issues' : params.sub_job_type === 2 ? 'Plumbing Categories' : 'Plumbing Sub Categories'}
            LeftIcon={BackArrow}
            onLeftClick={() => _toggleDrawer()}
        />
    }

    const renderListView = () => {
        return <View style={Container}>
            {renderSubCategoryList()}
        </View>
    }

    const renderSubCategoryList = () => {
        return <FlatList
            data={SubCategoryData}
            numColumns={3}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => _renderItem(item, index)}
        />
    }


    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}
            {renderListView()}
        </View>
    )
}

export default CustomerSubCategory;