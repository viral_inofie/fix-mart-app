//Global imports
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    Container: {
        paddingLeft: 20,
        paddingVertical: 20
    }
})