//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, ScrollView, StatusBar } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { COMPLETE_ORDER_DETAIL_SUBPROVIDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    HeaderView,
    HeaderTitle,
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ViewButtonStyle,
    ViewText,
    StarView,
    ConfirmButton,
    ButtonText,
    ModelMainContainer,
    ModelHeader,
    ModelTitle,
    ReviewText
} = Styles;

const SupplierCompletedOrderDetail = ({ navigation: { goBack, addListener }, route: { params } }) => {

    const [ReviewModel, SetReviewModel] = useState(false);

    const [Data, setData] = useState({
        image: '',
        name: '',
        provider_name: '',
        visit_data: '',
        visit_time: '',
        service: '',
        subCategory: '',
        price: '',
        qty: '',
        other_details: ''
    })

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getOrderDetail();
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(COMPLETE_ORDER_DETAIL_SUBPROVIDER, { order_id: params.order_id });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    setData({
                        ...Data,
                        image: res.data.image,
                        name: res.data.name,
                        provider_name: res.data.provider_name,
                        visit_data: res.data.visit_date,
                        visit_time: res.data.visit_time,
                        service: res.data.service,
                        subCategory: res.data.sub_category,
                        price: res.data.price,
                        qty: res.data.quantity,
                        other_details: res.data.other_details
                    })
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Completed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: Data.image }}
            imageStyle={{ borderRadius: height(1.5) }}
            style={ImageStyle}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(40) }}>
                <Text style={PlumberDetailTitle}>Customer Name :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(40), }}>
                <Text style={PlumberDetailValue}>{Data.provider_name}</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_data).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Service :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Price :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Quantity :</Text>

            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
        </View>
    }

    const renderServiceValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>{Data.service}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.subCategory}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.qty}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.other_details}</Text>
        </View>
    }


    const renderServiceDetail = () => {
        return <View style={PlumberDetailView}>
            {renderServiceHeading()}

            {renderServiceValue()}
        </View>
    }

    const renderReview = () => {
        return <View style={ModelMainContainer}>
            <View style={ModelHeader}>
                <Text style={ModelTitle}>Plumber Reviews</Text>
            </View>

            <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            </View>
        </View>
    }


    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>

            </View>
        </Modal>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                bounces={false}
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {/* {renderReview()} */}

            </ScrollView>

            {renderModel()}

        </View>
    )
}

export default SupplierCompletedOrderDetail;