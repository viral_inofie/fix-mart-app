//Global imports
import { StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainContainer: {
        padding: 20
    },

    RequestImage: {
        height: 80,
        width: 80,
        alignSelf: 'center'
    },

    ImageView: {
        height: 150,
        width: 150,
        backgroundColor: Colors.SERVICEIMAGE_BACK_COLOR,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },

    RequestTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.BLACK06,
        textAlign: 'center',
        marginTop: 10
    },

    InputFieldStyle: {
        width: '100%',
        borderBottomWidth: 1,
        paddingHorizontal: 0,
        borderColor: Colors.INPUT_FIELD_BORDER_COLOR,
        marginTop: 20
    },

    FieldStyle: {
        flex: 1,
        textAlign: 'left',
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.BLACK,
    },

    SheduleView: {
        marginTop: 20
    },

    SheduleViewHeading: {
        fontSize: 16,
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.BLACK06
    },

    SheduleItemView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20
    },

    RadioView: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        padding:5,
        borderColor: Colors.BLACK06,
        backgroundColor:Colors.WHITE,
        justifyContent:'center',
        alignItems:'center'
    },

    SheduleText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.PLACEHOLDER_COLOR,
        fontSize: 14,
        marginLeft: 10
    },

    SubRadioView:{
        height: 12,
        width: 12,
        borderRadius:7.5,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR
    },

    TodayView:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
      
    },

    SelectDateView:{
        flexDirection:'row',
        alignItems: 'center',
        borderRadius:10,
        padding:8,
        justifyContent:'center',
        borderWidth:1,
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    SelectDateText:{
        fontSize:12,
        color:Colors.PLACEHOLDER_COLOR,
        fontFamily:Fonts.POPPINS_REGULAR
    },

    ConfirmButton:{
        height:50,
        width:'100%',
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius:25,
        marginTop:20
    },

    SubCatDropDown:{
        height:50,
        width:'100%',
        // backgroundColor:'red',
        marginTop:20,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:10,
        borderBottomWidth:1,
        borderColor : Colors.INPUT_FIELD_BORDER_COLOR
    },

    SubCatText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.BLACK06,
        fontSize:16
    },

    SubCateDropView:{
        padding : 15
    },

    ItemText:{
        fontSize:16,
        color:Colors.PLACEHOLDER_COLOR,
        fontFamily:Fonts.POPPINS_REGULAR
    },

    CloseView:{
        height:30,
        width:30,
        borderRadius:15,
        backgroundColor:Colors.WHITE,
        justifyContent:'center',
        alignItems:'center'
    }
})