//Global imports
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    ImageBackground,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    FlatList
} from 'react-native';
import { useDispatch } from 'react-redux';
import Octicons from 'react-native-vector-icons/Octicons';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';

import DateTimePicker from "react-native-modal-datetime-picker"
import moment from 'moment';
import AntDesign from 'react-native-vector-icons/AntDesign';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { DOMAIN_URL, GET_SUB_CATEGORY, GET_SUB_CATEGORY_REQUEST_PAGE, OPTIONS, POST_JOB_IMAGE_UPLOAD, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import TakePicture from '../../Components/TakePicture';
import BackArrow from '../../../assets/images/backarrow.png';
import CustomInput from '../../Components/TextField/index';
import AttachImage from '../../../assets/images/attach_image.png';
import CustomeButton from '../../Components/Button/index';
import ErrorModal from '../../Components/ErrorModal';
import PhotoUploadDialogue from '../../Components/PhotoUploadDialogue';
import { requestCameraPermission } from '../../Helper/Utills';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    MainContainer,
    RequestImage,
    ImageView,
    RequestTitle,
    InputFieldStyle,
    FieldStyle,
    SheduleView,
    SheduleViewHeading,
    SheduleItemView,
    RadioView,
    SheduleText,
    SubRadioView,
    TodayView,
    SelectDateView,
    SelectDateText,
    ConfirmButton,
    SubCatDropDown,
    SubCatText,
    SubCateDropView,
    ItemText,
    CloseView
} = Styles;



const CustomerRequestDetail = ({ navigation: { navigate, goBack, addListener }, route: { params } }) => {
    const [Data, SetData] = useState({
        TypeOfProduct: '',
        AddOtherDetails: '',
        Quantity: '',
        SubCategory: 'Select a Sub Category',
        sub_category_id: ''
    })

    const [OnDemandSheduleData, SetOnDemandSheduleData] = useState([
        { key: '0', name: 'Today', isSelected: false },
    ])

    const [OffDemandSheduleData, SetOffDemandSheduleData] = useState([
        { key: '0', name: 'Today', isSelected: false },
        { key: '1', name: 'This week', isSelected: false },
        { key: '2', name: 'This month', isSelected: false },
    ])

    const [AttachImageList, SetAttachImageList] = useState([]);
    const [NextInvestmentDate, SetNextInvestmentDate] = useState('')
    const [isDateModalVisible, SetisDateModalVisible] = useState(false)
    const [requestDate, SetRequestDate] = useState({
        date: '',
        time: ''
    })
    let [need_to_be, Setneed_to_be] = useState('');
    const [SubCatView, SetSubCatView] = useState(false);
    const [SubCategoryData, SetSubCategoryData] = useState([]);
    const [randomKey, SetRandomKey] = useState(Math.random());

    const [ErrorValidation, SetErrorValidation] = useState({
        Message: '',
        ShowErrorModel: false
    })

    const dispatch = useDispatch();
    const [photoModalOpen, setPhotoModalOpen] = useState(false);


    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getSubCategory()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _goBack = () => {
        goBack()
    }

    const _navigateToConfirm = () => {
        navigate('CustomerRequestDetailConfirm', {
            image: params.image,
            category_id: params.sub_category_id,
            job_type: params.job_type,
            sub_cat_name: Data.SubCategory,
            sub_category_id: Data.sub_category_id,
            TypeOfProduct: Data.TypeOfProduct,
            OtherDetails: Data.AddOtherDetails,
            Quantity: Data.Quantity,
            need_to_done: need_to_be === 'Today' ? requestDate.date + ' ' + requestDate.time : need_to_be,
            order_date: requestDate.date,
            order_time: requestDate.time,
            imagelist: AttachImageList,
            sub_job_type: params.sub_job_type,
            name: params.sub_cat_name,
            address: params.address
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _onChangeTypeOfProduct = (text) => SetData({ ...Data, TypeOfProduct: text });

    const _onChangeAddOtherDetail = (text) => SetData({ ...Data, AddOtherDetails: text });

    const _onChangeQuantity = (text) => SetData({ ...Data, Quantity: text });

    const _renderSheduleItem = (item) => {
        if (item.name === 'Today') {
            return _renderItemToday(item)
        }
        else {
            return _renderItem(item);
        }
    }

    const _renderItem = (item) => {
        return <View style={TodayView}>
            <TouchableOpacity
                onPress={() => {
                    _SelectShedule(item.key)
                    Setneed_to_be(item.name)
                }}
                style={SheduleItemView}>
                <View style={RadioView}>
                    {
                        item.isSelected
                            ?
                            <View style={SubRadioView}></View>
                            :
                            null
                    }
                </View>
                <Text style={[SheduleText, {
                    color: item.isSelected ? Colors.BLACK06 : Colors.PLACEHOLDER_COLOR
                }]}>{item.name}</Text>

            </TouchableOpacity>
            {
                item.isSelected && (renderSelectDateTimeView())
            }
        </View>
    }

    const _renderItemToday = (item) => {
        return <View style={TodayView}>
            <TouchableOpacity
                onPress={() => {
                    _SelectShedule(item.key)
                    Setneed_to_be(item.name)
                }}
                style={SheduleItemView}>
                <View style={RadioView}>
                    {
                        item.isSelected
                            ?
                            <View style={SubRadioView}></View>
                            :
                            null
                    }
                </View>

                <Text style={[SheduleText, {
                    color: item.isSelected ? Colors.BLACK06 : Colors.PLACEHOLDER_COLOR
                }]}>{item.name}</Text>
            </TouchableOpacity>

            {
                item.isSelected && (renderSelectDateTimeView())
            }
        </View>
    }

    const renderSelectDateTimeView = () => {
        return <TouchableOpacity
            onPress={() => SetisDateModalVisible(true)}
            style={SelectDateView}>
            {
                NextInvestmentDate === ''
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={SelectDateText}>Select Date & Time</Text>
                        <Octicons
                            name='calendar'
                            size={20}
                            style={{ marginLeft: 10 }}
                            color={Colors.BLACK} />
                    </View>
                    :

                    <Text style={SelectDateText}>{NextInvestmentDate}</Text>
            }
        </TouchableOpacity>
    }

    const _SelectShedule = (key) => {
        if (params.job_type === 1) {
            let New_Array = OnDemandSheduleData.map((item, i) => {
                item.isSelected = false;
                if (key === item.key) {
                    item.isSelected = true
                    return item
                }
                return item
            })
            SetOnDemandSheduleData(New_Array)
        }
        else {
            let New_Array = OffDemandSheduleData.map((item, i) => {
                item.isSelected = false;
                if (key === item.key) {
                    item.isSelected = true
                    return item
                }
                return item
            })
            SetOffDemandSheduleData(New_Array)
        }
    }

    const _renderAttachedImage = (data) => {
        return <ImageBackground
            source={{ uri: DOMAIN_URL + data.item.toString() }}
            style={{ height: 100, width: 100, marginRight: 20, justifyContent: 'flex-start', alignItems: 'flex-end', padding: 5 }}>
            <TouchableOpacity
                onPress={() => {
                    let newArray = AttachImageList.filter((item, i) => item !== data.item)
                    SetAttachImageList(newArray)
                    SetRandomKey(Math.random());
                }}
                style={CloseView}>
                <AntDesign size={15} color={Colors.DRAWER_BACKGROUND_COLOR} name='close' />
            </TouchableOpacity>
        </ImageBackground>
    }

    const _renderAttachImageFooter = () => {

        return(
            <TakePicture 
                isShow={false}
                onPress={(ImageObj)=>{
                    console.log("ImageObj",ImageObj)
                    uploadImageToServer(ImageObj)
                // this.uploadImage(ImageObj)
                }}
            >
            <ImageBackground source={AttachImage} resizeMode='contain' style={{ height: 100, width: 100 }} />
          </TakePicture>
        )
        return <TouchableOpacity onPress={() => {
            if (requestCameraPermission()) {
                setPhotoModalOpen(true)
            }
        }}>
            
        </TouchableOpacity>
    }

    const _uploadRequestImage = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // this.setState({
                //     avatarSource: source,
                // });
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let ImageObj = {
                    uri: response.uri,
                    name: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    type: response.type
                }
                // let New_Image_Array = [ImageObj];
                // SetAttachImageList([...AttachImageList, ...New_Image_Array])
                // console.log(AttachImageList)
                uploadImageToServer(ImageObj)
            }
        });
    }

    const _open_camera = () => {
        launchCamera(OPTIONS, (response) => {
            console.log(response)
            if (response.didCancel) {
                setPhotoModalOpen(false)
            }
            else {
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let ImageObj = {
                    uri: response.uri,
                    name: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    type: response.type
                }
                // let New_Image_Array = [ImageObj];
                // SetAttachImageList([...AttachImageList, ...New_Image_Array])
                // console.log(AttachImageList)
                uploadImageToServer(ImageObj)
                setPhotoModalOpen(false)
            }
        });

    }

    const _open_gallery = () => {
        launchImageLibrary(OPTIONS, (response) => {
            console.log(response)
            if (response.didCancel) {
                setPhotoModalOpen(false)
            }
            else {
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let ImageObj = {
                    uri: response.uri,
                    name: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    type: response.type
                }
                // let New_Image_Array = [ImageObj];
                // SetAttachImageList([...AttachImageList, ...New_Image_Array])
                // console.log(AttachImageList)
                uploadImageToServer(ImageObj)
                setPhotoModalOpen(false)
            }
        });

    }


    const _handleDatePicked = (val) => {


        //Logger.log({ selectedDate: getFormatedate(selectedDate, 'DD-MM-yyyy HH:mm:ss') })

        var d = new Date()
        d.setSeconds(0);
        //Logger.log({ today: moment().format('DD-MM-yyyy HH:mm:ss') })        

        if (d.getTime() < moment(val).toDate().getTime()) {
            let selectedDate = moment(val).format('YYYY-MM-DD HH:mm')
            // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
            SetNextInvestmentDate(`${selectedDate}`)
            var dateArray = selectedDate.split(" ")
            SetRequestDate({
                ...requestDate,
                date: dateArray[0],
                time: dateArray[1]
            })

            // setApiTime(getFormatedate(val, 'HH:mm:ss'))
            // setTime(getFormatedate(val, 'hh:mm a'))
        } else {
            SetNextInvestmentDate('')
            RNToasty.Error({
                title: "Select time greater than current time",
                fontFamily: Fonts.POPPINS_REGULAR,
                position: 'bottom',
                withIcon: false
            })
        }


        SetisDateModalVisible(false)
    }

    const _hideDateTimePicker = () => {
        SetisDateModalVisible(false)
    }

    const uploadImageToServer = (image) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(POST_JOB_IMAGE_UPLOAD, { image })

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    // let New_Image_Array = [res,data];
                    SetAttachImageList([...AttachImageList, ...res.data])
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })

    }

    const getSubCategory = () => {
        const apiClass = new APICallService(GET_SUB_CATEGORY_REQUEST_PAGE, { category_id: params.sub_category_id })

        apiClass.callAPI()
            .then(res => {
                console.log(res)
                if (validateResponse(res)) {
                    SetSubCategoryData(res.data)
                    dispatch(updateLoader(false))
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _Validate = () => {
        const { TypeOfProduct, AddOtherDetails, SubCategory } = Data

        if (TypeOfProduct == '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Please describe your request', ShowErrorModel: true })
            return false
        } else if (SubCategory === 'Select a Sub Category' && params.sub_job_type != 1 && params.sub_job_type != 2) {
            SetErrorValidation({ ...ErrorValidation, Message: 'Please select a sub category', ShowErrorModel: true })
            return false
        } else if (AddOtherDetails == '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Please add other details', ShowErrorModel: true })
            return false
        } else if (need_to_be === '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Please select date and time', ShowErrorModel: true })
            return false
        }
        else if (AttachImageList.length === 0) {
            SetErrorValidation({ ...ErrorValidation, Message: 'Please select images', ShowErrorModel: true })
            return false
        }
        else if(!requestDate.date){
            SetErrorValidation({ ...ErrorValidation, Message: 'Please select date and time', ShowErrorModel: true })
            return false;
        }else{
            return true
        }
    }

    const _CloseValidationModel = () => {
        SetErrorValidation({ ...ErrorValidation, ShowErrorModel: false })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Describe your request'
            LeftIcon={BackArrow}
            onLeftClick={() => _goBack()}
        />
    }

    const renderSubCategory = () => {
        if (params.sub_job_type === 1 || params.sub_job_type === 2) {
            return null
        }
        else {
            return <>
                <TouchableOpacity
                    onPress={() => SetSubCatView(!SubCatView)}
                    style={SubCatDropDown}>
                    <Text style={[SubCatText, {
                        color: Data.SubCategory === 'Select a Sub Category' ? Colors.PLACEHOLDER_COLOR : Colors.BLACK06
                    }]}>{Data.SubCategory}</Text>

                    <Octicons name='chevron-down' size={25} color={Colors.INPUT_FIELD_BORDER_COLOR} />
                </TouchableOpacity>

                {
                    SubCatView
                        ?
                        <View style={SubCateDropView}>
                            <FlatList
                                data={SubCategoryData}
                                renderItem={({ item, index }) => {
                                    return <TouchableOpacity
                                        onPress={() => {
                                            SetData({ ...Data, SubCategory: item.name, sub_category_id: item.id })
                                            SetSubCatView(false)
                                        }}
                                        style={{ paddingVertical: 5 }}>
                                        <Text style={ItemText}>{item.name}</Text>
                                    </TouchableOpacity>
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        :
                        null
                }

            </>
        }
    }

    const renderMainView = () => {
        return <ScrollView
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}>
            <View style={MainContainer}>
                {renderImage()}
                {renderRequestTitle()}
                {renderTypeOfProductField()}
                {renderSubCategory()}
                {renderOtherDetail()}
                {renderQtyField()}
                {renderSheduleView()}
                {renderAttachImageView()}
                {renderContinueButton()}
            </View>
        </ScrollView>
    }

    const renderImage = () => {
        return <View style={ImageView}>
            <ImageBackground
                source={{ uri: params.image }}
                style={RequestImage}
            />
        </View>
    }

    const renderRequestTitle = () => {
        return <Text style={RequestTitle}>{params.sub_cat_name}</Text>
    }

    const renderTypeOfProductField = () => {
        return <CustomInput
            containerView={InputFieldStyle}
            placeholder={params.sub_job_type === 1 ? 'Describe (Which units are affected)' : 'Describe'}
            placeholderColor={Colors.PLACEHOLDER_COLOR}
            viewStyle={{ paddingHorizontal: 0 }}
            inputStyle={FieldStyle}
            value={Data.TypeOfProduct}
            onChangeText={(text) => _onChangeTypeOfProduct(text)}
        />
    }

    const renderOtherDetail = () => {
        return <CustomInput
            containerView={InputFieldStyle}
            multiline

            placeholder={'Add other details'}
            placeholderColor={Colors.PLACEHOLDER_COLOR}
            viewStyle={{ paddingHorizontal: 0 }}
            inputStyle={[FieldStyle, {
                height: 100,
            }]}
            maxLength={256}
            value={Data.AddOtherDetails}
            onChangeText={(text) => _onChangeAddOtherDetail(text)}
        />
    }

    const renderQtyField = () => {
        if (params.sub_job_type === 1 || params.sub_job_type === 2) {
            return null
        }
        else {
            return <CustomInput
                containerView={InputFieldStyle}
                placeholder={'Quantity'}
                placeholderColor={Colors.PLACEHOLDER_COLOR}
                viewStyle={{ paddingHorizontal: 0 }}
                inputStyle={FieldStyle}
                keyboardType='numeric'
                value={Data.Quantity}
                onChangeText={(text) => _onChangeQuantity(text)}
            />
        }
    }

    const renderSheduleView = () => {
        return <View style={SheduleView}>
            <Text style={SheduleViewHeading}>When you need to be done ?</Text>

            <FlatList
                data={params.job_type === 1 ? OnDemandSheduleData : OffDemandSheduleData}
                contentContainerStyle={{ marginTop: 20 }}
                renderItem={({ item }) => {
                    return _renderSheduleItem(item)
                }}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    }

    const renderAttachImageView = () => {
        return <View>
            <Text style={SheduleViewHeading}>Attach Image</Text>
            {renderAttachImageList()}
        </View>
    }

    const renderAttachImageList = () => {
        return <FlatList
            data={AttachImageList}
            horizontal
            key={randomKey}
            keyExtractor={(item, index) => index.toString()}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            contentContainerStyle={{ marginTop: 20 }}
            renderItem={(data) => _renderAttachedImage(data)}
            ListFooterComponent={() => _renderAttachImageFooter()}
        />
    }

    const renderContinueButton = () => {
        return <CustomeButton
            btnStyle={ConfirmButton}
            title='Continue'
            textColor={Colors.BLACK06}
            buttonAction={() => {
                if (_Validate()) {
                    _navigateToConfirm()
                }
            }}
        />
    }



    return (
        <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

                {renderHeader()}
                {renderMainView()}

                {
                    isDateModalVisible
                        ?
                        <DateTimePicker
                            isVisible={isDateModalVisible}
                            value={new Date()}
                            mode={params.job_type === 1 ? 'time' : 'datetime'}
                            minimumDate={new Date()}
                            maximumDate={
                                need_to_be === 'Today'
                                    ?
                                    new Date(new Date(Date()).getFullYear(), new Date(Date()).getMonth(), new Date(Date()).getDate())
                                    :
                                    need_to_be === 'This week'
                                        ?
                                        new Date(new Date(Date()).getFullYear(), new Date(Date()).getMonth(), new Date(Date()).getDate() + 7)
                                        :
                                        new Date(new Date(Date()).getFullYear(), new Date(Date()).getMonth() + 1, new Date(Date()).getDate())}
                            onConfirm={(date) => _handleDatePicked(date)}
                            onCancel={() => _hideDateTimePicker()}
                        />
                        :
                        null
                }
            </View>

            <ErrorModal
                message={ErrorValidation.Message}
                visible={ErrorValidation.ShowErrorModel}
                handleBack={() => _CloseValidationModel()}
            />

            <PhotoUploadDialogue
                closeModel={() => setPhotoModalOpen(false)}
                openCamera={() => _open_camera()}
                openGallery={() => _open_gallery()}
                isShow={photoModalOpen}
            />
        </KeyboardAvoidingView >
    )
}

export default CustomerRequestDetail;