//Global imports
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import DemandsList from '../../Components/DemandList';
import CustomHeader from '../../Components/CustomerHeader';
import CustomerOnDemandPendingList from '../../Components/CustomerOnDemandPendingList';
import Certi from '../../../assets/images/certi.png';


const { TabView, OnDemand, OnDemandText } = Styles;

const CustomerCertificateOnDemand = ({ navigation: { navigate, goBack }, route: { params } }) => {

    /*  
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        goBack()
    }



    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Certificate'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderCertificate = () => {
        return <ImageBackground
            source={{ uri: params.image }}
            resizeMode='stretch'
            style={{ height: '100%', width: '100%' }}
        />
    }


    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {renderCertificate()}

        </View>
    )
}

export default CustomerCertificateOnDemand;