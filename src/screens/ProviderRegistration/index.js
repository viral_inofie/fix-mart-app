// Global imports
import React, { Component } from 'react'
import {
  Text, View, TouchableOpacity, TextInput,
  Image,
  FlatList,
  ImageBackground,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux';

import { Dropdown } from 'react-native-material-dropdown'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import Octicons from 'react-native-vector-icons/Octicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { height, width } from 'react-native-dimension';

// File imports
import styles from './styles'
import COLORS from '../../Helper/Colors'
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { GET_SERVICE, OPTIONS, REGISTER } from '../../Helper/Constants';
import { BASE_URL, DOMAIN_URL, GET_PRODUCT_LIST, GET_SUBCATEGORY_BY_CATEGORY, GET_SUB_CATEGORY, GET_SUB_JOB_TYPE, RIGHT_SCROLL_INDICATOR_INSENTS, UPLOAD_CERTIFICATES, WIDTH } from '../../Helper/Constants'

// Component imports
import CustomHeader from '../../Components/CustomerHeader'
import CustomButton from '../../Components/Button/index'
import ImageUpload from '../../Components/TakePicture/index';
import BackArrow from '../../../assets/images/backarrow.png'
import imgDownArrow from '../../../assets/images/downArrow.png'
import imgAddImage from '../../../assets/images/addImage.png'
import { ProviderRegisterParam, SupplierRegisterParam } from '../../Api/APIJson';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import Logger from '../../Helper/Logger';
import ErrorModal from '../../Components/ErrorModal';
import PhotoUploadDialogue from '../../Components/PhotoUploadDialogue';
import { requestCameraPermission } from '../../Helper/Utills';


const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

var NewImageArray = [];

export class ProviderRegistration extends Component {
  constructor(props) {

    super(props)

    this.state = {
      selectedBtn: 'serviceProvider',
      serviceProvided: '',
      serviceProduct: 'Product',
      comapanyName: '',
      comapanyDetails: '',
      AttachImageList: [],
      refreshListKey: Math.random(),
      firstName: props.route.params.first_name,
      lastName: props.route.params.last_name,
      phone: props.route.params.phone,
      email: props.route.params.email,
      password: props.route.params.password,
      getServiceArray: [],
      serviceList: [],
      service_id: '',
      sub_job_type: [],
      RepairingData: [],
      InstallationData: [],
      SupplyAndInstallationData : [],
      RepairingView: false,
      InstallationView: false,
      SupplyAndInstallationView:false,
      SelectedCategoryArray: [],
      SubCategorybyCategoryArray: [],
      SelectedSubCategoryArray: [],
      ProductList: [],
      SelectedProductId: [],
      Message: '',
      ShowErrorModel: false,
      photoModalOpen: false
    }
  }

  componentDidMount() {
    this.props.Loading(true)
    this.GetProduct()
    // this.GetService()
  }

  GetProduct() {
    const apiClass = new APICallService(GET_PRODUCT_LIST, {})

    apiClass.callAPI()
      .then(res => {
        if (validateResponse(res)) {
          let NewArray = res.data.map((item, i) => {
            item.isSelect = false;
            return item
          })
          Logger.log('New array : => ', NewArray)
          this.setState({
            ProductList: NewArray
          }, () => {
            this.GetService()
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  GetService() {
    const apiClass = new APICallService(GET_SERVICE, {})
    apiClass.callAPI()
      .then(res => {
        Logger.log('res : => ', res)

        if (validateResponse(res)) {

          let NewArray = res.data.map((item, i) => {
            return {
              value: item.service
            }
          })
          this.setState({
            getServiceArray: res.data,
            serviceList: NewArray,
            serviceProvided: res.data[0].service,
            service_id: res.data[0].id
          }, () => {
            this.getRepairingCategory();
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  render() {
    return this.renderMainScreen()
  }

  _navigateToApp = () => {
    const { selectedBtn } = this.state;
    if (selectedBtn == 'serviceProvider') {
      if (this._validateProvider()) {
        this.doRegister()
      }
    }
    else {
      if (this._validateSupplier()) {
        this.supplierRegister();
      }
    }
    // this.props.navigation.navigate('CustomerDrawer', { type: 'serviceProvider' })
  }

  _validateProvider = () => {
    const { comapanyName, AttachImageList, SelectedCategoryArray, SelectedSubCategoryArray } = this.state;

    if (comapanyName == '') {
      this.setState({ Message: 'Please enter company name', ShowErrorModel: true })
      return false
    } else if (AttachImageList.length === 0) {
      this.setState({ Message: 'Please upload relevant certificates', ShowErrorModel: true })
      return false
    } else if (SelectedCategoryArray.length === 0) {
      this.setState({ Message: 'Please select category', ShowErrorModel: true })
      return false
    } else if (SelectedSubCategoryArray.length === 0) {
      this.setState({ Message: 'Please select sub category', ShowErrorModel: true })
      return false
    } else {
      return true
    }
  }

  _validateSupplier = () => {
    const { comapanyName,comapanyDetail,AttachImageList } = this.state;

    if (comapanyName === '') {
      this.setState({ Message: 'Please enter company name', ShowErrorModel: true })
      return false
    }else if (comapanyDetail === '') {
      this.setState({ Message: 'Please enter company detail', ShowErrorModel: true })
      return false
    } else if (AttachImageList.length === 0) {
      this.setState({ Message: 'Please upload relevant certificates', ShowErrorModel: true })
      return false
    }
    //  else if (SelectedProductId.length === 0) {
    //   this.setState({ Message: 'Please select product', ShowErrorModel: true })
    //   return false
    // } 
    else {
      return true
    }

  }

  _navigateToBack = () => {
    this.props.navigation.goBack()
  }


  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */

  getRepairingCategory() {
    const apiClass = new APICallService(GET_SUB_CATEGORY, { service_id: this.state.service_id, type: 1 });

    apiClass.callAPI()
      .then(res => {
        if (res.success) {
          let NewArray = res.data.map((item, i) => {
            item.isSelect = false
            return item
          })
          this.setState({ RepairingData: NewArray }, () => {
            this.getInstallationCategory()
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  getInstallationCategory() {
    const apiClass = new APICallService(GET_SUB_CATEGORY, { service_id: this.state.service_id, type: 2 });

    apiClass.callAPI()
      .then(res => {
        if (res.success) {
          let NewArray = res.data.map((item, i) => {
            item.isSelect = false
            return item
          })
          this.setState({ InstallationData: NewArray },()=>{
            this.getSupplyAndInstallationCat()
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  getSupplyAndInstallationCat() {
    const apiClass = new APICallService(GET_SUB_CATEGORY, { service_id: this.state.service_id, type: 3 });

    apiClass.callAPI()
      .then(res => {
        if (res.success) {
          this.props.Loading(false)
          let NewArray = res.data.map((item, i) => {
            item.isSelect = false
            return item
          })
          this.setState({ SupplyAndInstallationData: NewArray })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  doRegister = () => {
    const { firstName, lastName, password, phone, email, AttachImageList, serviceProvided, comapanyName, selectedBtn, service_id } = this.state;

    let CategoryIdParam = {};

    this.state.SelectedCategoryArray.map((item, i) => {
      CategoryIdParam[`category_id[${i}]`] = item
    })

    let SubCategoryIdParam = {};

    this.state.SelectedSubCategoryArray.map((item, i) => {
      SubCategoryIdParam[`sub_category_id[${i}]`] = item
    })

    let ProductIdParam = {};

    this.state.SelectedProductId.map((item, i) => {
      ProductIdParam[`product_id[${i}]`] = item
    })

    let documentFile = {};

    this.state.AttachImageList.map((item, i) => {
      documentFile[`document_file[${i}]`] = item
    })

    this.props.Loading(true)

    const apiClass = new APICallService(REGISTER,
      ProviderRegisterParam(3, firstName, lastName, phone, comapanyName, email, password, service_id, documentFile, CategoryIdParam, SubCategoryIdParam, ProductIdParam))

    apiClass.callAPI()
      .then(res => {
        this.props.Loading(false);
        if (validateResponse(res)) {
          this.props.navigation.navigate('Login')
          RNToasty.Success({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  supplierRegister = () => {
    const { firstName, lastName, password, phone, email, AttachImageList, serviceProvided, comapanyName,comapanyDetail, selectedBtn, service_id } = this.state;

    // let ProductIdParam = {};

    // this.state.SelectedProductId.map((item, i) => {
    //   ProductIdParam[`product_id[${i}]`] = item
    // })

    let documentFile = {};

    this.state.AttachImageList.map((item, i) => {
      documentFile[`document_file[${i}]`] = item
    })
console.log("supplierRegistration",SupplierRegisterParam(2, firstName, lastName, phone, comapanyName,comapanyDetail, email, password, service_id, documentFile))
    this.props.Loading(true)

    const apiClass = new APICallService(REGISTER, SupplierRegisterParam(2, firstName, lastName, phone, comapanyName,comapanyDetail, email, password, service_id, documentFile))

    apiClass.callAPI()
      .then(res => {
        this.props.Loading(false);
        if (validateResponse(res)) {
          this.props.navigation.navigate('Login')
          RNToasty.Success({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  _onPressHeaderBtn = (type) => {
    this.setState({ selectedBtn: type })
  }

  _uploadRequestImage = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({
        //     avatarSource: source,
        // });
        let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
        let ImageObj = {
          uri: response.uri,
          name: Platform.OS === 'ios' ? IosFileName : response.fileName,
          type: response.type
        }

        this.uploadImage(ImageObj)

        // let New_Image_Array = [ImageObj];
        // SetAttachImageList([...AttachImageList, ...New_Image_Array])
        // NewImageArray.push(ImageObj)
        // this.setState({ AttachImageList: NewImageArray, refreshListKey: Math.random() })
        // console.log(this.state.AttachImageList)
      }
    });
  }

  _open_camera = (type='Service') => {
    launchCamera(OPTIONS, (response) => {
      console.log(response)
      if (response.didCancel) {
        this.setState({ photoModalOpen: false })
      }
      else {
        let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
        let ImageObj = {
          uri: response.uri,
          name: Platform.OS === 'ios' ? IosFileName : response.fileName,
          type: response.type
        }
        this.uploadImage(ImageObj)
        this.setState({ photoModalOpen: false })
      }
    });
  }

  _open_gallery = () => {
    launchImageLibrary(OPTIONS, (response) => {
      console.log(response)
      if (response.didCancel) {
        this.setState({ photoModalOpen: false })
      }
      else {
        let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
        let ImageObj = {
          uri: response.uri,
          name: Platform.OS === 'ios' ? IosFileName : response.fileName,
          type: response.type
        }
        this.uploadImage(ImageObj)
        this.setState({ photoModalOpen: false })
      }
    });

  }

  uploadImage = (ImageObj) => {
    let image = {
      "image": ImageObj
    }
    this.props.Loading(true);
    const apiClass = new APICallService(UPLOAD_CERTIFICATES, { "image": ImageObj });

    apiClass.callAPI()
      .then(res => {
        this.props.Loading(false);
        if (validateResponse(res)) {
          this.state.AttachImageList.push(res.data[0]);
          this.setState({ refreshListKey: Math.random() })
        }
        else {
          RNToasty.Error({
            title: res.message,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
            withIcon: false
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err);
      })

    // const form = new FormData();
    // for (const key in image) {
    //   form.append(key, image[key]);
    // }

    // console.log(form, 'form')

    // fetch('http://172.105.35.50/fixmart/api/user/image/upload', {
    //   method: 'POST',
    //   body: form,
    //   // headers:{
    //   //   'Accept' : 'application/json',
    //   //   'Content-Type' : 'multipart/form-data'
    //   // }
    // })
    //   .then(data => data.json())
    //   .then(res => {
    //     if (validateResponse(res)) {
    //       this.state.AttachImageList.push(res.data[0]);
    //       this.setState({ refreshListKey: Math.random() })
    //       console.log(this.state.AttachImageList, 'AttachImageList')
    //     }
    //     else {
    //       RNToasty.Error({
    //         title: res.message,
    //         fontFamily: Fonts.POPPINS_REGULAR,
    //         position: 'bottom',
    //         withIcon: false
    //       })
    //     }
    //   })
    //   .catch(err => {
    //     console.log(err)
    //     this.props.Loading(false);
    //     // renderErrorToast('error', err);
    //   })

  }

  _setServiceId = (label) => {
    this.state.getServiceArray.map((item, i) => {
      if (item.service === label) {
        this.setState({ service_id: item.id })
      }
    })
  }

  SelectRepair = (repair_id) => {
    let NewArray = this.state.RepairingData.map((item, i) => {
      if (item.id === repair_id) {
        if (item.isSelect) {
          item.isSelect = false
          let index = this.state.SelectedCategoryArray.indexOf(repair_id)
          this.state.SelectedCategoryArray.splice(index, 1)
        }
        else {
          item.isSelect = true
          this.state.SelectedCategoryArray.push(repair_id);
        }
        return item
      }
      return item
    })

    this.setState({ RepairingData: NewArray })
  }

  SelectInstallation = (Install_id) => {
    let NewArray = this.state.InstallationData.map((item, i) => {
      if (item.id === Install_id) {
        if (item.isSelect) {
          item.isSelect = false
          let index = this.state.SelectedCategoryArray.indexOf(Install_id)
          this.state.SelectedCategoryArray.splice(index, 1)
        }
        else {
          item.isSelect = true
          this.state.SelectedCategoryArray.push(Install_id);
        }
        return item
      }
      return item
    })
    this.setState({ InstallationData: NewArray })
  }

  SelectSupplyAndInstallation = (SupplyAInstall_id) => {
    let NewArray = this.state.SupplyAndInstallationData.map((item, i) => {
      if (item.id === SupplyAInstall_id) {
        if (item.isSelect) {
          item.isSelect = false
          let index = this.state.SelectedCategoryArray.indexOf(SupplyAInstall_id)
          this.state.SelectedCategoryArray.splice(index, 1)
        }
        else {
          item.isSelect = true
          this.state.SelectedCategoryArray.push(SupplyAInstall_id);
        }
        return item
      }
      return item
    })
    this.setState({ SupplyAndInstallationData: NewArray })
  }

  getSubCategoryByCategory = () => {

    this.props.Loading(true)

    let SubCategoryByCategoryParam = {};

    this.state.SelectedCategoryArray.map((item, i) => {
      SubCategoryByCategoryParam[`category_id[${i}]`] = item
    })

    const apiClass = new APICallService(GET_SUBCATEGORY_BY_CATEGORY, SubCategoryByCategoryParam);

    apiClass.callAPI()
      .then(res => {
        this.props.Loading(false)
        if (validateResponse(res)) {
          let NewArray = res.data.map((item, i) => {
            let newSubCate = item.subCategory.map((d, index) => {
              d.isSelect = false
              return d
            })
            item.subCategory = newSubCate;
            return item
          })

          this.setState({ SubCategorybyCategoryArray: NewArray });
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err);
      })
  }

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########..######.
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##....##....##
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##....##......
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##.....######.
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##..........##
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##....##....##
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##.....######.
  */

  renderMainScreen = () => {
    const { selectedBtn } = this.state
    return (
      <View style={styles.container}>
        <ScrollView scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}>
          {this.renderHeader()}
          {this.renderHeaderButtons()}
          {selectedBtn == 'serviceProvider' ? this.renderServiceProviderView() : this.renderSupplierView()}
          {this.renderSignUpBtn()}
          <ErrorModal
            message={this.state.Message}
            visible={this.state.ShowErrorModel}
            handleBack={() => this._CloseValidationModel()}
          />
        </ScrollView>
      </View>
    )
  }

  _CloseValidationModel = () => {
    this.setState({ ShowErrorModel: false })
  }

  renderHeader = () => {
    return (
      <CustomHeader
        LeftIcon={BackArrow}

        onLeftClick={() => this._navigateToBack()}
      />
    )
  }

  renderHeaderButtons = () => {
    const { selectedBtn } = this.state
    return (
      <View style={styles.headerBtnCont}>
        <TouchableOpacity style={[styles.headerBtn, { backgroundColor: selectedBtn == 'serviceProvider' ? COLORS.APP_PRIMARY : COLORS.WHITE }]} onPress={() => this._onPressHeaderBtn('serviceProvider')}>
          <Text style={styles.headerBtnText}>Service Provider</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.headerBtn, { backgroundColor: selectedBtn != 'serviceProvider' ? COLORS.APP_PRIMARY : COLORS.WHITE }]} onPress={() => this._onPressHeaderBtn('supplier')}>
          <Text style={styles.headerBtnText}>Supplier</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderRepairing = () => {
    return <View>
      <TouchableOpacity
        onPress={() => this.setState({ RepairingView: !this.state.RepairingView })}
        style={styles.expandView}>
        <Text style={styles.Heading}>Repairing</Text>
        <Octicons name={this.state.RepairingView ? 'chevron-down' : 'chevron-right'} size={25} color={COLORS.BLACK06} />
      </TouchableOpacity>

      {
        this.state.RepairingData.length > 0 && this.state.RepairingView && (
          <View style={styles.RepairingView}>
            {
              this.state.RepairingData.map((item, i) => {
                return <TouchableOpacity
                  onPress={() => this.SelectRepair(item.id)}
                  style={[styles.RepairingSelection, {

                    backgroundColor: item.isSelect ? COLORS.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: item.isSelect ? 0 : 1
                  }]}>
                  <Text style={[styles.RepairingText, {
                    color: item.isSelect ? COLORS.BLACK06 : COLORS.PRIMARY_BORDER_COLOR
                  }]}>{item.name}</Text>
                </TouchableOpacity>
              })
            }
          </View>
        )
      }
    </View>
  }

  renderInstallation = () => {
    return <View>
      <TouchableOpacity
        onPress={() => this.setState({ InstallationView: !this.state.InstallationView })}
        style={styles.expandView}>
        <Text style={styles.Heading}>Installation</Text>
        <Octicons name={this.state.InstallationView ? 'chevron-down' : 'chevron-right'} size={25} color={COLORS.BLACK06} />
      </TouchableOpacity>


      {
        this.state.InstallationData.length > 0 && this.state.InstallationView && (
          <View style={styles.RepairingView}>
            {
              this.state.InstallationData.map((item, i) => {
                return <TouchableOpacity
                  onPress={() => this.SelectInstallation(item.id)}
                  style={[styles.RepairingSelection, {
                    backgroundColor: item.isSelect ? COLORS.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: item.isSelect ? 0 : 1
                  }]}>
                  <Text style={[styles.RepairingText, {
                    color: item.isSelect ? COLORS.BLACK06 : COLORS.PRIMARY_BORDER_COLOR
                  }]}>{item.name}</Text>
                </TouchableOpacity>
              })
            }
          </View>
        )
      }

    </View>
  }

  renderSupplyAndInstallation = () => {
    return <View>
    <TouchableOpacity
      onPress={() => this.setState({ SupplyAndInstallationView: !this.state.SupplyAndInstallationView })}
      style={styles.expandView}>
      <Text style={styles.Heading}>Supply And Installation</Text>
      <Octicons name={this.state.SupplyAndInstallationView ? 'chevron-down' : 'chevron-right'} size={25} color={COLORS.BLACK06} />
    </TouchableOpacity>


    {
      this.state.SupplyAndInstallationData.length > 0 && this.state.SupplyAndInstallationView && (
        <View style={styles.RepairingView}>
          {
            this.state.SupplyAndInstallationData.map((item, i) => {
              return <TouchableOpacity
                onPress={() => this.SelectSupplyAndInstallation(item.id)}
                style={[styles.RepairingSelection, {
                  backgroundColor: item.isSelect ? COLORS.DRAWER_BACKGROUND_COLOR : 'transparent',
                  borderWidth: item.isSelect ? 0 : 1
                }]}>
                <Text style={[styles.RepairingText, {
                  color: item.isSelect ? COLORS.BLACK06 : COLORS.PRIMARY_BORDER_COLOR
                }]}>{item.name}</Text>
              </TouchableOpacity>
            })
          }
        </View>
      )
    }

  </View>
  }

  renderServiceProviderView = () => {
    return (
      <View style={{ flex: 1 }}>
        {this.renderCompanyName()}
        {this.renderRelevantCertificates()}
        {this.CertificateView()}
        {this.renderServiceProviderDropdown()}
        {this.renderRepairing()}
        {this.renderInstallation()}
        {this.renderSupplyAndInstallation()}
        {this.renderSelectSubBtn()}
        {this.renderSubCategory()}
        {this.renderPhotoModal()}
        {/* {this.renderSubJobTypeDropdown()} */}
      </View>
    )
  }

  _renderCertiImage = (item, index) => {
    return (
      <ImageBackground
        source={{ uri: DOMAIN_URL + item.toString() }}
        style={{ height: (WIDTH - 70) / 2, width: (WIDTH - 70) / 2, marginHorizontal: 10, marginTop: 10, alignItems: 'flex-end', justifyContent: 'flex-start', padding: 10 }}>
        <TouchableOpacity
          onPress={() => {
            this.state.AttachImageList.splice(index, 1)
            this.setState({ refreshListKey: Math.random() })
          }}
          style={{ height: 30, width: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 25, backgroundColor: COLORS.WHITE }}>
          <AntDesign name='close' color={COLORS.DRAWER_BACKGROUND_COLOR} size={20} />
        </TouchableOpacity>
      </ImageBackground>
    )
  }

  CertificateView = () => {
    return (
      <FlatList
        numColumns={2}
        ListEmptyComponent={() => {
          return null
        }}
        key={this.state.refreshListKey}
        data={this.state.AttachImageList}
        contentContainerStyle={{ paddingHorizontal: 15 }}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => this._renderCertiImage(item, index)}
      />
    )
  }

  renderSupplierView = () => {
    return (
      <View style={{ flex: 1 }}>
        {this.renderCompanyName()}
        {this.renderCompanyDetails()}
        {this.renderRelevantCertificates()}
        {this.CertificateView()}
        {/* {this.renderProducts()} */}
        {this.renderSupplierPhotoModal()}
      </View>
    )
  }

  renderCompanyName = () => {
    return (
      <TextInput
        style={styles.textInputSty}
        placeholder='Company name'
        value={this.state.comapanyName}
        onChangeText={(text) => this.setState({ comapanyName: text })}
      />
    )
  }

  renderServiceProviderDropdown = () => {
    return (
      <Dropdown
        rippleOpacity={0}
        data={this.state.serviceList}
        textColor={'#000'}
        baseColor={'#000'}
        value={this.state.serviceProvided}
        onChangeText={label => {
          this.setState({ serviceProvided: label }, () => {
            this._setServiceId(label)
          });
        }}

        renderBase={() => {
          return (
            <View style={styles.dropdownTextInputCont}>
              <TextInput
                placeholderTextColor='#666'
                style={styles.textInput}
                value={this.state.serviceProvided}
                editable={false}
                pointerEvents='none'
              />
              <Image source={imgDownArrow} style={{ width: 12, height: 12 }} />
            </View>
          );
        }}
      />
    )
  }

  renderRelevantCertificates = () => {

    return(
      <ImageUpload 
            isShow={false}
            onPress={(ImageObj)=>{
              this.uploadImage(ImageObj)
            }}
            style={styles.attchamentCont}
          >
            <Text style={styles.text}>Attach relevant certificates/licenses</Text>
            <Image
              source={imgAddImage}
              style={{ width: 45, height: 25 }}
              resizeMode='contain' />
          </ImageUpload>
    )
    return (
      <TouchableOpacity
        onPress={() => {
          if (requestCameraPermission()) {
            this.setState({ photoModalOpen: true })
          }
        }}
        style={styles.attchamentCont}>
        <Text style={styles.text}>Attach relevant certificates/licenses</Text>
        <Image
          source={imgAddImage}
          style={{ width: 45, height: 25 }}
          resizeMode='contain' />
      </TouchableOpacity>
    )
  }

  renderCompanyDetails = () => {
    return (
      <TextInput
        style={styles.textInputSty}
        placeholder='Company details'
        value={this.state.comapanyDetail}
        onChangeText={(text) => this.setState({ comapanyDetail: text })}
      />
    )
  }

  renderProducts = () => {
    return (
      <View>
        <Text style={[styles.Heading, { marginLeft: width(7), marginTop: 10 }]}>Select Products</Text>

        <View style={[styles.RepairingView, { paddingHorizontal: width(7) }]}>
          {
            this.state.ProductList.map((item, i) => {
              return <TouchableOpacity
                key={item.name}
                onPress={() => { this.SelectProduct(item.id) }}
                style={[styles.RepairingSelection, {
                  backgroundColor: item.isSelect ? COLORS.DRAWER_BACKGROUND_COLOR : 'transparent',
                  borderWidth: item.isSelect ? 0 : 1
                }]}>
                <Text style={[styles.RepairingText, {
                  color: item.isSelect ? COLORS.BLACK06 : COLORS.PRIMARY_BORDER_COLOR
                }]}>{item.name}</Text>
              </TouchableOpacity>
            })
          }
        </View>
      </View>
    )
  }

  SelectProduct = (product_id) => {
    let NewArray = this.state.ProductList.map((item, i) => {
      if (item.id === product_id) {
        if (item.isSelect) {
          item.isSelect = false
          let index = this.state.SelectedProductId.indexOf(product_id)
          this.state.SelectedProductId.splice(index, 1)
        }
        else {
          item.isSelect = true
          this.state.SelectedProductId.push(product_id)

        }
        return item
      }
      return item
    })
    this.setState({ ProductList: NewArray })
  }

  renderProductlist = (data) => {

  }

  renderSignUpBtn = () => {
    return (
      <CustomButton
        title={'Sign Up'}
        buttonAction={() => this._navigateToApp()}
        btnStyle={styles.signUpBtn}
      />
    )
  }

  renderSelectSubBtn = () => {
    if (this.state.SubCategorybyCategoryArray.length != 0) {
      return null
    }
    else {
      return (
        <CustomButton
          title={'Select Sub Category'}
          buttonAction={() => this.getSubCategoryByCategory()}
          btnStyle={styles.signUpBtn}
        />
      )
    }
  }

  renderSubCategory = () => {
    if (this.state.SubCategorybyCategoryArray.length > 0) {
      return (
        <FlatList
          contentContainerStyle={{ paddingHorizontal: width(7), marginVertical: height(2) }}
          data={this.state.SubCategorybyCategoryArray}
          renderItem={(data) => this.RenderSubCatList(data)}
          keyExtractor={(item, index) => index.toString()}
        />
      )
    }
    else {
      return null
    }
  }

  renderPhotoModal = () => (
    <PhotoUploadDialogue
      closeModel={() => this.setState({ photoModalOpen: false })}
      openCamera={() => this._open_camera()}
      openGallery={() => this._open_gallery()}
      isShow={this.state.photoModalOpen}
    />
  )

  renderSupplierPhotoModal = () => (
    <PhotoUploadDialogue
      closeModel={() => this.setState({ photoModalOpen: false })}
      openCamera={() => this._open_camera('Supplier')}
      openGallery={() => this._open_gallery('Supplier')}
      isShow={this.state.photoModalOpen}
    />
  )

  RenderSubCatList = (data) => {
    return (
      <>
        <Text style={styles.Heading}>{data.item.name}</Text>

        <View style={[styles.RepairingView, { paddingHorizontal: 0 }]}>
          {
            data.item.subCategory.map((item, childIndex) => {
              return <TouchableOpacity
                key={item.name}
                onPress={() => { this.SelectSubCate(item.id, childIndex, data.index) }}
                style={[styles.RepairingSelection, {
                  backgroundColor: item.isSelect ? COLORS.DRAWER_BACKGROUND_COLOR : 'transparent',
                  borderWidth: item.isSelect ? 0 : 1
                }]}>
                <Text style={[styles.RepairingText, {
                  color: item.isSelect ? COLORS.BLACK06 : COLORS.PRIMARY_BORDER_COLOR
                }]}>{item.name}</Text>
              </TouchableOpacity>
            })
          }
        </View>
      </>
    )
  }

  SelectSubCate = (sub_cat_id, childIndex, parentIndex) => {
    let newArray = this.state.SubCategorybyCategoryArray.map((d, pIndex) => {
      if (parentIndex === pIndex) {
        let subArray = d.subCategory.map((subCate, cIndex) => {
          if (childIndex === cIndex) {
            if (subCate.isSelect) {
              subCate.isSelect = false;
              let index = this.state.SelectedSubCategoryArray.indexOf(sub_cat_id);
              this.state.SelectedSubCategoryArray.splice(index, 1)
            }
            else {
              subCate.isSelect = true;
              this.state.SelectedSubCategoryArray.push(sub_cat_id);

            }
            return subCate
          }
          return subCate
        })
        d.subCategory = subArray;
        return d
      }
      return d
    })
    this.setState({ SubCategorybyCategoryArray: newArray })
  }


}


const mapDispatchToProps = dispatch => {
  return {
    Loading: (loader) => dispatch(updateLoader(loader))
  }
}



export default connect(null, mapDispatchToProps)(ProviderRegistration) 
