import { StyleSheet } from 'react-native'
import { width } from 'react-native-dimension'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import COLORS from '../../Helper/Colors'
import { WIDTH } from '../../Helper/Constants'
import Fonts from '../../Helper/Fonts'
import FONTS from '../../Helper/Fonts'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  headerBtn: {
    flex: 1,
    height: 40,
    marginRight: 12,
    backgroundColor: COLORS.APP_PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerBtnText: {
    fontFamily: FONTS.POPPINS_REGULAR,
    fontSize: 16
  },
  headerBtnCont: {
    paddingHorizontal: 25,
    paddingVertical: 4,
    flexDirection: 'row',
    marginTop: 12
  },
  signUpBtn: {
    height: 60,
    width: 260,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: COLORS.APP_PRIMARY,
    borderRadius: 30,
    marginTop: 10,
    marginBottom: 20
  },
  textInput: {
    flex: 1,
    marginHorizontal: 0,
    fontSize: 16,
    fontFamily: FONTS.POPPINS_REGULAR,
  },
  textInputSty: {
    marginHorizontal: 30,
    height: 50,
    borderBottomColor: '#666',
    borderBottomWidth: 0.5,
    fontSize: 16,
    fontFamily: FONTS.POPPINS_REGULAR,
    marginTop: 15
  },
  dropdownTextInputCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: '#666',
    borderBottomWidth: 0.5,
    marginHorizontal: 30,
    marginTop: 30
  },
  text: {
    fontFamily: FONTS.POPPINS_REGULAR,
    fontSize: 16,
    color: COLORS.LIGHT_GRAY
  },
  attchamentCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 25,
    marginHorizontal: 30,
    borderBottomColor: '#666',
    borderBottomWidth: 0.5,
  },

  Heading:{
    fontFamily:Fonts.POPPINS_REGULAR,
    fontSize:16,
    color:COLORS.BLACK06,
    // marginLeft:width(7),
  },

  RepairingSelection:{
    height:40,
    paddingHorizontal:15,
    borderRadius:20,
    borderWidth:1,
    justifyContent:'center',
    alignItems:'center',
    marginRight:15,
    marginBottom:15,
    borderColor:COLORS.PRIMARY_BORDER_COLOR
  },

  RepairingText:{
    fontFamily:Fonts.POPPINS_REGULAR,
    fontSize:14,
    color:COLORS.PRIMARY_BORDER_COLOR
  },

  RepairingView:{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', paddingHorizontal:width(7) , marginTop:15},

  expandView:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    height:60,
    borderBottomWidth:1,
    borderColor:COLORS.PLACEHOLDER_COLOR,
    width:width(85),
    alignSelf:'center',
    marginTop:10
  }
})