//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, RefreshControl, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { CUSTOMER_DISPUTE_ORDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallservice from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';


//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomerDisputedOrderList from '../../Components/CustomerDisputedOrderList';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import EmptyDataComponent from '../../Components/EmptyDataComponent';


const { TabView, OnDemand, OnDemandText } = Styles;

const CustomerDisputedOrder = ({ navigation: { navigate, addListener } }) => {
    const [DemandList, SetDemadList] = useState([])

    const [isOnDemand, SetIsOnDemand] = useState(true);
    const [OffDemand, setOffDemand] = useState([]);
    const [refreshing,setRefreshing] = useState(false)

    const dispatch = useDispatch();

    useEffect(() => {
        
        // addListener('focus', () => {
            
        // })
        const unsubscribe = addListener('focus', () => {
            // The screen is focused
            // Call any action
            dispatch(updateLoader(true))
            getDisputeOrder();
          });
      
          // Return the function to unsubscribe from the event so it gets removed on unmount
          return unsubscribe;
      
    }, [navigate])
    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('Home')
    }

    const _navigateToDetail = (job_id) => {
        navigate('CustomerDisputeOrderDetail', { job_id })
    }

    const _navigateToNote = (job_id) => {
        navigate('CustomerSubmitNoteDispute', { job_id })
    }

    const getDisputeOrder = () => {
        setRefreshing(true)
        const apiClass = new APICallservice(CUSTOMER_DISPUTE_ORDER, {});

        apiClass.callAPI()
            .then(res => {
                setRefreshing(false)
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetDemadList(res.data.onDemand)
                    setOffDemand(res.data.offDemand)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                setRefreshing(false)
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            LeftIcon={null}
            title='Disputed Orders'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: isOnDemand ? 0 : 1
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: !isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 0 : 1
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemOrderList = (item, index) => {
        return <CustomerDisputedOrderList
            item={item}
            time={100 * index}
            _navigateToNote={(job_id) => _navigateToNote(job_id)}
            navigateToDetail={(job_id) => _navigateToDetail(job_id)}
        />
    }

    const renderOrderList = () => {
        return <FlatList
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            data={isOnDemand ? DemandList : OffDemand}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemOrderList(item, index)}
            refreshControl={<RefreshControl
                refreshing={refreshing}
                onRefresh={getDisputeOrder} />}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {renderTabView()}

            {renderOrderList()}

        </View>
    )
}

export default CustomerDisputedOrder;