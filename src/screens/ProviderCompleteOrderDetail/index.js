//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, ScrollView, StatusBar } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import moment from 'moment';
//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { COMPLETED_ORDER_DETAIL, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { FlatList } from 'react-native';
import Fonts from '../../Helper/Fonts';

const {
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ModelMainContainer,
    ModelHeader,
    ModelTitle,
    ReviewText
} = Styles;

const ProviderCompletedOrderDetail = ({ navigation: { goBack, addListener }, route: { params } }) => {

    const [ReviewModel, SetReviewModel] = useState(false);
    const dispatch = useDispatch();
    const [Data, SetData] = useState({
        image: '',
        name: '',
        service: '',
        type_of_product: '',
        other_detail: '',
        price: '',
        address: '',
        customer_name: '',
        address: '',
        provider_mobile_no: '',
        estimate_cost: '',
        transport_charge: '',
        assessment_charge: '',
        arrival_time: '',
        visit_date: '',
        visit_time: '',
        total_cost: '',
        isCustomerPaid: '',
        job_id: '',
        isCustomerConfirmCompleted: '',
        status: '',
        isProviderCompleted: '',
        isFeedbackSent: '',
        sub_job_type: ''
    })

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getJobDetail();
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);


    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */
    const getJobDetail = () => {
        const apiClass = new APICallService(COMPLETED_ORDER_DETAIL, { job_id: params.job_id });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetData({
                        job_id: res.data.id,
                        image: res.data.image,
                        name: res.data.category,
                        service: res.data.service,
                        total_cost: res.data.totalCost,
                        type_of_product: res.data.type_product,
                        other_detail: res.data.other_details,
                        price: res.data.totalCost,
                        address: res.data.address,
                        estimate_cost: res.data.answer ? res.data.answer[0].answer : '',
                        transport_charge: res.data.answer ? res.data.answer[1].answer : '',
                        assessment_charge: res.data.answer ? res.data.answer[2].answer : '',
                        arrival_time: res.data.answer ? res.data.answer[3].answer : '',
                        visit_date: res.data.visit_date,
                        visit_time: res.data.visit_time,
                        provider_mobile_no: res.data.phone,
                        customer_name: res.data.customer_firstname + " " + res.data.customer_lastname,
                        isCustomerPaid: res.data.isCustomerPaid,
                        // isCustomerConfirmCompleted: res.data.isCustomerConfirmCompleted,
                        status: res.data.status,
                        isProviderCompleted: res.data.isProviderCompleted,
                        isFeedbackSent: res.data.isFeedbackSent,
                        sub_job_type: res.data.sub_job_type
                    })
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Completed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <FlatList
            data={Data.image}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => (
                <ImageBackground
                    source={{ uri: item.toString() }}
                    imageStyle={{ borderRadius: height(1.5) }}
                    style={[ImageStyle, { marginRight: 20 }]}
                />
            )}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(40) }}>
                <Text style={PlumberDetailTitle}>Customer Name :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(40), }}>
                <Text style={PlumberDetailValue}>{Data.customer_name}</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_date).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Service :</Text>
            {/* <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text> */}
            {/* <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Price :</Text> */}
            {/* <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Quantity :</Text> */}

            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
        </View>
    }

    const renderServiceValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>{Data.service}</Text>
            {/* <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>Bath Fitting</Text> */}
            {/* <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text> */}
            {/* <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>1</Text> */}
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.other_detail}</Text>
        </View>
    }


    const renderServiceDetail = () => {
        return <View style={PlumberDetailView}>
            {renderServiceHeading()}

            {renderServiceValue()}
        </View>
    }

    const renderReview = () => {
        return <View style={ModelMainContainer}>
            <View style={ModelHeader}>
                <Text style={ModelTitle}>Plumber Reviews</Text>
            </View>

            <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            </View>
        </View>
    }


    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>

            </View>
        </Modal>
    }

    const renderPaymentView = () => {
        if (Data.sub_job_type === 3) {
            return null
        }
        else {
            return <View>
                {renderPaymentDetailTitle()}
                {renderPaymentDetailView()}
            </View>
        }
    }

    const renderPaymentDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Payment Details</Text>
        </View>
    }

    const renderPaymentDetailView = () => {
        return <View style={PlumberDetailView}>
            {renderPaymentsHeading()}
        </View>
    }

    const renderPaymentsHeading = () => {
        return <View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Repair cost :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.estimate_cost}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Transport Charge :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.assessment_charge}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Total amount :</Text>
                </View>

                <View style={{ width: width(45), }}>
                    <Text style={[PlumberDetailValue, { color: Colors.BLACK, fontFamily: Fonts.POPPINS_BOLD }]}>$ {Data.total_cost}</Text>
                </View>
            </View>

        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                bounces={false}
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {renderPaymentView()}

                {/* {renderReview()} */}

            </ScrollView>

            {renderModel()}

        </View>
    )
}

export default ProviderCompletedOrderDetail;