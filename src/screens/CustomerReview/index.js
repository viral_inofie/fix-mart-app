//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, Platform, ScrollView, ImageBackground, TextInput } from 'react-native';
import { width } from 'react-native-dimension';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import { CUSTOMER_REVIEW, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import Styles from './Styles';
//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import Review from '../../../assets/images/review.png';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import ErrorModal from '../../Components/ErrorModal';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';


const {
    ImageStyle,
    JobCompletedTitle,
    TabView,
    OnDemand,
    OnDemandText,
    StarRatingContainer,
    FeedbackInputStyle,
    SubmitStyle
} = Styles;

const CustomerReview = ({ navigation: { goBack, navigate, pop }, route: { params } }) => {

    const [FeedbackText, SetFeedbackText] = useState('');
    const [ratings, setRatings] = useState(3);
    const [ErrorValidation, SetErrorValidation] = useState({
        Message: '',
        ShowErrorModel: false
    })
    const dispatch = useDispatch();

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => {
        goBack()
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */
    const _validate = () => {

        if (FeedbackText == '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Please enter your review', ShowErrorModel: true })
            return false
        } else {
            return true
        }
    }

    const _CloseValidationModel = () => {
        SetErrorValidation({ ...ErrorValidation, ShowErrorModel: false })
    }

    const _doSubmit = () => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(CUSTOMER_REVIEW, { rating: ratings, comment: FeedbackText, job_id: params.job_id });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                    })
                    pop();
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeaderView = () => {
        return <CustomHeader
            title={'Ratings & Reviews'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderMainView = () => {
        return <ScrollView
            bounces={false}
            contentContainerStyle={{ padding: 20 }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}>
            {renderImage()}
            {/* {renderJobView()} */}
            {renderExperienceView()}
            {renderFeedbackView()}
        </ScrollView>
    }

    const renderImage = () => {
        return <ImageBackground resizeMode='contain' source={Review} style={ImageStyle} />
    }

    const renderJobView = () => {
        return <View style={{ marginTop: 30 }}>
            <Text style={JobCompletedTitle}>Job Completed ?</Text>

            <View style={TabView}>
                <TouchableOpacity style={OnDemand}>
                    <Text style={OnDemandText}>Yes</Text>
                </TouchableOpacity>

                <TouchableOpacity style={[OnDemand, { marginLeft: width(5), backgroundColor: Colors.DRAWER_BACKGROUND_COLOR, borderWidth: 0 }]}>
                    <Text style={OnDemandText}>No</Text>
                </TouchableOpacity>
            </View>
        </View>
    }

    const renderExperienceView = () => {
        return <View style={{ marginTop: 0 }}>
            <Text style={JobCompletedTitle}>Please rate your experience for Service {params.name}</Text>

            <AirbnbRating
                type='custom'
                ratingBackgroundColor='#c8c7c8'
                selectedColor={Colors.YELLOW}
                count={5}
                showRating={false}
                starContainerStyle={StarRatingContainer}
                size={40}
                onStartRating={(val) => { setRatings(val) }}
            />
        </View>
    }

    const renderFeedbackView = () => {
        return <View style={{ marginTop: 30 }}>
            <Text style={JobCompletedTitle}>What can we do to improve service?</Text>

            <TextInput
                placeholderTextColor={Colors.PLACEHOLDER_COLOR}
                style={FeedbackInputStyle}
                placeholder='Please enter your review'
                textAlignVertical='top'
                value={FeedbackText}
                onChangeText={(text) => SetFeedbackText(text)}
            />

            <CustomButton
                buttonAction={() => {
                    if (_validate()) {
                        _doSubmit()
                    }
                }}
                btnStyle={SubmitStyle}
                title='Submit'
            />

        </View>
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.WHITE }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                {renderHeaderView()}
                {renderMainView()}
                <ErrorModal
                    message={ErrorValidation.Message}
                    visible={ErrorValidation.ShowErrorModel}
                    handleBack={() => _CloseValidationModel()}
                />
            </View>
        </KeyboardAvoidingView>
    )
}

export default CustomerReview;