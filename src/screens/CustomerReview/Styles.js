//Global imports
import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    ImageStyle:{
        height : 250,
        width : 300,
        alignSelf:'center'
    },

    JobCompletedTitle:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:18,
        color:Colors.BLACK
    },

    TabView: {
        paddingVertical: height(3),
        paddingHorizontal: width(5),
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center'
      },
    
      OnDemand: {
        height: height(6),
        width: width(30),
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderRadius:10,
        borderColor: Colors.PRIMARY_BORDER_COLOR
      },
    
      OnDemandText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.8),
        color: Colors.BLACK06
      },

      StarRatingContainer:{ height: 40,marginTop:30, width:'80%', justifyContent: 'space-between', },
      
      FeedbackInputStyle:{
            height:100,
            backgroundColor:Colors.CHAT_INPUT_BACKGROUND_COLOR,
            padding : 15,
            width:'100%',
            marginTop:20,
            color:Colors.BLACK,
            fontSize:18
      },

      SubmitStyle:{
          height:55,
          backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:30,
          marginTop:30
      }
})