//Global imports
import React, { useEffect, useState } from 'react';
import {
  View, Text, TouchableOpacity, SafeAreaView,
  ImageBackground, ScrollView, TextInput, FlatList,
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import {
  PENDING_ORDER_CONFIRM,
  PENDING_ORDER_DECLINE,
  PENDING_ORDER_DETAIL,
  RIGHT_SCROLL_INDICATOR_INSENTS,
  PENDING_ORDER_CANCEL
} from '../../Helper/Constants';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { PendingOrderCancelParam, PendingOrderDetailConfirm, PendingOrderDetailParam } from '../../Api/APIJson';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { Alert } from 'react-native';

const {
  ImageStyle,
  Title,
  PlumberDetailTitle,
  PlumberDetailValue,
  PlumberDetailView,
  ServiceDetailStyle,
  ViewButtonStyle,
  StarView,
  ConfirmButton,
  ModelMainContainer,
  ModelHeader,
  ModelTitle,
  ReviewText,
  radioBtnImg,
  radiBtnImgCont,
  textInput
} = Styles;

const CustomerPendingOrderDetail = ({ navigation: { goBack, pop,navigate,addListener }, route: { params } }) => {

  const [ReviewModel, SetReviewModel] = useState(false);
  const [DeclineOfferModel, SetDeclineOfferModel] = useState(false)
  const [DoYouWantVisit, SetDoYouWantVisit] = useState(true)
  const [CommentHere, SetCommentHere] = useState('(Optional) Comment here...');

  const [Data, SetData] = useState({
    order_image: '',
    order_title: '',
    plumber_name: '',
    scheduleDate: '',
    service: '',
    sub_category: '',
    price: '',
    otherDetail: '',
    rating: '',
    review: [],
    scheduleTime: '',
    status: '',
    repair_cost: '',
    transport_charge: '',
    assessment_charge: '',
    arrival_time: '',
    total_amount: '',
    sub_job_type: ''
  })
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      dispatch(updateLoader(true))
      getDemandDetail()
      });
      return unsubscribe;
  
}, [navigate])
  // useEffect(() => {
  //   dispatch(updateLoader(true))
  //   getDemandDetail()
  // }, [])
  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack();

  const _openModel = () => SetReviewModel(true);

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */


  const getDemandDetail = () => {

    const apiClass = new APICallService(PENDING_ORDER_DETAIL, PendingOrderDetailParam(params.job_id));

    apiClass.callAPI()
      .then(res => {
        console.log(res, 'ffffffff')
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          SetData({
            ...Data,
            order_image: res.data.image,
            order_title: res.data.name,
            plumber_name: res.data.service_provider_firstname + " " + res.data.service_provider_lastname,
            service: res.data.service,
            sub_category: res.data.subcategory,
            price: res.data.price,
            otherDetail: res.data.description,
            rating: res.data.rating,
            review: res.data.reviews,
            scheduleDate: res.data.visit_date,
            scheduleTime: res.data.visit_time,
            status: res.data.status,
            repair_cost: res.data.answer.length > 0 ? res.data.answer[0].answer : '',
            transport_charge: res.data.answer.length > 0 ? res.data.answer[1].answer : '',
            assessment_charge: res.data.answer.length > 0 ? res.data.answer[2].answer : '',
            arrival_time: res.data.answer.length > 0 ? res.data.answer[3].answer : '',
            total_amount: res.data.totalCost,
            sub_job_type: res.data.sub_job_type
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false))
        renderErrorToast('error', err)
      })
  }

  const renderTime = (time) => {
    let hours = time.substring(0, 2);

    let minutes = time.substring(3, 5);

    let isAmPm = hours >= 12 ? 'PM' : 'AM'

    return `${hours} : ${minutes} ${isAmPm}`
  }

  const _doAcceptOrder = () => {

    dispatch(updateLoader(true))

    const apiClass = new APICallService(PENDING_ORDER_CONFIRM, PendingOrderDetailConfirm(params.job_id));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          pop();
          RNToasty.Success({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false))
        renderErrorToast('error', err)
      })
  }

  const _doDecline = () => {

    dispatch(updateLoader(true))

    const apiClass = new APICallService(PENDING_ORDER_DECLINE, PendingOrderDetailConfirm(params.job_id));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          pop();
          RNToasty.Success({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false))
        renderErrorToast('error', err)
      })
  }

  const _doCancelOrder = () => {

    dispatch(updateLoader(true))

    const apiClass = new APICallService(PENDING_ORDER_CANCEL, PendingOrderCancelParam(params.job_id, Data.status));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          pop();
          RNToasty.Success({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false))
        renderErrorToast('error', err)
      })
  }



  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeader = () => {
    return <CustomHeader
      title={'Sent Offers'}
      onLeftClick={() => _navigateToBack()}
    />
  }

  const renderDemandImage = () => {
    return <FlatList
      data={Data.order_image}
      horizontal
      showsHorizontalScrollIndicator={false}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index }) => (
        <ImageBackground
          source={{ uri: item.toString() }}
          imageStyle={{ borderRadius: height(1.5) }}
          style={[ImageStyle, { marginRight: 20 }]}
        />
      )}
    />
  }

  const renderDemandName = () => {
    return <Text style={Title}>{Data.order_title}</Text>
  }

  const renderPlumberDetailView = () => {
    return <View style={PlumberDetailView}>
      <View>
        <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
          <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Service Provider Name :</Text>
          </View>
          <View style={{ width: width(45) }}>
            <Text style={PlumberDetailValue}>{Data.plumber_name}</Text>
          </View>
        </View>

        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
          <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Scheduled Date & Time :</Text>
          </View>
          <View style={{ width: width(45) }}>
            <Text style={PlumberDetailValue}>{moment(Data.scheduleDate).format('DD MMM, YYYY') + " " + renderTime(Data.scheduleTime)}</Text>
          </View>
        </View>
      </View>
    </View>
  }

  const renderServiceDetailTitle = () => {
    return <View style={[PlumberDetailView, {
      marginTop: height(3),
      height: height(4),
      borderBottomWidth: 0.5,
      borderColor: '#CCCCCC'
    }]}>
      <Text style={ServiceDetailStyle}>Service Details</Text>
    </View>
  }

  const renderServiceHeading = () => {
    return <View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Service :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.service}</Text>
        </View>
      </View>

      {
        Data.sub_category != null
          ?
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
            <View style={{ width: width(35) }}>
              <Text style={PlumberDetailTitle}>Sub Category :</Text>
            </View>
            <View style={{ width: width(45) }}>
              <Text style={PlumberDetailValue}>{Data.sub_category}</Text>
            </View>
          </View>
          :
          null
      }

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Price :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.price}</Text>
        </View>
      </View>


      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Other Details :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.otherDetail}</Text>
        </View>
      </View>

      {
        Data.sub_job_type === 3
          ?
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
            <View style={{ width: width(35) }}>
              <Text style={PlumberDetailTitle}>Product Ratings :</Text>
            </View>
            <View style={{ width: width(45) }}>
              {renderStarView()}
            </View>
          </View>
          :
          null
      }

      {
        Data.review.length > 0
          ?
          <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
            <View style={{ width: width(35) }}>
              <Text style={PlumberDetailTitle}>Plumber Reviews :</Text>
            </View>
            <View style={{ width: width(45) }}>
              {renderViewButton()}
            </View>
          </View>
          :
          null
      }


    </View>
  }

  const renderStarView = () => {
    return <View style={[StarView, { marginTop: 0 }]}>
      <AirbnbRating
        selectedColor={Colors.YELLOW}
        count={5}
        showRating={false}
        defaultRating={Data.rating.substring(0, 3)}
        reviewColor={Colors.PRIMARY_DARK}
        size={10}
        isDisabled
        onFinishRating={(val) => { }}
      />
      <Text style={PlumberDetailValue}>{` / ${Data.rating.substring(3, 5)}`}</Text>
    </View>
  }

  const renderViewButton = () => {
    return <CustomButton
      title='View'
      textStyle={{ fontSize: 12 }}
      textColor={Colors.BLACK}
      buttonAction={() => _openModel()}
      btnStyle={ViewButtonStyle}
    />
  }

  // const renderServiceValue = () => {
  //   return <View>
  //     {
  //       Data.sub_category != null
  //         ?
  //         :
  //         null
  //     }
  //     <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
  //     <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.otherDetail}</Text>



  //     {renderViewButton()}

  //   </View>
  // }


  const renderServiceDetail = () => {
    return <View style={PlumberDetailView}>
      {renderServiceHeading()}

      {/* {renderServiceValue()} */}
    </View>
  }

  const renderConfirmButton = () => {
    return <CustomButton
      buttonAction={() => _doAcceptOrder()}
      title='Accept'
      btnStyle={[ConfirmButton, {
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        borderWidth: 0
      }]}
    />
  }

  const renderDeclineButton = () => {
    return <CustomButton
      buttonAction={() => _doDecline()}
      title='Decline'
      btnStyle={[ConfirmButton, { marginTop: 20 }]}
    // buttonAction={() => SetDeclineOfferModel(true)}
    />
  }

  const renderCancelButton = () => {
    return <CustomButton
      buttonAction={() =>
        Alert.alert(
          "Cancel Job",
          "Are you sure you want to cancel the Job?",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            {
              text: "OK",
              onPress: () => { _doCancelOrder() }
            }
          ],
          { cancelable: false }
        )
      }
      title='Cancel Order'
      btnStyle={[ConfirmButton, { marginTop: 20 }]}
    />
  }

  const renderButtonView = () => {
    return <View style={{ marginTop: height(5) }}>
      {renderConfirmButton()}

      {renderDeclineButton()}

      {renderCancelButton()}
    </View>
  }

  const renderModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetReviewModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>

            <FlatList
              contentContainerStyle={{ paddingBottom: height(5) }}
              data={Data.review}
              scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
              bounces={false}
              renderItem={({ item, index }) => {
                return <Text style={[ReviewText, { marginBottom: height(1) }]}>{`${index + 1}. ${item.comment}`}</Text>
              }}
              keyExtractor={(item, index) => index.toString()}
            />

          </View>
        </View>
      </View>
    </Modal>
  }

  const renderDeclineModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={DeclineOfferModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetDeclineOfferModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>Do you want to visit at different time and location?</Text>
            <View style={{ flexDirection: 'row', marginTop: 8 }}>
              <TouchableOpacity style={{ flexDirection: 'row', marginRight: 20, alignItems: 'center', justifyContent: 'center', }} onPress={() => SetDoYouWantVisit(true)}>
                <View style={radiBtnImgCont}>
                  {DoYouWantVisit && <View style={radioBtnImg} />}
                </View>
                <Text style={ReviewText}>Yes</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }} onPress={() => SetDoYouWantVisit(false)}>
                <View style={radiBtnImgCont}>
                  {!DoYouWantVisit && <View style={radioBtnImg} />}
                </View>
                <Text style={ReviewText}>No</Text>
              </TouchableOpacity>
            </View>

            <TextInput
              style={textInput}
              multiline
              value={CommentHere}
              onChangeText={(text) => SetCommentHere(text)}
            />
            <CustomButton
              title='Submit'
              btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0,
                height: 40,
                marginTop: 15
              }]}
              buttonAction={() => SetDeclineOfferModel(false)}
            />
          </View>
        </View>
      </View>
    </Modal>
  }

  const renderPaymentDetailView = () => {
    if (Data.sub_job_type === 3) {
      return null
    }
    else {
      return <View style={PlumberDetailView}>
        {renderPaymentsHeading()}

      </View>
    }
  }

  const renderPaymentsHeading = () => {
    return <View>
      <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Repair cost :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.repair_cost}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Transport Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.assessment_charge}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Total amount :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.total_amount}</Text>
        </View>
      </View>


      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.arrival_time}</Text>
        </View>
      </View>


    </View>
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeader()}

      <ScrollView
        scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
        bounces={false}
        contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
        {renderDemandImage()}

        {renderDemandName()}

        {renderPlumberDetailView()}

        {renderServiceDetailTitle()}

        {renderServiceDetail()}

        {renderPaymentDetailView()}

        {renderButtonView()}
      </ScrollView>

      {renderModel()}
      {renderDeclineModel()}

    </View>
  )
}

export default CustomerPendingOrderDetail;