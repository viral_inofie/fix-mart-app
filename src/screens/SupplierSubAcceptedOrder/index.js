//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList, TextInput } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import CloseIcon from 'react-native-vector-icons/AntDesign';
import { useDispatch } from 'react-redux';
import stripe from 'tipsi-stripe';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment'
import DateTimePicker from "react-native-modal-datetime-picker";

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import {
    RIGHT_SCROLL_INDICATOR_INSENTS,
    SUPPLIER_ORDER_ACCEPTED,
    PUBLISHABLE_KEY,
    SECRET_KEY,
    ANDROID_MODE,
    STRIPE_PAY,
    CUSTOMER_PAY_NOW,
    PAYNOW_PROVIDER
} from '../../Helper/Constants';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import SupplierSubAcceptedOrderList from '../../Components/SupplierSubAcceptedOrderList';
import EmptyDataComponent from '../../Components/EmptyDataComponent/index';
import { PayNowProviderParam } from '../../Api/APIJson';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    TabView,
    OnDemand,
    OnDemandText,
    MainWhiteView,
    ModalHeaderView,
    HeadingText,
    RowView,
    RadioView,
    PaymentNameStyle,
    EnableRadio,
    PayNowButton,
    ModelMainContainer, ModelHeader, ModelTitle, ReviewText,
    moneyTextInput,
    ConfirmButton
} = Styles;

stripe.setOptions({
    publishableKey: PUBLISHABLE_KEY,
    merchantId: SECRET_KEY,
    androidPayMode: ANDROID_MODE,
})


const SupplierSubAcceptedOrder = ({ navigation: { navigate, addListener } }) => {

    const [PayNowModel, SetPayNowModel] = useState(false)
    const [DemandList, SetDemadList] = useState([])
    const [PayLaterModel, SetPayLaterModel] = useState(false)
    const [NextInvestmentDate, SetNextInvestmentDate] = useState(moment(new Date()).format('YYYY-MM-DD'))
    const [isDateModalVisible, SetisDateModalVisible] = useState(false)

    const [PaymentMode, SetPaymentMode] = useState([
        { key: 0, name: 'Debit Card', isSelect: false },
        { key: 1, name: 'Credit Card', isSelect: false },
        { key: 2, name: 'Paypal', isSelect: false },
        { key: 3, name: 'Paybalance', isSelect: false },
    ])
    const [isOnDemand, SetIsOnDemand] = useState(true);
    const [Money, SetMoney] = useState('')
    const [activeOrderId, setActiveOrderId] = useState('');

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getOrder()
        })
    }, [])


    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('ProviderJobRequestStack')
    }

    const _navigateToDetail = (order_id) => {
        navigate('SupplierSubAcceptedOrderDetail', { order_id })
    }

    const _closeModal = () => {
        SetPayNowModel(false)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _SelectPaymentMethod = (d) => {
        console.log(d, 'item')
        let NewArray = PaymentMode.map((item, i) => {
            item.isSelect = false;
            if (item.key === d.key) {
                item.isSelect = true
                return item
            }
            return item
        })
        SetPaymentMode(NewArray)
    }

    const getOrder = () => {
        const apiClass = new APICallService(SUPPLIER_ORDER_ACCEPTED, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetDemadList(res.data)
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doPayNow = (token, price, order_id) => {
        SetPayNowModel(false)
        dispatch(updateLoader(true));

        const apiClass = new APICallService(PAYNOW_PROVIDER, PayNowProviderParam(order_id, 1, 0, token, price, 0));

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrder()
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }


    const StripePay = (token, price, order_id) => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(STRIPE_PAY, { amount: price, token: token });

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'stripe pay')
                if (validateResponse(res)) {
                    _doPayNow(res.data.txn_id, price, order_id)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const _handleDatePicked = (val) => {
        console.log(val)
        let selectedDate = moment(val).format('YYYY-MM-DD')
        console.log(selectedDate,'selecDate')
        // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
        SetNextInvestmentDate(selectedDate)
        SetisDateModalVisible(false)
    }

    const _hideDateTimePicker = () => {
        SetisDateModalVisible(false)
    }

    const generateStripeToken = async (price, order_id) => {
        const token = await stripe.paymentRequestWithCardForm()
        console.log(token, 'stripe token')
        StripePay(token.tokenId, price, order_id)
    }


    const _doPayLater = (token) => {
        SetPayNowModel(false)
        console.log('hello')
        dispatch(updateLoader(true));

        const apiClass = new APICallService(PAYNOW_PROVIDER, PayNowProviderParam(activeOrderId, 2, 0, token, Money, NextInvestmentDate));

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrder()
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }



    const StripePayForPayLater = (token) => {
        SetPayLaterModel(false)

        dispatch(updateLoader(true))
        const apiClass = new APICallService(STRIPE_PAY, { amount: Money, token: token });

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'stripe pay')
                if (validateResponse(res)) {
                    _doPayLater(res.data.txn_id)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }



    const generateStripeTokenForPayLater = async () => {
        const token = await stripe.paymentRequestWithCardForm()
        console.log(token, 'stripe token')
        StripePayForPayLater(token.tokenId)
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Confirmed Offers'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: isOnDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemDemandList = (item, index) => {
        return <SupplierSubAcceptedOrderList
            item={item}
            time={100 * index}

            _onPayNow={(price, order_id) => generateStripeToken(price, order_id)}
            _onPayLater={(order_id) =>  {
                setActiveOrderId(order_id)
                SetPayLaterModel(true)
            }}
            navigateToDetail={(order_id) => _navigateToDetail(order_id)}
        />
    }

    const renderDemandList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={DemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
        />
    }

    const _renderListPayment = (data) => {
        return <TouchableOpacity
            onPress={() => _SelectPaymentMethod(data.item)}
            style={RowView}>
            <View style={RadioView}>
                {
                    data.item.isSelect
                        ?
                        <View style={EnableRadio}>
                        </View>
                        :
                        null
                }
            </View>

            <Text style={PaymentNameStyle}>{data.item.name}</Text>
        </TouchableOpacity>
    }

    const renderPaymentModalList = () => {
        return <FlatList
            contentContainerStyle={{ paddingHorizontal: width(5), paddingTop: height(2) }}
            data={PaymentMode}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(data) => _renderListPayment(data)}
        />
    }

    const renderPayNowModel = () => {
        return <Modal style={{ marginHorizontal: 0 }} isVisible={PayNowModel}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={MainWhiteView}>

                    <View style={ModalHeaderView}>
                        <Text style={HeadingText}>Payment Mode</Text>

                        <TouchableOpacity onPress={() => _closeModal()}>
                            <CloseIcon name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    {renderPaymentModalList()}

                    {renderPayNowButton()}
                </View>
            </View>
        </Modal>
    }

    const renderPayNowButton = () => {
        return <CustomButton
            buttonAction={() => { }}
            title='Pay Now'
            btnStyle={PayNowButton}
        />
    }

    const renderPayLaterModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={PayLaterModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={ModelTitle}>How much money</Text>
                            <TextInput
                                style={moneyTextInput}
                                value={Money}
                                keyboardType='phone-pad'
                                onChangeText={(text) => SetMoney(text)}
                            />
                        </View>

                        <TouchableOpacity onPress={() => SetPayLaterModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>



                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>Next Installment date</Text>

                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ height: 40, width: 100, backgroundColor: Colors.INDEX_GREY, justifyContent: 'center', alignItems: 'center' }}
                            // onPress={() => SetNextInvestmentDate(true)}
                            onPress={() =>
                                SetisDateModalVisible(!isDateModalVisible)}>
                            <Text>{moment(NextInvestmentDate).format('DD MMM, YYYY')}</Text>
                        </TouchableOpacity>

                        {
                            isDateModalVisible
                            &&
                            (
                                <DateTimePicker
                                    isVisible={isDateModalVisible}
                                
                                    mode='date'
                                
                                    accessibilityValue={new Date()}
                                    onConfirm={(date) => _handleDatePicked(date)}
                                    onCancel={() => _hideDateTimePicker()}
                                />
                            )

                        }

                        <CustomButton
                            title='Submit'
                            btnStyle={[ConfirmButton, {
                                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                                borderWidth: 0,
                                height: 40,
                                marginTop: 15
                            }]}
                            buttonAction={() => generateStripeTokenForPayLater()}
                        />
                    </View>

                </View>
            </View>
        </Modal>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {/* {renderTabView()} */}

            {renderDemandList()}

            {renderPayNowModel()}

            {renderPayLaterModel()}
        </View>
    )
}

export default SupplierSubAcceptedOrder;