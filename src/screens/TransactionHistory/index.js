//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';


//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, TRANSACTIONS } from '../../Helper/Constants';
import APICallservice from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomerTransactionList from '../../Components/CustomerTransactionList';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import EmptyDataComponent from '../../Components/EmptyDataComponent';


const {
    TabView,
    OnDemand,
    OnDemandText
} = Styles;

const TransactionHistory = ({ navigation: { navigate, goBack, addListener }, route: { params: { user_type } } }) => {
    const UserType = user_type

    const [TransactionList, SetTransactionList] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true))
        addListener('focus', () => {
            getTransaction()
        })
    }, [])
    /*  
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToHome = () => {
        if (UserType === 'supplier') {
            navigate('SupplierHomeStack')
        }
        else if (UserType === 'customer') {
            goBack();
        }
        else {
            navigate('ProviderJobRequestStack')
        }
    }

    const _navigateToDetail = (id) => {
        navigate('CustomerTransactionHistoryDetail', { id })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _renderTransactionItem = (item, index) => {
        return <CustomerTransactionList
            item={item}
            time={100 * index}
            navigateToDetail={(id) => _navigateToDetail(id)}
        />
    }

    const getTransaction = () => {
        const apiClass = new APICallservice(TRANSACTIONS, {});
        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetTransactionList(res.data)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeaderView = () => {
        return <CustomHeader
            title='Transaction history'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity style={OnDemand}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[OnDemand, { marginLeft: width(5), backgroundColor: Colors.DRAWER_BACKGROUND_COLOR, borderWidth: 0 }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderTransactionList = () => {
        return <FlatList
            bounces={false}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            keyExtractor={(item, index) => index.toString()}
            data={TransactionList}
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            renderItem={({ item, index }) => _renderTransactionItem(item, index)}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeaderView()}

            {/* {renderTabView()} */}

            {renderTransactionList()}
        </View>
    )
}

export default TransactionHistory;