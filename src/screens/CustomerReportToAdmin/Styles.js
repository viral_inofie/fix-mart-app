//Global imports

import { StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainContainer:{
        flex:1,
        justifyContent:'space-between',
        padding:20
    },

    InputField:{
        width:'100%',
        height:150,
        backgroundColor:Colors.INDEX_GREY,
        paddingHorizontal:20,
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.BLACK06,
        fontSize:14,
        paddingVertical:30
    },

    sendButton:{
        width:'90%',
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        borderRadius:25,
        height:50,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center',
        marginBottom:30
    }
})