//Global imports
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import { useDispatch } from 'react-redux';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import APICallService from '../../Api/APICallService';
import { REPORT_TO_ADMIN } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomInput from '../../Components/TextField/index';
import CustomButton from '../../Components/Button/index';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

const {
    MainContainer,
    InputField,
    sendButton
} = Styles;

const CustomerReportToAdmin = ({ navigation: { navigate, goBack }, route: { params: { type } } }) => {
    const [Report, SetReport] = useState('');

    const dispatch = useDispatch();

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToHome = () => {
        if (type === 'supplier') {
            navigate('SupplierHome')
        }
        else if (type === 'customer'){
            goBack();
        }
        else {
            navigate('Home')
        }
    }


    const _onChangeTextReport = (text) => SetReport(text);

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _SendReportToAdmin = () => {
        dispatch(updateLoader(true));

        const apiClass = new APICallService(REPORT_TO_ADMIN, { comment: Report })

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetReport('');
                    goBack()
                }

            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Report to Admin'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderReportField = () => {
        return <CustomInput
            multiline
            inputStyle={InputField}
            placeholder='Type here...'
            maxLength={256}
            textAlignVertical='top'

            value={Report}
            viewStyle={{ paddingHorizontal: 0 }}
            onChangeText={(text) => _onChangeTextReport(text)}
        />
    }

    const renderSendButton = () => {
        return <CustomButton
            btnStyle={sendButton}
            title='Send'
            textColor={Colors.BLACK06}
            buttonAction={() => {
                if (Report != '') {
                    _SendReportToAdmin()
                }
            }}
        />
    }

    const renderMainContainer = () => {
        return <View style={MainContainer}>
            {renderReportField()}

        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <ScrollView keyboardShouldPersistTaps='handled'>
                {renderHeader()}
                {renderMainContainer()}

            </ScrollView>
            {renderSendButton()}

        </View>
    )
}

export default CustomerReportToAdmin;