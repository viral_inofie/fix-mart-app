//Global imports
import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import LockIcon from '../../../assets/images/lock.png'
import CustomButton from '../../Components/Button';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomTextField from '../../Components/TextField';
import Colors from '../../Helper/Colors';
import Styles from './Styles'

//File imports
import APICallservice from '../../Api/APICallService';
import { RESET_PASSWORD } from '../../Helper/Constants';
import { renderErrorToast, validateResponse } from '../../Helper/Helper'
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions'
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';



const { InputField, LoginButton, validationStyle } = Styles

const ChangePassword = ({ navigation: { goBack } }) => {

    const [Data, setData] = useState({
        current_password: '',
        new_password: '',
        confirm_password: ''
    })
    const dispatch = useDispatch();

    const [badCurrentPassword, setbadCurrentPassword] = useState(false);
    const [badNewPassword, setBadNewPassword] = useState(false)

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _validate = () => {
        const { current_password, new_password, confirm_password } = Data

        let valid = true

        if (current_password === '') {
            setbadCurrentPassword(true);
            valid = false
        }
        else {
            setbadCurrentPassword(false)
        }

        if (new_password != confirm_password) {
            setBadNewPassword(true);
            valid = false
        }
        else {
            setBadNewPassword(false)
        }

        return valid
    }

    const _doResetPassword = () => {
        dispatch(updateLoader(true))
        const apiClass = new APICallservice(RESET_PASSWORD, {
            old_password: Data.current_password,
            new_password: Data.new_password
        })


        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                        withIcon: false
                    })
                    goBack();
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                        withIcon: false
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Change password'
            onLeftClick={() => goBack()}
        />
    }


    const renderPasswordField = () => {
        return <View>
            <CustomTextField
                containerView={[InputField, { marginTop: 20 }]}
                leftImg={LockIcon}
                placeholder='Old Password'
                value={Data.current_password}
                secureTextEntry
                onChangeText={(text) => setData({ ...Data, current_password: text })}
            />
            {
                badCurrentPassword && (
                    <Text style={validationStyle}>Please enter current password</Text>
                )
            }
        </View>
    }

    const renderNewPassword = () => (
        <View>
            <CustomTextField
                containerView={[InputField, { marginTop: 20 }]}
                leftImg={LockIcon}
                placeholder='New Password'
                value={Data.new_password}
                secureTextEntry
                onChangeText={(text) => setData({ ...Data, new_password: text })}
            />

        </View>
    )

    const renderConfirmPassword = () => (
        <View>
            <CustomTextField
                containerView={[InputField, { marginTop: 20 }]}
                leftImg={LockIcon}
                placeholder='Confirm Password'
                value={Data.confirm_password}
                secureTextEntry
                onChangeText={(text) => setData({ ...Data, confirm_password: text })}
            />
            {
                badNewPassword && (
                    <Text style={validationStyle}>New password and confirm password should be same</Text>
                )
            }
        </View>
    )

    const renderSubmitButton = () => {
        return <CustomButton
            title={'Submit'}
            buttonAction={() => {
                if (_validate()) {
                    _doResetPassword()
                }
            }}
            btnStyle={LoginButton}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}

            {renderPasswordField()}

            {renderNewPassword()}

            {renderConfirmPassword()}

            {renderSubmitButton()}
        </View>
    )
}

export default ChangePassword;