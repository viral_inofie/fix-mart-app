import { StyleSheet } from 'react-native';
import { height, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

const Styles = StyleSheet.create({

    InputField: {
        height: height(7),
        width: width(80),
        borderRadius: height(3.5),
        borderWidth: 1,
        borderColor: Colors.INDEX_GREY,
        // paddingHorizontal:width(5),
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: height(8),
        alignSelf : 'center'
    },

    LoginButton:{
        height:height(7),
        width:width(80),
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.APP_PRIMARY,
        borderRadius:height(3.5),
        marginTop:height(5),
        alignSelf :'center'
    },

    validationStyle : {
        fontFamily : Fonts.POPPINS_REGULAR,
        fontSize : 14,
        color :'red',
        marginLeft :width(15),
        marginTop :1
    }
})

export default Styles;