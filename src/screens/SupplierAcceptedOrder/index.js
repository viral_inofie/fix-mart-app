//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { RIGHT_SCROLL_INDICATOR_INSENTS, SUPPLIER_ORDER_ACCEPTED, SUPPLIER_PRODUCT_DELIVER } from '../../Helper/Constants';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import DemandsList from '../../Components/DemandList';
import CustomHeader from '../../Components/CustomerHeader';
import SupplierAcceptedOrderList from '../../Components/SupplierAccpetedOrderList';
import { useDispatch } from 'react-redux';
import { ProductDeliverOrderParam } from '../../Api/APIJson';
import EmptyDataComponent from '../../Components/EmptyDataComponent';


const { TabView, OnDemand, OnDemandText } = Styles;

const SupplierAcceptedOrder = ({ navigation: { navigate, addListener } }) => {
    const [DemandList, SetDemadList] = useState([])
    const [isOnDemand, SetIsOnDemand] = useState(true);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getOrder();
        })
    }, [])


    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('SupplierHome')
    }

    const _navigateToDetail = (order_id) => {
        navigate('SupplierAcceptedOrderDetail', { order_id })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrder = () => {
        const apiClass = new APICallService(SUPPLIER_ORDER_ACCEPTED, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetDemadList(res.data)
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doProductDeliver = (order_id) => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(SUPPLIER_PRODUCT_DELIVER, ProductDeliverOrderParam(order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    getOrder();
                }
            })
            .catch(err => {
                renderErrorToast('error', err)
                dispatch(updateLoader(false))
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Confirmed Offers'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: isOnDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemDemandList = (item, index) => {
        return <SupplierAcceptedOrderList
            item={item}
            time={100 * index}
            _doProductDeliver={(order_id) => _doProductDeliver(order_id)}
            navigateToDetail={(order_id) => _navigateToDetail(order_id)}
        />
    }

    const renderDemandList = () => {
        return <FlatList
            ListEmptyComponent={()=>{
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={DemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {/* {renderTabView()} */}

            {renderDemandList()}

        </View>
    )
}

export default SupplierAcceptedOrder;