//Global imports
import React, { useEffect, useState } from 'react';
import { View, FlatList } from 'react-native';
import { RNToasty } from 'react-native-toasty';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { DO_ACCEPT_ORDER, RIGHT_SCROLL_INDICATOR_INSENTS, SUPPLIER_DECLINED_ORDER, SUPPLIER_PENDING_ORDER } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import EmptyDataComponent from '../../Components/EmptyDataComponent/index';
import Fonts from '../../Helper/Fonts';
import { SupplierAcceptOrderParam, SupplierDeclinedOrderParam } from '../../Api/APIJson';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import MenuIcon from '../../../assets/images/Menu.png';
import SupplierNewOrderList from '../../Components/SupplierNewOrderList';


const { TabView, OnDemand, OnDemandText } = Styles;

const SupplierHome = ({ navigation: { navigate, toggleDrawer, addListener } }) => {
    const [DemandList, SetDemadList] = useState()

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getOrder()
        })

    }, []);

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _toggleDrawer = () => {
        toggleDrawer()
    }

    const _navigateToDetail = (order_id) => {
        navigate('SupplierNewOrderDetail', { order_id })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrder = () => {

        const apiClass = new APICallService(SUPPLIER_PENDING_ORDER, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));

                if (validateResponse(res)) {
                    SetDemadList(res.data)
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }


    const _doAcceptOrder = (order_id) => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(DO_ACCEPT_ORDER, SupplierAcceptOrderParam(order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    getOrder()
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const _doDeclineOrder = (order_id) => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(SUPPLIER_DECLINED_ORDER, SupplierDeclinedOrderParam(order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    getOrder()
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='New Orders'
            LeftIcon={MenuIcon}
            onLeftClick={() => _toggleDrawer()}
        />
    }

    const renderItemDemandList = (item, index) => {
        return <SupplierNewOrderList
            item={item}
            time={100 * index}
            _doDeclineOrder={(order_id) => _doDeclineOrder(order_id)}
            _doAcceptOrder={(order_id) => _doAcceptOrder(order_id)}
            navigateToDetail={(order_id) => _navigateToDetail(order_id)}
        />
    }

    const renderDemandList = () => {
        return <FlatList
            ListEmptyComponent={()=>{
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={DemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}



            {renderDemandList()}

        </View>
    )
}

export default SupplierHome;