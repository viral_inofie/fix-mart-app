//Global import

import React from 'react';
import { SafeAreaView } from 'react-native';
import { View, Text, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';

const PaymentWebView = (props) => {
    const { route: { params }, navigation: { navigate } } = props;
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <WebView onNavigationStateChange={(event) => {
                console.log(event, 'event')

                if (event.url === "https://fix-mart.com/api/stripe/connect/failed" && !event.loading) {
                    navigate('Login')
                }
                else if (event.url === "https://fix-mart.com/api/stripe/connect/success" && !event.loading) {
                    navigate('Login')
                }
            }} source={{ uri: params.url }} />
        </SafeAreaView>
    )
}

export default PaymentWebView;