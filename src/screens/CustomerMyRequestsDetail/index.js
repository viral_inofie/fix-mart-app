//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, ScrollView, StatusBar, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useDispatch } from 'react-redux';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { CUSTOMER_MY_REQUEST_DETAIL, PENDING_ORDER_CANCEL, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { PendingOrderCancelParam } from '../../Api/APIJson';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ViewButtonStyle,
    StarView,
    ConfirmButton,
    ModelMainContainer,
    ModelHeader,
    ModelTitle,
    ReviewText,
    RowView
} = Styles;

const CustomerMyRequestsDetail = ({ navigation: { goBack, pop,navigate,addListener }, route: { params } }) => {

    const [ReviewModel, SetReviewModel] = useState(false);
    const [OrderDetail, SetOrderDetail] = useState({
        image: '',
        name: '',
        service: '',
        price: '',
        otherDetails: '',
        subCategory: '',
        status: '',
        visit_date: '',
        visit_time: ''
    });
    console.log(params, "paramsssss")

    const dispatch = useDispatch();

    useEffect(() => {
        
        const unsubscribe = addListener('focus', () => {
            // The screen is focused
            // Call any action
            dispatch(updateLoader(true));
            getRequestDetail();
          });
      
          // Return the function to unsubscribe from the event so it gets removed on unmount
          return unsubscribe;
      
    }, [navigate])
    // useEffect(() => {
    //     dispatch(updateLoader(true));
    //     getRequestDetail();
    // }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getRequestDetail = () => {
        const apiClass = new APICallService(CUSTOMER_MY_REQUEST_DETAIL, { job_id: params.job_id })

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    dispatch(updateLoader(false))
                    SetOrderDetail({
                        ...OrderDetail,
                        image: res.data.image,
                        name: res.data.type_product,
                        service: res.data.service,
                        price: res.data.price,
                        otherDetails: res.data.other_details,
                        subCategory: res.data.sub_category,
                        status: res.data.status,
                        visit_date: res.data.visit_date,
                        visit_time: res.data.visit_time
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _doOrderCancel = () => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(PENDING_ORDER_CANCEL, PendingOrderCancelParam(params.job_id, OrderDetail.status, 'myjob'))

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    dispatch(updateLoader(false))
                    pop();
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'New Job Requests Detail'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <FlatList
            horizontal
            data={OrderDetail.image}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
                console.log(item, 'item')
                return <ImageBackground
                    source={{ uri: item.toString() }}
                    imageStyle={{ borderRadius: height(1.5) }}
                    style={ImageStyle}
                />
            }}
        />

    }

    const renderDemandName = () => {
        return <Text style={Title}>{OrderDetail.name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(35) }}>
                <Text style={PlumberDetailTitle}>Plumber Name :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Shedule Date & time :</Text>
            </View>

            <View style={{ width: width(45), }}>
                <Text style={PlumberDetailValue}>Russel</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>22 Feb 2020, 04:00 Pm</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View>
            <View style={RowView}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{OrderDetail.service}</Text>
                </View>
            </View>

            <View style={RowView}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Description :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{OrderDetail.name}</Text>
                </View>
            </View>

            {
                OrderDetail.subCategory != null
                    ?
                    <View style={RowView}>
                        <View style={{ width: width(35) }}>
                            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text>
                        </View>
                        <View style={{ width: width(45) }}>
                            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{OrderDetail.subCategory}</Text>
                        </View>
                    </View>
                    :
                    null
            }
            {
                OrderDetail.price != ""
                    ?
                    <View style={RowView}>
                        <View style={{ width: width(35) }}>
                            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Price :</Text>
                        </View>
                        <View style={{ width: width(45) }}>
                            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {OrderDetail.price}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={RowView}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(OrderDetail.visit_date).format('DD MMM, YYYY')}</Text>
                    <Text style={[PlumberDetailValue]}>{renderTime(OrderDetail.visit_time)}</Text>
                </View>
            </View>

            <View style={RowView}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{OrderDetail.otherDetails}</Text>
                </View>
            </View>

            {/* <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Product Ratings :</Text> */}
            {/* <Text style={[PlumberDetailTitle, { marginTop: height(1.5) }]}>Plumber Reviews :</Text> */}
        </View>
    }

    const renderServiceValue = () => {
        return <View style={{ width: width(45), }}>


            {/* {
                OrderDetail.subCategory != null
                    ?
                    :
                    null
            } */}
            {
                OrderDetail.price != ""
                    ?
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {OrderDetail.price}</Text>
                    :
                    null
            }

            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(OrderDetail.visit_date).format('DD MMM, YYYY')}</Text>
            <Text style={[PlumberDetailValue]}>{renderTime(OrderDetail.visit_time)}</Text>


            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{OrderDetail.otherDetails}</Text>

            {/* {renderStarView()} */}

            {/* {renderViewButton()} */}

        </View>
    }


    const renderServiceDetail = () => {
        return <View style={PlumberDetailView}>
            {renderServiceHeading()}

            {/* {renderServiceValue()} */}
        </View>
    }

    const renderCancelOrderButton = () => {
        if (params.user != "provider") {
            return <CustomButton
                title='Cancel Order'
                buttonAction={() => _doOrderCancel()}
                btnStyle={[ConfirmButton, {
                    backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: 0
                }]}
            />
        }
    }

    const renderDeclineButton = () => {
        return <CustomButton
            title='Decline'
            btnStyle={[ConfirmButton, { marginTop: 20 }]}
        />
    }

    const renderCancelButton = () => {
        return <CustomButton
            title='Cancel Order'
            btnStyle={[ConfirmButton, { marginTop: 20 }]}
        />
    }

    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>
            {renderCancelOrderButton()}

            {/* {renderDeclineButton()} */}

            {/* {renderCancelButton()} */}
        </View>
    }

    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <Text style={ModelTitle}>Plumber Reviews</Text>

                        <TouchableOpacity onPress={() => SetReviewModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                    </View>
                </View>
            </View>
        </Modal>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                bounces={false}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {/* {renderDemandName()} */}

                {/* {renderPlumberDetailView()} */}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {renderButtonView()}


            </ScrollView>

            {renderModel()}

        </View>
    )
}

export default CustomerMyRequestsDetail;