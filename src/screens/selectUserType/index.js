// Globla imports
import React, { Component } from 'react'
import {
  Text, View, Image, TouchableOpacity
} from 'react-native'

// File imports
import styles from './styles'
import COLORS from '../../Helper/Colors'

// Component imports
import imgLogo from '../../../assets/images/logo.png'
import imgCustomer from '../../../assets/images/customer.png'
import imgServiceProvider from '../../../assets/images/serviceProvider.png'

export class selectUserType extends Component {
  render() {
    return this.renderMainView()
  }

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########..######.
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##....##....##
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##....##......
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##.....######.
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##..........##
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##....##....##
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##.....######.
  */

  renderMainView = () => {
    return (
      <View style={styles.container}>
        <Image source={imgLogo} style={styles.headerImg} resizeMode='contain' />
        {this.renderSelectionType()}
      </View>
    )
  }

  renderSelectionType = () => {
    const { navigation } = this.props;
    return (
      <View style={styles.selectionTyoeCont}>
        <View style={{ alignItems: 'center', }}>
          <TouchableOpacity
            onPress={() => navigation.replace('Login', { type: 'customer' })}
            style={[styles.btn, { backgroundColor: COLORS.APP_PRIMARY_LIGHT, }]}>
            <Image
              source={imgCustomer}
              resizeMode='contain'
              style={{ height: 60, width: 60 }}
            />
          </TouchableOpacity>
          <Text style={styles.indicateTitle}>Customer</Text>
        </View>

        <View style={{ width: 20 }} />

        <View style={{ alignItems: 'center', }}>
          <TouchableOpacity style={[styles.btn, { backgroundColor: COLORS.OFF_WHITE, }]} 
          onPress={() => navigation.replace('Login', { type: 'serviceProvider' || 'supplier' })}>
            <Image
              resizeMode='contain'
              source={imgServiceProvider}
              style={{ height: 60, width: 60 }}
            />
          </TouchableOpacity>
          <Text style={styles.indicateTitle}>Service Provider/{'\n'}Supplier</Text>
        </View>

      </View>
    )
  }
}

export default selectUserType
