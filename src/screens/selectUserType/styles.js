import { StyleSheet } from 'react-native'
import FONTS from '../../Helper/Fonts'
import COLORS from '../../Helper/Colors'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  headerImg: {
    height: 180,
    width: 180,
    marginTop: 150,
    alignSelf: 'center'
  },
  btn: {
    height: 100,
    width: 100,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  indicateTitle: {
    fontSize: 16,
    fontFamily: FONTS.POPPINS_REGULAR,
    textAlign: 'center',
    marginTop: 10
  },
  selectionTyoeCont: {
    paddingHorizontal: 30,
    flexDirection: 'row',
    marginTop: 50,
    justifyContent: 'center',
  }
})