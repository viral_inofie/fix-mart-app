//Global imports
import { Platform, StyleSheet } from 'react-native';
import { hasNotch } from 'react-native-device-info';

//File imports
import Colors from '../../Helper/Colors';
import { WIDTH } from '../../Helper/Constants';
import Fonts from '../../Helper/Fonts';


const isNotchDevice = hasNotch();


export default StyleSheet.create({
    BottomFieldView: {
        paddingBottom: Platform.OS === 'ios' && isNotchDevice ? 25 : 10,
        width: WIDTH,
        // position: 'absolute',
        // bottom: 0,
        backgroundColor: Colors.CHAT_INPUT_BACKGROUND_COLOR,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10
    },

    InputFieldView: {
        width: (WIDTH - 40) * 0.85,
        backgroundColor: Colors.WHITE,
        height: 50,
        borderRadius:10,
        paddingHorizontal:10,
        justifyContent:'center'
    },

    SendButton:{
        width : (WIDTH - 40) * 0.13,
        marginLeft:(WIDTH - 40) * 0.02,
        height : 50,
        alignItems:'flex-end',
        justifyContent:'center'
    },

    ChatInputField:{
        width:'100%',
        fontSize:14,
        fontFamily:Fonts.POPPINS_LIGHT,
        color:Colors.BLACK06,
        height:40
    }


})