// //Global imports
// import React, { useEffect, useRef, useState } from 'react';
// import {
//     View,
//     KeyboardAvoidingView,
//     Platform,
//     ImageBackground,
//     TextInput,
//     FlatList,
//     Text,
//     TouchableOpacity,
//     ActivityIndicator
// } from 'react-native';
// import io, { Socket } from "socket.io-client";
// import AsyncStorage from '@react-native-async-storage/async-storage';

// //Component imports
// import CustomHeader from '../../Components/CustomerHeader';
// import BackArrow from '../../../assets/images/backarrow.png';
// import BlackSend from '../../../assets/images/blacksend.png';
// import CustomerChatComponent from '../../Components/CustomerChatComponent';

// //File imports
// import Colors from '../../Helper/Colors';
// import Styles from './Styles';
// import { RIGHT_SCROLL_INDICATOR_INSENTS, SOCKET_URL, } from '../../Helper/Constants';
// import Logger from '../../Helper/Logger';
// import { useSelector } from 'react-redux';
// import { socket } from '../../../App';
// import Fonts from '../../Helper/Fonts';
// import { totalSize } from 'react-native-dimension';
// import Loader from '../../Components/Loader';


// const {
//     BottomFieldView,
//     InputFieldView,
//     SendButton,
//     ChatInputField
// } = Styles;




// const CustomerMessages = ({ navigation: { goBack }, route: { params } }) => {
//     console.log(params)
//     const [MsgText, SetMsgText] = useState('');
//     const MyFlatList = useRef(null);

//     const { user_id } = useSelector(state => ({
//         user_id: state.AuthReducer.loginRes.id
//     }))

//     let [MessageData, SetMessageData] = useState([]);
//     const [userProfile, Set_userProfile] = useState('');
//     const [opponentProfile, Set_opponentProfile] = useState('');
//     const [random_key, Set_random_key] = useState(Math.random());
//     const [loader, setLoader] = useState(true)
//     useEffect(() => {

//         socket.emit('join', user_id)

//         socket.emit('getMessages', { toUserId: params.supplier_id, fromUserId: user_id });

//         socket.on('messageList', (data) => {
//             console.log('messageList : => ', data)
//             if (MessageData.length === 0) {
//                 SetMessageData(data);
//                 setLoader(false)
//             }

//             if (data.length > 0) {
//                 Set_userProfile(data[0].to_profile);
//                 Set_opponentProfile(data[0].from_profile)
//                 setLoader(false)
//             }
//         })
//     }, [])

//     socket.on('messageReceive', (data) => {
//         console.log('messageReceive data : =>', data)
//         // let newArray = [data];
//         // let newMsg = [...MessageData, ...newArray ];
//         if (MessageData.length > 0) {
//             if (MessageData[MessageData.length - 1].message != data.message) {
//                 MessageData.push(data);
//                 // Set_random_key(Math.random());
//             }
//         }
//         else {
//             MessageData.push(data)
//             // Set_random_key(Math.random());
//         }

//         // console.log(newMsg, 'newMsg')
//         // SetMessageData(newMsg);
//         if (data.length > 0) {
//             Set_userProfile(data[0].to_profile);
//             Set_opponentProfile(data[0].from_profile)
//             Set_random_key(Math.random());
//         }
//         SetMsgText('')
//     })






//     /*
//     .##....##....###....##.....##.####..######......###....########.####..#######..##....##
//     .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
//     .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
//     .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
//     .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
//     .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
//     .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
//     */

//     const _navigateToBack = () => {
//         goBack()
//     }

//     /*
//     .##........#######...######...####..######...######.
//     .##.......##.....##.##....##...##..##....##.##....##
//     .##.......##.....##.##.........##..##.......##......
//     .##.......##.....##.##...####..##..##........######.
//     .##.......##.....##.##....##...##..##.............##
//     .##.......##.....##.##....##...##..##....##.##....##
//     .########..#######...######...####..######...######.
//     */

//     const _renderMessages = (data) => {
//         return <CustomerChatComponent
//             userProfile={userProfile}
//             opponentProfile={opponentProfile}
//             data={data}
//             user_id={params.supplier_id}
//         />
//     }

//     const _scrollToBottom = () => {
//         MyFlatList.current.scrollToEnd({ animate: true })
//     }

//     const _sendMessage = async () => {
//         socket.emit('messageSent', {
//             message: MsgText,
//             toUserId: params.supplier_id,
//             fromUserId: user_id
//         })
//     }

//     /*
//     ..######...#######..##.....##.########...#######..##....##.########.##....##.########
//     .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
//     .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
//     .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
//     .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
//     .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
//     ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
//     */

//     const renderHeader = () => {
//         return <CustomHeader
//             title={params.supplier_name}
//             LeftIcon={BackArrow}
//             onLeftClick={() => _navigateToBack()}
//         />
//     }

//     const renderInputField = () => {
//         return <View style={BottomFieldView}>
//             {renderTextField()}
//             {renderSendButton()}
//         </View>
//     }

//     const renderTextField = () => {
//         return <View style={InputFieldView}>
//             <TextInput
//                 style={ChatInputField}
//                 value={MsgText}
//                 keyboardType='default'
//                 onChangeText={(text) => SetMsgText(text)}
//                 placeholder='Type message'
//                 placeholderTextColor={Colors.PLACEHOLDER_COLOR}
//             />
//         </View>
//     }

//     const renderSendButton = () => {
//         return <TouchableOpacity
//             onPress={() => {
//                 if (MsgText != '') {
//                     _sendMessage()
//                 }
//             }} style={SendButton}>
//             <ImageBackground resizeMode='contain' source={BlackSend} style={{ height: 25, width: 25 }} />
//         </TouchableOpacity>
//     }

//     const renderChatList = () => {
//         if (MessageData.length === 0) {
//             return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
//                 <Text style={{ fontFamily: Fonts.POPPINS_REGULAR, fontSize: totalSize(1.3), color: Colors.BLACK06 }}>No message found</Text>
//             </View>
//         }
//         else {
//             return <FlatList
//                 ref={MyFlatList}
//                 bounces={false}
//                 key={random_key}
//                 extraData={MessageData.length}
//                 onLayout={() => MyFlatList.current.scrollToEnd({ animated: true })}
//                 onContentSizeChange={() => MyFlatList.current.scrollToEnd({ animated: true })}
//                 contentContainerStyle={{ paddingVertical: 40 }}
//                 scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
//                 data={MessageData}
//                 renderItem={(data) => _renderMessages(data)}
//             />
//         }

//     }


//     return (
//         <KeyboardAvoidingView
//             enabled={Platform.OS === 'ios' ? true : false}
//             behavior={Platform.OS === 'ios' ? 'padding' : null}
//             style={{ flex: 1, backgroundColor: Colors.WHITE }}>
//             <View style={{ flex: 1, backgroundColor: Colors.PRIMARY_WHITE }}>
//                 {
//                     loader ? (
//                         <Loader />
//                     ) : (
//                             <>
//                                 {renderHeader()}
//                                 {renderChatList()}
//                                 {renderInputField()}
//                             </>
//                         )
//                 }

//             </View>
//         </KeyboardAvoidingView>
//     )
// }

// export default CustomerMessages;

import React, { useEffect, useRef, useState, Component } from 'react';
import {
    View,
    KeyboardAvoidingView,
    Platform,
    ImageBackground,
    TextInput,
    FlatList,
    Text,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
// import io, { Socket } from "socket.io-client";
import AsyncStorage from '@react-native-async-storage/async-storage';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import BackArrow from '../../../assets/images/backarrow.png';
import BlackSend from '../../../assets/images/blacksend.png';
import CustomerChatComponent from '../../Components/CustomerChatComponent';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, SOCKET_URL, } from '../../Helper/Constants';
import Logger from '../../Helper/Logger';
import { useSelector } from 'react-redux';
import { socket } from '../../../App';
import Fonts from '../../Helper/Fonts';
import { totalSize } from 'react-native-dimension';
import Loader from '../../Components/Loader';
import { connect } from 'react-redux'
const {
    BottomFieldView,
    InputFieldView,
    SendButton,
    ChatInputField
} = Styles;
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loader: false,
            MsgText: '',
            messagedata: [],
            userProfile: '',
            opponentProfile: '',
            random_key: Math.random()
        }
        this.MyFlatList = React.createRef();
    }
    _navigateToBack = () => {
        this.props.navigation.goBack()
    }
    componentDidMount() {
        try {
            socket.emit('join', this.props.auth.loginRes.id)

            socket.emit('getMessages', { toUserId: this.props.route.params.supplier_id, fromUserId: this.props.auth.loginRes.id });
            console.log('messageList : => ')
            socket.on('messageList', (data) => {
                if (this.state.messagedata.length === 0) {
                    this.setState({
                        messagedata: data,
                        loader: false
                    })
                }
                if (data.length > 0) {
                    this.setState({
                        userProfile: data[0].to_profile,
                        opponentProfile: data[0].from_profile,
                        loader: false
                    }, () => {
                        console.log('state => ', this.state)
                    })

                }
            })
            socket.on('messageReceive', (data) => {
                const { messagedata } = this.state
                const previousdata = messagedata
                console.log('messageReceive data : =>', data)
                // let newArray = [data];
                // let newMsg = [...MessageData, ...newArray ];
                if (this.state.messagedata.length > 0) {
                    if (messagedata[messagedata.length - 1].message != data.message) {
                        previousdata.push(data);
                        // Set_random_key(Math.random());
                    }
                    this.setState({
                        messagedata: previousdata,
                        // MsgText: ''
                    })
                }
                else {
                    previousdata.push(data)
                    this.setState({
                        messagedata: previousdata,
                        // MsgText: ''
                    })
                    // Set_random_key(Math.random());
                }

                // console.log(newMsg, 'newMsg')
                // SetMessageData(newMsg);
                if (data.length > 0) {
                    this.setState({
                        userProfile: data[0].to_profile,
                        opponentProfile: data[0].from_profile,
                        random_key: Math.random(),
                        // MsgText: ''
                    })
                }
            })
        } catch (e) {
            console.log(e)
        }


    }
    renderHeader = () => {
        return <CustomHeader
            title={this.props.route.params.supplier_name}
            LeftIcon={BackArrow}
            onLeftClick={() => this._navigateToBack()}
        />
    }
    renderSendButton = () => {
        return <TouchableOpacity
            onPress={() => {
                if (this.state.MsgText != '') {
                    this._sendMessage()
                }
            }} style={SendButton}>
            <ImageBackground resizeMode='contain' source={BlackSend} style={{ height: 25, width: 25 }} />
        </TouchableOpacity>
    }
    _sendMessage = async () => {
        socket.emit('messageSent', {
            message: this.state.MsgText,
            toUserId: this.props.route.params.supplier_id,
            fromUserId: this.props.auth.loginRes.id
        })
        this.setState({
            MsgText: ''
        })
    }
    renderInputField = () => {
        return <View style={BottomFieldView}>
            {this.renderTextField()}
            {this.renderSendButton()}
        </View>
    }
    renderTextField = () => {
        return <View style={InputFieldView}>
            <TextInput
                style={ChatInputField}
                value={this.state.MsgText}
                keyboardType='default'
                onChangeText={(text) => this.setState({
                    MsgText: text
                })}
                placeholder='Type message'
                placeholderTextColor={Colors.PLACEHOLDER_COLOR}
            />
        </View>
    }
    _renderMessages = (data) => {
        return <CustomerChatComponent
            userProfile={this.state.userProfile}
            opponentProfile={this.state.opponentProfile}
            data={data}
            user_id={this.props.route.params.supplier_id}
        />
    }
    renderChatList = () => {
        if (this.state.messagedata.length === 0) {
            return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                <Text style={{ fontFamily: Fonts.POPPINS_REGULAR, fontSize: totalSize(1.3), color: Colors.BLACK06 }}>No message found</Text>
            </View>
        }
        else {
            return <FlatList
                // inverted={true}
                ref={this.MyFlatList}
                bounces={false}
                key={this.state.random_key}
                extraData={this.state.messagedata.length}
                onLayout={() => this.MyFlatList.current.scrollToEnd({ animated: true })}
                onContentSizeChange={() => this.MyFlatList.current.scrollToEnd({ animated: true })}
                contentContainerStyle={{ paddingVertical: 40 }}
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                data={this.state.messagedata}
                renderItem={(data) => this._renderMessages(data)}
            />
        }

    }
    render() {
        return (
            <KeyboardAvoidingView
                enabled={Platform.OS === 'ios' ? true : false}
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                <View style={{ flex: 1, backgroundColor: Colors.PRIMARY_WHITE }}>
                    {
                        this.state.loader ? (
                            null
                        ) : (
                                <>
                                    {this.renderHeader()}
                                    {this.renderChatList()}
                                    {this.renderInputField()}
                                </>
                            )
                    }

                </View>
            </KeyboardAvoidingView>
        )
    }
}
const mapStateToProps = ({ AuthReducer }) => {
    return {
        auth: AuthReducer
    }
};

export default connect(mapStateToProps, null)(index);