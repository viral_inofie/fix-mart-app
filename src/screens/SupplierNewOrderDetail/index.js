//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, ScrollView } from 'react-native';
import { height, width } from 'react-native-dimension';
import moment from 'moment';
//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, PENDING_ORDER_DETAIL_SUPPLIER, DO_ACCEPT_ORDER, SUPPLIER_DECLINED_ORDER } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { PendingOrderDetailParamSupplier, SupplierAcceptOrderParam, SupplierDeclinedOrderParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
const {
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ConfirmButton,
} = Styles;

const SupplierNewOrderDetail = ({ navigation: { goBack, addListener, pop }, route: { params } }) => {

    const dispatch = useDispatch();
    const [Data, SetData] = useState({
        product_image: '',
        product_name: '',
        plumber_name: '',
        scheduleDate: '',
        scheduleTime: '',
        service: '',
        sub_category: '',
        qty: '',
        otherDetails: '',
        price: '',
        isAccepted: '',
        visit_time: '',
        visit_date: ''
    })

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getOrderDetail()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(PENDING_ORDER_DETAIL_SUPPLIER, PendingOrderDetailParamSupplier(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetData({
                        ...Data,
                        product_image: res.data.image,
                        product_name: res.data.name,
                        plumber_name: res.data.provider_name,
                        service: res.data.service,
                        sub_category: res.data.sub_category,
                        qty: res.data.quantity,
                        otherDetails: res.data.other_details,
                        price: res.data.price,
                        isAccepted: res.data.isAccepted,
                        visit_date: res.data.visit_date,
                        visit_time: res.data.visit_time
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doAcceptOrder = () => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(DO_ACCEPT_ORDER, SupplierAcceptOrderParam(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    getOrderDetail()
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const _doDeclineOrder = (order_id) => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(SUPPLIER_DECLINED_ORDER, SupplierDeclinedOrderParam(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    pop();
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'New Orders'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: Data.product_image }}
            imageStyle={{ borderRadius: height(1.5) }}
            style={ImageStyle}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.product_name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                    <View style={{ width: width(40) }}>
                        <Text style={PlumberDetailTitle}>Plumber Name :</Text>
                    </View>

                    <View style={{ width: width(40) }}>
                        <Text style={PlumberDetailValue}>{Data.plumber_name}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                    <View style={{ width: width(40) }}>
                        <Text style={PlumberDetailTitle}>Scheduled Appointment :</Text>
                    </View>

                    <View style={{ width: width(40) }}>
                        <Text style={PlumberDetailValue}>{moment(Data.visit_date).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
                    </View>
                </View>

            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderOrderHeading = () => {
        return <View style={{ width: width(35) }}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            {
                Data.sub_category != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Sub Category :</Text>
                        </View>
                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{Data.sub_category}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Quantity :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.qty}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.otherDetails}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Price :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.price}</Text>
                </View>
            </View>
        </View>
    }

    const renderOrderDetail = () => {
        return <View style={PlumberDetailView}>
            {renderOrderHeading()}
        </View>
    }

    const renderAcceptButton = () => {
        if (Data.isAccepted) {
            return null
        }
        else {
            return <CustomButton
                buttonAction={() => _doAcceptOrder()}
                title='Accept'
                btnStyle={[ConfirmButton, {
                    backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: 0
                }]}
            />
        }
    }

    const renderDeclineButton = () => {
        if (Data.isAccepted) {
            return null
        }
        else {
            return <CustomButton
                title='Decline'
                buttonAction={() => _doDeclineOrder()}
                btnStyle={[ConfirmButton, { marginTop: 20 }]}
            />
        }
    }

    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>
            {renderAcceptButton()}

            {renderDeclineButton()}
        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                bounces={false}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderOrderDetail()}

                {renderButtonView()}


            </ScrollView>



        </View>
    )
}

export default SupplierNewOrderDetail;