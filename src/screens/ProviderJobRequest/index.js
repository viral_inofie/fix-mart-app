//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import GetLocation from 'react-native-get-location';
import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { RIGHT_SCROLL_INDICATOR_INSENTS, GET_CUSTOMER_MY_REQUEST, UPDATE_LATLONG } from '../../Helper/Constants';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import APICallService from '../../Api/APICallService';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import ProviderJobRequestList from '../../Components/ProviderJobRequestList';
import MenuIcon from '../../../assets/images/Menu.png';
import EmptyDataComponent from '../../Components/EmptyDataComponent';
import { UpdateLocation } from '../../ReduxStore/Actions/AuthActions';
import Logger from '../../Helper/Logger';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import BackIcon from '../../../assets/images/backarrow.png';
import { RefreshControl } from 'react-native';

const { TabView, OnDemand, OnDemandText } = Styles;

const ProviderJobRequest = ({ navigation: { navigate, toggleDrawer, addListener }}) => {
    const [OnDemandJob, SetOnDemandJob] = useState([]);
    const [OffDemandJob, SetOffDemandJob] = useState([]);

    const [isOnDemand, SetIsOnDemand] = useState(true);

    const dispatch = useDispatch();
    const [refresh, setRefresh] = useState(false);

    useEffect(() => {
        
        const unsubscribe = addListener('focus', () => {
            dispatch(updateLoader(true));
            checkLocationPermission();
            getJob(false);
          });
      
          // Return the function to unsubscribe from the event so it gets removed on unmount
          return unsubscribe;
      
    }, [navigate])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _toggleDrawer = () => {
        console.log(toggleDrawer, 'toggleDrawer')
        if (toggleDrawer) {
            toggleDrawer()
        }
        else {
            navigate('Home')
        }
        
    }

    const _navigateToDetail = (type, job_id) => {
        navigate('ProviderJobRequestDetail', {
            type: type,
            job_id: job_id
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getJob = (loader) => {
        setRefresh(loader)
        const apiClass = new APICallService(GET_CUSTOMER_MY_REQUEST, {isnewjob:1});

        apiClass.callAPI()
            .then(res => {
                
                if (validateResponse(res)) {
                    console.log("res.data.onDemand===>",res.data.onDemand);
                    SetOnDemandJob(res.data.onDemand);
                    SetOffDemandJob(res.data.offDemand);
                    dispatch(updateLoader(false))
                }
                setRefresh(false)
            })
            .catch(err => {
                setRefresh(false)
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    function checkLocationPermission() {
        check(
            Platform.select({
                ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
                android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            }),
        ).then((result) => {
            switch (result) {
                case RESULTS.DENIED:
                    request(
                        Platform.select({
                            ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
                            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                        })
                    ).then((result) => {
                        if (result === RESULTS.GRANTED) {
                            GetLocation.getCurrentPosition({
                                enableHighAccuracy: true,
                                timeout: 10000,
                            }).then(location => {
                                Logger.log("Location::", location);
                                dispatch(UpdateLocation({ latitude: location.latitude, longitude: location.longitude }))
                                updateLocation(location.latitude, location.longitude);
                                // onSelectRegion({ latitude: location.latitude, longitude: location.longitude })
                            }).catch(error => {
                                const { code, message } = error;
                                Logger.log(code, message);
                                if (code === 'UNAVAILABLE')
                                    dispatch(updateLoader(false))
                                alert('Please enable location on device');
                            })
                        } else {
                            dispatch(updateLoader(false))
                            alert('Please give location permission');
                        }
                    });
                    break;
                case RESULTS.GRANTED:
                    GetLocation.getCurrentPosition({
                        enableHighAccuracy: true,
                        timeout: 10000,
                    })
                        .then(location => {
                            Logger.log("Location::", location);
                            dispatch(UpdateLocation({ latitude: location.latitude, longitude: location.longitude }))
                            updateLocation(location.latitude, location.longitude);
                            // onSelectRegion({ latitude: location.latitude, longitude: location.longitude })
                        })
                        .catch(error => {
                            const { code, message } = error;
                            Logger.log(code, message);
                            if (code === 'UNAVAILABLE')
                                dispatch(updateLoader(false))
                            alert('Please enable location on device');
                        })
                    break;
                case RESULTS.BLOCKED:
                    dispatch(updateLoader(false))
                    alert('Please give location permission');
                    break;
            }
        }).catch((error) => {
            dispatch(updateLoader(false))
            Logger.log("Error ::", error);
        });
    }

    const updateLocation = (latitude, longitude) => {
        const apiClass = new APICallService(UPDATE_LATLONG, { latitude: latitude, longitude: longitude });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    console.log(res, 'res')
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='New Job Request'
            LeftIcon={toggleDrawer ? MenuIcon : BackIcon}
            onLeftClick={() => _toggleDrawer()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: isOnDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemDemandList = (item, index) => {
        if (isOnDemand) {
            console.log("isOnDemand====>item",item)
            return <ProviderJobRequestList
                item={item}
                time={100 * index}
                navigateToDetail={(job_id) => {
                    _navigateToDetail('ondemand', job_id)
                }}
            />
        }
        else {
            return <ProviderJobRequestList
                item={item}
                time={100 * index}
                navigateToDetail={(job_id) => _navigateToDetail('offdemand', job_id)}
            />
        }
    }

    const renderDemandList = () => {

        return <FlatList
            // onRefresh={()=><RefreshControl  onRefresh={()=>getJob(true)} refreshing={refresh} />}
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            bounces={false}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            data={isOnDemand ? OnDemandJob : OffDemandJob}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
            refreshControl={<RefreshControl
                refreshing={refresh}
                onRefresh={() => getJob(true)} />}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {renderTabView()}

            {renderDemandList()}

        </View>
    )
}

export default ProviderJobRequest;