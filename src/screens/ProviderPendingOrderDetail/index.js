//Global imports
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TextInput,
  FlatList
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DateTimePicker from "@react-native-community/datetimepicker"
import moment from 'moment'
import { useDispatch } from 'react-redux';
import { AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { PENDING_ORDER_DETAIL, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { PendingOrderDetailParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import Fonts from '../../Helper/Fonts';

const {
  HeaderView, HeaderTitle, ImageStyle, Title, PlumberDetailTitle,
  PlumberDetailValue, PlumberDetailView, ServiceDetailStyle,
  ViewButtonStyle, ViewText, StarView, ConfirmButton, ButtonText,
  ModelMainContainer, ModelHeader, ModelTitle, ReviewText,
  radioBtnImg, radiBtnImgCont, textInput, moneyTextInput,
  MainWhiteView,
  ModalHeaderView,
  HeadingText,
  RowView,
  RadioView,
  PaymentNameStyle,
  EnableRadio,
  PayNowButton,
  AdjustmentDateField,
  EstimateCostStyle
} = Styles;

const ProviderPendingOrderDetail = ({ navigation: { goBack, addListener }, route: { params } }) => {

  const [PayNowModel, SetPayNowModel] = useState(false)
  const [ReviewModel, SetReviewModel] = useState(false);
  const [PayLaterModel, SetPayLaterModel] = useState(false)
  const [mode, setMode] = useState('date')
  const [Money, SetMoney] = useState('')
  const [NextInvestmentDate, SetNextInvestmentDate] = useState(new Date())
  const [isDateModalVisible, SetisDateModalVisible] = useState(false)

  const [Data, SetData] = useState({
    product_image: '',
    product_name: '',
    customer_name: '',
    schedule_date: '',
    schedule_time: '',
    service: '',
    sub_category: '',
    price: '',
    other_details: '',
    rating: '',
    estimate_cost: '',
    transport_charge: '',
    assessment_charge: '',
    arrival_time: '',
    sub_job_type: '',
    isCustomerPaid: ''
  })

  const [PaymentMode, SetPaymentMode] = useState([
    { key: 0, name: 'Debit Card', isSelect: false },
    { key: 1, name: 'Credit Card', isSelect: false },
    { key: 2, name: 'Paypal', isSelect: false },
    { key: 3, name: 'Paybalance', isSelect: false },
  ])

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateLoader(true))
    // addListener('focus', () => {
    getOrderDetail()
    // })
  }, [])

  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack();

  const _openModel = () => SetReviewModel(true);

  const _handleDatePicked = (val) => {
    let selectedDate = moment(val).format('MMM DD, YYYY')
    // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
    NextInvestmentDate(`${selectedDate}`)
    SetisDateModalVisible(false)
  }

  const _hideDateTimePicker = () => {
    SetisDateModalVisible(false)
  }

  const _closeModal = () => {
    SetPayNowModel(false)
  }

  /*
     .##........#######...######...####..######...######.
     .##.......##.....##.##....##...##..##....##.##....##
     .##.......##.....##.##.........##..##.......##......
     .##.......##.....##.##...####..##..##........######.
     .##.......##.....##.##....##...##..##.............##
     .##.......##.....##.##....##...##..##....##.##....##
     .########..#######...######...####..######...######.
     */

  const _SelectPaymentMethod = (d) => {
    console.log(d, 'item')
    let NewArray = PaymentMode.map((item, i) => {
      item.isSelect = false;
      if (item.key === d.key) {
        item.isSelect = true
        return item
      }
      return item
    })
    SetPaymentMode(NewArray)
  }

  const getOrderDetail = () => {
    const apiClass = new APICallService(PENDING_ORDER_DETAIL, PendingOrderDetailParam(params.job_id));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          SetData({
            ...Data,
            product_image: res.data.single_image,
            product_name: res.data.name,
            customer_name: res.data.customer_firstname + " " + res.data.customer_lastname,
            schedule_date: res.data.visit_date,
            schedule_time: res.data.visit_time,
            service: res.data.service,
            sub_category: res.data.subcategory,
            price: res.data.price,
            other_details: res.data.description,
            rating: res.data.rating,
            isCustomerPaid: res.data.isCustomerPaid,
            sub_job_type: res.data.sub_job_type,

            estimate_cost: res.data.answer[0].answer,
            transport_charge: res.data.answer[1].answer,
            assessment_charge: res.data.answer[2].answer,
            arrival_time: res.data.answer[3].answer,
          })
        }
      })
      .catch(err => {
        renderErrorToast('error', err)
        dispatch(updateLoader(false))
      })
  }

  const renderTime = (Time) => {
    let Hours = Time.substring(0, 3);

    let Minutes = Time.substring(3, 5);

    let isAmPm = Hours >= 12 ? "PM" : 'AM'

    return `${Hours} : ${Minutes} ${isAmPm}`
  }

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeader = () => {
    return <CustomHeader
      title={'Sent Offers'}
      onLeftClick={() => _navigateToBack()}
    />
  }

  const renderDemandImage = () => {
    return <ImageBackground
      source={{ uri: Data.product_image }}
      imageStyle={{ borderRadius: height(1.5) }}
      style={ImageStyle}
    />
  }

  const renderDemandName = () => {
    return <Text style={Title}>{Data.product_name}</Text>
  }

  const renderPlumberDetailView = () => {
    return <View style={PlumberDetailView}>
      <View style={{ width: width(35) }}>
        <Text style={PlumberDetailTitle}>Plumber Name :</Text>
        <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Shedule Date & time :</Text>
      </View>

      <View style={{ width: width(45), }}>
        <Text style={PlumberDetailValue}>{Data.customer_name}</Text>
        <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.schedule_date).format('DD MMM, YYYY') + " " + renderTime(Data.schedule_time)}</Text>
      </View>
    </View>
  }

  const renderServiceDetailTitle = () => {
    return <View style={[PlumberDetailView, {
      marginTop: height(3),
      height: height(4),
      borderBottomWidth: 0.5,
      borderColor: '#CCCCCC'
    }]}>
      <Text style={ServiceDetailStyle}>Service Details</Text>
    </View>
  }

  const renderServiceHeading = () => {
    return <View style={{ width: width(35) }}>
      <Text style={PlumberDetailTitle}>Service :</Text>
      {
        Data.sub_category != null
          ?
          <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text>
          :
          null
      }
      <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Price :</Text>
      <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
      {
        Data.sub_job_type === 3
          ?
          <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Product Ratings :</Text>
          :
          null
      }
      {/* <Text style={[PlumberDetailTitle, { marginTop: height(1.5) }]}>Date & Time : </Text> */}

      {/* <Text style={[PlumberDetailTitle, { marginTop: height(1.5) }]}>Plumber Reviews :</Text> */}

    </View>
  }

  const renderStarView = () => {
    if (Data.sub_job_type === 3) {
      return <View style={StarView}>
        <AirbnbRating
          selectedColor={Colors.YELLOW}
          count={5}
          showRating={false}
          defaultRating={Data.rating.substring(0, 3)}
          reviewColor={Colors.PRIMARY_DARK}
          size={10}
          isDisabled
          onFinishRating={(val) => { }}
        />
        <Text style={PlumberDetailValue}>/ {Data.rating.substring(4, 6)}</Text>
      </View>
    }
    else {
      return null
    }

  }

  const renderViewButton = () => {
    return <CustomButton
      title='View'
      textStyle={{ fontSize: 12 }}
      textColor={Colors.BLACK}
      buttonAction={() => _openModel()}
      btnStyle={ViewButtonStyle}
    />
  }

  const renderServiceValue = () => {
    return <View style={{ width: width(45), }}>
      <Text style={PlumberDetailValue}>{Data.service}</Text>
      {
        Data.sub_category != null
          ?
          <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.sub_category}</Text>
          :
          null
      }
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.other_details}</Text>

      {renderStarView()}
      {/* {renderAdjustDateField()} */}
      {/* {renderViewButton()} */}

    </View>
  }

  const renderAdjustDateField = () => {
    return <TextInput style={AdjustmentDateField} />
  }

  const renderServiceDetail = () => {
    return <View style={PlumberDetailView}>
      {renderServiceHeading()}

      {renderServiceValue()}
    </View>
  }

  const renderPayNowButton = () => {
    return <CustomButton
      title='Pay Now'
      buttonAction={() => SetPayNowModel(true)}
      btnStyle={[ConfirmButton, {
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        borderWidth: 0
      }]}
    />
  }

  const renderPayLaterButton = () => {
    return <CustomButton
      title='Pay Later'
      btnStyle={[ConfirmButton, { marginTop: 20 }]}
      buttonAction={() => SetPayLaterModel(true)}
    />
  }

  const renderCancelButton = () => {
    return <CustomButton
      title='Cancel Order'
      btnStyle={[ConfirmButton, { marginTop: 20 }]}
    />
  }

  const renderButtonView = () => {
    return <View style={{ marginTop: height(5) }}>
      {renderPayNowButton()}

      {renderPayLaterButton()}

      {/* {renderCancelButton()} */}
    </View>
  }

  const renderModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetReviewModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
          </View>
        </View>
      </View>
    </Modal>
  }

  const renderPayLaterModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={PayLaterModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <View style={{ flexDirection: 'row', }}>
              <Text style={ModelTitle}>How much money</Text>
              <TextInput
                style={moneyTextInput}
                value={Money}
                onChangeText={(text) => SetMoney(text)}
              />
            </View>

            <TouchableOpacity onPress={() => SetPayLaterModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>Next Installment date</Text>

            <TouchableOpacity activeOpacity={1} onPress={() => SetNextInvestmentDate(true)}>
              <TextInput
                style={[moneyTextInput, { width: 200 }]}
                value={NextInvestmentDate}
                onChangeText={(text) => SetNextInvestmentDate(text)}
              />
            </TouchableOpacity>

            <CustomButton
              title='Submit'
              btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0,
                height: 40,
                marginTop: 15
              }]}
              buttonAction={() => SetPayLaterModel(false)}
            />
          </View>
        </View>
      </View>
    </Modal>
  }


  const renderPayNowModel = () => {
    return <Modal style={{ marginHorizontal: 0 }} isVisible={PayNowModel}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <View style={MainWhiteView}>

          <View style={ModalHeaderView}>
            <Text style={HeadingText}>Payment Mode</Text>

            <TouchableOpacity onPress={() => _closeModal()}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          {renderPaymentModalList()}

          {renderPayNowButton()}
        </View>
      </View>
    </Modal>
  }


  const renderPaymentModalList = () => {
    return <FlatList
      contentContainerStyle={{ paddingHorizontal: width(5), paddingTop: height(2) }}
      data={PaymentMode}
      keyExtractor={(item, index) => index.toString()}
      renderItem={(data) => _renderListPayment(data)}
    />
  }

  // const renderPayNowButton = () => {
  //   return <CustomButton
  //       buttonAction={() => SetPayNowModel(true)}
  //       title='Pay Now'
  //       btnStyle={PayNowButton}
  //   />
  // }

  const _renderListPayment = (data) => {
    return <TouchableOpacity
      onPress={() => _SelectPaymentMethod(data.item)}
      style={RowView}>
      <View style={RadioView}>
        {
          data.item.isSelect
            ?
            <View style={EnableRadio}>
            </View>
            :
            null
        }
      </View>

      <Text style={PaymentNameStyle}>{data.item.name}</Text>
    </TouchableOpacity>
  }

  const renderPaymentDetail = () => {
    if (Data.sub_job_type != 3) {
      return <View style={PlumberDetailView}>
        {renderPaymentHeading()}
      </View>
    }
    else {
      return null
    }

  }

  const renderPaymentHeading = () => {
    return <View>
      <View style={RowView}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Estimate cost :</Text>
        </View>
        <View style={{ width: width(45), }}>
          <Text style={PlumberDetailValue}>$ {Data.estimate_cost}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Transport Charge :</Text>
        </View>
        <View style={{ width: width(45), }}>
          <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
        </View>
        <View style={{ width: width(45), }}>
          <Text style={PlumberDetailValue}>$ {Data.assessment_charge}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
        </View>
        <View style={{ width: width(45), }}>
          <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.arrival_time}</Text>
        </View>
      </View>

    </View>

  }

  const renderPaymentValue = () => {
    return <View style={{ width: width(45), }}>

      <Text style={[PlumberDetailValue, { marginTop: height(5) }]}>$ {Data.transport_charge}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(5) }]}>$ {Data.assessment_charge}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(5) }]}>$ {Data.arrival_time}</Text>

    </View>
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeader()}

      <ScrollView
        scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
        bounces={false}
        contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
        {renderDemandImage()}

        {renderDemandName()}

        {renderPlumberDetailView()}

        {renderServiceDetailTitle()}

        {renderServiceDetail()}

        {renderPaymentDetail()}

        {/* {renderButtonView()} */}

        {
          isDateModalVisible
            ?
            <DateTimePicker
              value={NextInvestmentDate}
              mode={mode}
              onConfirm={(val) => _handleDatePicked(val)}
              onCancel={() => _hideDateTimePicker()}
            />
            :
            null
        }

      </ScrollView>

      {renderModel()}
      {renderPayLaterModel()}
      {renderPayNowModel()}

    </View>
  )
}

export default ProviderPendingOrderDetail