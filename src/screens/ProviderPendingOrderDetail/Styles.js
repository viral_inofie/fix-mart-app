import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    HeaderView: {
        height: height(7),
        paddingHorizontal: width(5),
        width: '100%',

        borderBottomWidth: 1,
        borderColor: Colors.GRAY,
        flexDirection: 'row',
        alignItems: 'center'
    },

    HeaderTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(2.5),
        color: Colors.BLACK06,
        marginLeft: width(5)
    },

    ImageStyle: {
        height: height(20),
        width: width(90),
    },

    Title: {
        fontSize: totalSize(2.2),
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.BLACK06,
        textAlign: 'center',
        marginTop: height(2)
    },

    PlumberDetailTitle: {
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: totalSize(1.5),
        color: Colors.BLACK05,
    },

    PlumberDetailValue: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.5),
        color: '#B7B6B6',
    },

    PlumberDetailView: {
        width: width(80),
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginTop: height(3)
    },

    ServiceDetailStyle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.9),
        color: Colors.BLACK06
    },

    ViewButtonStyle: {
        height: height(5),
        width: width(20),
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: height(0.5)
    },

    ViewText: {
        fontSize: totalSize(1.8),
        color: Colors.BLACK06,
        fontFamily: Fonts.POPPINS_REGULAR,
    },

    StarView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: height(1)
    },

    ConfirmButton: {
        height: height(7),
        width: width(80),
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        borderWidth: 0.5,
        borderColor: Colors.PRIMARY_BORDER_COLOR,
        borderRadius: height(3.5)
    },

    ButtonText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.8),
        color: Colors.BLACK06
    },

    ModelMainContainer: {
        height: height(40),
        width: '100%',
        backgroundColor: Colors.WHITE
    },

    ModelHeader: {
        height: height(8),
        paddingHorizontal: width(5),
        borderBottomWidth: 0.5,
        borderColor: Colors.PRIMARY_BORDER_COLOR,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },

    ModelTitle: {
        fontSize: totalSize(2),
        fontFamily: Fonts.POPPINS_MEDIUM,
        color: Colors.BLACK
    },

    ReviewText: {
        fontFamily: Fonts.POPPINS_LIGHT,
        fontSize: totalSize(1.6),
        color: Colors.BLACK
    },
    radiBtnImgCont: {
        height: 20,
        width: 20,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
        borderRadius: 15,
        borderColor: Colors.BLACK,
        borderWidth: 0.5,
        marginRight: 8
    },
    radioBtnImg: {
        height: 12,
        width: 12,
        backgroundColor: Colors.APP_PRIMARY,
        borderRadius: 10
    },
    textInput: {
        height: 100,
        borderColor: Colors.BLACK,
        borderWidth: 1,
        borderRadius: 8,
        padding: 8,
        marginTop: 12
    },
    moneyTextInput: {
        height: 30,
        borderRadius: 8,
        padding: 8,
        width: 100,
        marginLeft: 12,
        backgroundColor: Colors.GRAY,
    },

    MainWhiteView: {
        paddingBottom: height(2),
        width: '100%',
        backgroundColor: Colors.WHITE
    },
    ModalHeaderView: {
        height: height(7),
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: width(5),
        borderBottomWidth: 0.5,
        borderColor: Colors.PRIMARY_BORDER_COLOR,
        justifyContent: 'space-between'
    },

    HeadingText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.BLACK
    },

    RowView: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    RadioView: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.PRIMARY_BORDER_COLOR,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },

    PaymentNameStyle: {
        fontSize: 16,
        color: Colors.BLACK06,
        fontFamily: Fonts.POPPINS_REGULAR,
        marginLeft: width(5),
        marginBottom: height(1.5)
    },

    EnableRadio: {
        height: 12,
        width: 12,
        borderRadius: 6,
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR
    },

    PayNowButton: {
        height: 50,
        width: width(70),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        borderRadius: 25,
        marginTop: 10
    },

    AdjustmentDateField: {
        height: 40,
        backgroundColor: Colors.INDEX_GREY,
        fontSize: 14,
        color: Colors.BLACK,
        width: 100,
        paddingHorizontal: 10,
        marginTop: 0
    },


    EstimateCostStyle: {
        height: 40,
        width: 100,
        backgroundColor: Colors.CHAT_INPUT_BACKGROUND_COLOR,
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 14,
        color: Colors.BLACK06,
        paddingHorizontal: 10
    }
})