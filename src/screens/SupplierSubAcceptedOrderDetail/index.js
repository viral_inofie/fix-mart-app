//Global imports
import React, { useEffect, useState } from 'react';
import {
    View, Text, TouchableOpacity, SafeAreaView,
    ImageBackground, ScrollView, TextInput, FlatList,
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
import BraintreeDropIn from 'react-native-braintree-dropin-ui';
import stripe from 'tipsi-stripe';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import {
    ACCEPTED_ORDER_DETAIL_SUBPROVIDER,
    BRAINTREE_PAY,
    GET_BRAINTREE_TOKEN,
    PAYNOW_PROVIDER,
    REVIEW_PRODUCT,
    RIGHT_SCROLL_INDICATOR_INSENTS,
    PUBLISHABLE_KEY,
    SECRET_KEY,
    ANDROID_MODE,
    STRIPE_PAY,
    CUSTOMER_PAY_NOW
} from '../../Helper/Constants';
import { CustomerPayNowParam, PayNowProviderParam, ReviewProductParam, SubProviderAcceptedOrderDetailParam } from '../../Api/APIJson';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { AirbnbRating } from 'react-native-ratings';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    HeaderView, HeaderTitle, ImageStyle, Title, PlumberDetailTitle,
    PlumberDetailValue, PlumberDetailView, ServiceDetailStyle,
    ViewButtonStyle, ViewText, StarView, ConfirmButton, ButtonText,
    ModelMainContainer, ModelHeader, ModelTitle, ReviewText,
    radioBtnImg, radiBtnImgCont, textInput, moneyTextInput,
    MainWhiteView,
    ModalHeaderView,
    HeadingText,
    RowView,
    RadioView,
    PaymentNameStyle,
    EnableRadio,
} = Styles;

stripe.setOptions({
    publishableKey: PUBLISHABLE_KEY,
    merchantId: SECRET_KEY,
    androidPayMode: ANDROID_MODE,
})

const SupplierSubAcceptedOrderDetail = ({ navigation: { goBack, addListener, pop }, route: { params } }) => {

    const [PayNowModel, SetPayNowModel] = useState(false)
    const [ReviewModel, SetReviewModel] = useState(false);
    const [PayLaterModel, SetPayLaterModel] = useState(false)
    const [mode, setMode] = useState('date')
    const [Money, SetMoney] = useState('')
    const [NextInvestmentDate, SetNextInvestmentDate] = useState(moment(new Date()).format('YYYY-MM-DD'))
    const [isDateModalVisible, SetisDateModalVisible] = useState(false)
    const [PaymentMode, SetPaymentMode] = useState([
        { key: 4, name: 'Debit Card', isSelect: false },
        { key: 3, name: 'Credit Card', isSelect: false },
        { key: 1, name: 'Paypal', isSelect: false },
        { key: 2, name: 'Paybalance', isSelect: false },
    ])
    const [SelectedpaymentMode, SetSelectedpaymentMode] = useState('');
    const dispatch = useDispatch();

    const [Data, SetData] = useState({
        product_image: '',
        product_name: '',
        scheduleDate: '',
        scheduleTime: '',
        service: '',
        sub_category: '',
        price: '',
        other_details: '',
        remaining_price: '',
        rating: '',
        dueDate: '',
        isProductDelivered: '',
        isProviderPaid: '',
        isProductVerified: ''
    })

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getOrderDetail()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);

    const _handleDatePicked = (val) => {
        console.log(val)
        let selectedDate = moment(val).format('YYYY-MM-DD')
        console.log(selectedDate,'selecDate')
        // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
        SetNextInvestmentDate(selectedDate)
        SetisDateModalVisible(false)
    }

    const _hideDateTimePicker = () => {
        SetisDateModalVisible(false)
    }

    const _closeModal = () => {
        SetPayNowModel(false)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(ACCEPTED_ORDER_DETAIL_SUBPROVIDER, SubProviderAcceptedOrderDetailParam(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetData({
                        ...Data,
                        product_image: res.data.image,
                        product_name: res.data.name,
                        scheduleDate: res.data.visit_date,
                        scheduleTime: res.data.visit_time,
                        service: res.data.service,
                        sub_category: res.data.sub_category,
                        price: res.data.price,
                        other_details: res.data.other_details,
                        remaining_price: res.data.remaning_price,
                        rating: res.data.product_rating,
                        dueDate: res.data.dueDate,
                        isProductDelivered: res.data.isProductDelivered,
                        isProviderPaid: res.data.isProviderPaid,
                        isProductVerified: res.data.isProductVerified
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    const _SelectPaymentMethod = (d) => {
        console.log(d, 'item')
        SetSelectedpaymentMode(d.key);
        let NewArray = PaymentMode.map((item, i) => {
            item.isSelect = false;
            if (item.key === d.key) {
                item.isSelect = true
                return item
            }
            return item
        })
        SetPaymentMode(NewArray)
    }

    const _doPayNowPayment = (token) => {
        SetPayNowModel(false);
        dispatch(updateLoader(true));

        const apiClass = new APICallService(PAYNOW_PROVIDER, PayNowProviderParam(params.order_id, 1, SelectedpaymentMode, token, Data.price, ''));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    getOrderDetail()
                    RNToasty.Success({
                        title: res.message,
                        withIcon: false,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })

    }

    const BrainTreePay = (token) => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(BRAINTREE_PAY, { amount: Data.price, paymentToken: token });

        apiClass.callAPI()
            .then(res => {

                if (validateResponse(res)) {
                    _doPayNowPayment(res.data.txn_id)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const BrainTreePaymentGateway = (Token) => {
        // alert(Token)
        dispatch(updateLoader(true))
        BraintreeDropIn.show({
            clientToken: Token,
            merchantIdentifier: 'tbgv6k8nk99gwhfp',
            // googlePayMerchantId: 'googlePayMerchantId',
            countryCode: 'SG',    //apple pay setting
            currencyCode: 'USD',   //apple pay setting
            merchantName: 'Your Merchant Name for Apple Pay',
            orderTotal: Data.price,
            googlePay: false,
            applePay: false,
            vaultManager: true,
            cardDisabled: false,
            darkTheme: true,
        })
            .then(result => {
                console.log(result, 'result');
                dispatch(updateLoader(false));
                // _doPayNow(result.nonce)
                BrainTreePay(result.nonce)
            })
            .catch((error) => {
                if (error.code === 'USER_CANCELLATION') {
                    dispatch(updateLoader(false))
                    console.log(error)
                    // update your UI to handle cancellation
                } else {
                    console.log(error)
                    dispatch(updateLoader(false))
                    // update your UI to handle other errors
                }
            });
    }

    const _getBraintreeToken = () => {

        SetPayNowModel(false);

        dispatch(updateLoader(true))

        const apiClass = new APICallService(GET_BRAINTREE_TOKEN, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    // _doPayNow(res.data.token);
                    BrainTreePaymentGateway(res.data.token)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }



    const _doReviewProduct = () => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(REVIEW_PRODUCT, ReviewProductParam(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    pop();
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doPayNow = (token) => {
        SetPayNowModel(false)
        console.log('hello')
        dispatch(updateLoader(true));

        const apiClass = new APICallService(PAYNOW_PROVIDER, PayNowProviderParam(params.order_id, 1, 0, token, Data.price, 0));

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const StripePay = (token) => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(STRIPE_PAY, { amount: Data.price, token: token });

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'stripe pay')
                if (validateResponse(res)) {
                    _doPayNow(res.data.txn_id)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const _doPayLater = (token) => {
        SetPayNowModel(false)
        console.log('hello')
        dispatch(updateLoader(true));

        const apiClass = new APICallService(PAYNOW_PROVIDER, PayNowProviderParam(params.order_id, 2, 0, token, Money, NextInvestmentDate));

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const StripePayForPayLater = (token) => {
        SetPayLaterModel(false)

        dispatch(updateLoader(true))
        const apiClass = new APICallService(STRIPE_PAY, { amount: Money, token: token });

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'stripe pay')
                if (validateResponse(res)) {
                    _doPayLater(res.data.txn_id)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }



    const generateStripeToken = async () => {
        const token = await stripe.paymentRequestWithCardForm()
        console.log(token, 'stripe token')
        StripePay(token.tokenId)
    }

    const generateStripeTokenForPayLater = async () => {
        const token = await stripe.paymentRequestWithCardForm()
        console.log(token, 'stripe token')
        StripePayForPayLater(token.tokenId)
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Confirmed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: Data.product_image }}
            imageStyle={{ borderRadius: height(1.5) }}
            style={ImageStyle}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.product_name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(35) }}>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Shedule Date & time :</Text>
            </View>

            <View style={{ width: width(45), }}>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.scheduleDate).format('DD MMM, YYYY') + " " + renderTime(Data.scheduleTime)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Sub Category :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.sub_category}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Price :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.price}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.other_details}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Remaining Price :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.remaining_price}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Product Ratings :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    {renderStarView()}
                </View>
            </View>

            {
                Data.dueDate != ''
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Due Date :</Text>
                        </View>

                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{moment(Data.dueDate).format('DD MMM, YYYY')}</Text>
                        </View>
                    </View>
                    :
                    null
            }
        </View>
    }

    const renderStarView = () => {
        return <View style={[StarView, { marginTop: 0 }]}>
            <AirbnbRating
                selectedColor={Colors.YELLOW}
                count={5}
                showRating={false}
                defaultRating={Data.rating.substring(0, 3)}
                reviewColor={Colors.PRIMARY_DARK}
                size={10}
                isDisabled
                onFinishRating={(val) => { }}
            />
            <Text style={PlumberDetailValue}> / {Data.rating.substring(4, 5)}</Text>
        </View>
    }

    const renderViewButton = () => {
        return <CustomButton
            title='View'
            textStyle={{ fontSize: 12 }}
            textColor={Colors.BLACK}
            buttonAction={() => _openModel()}
            btnStyle={ViewButtonStyle}
        />
    }

    const renderServiceDetail = () => {

        return <View style={PlumberDetailView}>
            {renderServiceHeading()}
        </View>

    }

    const renderPayNowButton = () => {
        if (Data.isProviderPaid) {
            return null
        }
        else {
            return <CustomButton
                buttonAction={() => generateStripeToken()}
                title='Pay Now'
                btnStyle={[ConfirmButton, {
                    backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: 0
                }]}
            />
        }

    }


    const renderPayNowButtonModel = () => {
        return <CustomButton
            buttonAction={() => generateStripeToken()}
            title='Pay Now'
            btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0
            }]}
        />
    }

    const renderPayLaterButton = () => {
        if (Data.isProviderPaid) {
            return null
        }
        else {
            return <CustomButton
                title='Pay Later'
                btnStyle={[ConfirmButton, { marginTop: 10 }]}
                buttonAction={() => SetPayLaterModel(true)}
            />
        }
    }

    const renderCancelButton = () => {
        if (Data.isProductDelivered) {
            if (Data.isProductVerified) {
                return null
            }
            else {
                return <CustomButton
                    buttonAction={() => _doReviewProduct()}
                    title='Review Product'
                    btnStyle={[ConfirmButton, { marginTop: 20 }]}
                />
            }
        }
        else {
            return null
        }

    }

    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>

            {renderPayNowButton()}

            {renderPayLaterButton()}

            {renderCancelButton()}
        </View>
    }

    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <Text style={ModelTitle}>Plumber Reviews</Text>

                        <TouchableOpacity onPress={() => SetReviewModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                    </View>
                </View>
            </View>
        </Modal>
    }

    const renderPayLaterModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={PayLaterModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={ModelTitle}>How much money</Text>
                            <TextInput
                                style={moneyTextInput}
                                value={Money}
                                keyboardType='phone-pad'
                                onChangeText={(text) => SetMoney(text)}
                            />
                        </View>

                        <TouchableOpacity onPress={() => SetPayLaterModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>



                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>Next Installment date</Text>

                        <TouchableOpacity
                            activeOpacity={1}
                            style={{ height: 40, width: 100, backgroundColor: Colors.INDEX_GREY, justifyContent: 'center', alignItems: 'center' }}
                            // onPress={() => SetNextInvestmentDate(true)}
                            onPress={() =>
                                SetisDateModalVisible(!isDateModalVisible)}>
                            <Text>{moment(NextInvestmentDate).format('DD MMM, YYYY')}</Text>
                        </TouchableOpacity>

                        {
                            isDateModalVisible
                            &&
                            (
                                <DateTimePicker
                                    isVisible={isDateModalVisible}
                                
                                    mode='date'
                                
                                    accessibilityValue={new Date()}
                                    onConfirm={(date) => _handleDatePicked(date)}
                                    onCancel={() => _hideDateTimePicker()}
                                />
                            )

                        }

                        <CustomButton
                            title='Submit'
                            btnStyle={[ConfirmButton, {
                                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                                borderWidth: 0,
                                height: 40,
                                marginTop: 15
                            }]}
                            buttonAction={() => generateStripeTokenForPayLater()}
                        />
                    </View>

                </View>
            </View>
        </Modal>
    }


    const renderPayNowModel = () => {
        return <Modal style={{ marginHorizontal: 0 }} isVisible={PayNowModel}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={MainWhiteView}>

                    <View style={ModalHeaderView}>
                        <Text style={HeadingText}>Payment Mode</Text>

                        <TouchableOpacity onPress={() => _closeModal()}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    {renderPaymentModalList()}

                    {renderPayNowButtonModel()}
                </View>
            </View>
        </Modal>
    }


    const renderPaymentModalList = () => {
        return <FlatList
            contentContainerStyle={{ paddingHorizontal: width(5), paddingTop: height(2) }}
            data={PaymentMode}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(data) => _renderListPayment(data)}
        />
    }

    const _renderListPayment = (data) => {
        return <TouchableOpacity
            onPress={() => _SelectPaymentMethod(data.item)}
            style={RowView}>
            <View style={RadioView}>
                {
                    data.item.isSelect
                        ?
                        <View style={EnableRadio}>
                        </View>
                        :
                        null
                }
            </View>

            <Text style={PaymentNameStyle}>{data.item.name}</Text>
        </TouchableOpacity>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                bounces={false}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {renderButtonView()}


            </ScrollView>

            {renderModel()}
            {renderPayLaterModel()}
            {renderPayNowModel()}

        </View>
    )
}

export default SupplierSubAcceptedOrderDetail