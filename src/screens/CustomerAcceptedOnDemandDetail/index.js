//Global imports
import React, { useEffect, useState } from 'react';
import {
  View, Text, TouchableOpacity, SafeAreaView,
  ImageBackground, ScrollView, TextInput, FlatList, Alert
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DateTimePicker from "@react-native-community/datetimepicker"
import moment from 'moment'
import BraintreeDropIn from 'react-native-braintree-dropin-ui';
import stripe from 'tipsi-stripe';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import {
  ACCEPTED_ORDER_DETAIL,
  BRAINTREE_PAY,
  CUSTOMER_ACCEPTED_CANCEL,
  CUSTOMER_COMPLETE_ORDER,
  CUSTOMER_PAY_NOW,
  GET_BRAINTREE_TOKEN,
  RIGHT_SCROLL_INDICATOR_INSENTS,
  PUBLISHABLE_KEY,
  SECRET_KEY,
  ANDROID_MODE,
  STRIPE_PAY
} from '../../Helper/Constants';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import styles from '../ProviderRegistration/styles';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { CustomerAccpetedOrderCancelParam, CustomerCompletedJobParam, CustomerPayNowParam } from '../../Api/APIJson';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';




const {
  HeaderView, HeaderTitle, ImageStyle, Title, PlumberDetailTitle,
  PlumberDetailValue, PlumberDetailView, ServiceDetailStyle,
  ViewButtonStyle, ViewText, StarView, ConfirmButton, ButtonText,
  ModelMainContainer, ModelHeader, ModelTitle, ReviewText,
  radioBtnImg, radiBtnImgCont, textInput, moneyTextInput,
  MainWhiteView,
  ModalHeaderView,
  HeadingText,
  RowView,
  RadioView,
  PaymentNameStyle,
  EnableRadio,
  PayNowButton,
  AdjustmentDateField
} = Styles;

stripe.setOptions({
  publishableKey: PUBLISHABLE_KEY,
  merchantId: SECRET_KEY,
  androidPayMode: ANDROID_MODE,
})

const CustomerAcceptedOnDemandDetail = ({ navigation: { goBack, navigate, addListener, pop }, route: { params } }) => {

  const [PayNowModel, SetPayNowModel] = useState(false)
  const [ReviewModel, SetReviewModel] = useState(false);
  const [PayLaterModel, SetPayLaterModel] = useState(false)
  const [mode, setMode] = useState('date')
  const [DoYouWantVisit, SetDoYouWantVisit] = useState(true)
  const [CommentHere, SetCommentHere] = useState('(Optional) Comment here...')
  const [Money, SetMoney] = useState('')
  const [NextInvestmentDate, SetNextInvestmentDate] = useState(new Date())
  const [isDateModalVisible, SetisDateModalVisible] = useState(false);

  const dispatch = useDispatch();
  const [Data, SetData] = useState({
    image: '',
    name: '',
    service: '',
    type_of_product: '',
    other_detail: '',
    price: '',
    address: '',
    provider_name: '',
    address: '',
    provider_mobile_no: '',
    estimate_cost: '',
    transport_charge: '',
    assessment_charge: '',
    arrival_time: '',
    visit_date: '',
    visit_time: '',
    total_cost: '',
    isCustomerPaid: '',
    job_id: '',
    isCustomerConfirmCompleted: '',
    status: '',
    isProviderCompleted: '',
    isQuatationUpdate: '',
    sub_job_type: ""
  })
  const [WalletType, SetWalletType] = useState('');
  const [BraintreeToken, SetBrantreeToken] = useState('');

  const [PaymentMode, SetPaymentMode] = useState([
    { key: 0, name: 'Debit Card', isSelect: false },
    { key: 1, name: 'Credit Card', isSelect: false },
    { key: 2, name: 'Paypal', isSelect: false },
    { key: 3, name: 'Paybalance', isSelect: false },
  ])

  useEffect(() => {
    dispatch(updateLoader(true));
    addListener('focus', () => {
      getJobDetail();
    })
  }, []);




  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack();

  const _openModel = () => SetReviewModel(true);

  const _handleDatePicked = (val) => {
    let selectedDate = moment(val).format('MMM DD, YYYY')
    // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
    NextInvestmentDate(`${selectedDate}`)
    SetisDateModalVisible(false)
  }

  const _hideDateTimePicker = () => {
    SetisDateModalVisible(false)
  }

  const _closeModal = () => {
    SetPayNowModel(false)
  }

  const _navigateToHelp = () => {
    navigate('ProviderOnDemandHelp')
  }

  /*
     .##........#######...######...####..######...######.
     .##.......##.....##.##....##...##..##....##.##....##
     .##.......##.....##.##.........##..##.......##......
     .##.......##.....##.##...####..##..##........######.
     .##.......##.....##.##....##...##..##.............##
     .##.......##.....##.##....##...##..##....##.##....##
     .########..#######...######...####..######...######.
     */

  const _SelectPaymentMethod = (d) => {
    SetWalletType(d.name === 'Debit Card' ? 4 : d.name === 'Credit Card' ? 3 : d.name === 'Paypal' ? 1 : 2)
    let NewArray = PaymentMode.map((item, i) => {
      item.isSelect = false;
      if (item.key === d.key) {
        item.isSelect = true
        return item
      }
      return item
    })
    SetPaymentMode(NewArray)
  }

  const getJobDetail = () => {
    const apiClass = new APICallService(ACCEPTED_ORDER_DETAIL, { job_id: params.job_id });

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          SetData({
            job_id: res.data.id,
            image: res.data.image.length > 0 ? res.data.image : [],
            name: res.data.category,
            service: res.data.service,
            total_cost: res.data.totalCost,
            type_of_product: res.data.type_product,
            other_detail: res.data.other_details,
            price: res.data.totalCost,
            address: res.data.address,
            estimate_cost: res.data.answer[0].answer,
            transport_charge: res.data.answer[1].answer,
            assessment_charge: res.data.answer[2].answer,
            arrival_time: res.data.answer[3].answer,
            visit_date: res.data.visit_date,
            visit_time: res.data.visit_time,
            provider_mobile_no: res.data.phone,
            provider_name: res.data.service_provicer_firstname + " " + res.data.service_provicer_lastname,
            isCustomerPaid: res.data.isCustomerPaid,
            isCustomerConfirmCompleted: res.data.isCustomerConfirmCompleted,
            status: res.data.status,
            isProviderCompleted: res.data.isProviderCompleted,
            isQuatationUpdate: res.data.isQuatationUpdate,
            sub_job_type: res.data.sub_job_type
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const renderTime = (Time) => {
    let Hours = Time.substring(0, 2);

    let Minutes = Time.substring(3, 5);

    let isAmPm = Hours >= 12 ? 'PM' : 'AM'

    return `${Hours} : ${Minutes} ${isAmPm}`
  }

  const _doCancel = () => {
    dispatch(updateLoader(true))

    const apiClass = new APICallService(CUSTOMER_ACCEPTED_CANCEL, CustomerAccpetedOrderCancelParam(Data.job_id, Data.status, 'confirm'))

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          RNToasty.Success({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
          goBack()
        }
        else {
          RNToasty.Error({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false))
        renderErrorToast('error', err)
      })
  }

  const _doPayNow = (token, type) => {
    SetPayNowModel(false)

    dispatch(updateLoader(true));

    const apiClass = new APICallService(CUSTOMER_PAY_NOW, CustomerPayNowParam(Data.job_id, type === 'transport_charge' ? Data.transport_charge : Data.total_cost, 0, token, type === 'transport_charge' ? { isTransportation: 1 } : {}));

    apiClass.callAPI()
      .then(res => {
        console.log(res, 'res')
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          RNToasty.Success({
            title: res.message,
            fontFamily: Fonts.POPPINS_REGULAR,
            withIcon: false,
            position: 'bottom'
          })
          if (type === 'transport_charge') {
            _doCancel()
          }
          else {
            getJobDetail()
          }
        }
      })
      .catch(err => {
        console.log(err, 'err')
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const _doCompleteOrder = () => {
    dispatch(updateLoader(true));
console.log("CUSTOMER_COMPLETE_ORDER",CUSTOMER_COMPLETE_ORDER)
    const apiClass = new APICallService(CUSTOMER_COMPLETE_ORDER, CustomerCompletedJobParam(Data.job_id, 1));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          RNToasty.Success({
            title: res.message,
            fontFamily: Fonts.POPPINS_REGULAR,
            withIcon: false,
            position: 'bottom'
          })
          pop();
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const _doDisputeOrder = () => {
    dispatch(updateLoader(true));

    const apiClass = new APICallService(CUSTOMER_COMPLETE_ORDER, CustomerCompletedJobParam(params.job_id, 0));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          RNToasty.Success({
            title: res.message,
            fontFamily: Fonts.POPPINS_REGULAR,
            withIcon: false,
            position: 'bottom'
          })
          pop()
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const StripePay = (token, type) => {
    dispatch(updateLoader(true))
    const apiClass = new APICallService(STRIPE_PAY, { amount: Data.total_cost, token: token });

    apiClass.callAPI()
      .then(res => {
        console.log(res, 'stripe pay')
        if (validateResponse(res)) {
          _doPayNow(res.data.txn_id, type)
        }
        else {
          RNToasty.Error({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom'
          })
        }
      })
      .catch(err => {
        renderErrorToast('error', err);
        dispatch(updateLoader(false))
      })
  }

  const BrainTreePaymentGateway = (Token, type) => {
    // alert(Token)
    dispatch(updateLoader(true))
    BraintreeDropIn.show({
      clientToken: Token,
      merchantIdentifier: 'tbgv6k8nk99gwhfp',
      // googlePayMerchantId: 'googlePayMerchantId',
      countryCode: 'SG',    //apple pay setting
      currencyCode: 'USD',   //apple pay setting
      merchantName: 'Your Merchant Name for Apple Pay',
      orderTotal: type === 'transport_charge' ? Data.transport_charge : Data.total_cost,
      googlePay: false,
      applePay: false,
      vaultManager: true,
      cardDisabled: false,
      darkTheme: true,
    })
      .then(result => {
        console.log(result, 'result');
        dispatch(updateLoader(false));
        // _doPayNow(result.nonce)
        BrainTreePay(result.nonce, type)
      })
      .catch((error) => {
        if (error.code === 'USER_CANCELLATION') {
          dispatch(updateLoader(false))
          console.log(error)
          // update your UI to handle cancellation
        } else {
          console.log(error)
          dispatch(updateLoader(false))
          // update your UI to handle other errors
        }
      });
  }

  const _getBraintreeToken = (type) => {

    SetPayNowModel(false);

    dispatch(updateLoader(true))

    const apiClass = new APICallService(GET_BRAINTREE_TOKEN, {});

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          // _doPayNow(res.data.token);
          BrainTreePaymentGateway(res.data.token, type)
        }
        else {
          RNToasty.Error({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR,
            withIcon: false
          })
        }
      })
      .catch(err => {
        renderErrorToast('error', err);
        dispatch(updateLoader(false))
      })
  }

  const generateStripeToken = async (type) => {
    const token = await stripe.paymentRequestWithCardForm()
    console.log(token, 'stripe token')
    StripePay(token.tokenId, type)
  }


  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeader = () => {
    return <CustomHeader
      title={'Confirmed Offers'}
      onLeftClick={() => _navigateToBack()}
    />
  }

  const renderDemandImage = () => {
    return <FlatList
      data={Data.image}
      keyExtractor={(item, index) => index.toString()}
      horizontal
      renderItem={({ item, index }) => {
        return <ImageBackground
          source={{ uri: item.toString() }}
          imageStyle={{ borderRadius: height(1.5) }}
          style={[ImageStyle, { marginRight: width(5) }]}
        />
      }}
    />

  }

  const renderDemandName = () => {
    return <Text style={Title}>{Data.name}</Text>
  }

  const renderPlumberDetailView = () => {
    return <View style={PlumberDetailView}>
      <View style={{ width: width(35) }}>
        <Text style={PlumberDetailTitle}>Plumber Name :</Text>
        <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Shedule Date & time :</Text>
      </View>

      <View style={{ width: width(45), }}>
        <Text style={PlumberDetailValue}>{Data.provider_name}</Text>
        <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_date).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
      </View>
    </View>
  }

  const renderServiceDetailTitle = () => {
    return <View style={[PlumberDetailView, {
      marginTop: height(3),
      height: height(4),
      borderBottomWidth: 1,
      borderColor: '#CCCCCC'
    }]}>
      <Text style={ServiceDetailStyle}>Service Details</Text>
    </View>
  }

  const renderServiceHeading = () => {
    return <View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Service :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.service}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Issue :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.type_of_product}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Other Details :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.other_detail}</Text>
        </View>
      </View>
    </View>
  }

  const renderAdjustDateField = () => {
    return <TextInput style={AdjustmentDateField} />
  }

  const renderServiceDetail = () => {
    return <View style={PlumberDetailView}>
      {renderServiceHeading()}
    </View>
  }

  const renderPayNowButton = () => {
    if (Data.isProviderCompleted) {
      return null
    }
    else {
      if (Data.isCustomerPaid) {
        return null
      }
      else {
        if (Data.isQuatationUpdate) {
          return <CustomButton
            title='Confirm & Pay Now'
            buttonAction={() => generateStripeToken('order_charge')}
            btnStyle={[ConfirmButton, {
              backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
              borderWidth: 0
            }]}
          />
        }
        else {
          return null
        }

      }
    }
  }

  const renderPayModalNowButton = () => {
    return <CustomButton
      title='Confirm & Pay Now'
      buttonAction={() => {
        generateStripeToken('order_charge')
      }}
      // _getBraintreeToken('order_pay')}

      btnStyle={[ConfirmButton, {
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        borderWidth: 0
      }]}
    />
  }

  const renderPayLaterButton = () => {
    if (Data.isProviderCompleted) {
      return <CustomButton
        title='Complete'
        btnStyle={[ConfirmButton, { marginTop: 10 }]}
        buttonAction={() => _doCompleteOrder()}
      />
    }
    else {
      return null
    }
  }

  const renderDisputeButton = () => {
    if (Data.isProviderCompleted) {
      return <CustomButton
        title='Dispute'
        buttonAction={() => _doDisputeOrder()}
        btnStyle={[ConfirmButton, { marginTop: 10 }]}
      />
    }
    else {
      return null
    }
  }

  const renderCancelButton = () => {
    if (Data.isProviderCompleted) {
      return null
    }
    else {
      if (Data.isCustomerPaid) {
        return null
      }
      else {
        return <CustomButton
          title='Cancel'
          buttonAction={() => {
            if (Data.sub_job_type === 3) {
              Alert.alert(
                "Cancel Job",
                "Are you sure you want to cancel the Job?",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => _doCancel() }
                ],
                { cancelable: false }
              );

            }
            else {
              Alert.alert(
                "Cancel Order",
                "You will be charged for provider transportation cost ?",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => generateStripeToken('transport_charge') }
                ],
                { cancelable: false }
              );
            }
          }}
          btnStyle={[ConfirmButton, { marginTop: 10 }]}
        />
      }
    }

  }

  const renderButtonView = () => {
    return <View style={{ marginTop: height(5) }}>
      {renderPayNowButton()}

      {renderPayLaterButton()}

      {renderDisputeButton()}

      {renderCancelButton()}

    </View>
  }

  const renderModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetReviewModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
          </View>
        </View>
      </View>
    </Modal>
  }

  const renderPayLaterModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={PayLaterModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <View style={{ flexDirection: 'row', }}>
              <Text style={ModelTitle}>How much money</Text>
              <TextInput
                style={moneyTextInput}
                value={Money}
                onChangeText={(text) => SetMoney(text)}
              />
            </View>

            <TouchableOpacity onPress={() => SetPayLaterModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>Next Installment date</Text>

            <TouchableOpacity activeOpacity={1} onPress={() => SetNextInvestmentDate(true)}>
              <TextInput
                style={[moneyTextInput, { width: 200 }]}
                value={NextInvestmentDate}
                onChangeText={(text) => SetNextInvestmentDate(text)}
              />
            </TouchableOpacity>

            <CustomButton
              title='Submit'
              btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0,
                height: 40,
                marginTop: 15
              }]}
              buttonAction={() => SetPayLaterModel(false)}
            />
          </View>
        </View>
      </View>
    </Modal>
  }


  const renderPayNowModel = () => {
    return <Modal style={{ marginHorizontal: 0 }} isVisible={PayNowModel}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <View style={MainWhiteView}>

          <View style={ModalHeaderView}>
            <Text style={HeadingText}>Payment Mode</Text>

            <TouchableOpacity onPress={() => _closeModal()}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          {renderPaymentModalList()}

          {renderPayModalNowButton()}
        </View>
      </View>
    </Modal>
  }


  const renderPaymentModalList = () => {
    return <FlatList
      contentContainerStyle={{ paddingHorizontal: width(5), paddingTop: height(2) }}
      data={PaymentMode}
      keyExtractor={(item, index) => index.toString()}
      renderItem={(data) => _renderListPayment(data)}
    />
  }

  // const renderPayNowButton = () => {
  //   return <CustomButton
  //       buttonAction={() => SetPayNowModel(true)}
  //       title='Pay Now'
  //       btnStyle={PayNowButton}
  //   />
  // }

  const _renderListPayment = (data) => {
    return <TouchableOpacity
      onPress={() => _SelectPaymentMethod(data.item)}
      style={RowView}>
      <View style={RadioView}>
        {
          data.item.isSelect
            ?
            <View style={EnableRadio}>
            </View>
            :
            null
        }
      </View>

      <Text style={PaymentNameStyle}>{data.item.name}</Text>
    </TouchableOpacity>
  }

  const renderCustomerDetailTitle = () => {
    return <View style={[PlumberDetailView, {
      marginTop: height(3),
      height: height(4),
      borderBottomWidth: 0.5,
      borderColor: '#CCCCCC'
    }]}>
      <Text style={ServiceDetailStyle}>Customer Details</Text>
    </View>
  }

  const renderCustomerDetailView = () => {
    return <View style={PlumberDetailView}>
      {renderProfileHeading()}

      {renderProfileValue()}
    </View>
  }

  const renderProfileHeading = () => {
    return <View style={{ width: width(35) }}>
      <Text style={PlumberDetailTitle}>Customer Name :</Text>
      <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Address :</Text>
      <Text style={[PlumberDetailTitle, { marginTop: height(3.5) }]}>Mobile Number :</Text>
    </View>
  }


  const renderProfileValue = () => {
    return <View style={{ width: width(45), }}>
      <Text style={PlumberDetailValue}>Aleson</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>25-301, shyam society naranpura ahmedabad</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>+91 9737624720</Text>
      {/* {renderStarView()} */}
    </View>
  }

  const renderPaymentDetailTitle = () => {
    return <View style={[PlumberDetailView, {
      marginTop: height(3),
      height: height(4),
      borderBottomWidth: 0.5,
      borderColor: '#CCCCCC'
    }]}>
      <Text style={ServiceDetailStyle}>Payment Details</Text>
    </View>
  }

  const renderPaymentDetailView = () => {
    return <View style={PlumberDetailView}>
      {renderPaymentsHeading()}

    </View>
  }

  const renderPaymentsHeading = () => {
    return <View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Repair cost :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.estimate_cost}</Text>
        </View>
      </View>

      {/* <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Transport Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
        </View>
      </View> */}

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Transport Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.assessment_charge}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Total amount :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={[PlumberDetailValue, { fontSize: totalSize(1.8), fontFamily: Fonts.POPPINS_BOLD, color:Colors.APP_PRIMARY }]}>$ {Data.total_cost}</Text>
        </View>
      </View>

    </View>
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeader()}

      <ScrollView
        scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
        bounces={false}
        contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
        {renderDemandImage()}

        {renderDemandName()}

        {renderPlumberDetailView()}

        {renderServiceDetailTitle()}

        {renderServiceDetail()}

        {/* {renderCustomerDetailTitle()} */}

        {/* {renderCustomerDetailView()} */}

        {renderPaymentDetailTitle()}

        {renderPaymentDetailView()}

        {renderButtonView()}

        {
          isDateModalVisible
            ?
            <DateTimePicker
              value={NextInvestmentDate}
              mode={mode}
              onConfirm={(val) => _handleDatePicked(val)}
              onCancel={() => _hideDateTimePicker()}
            />
            :
            null
        }

      </ScrollView>

      {renderModel()}
      {renderPayLaterModel()}
      {renderPayNowModel()}

    </View>
  )
}

export default CustomerAcceptedOnDemandDetail