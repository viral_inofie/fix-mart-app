//Global imports
import React from 'react';
import {
    View,
    Text,
    ScrollView,
    ImageBackground,
    FlatList
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';
import GetLocation from 'react-native-get-location';

//File imports
import { CREATE_ORDER, DOMAIN_URL, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import APICallService from '../../Api/APICallService';
import { PostOrderParam } from '../../Api/APIJson';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { validateResponse } from '../../Helper/Helper';
//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import BackArrow from '../../../assets/images/backarrow.png';
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import CustomButton from '../../Components/Button/index';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import Logger from '../../Helper/Logger';

const {
    MainContainer,
    RequestImage,
    ImageView,
    RequestTitle,
    ServiceTitle,
    RequestNameView,
    RequestNameTitle,
    ConfirmButton,
    SheduleViewHeading
} = Styles;

const CustomerRequestDetailConfirm = ({ navigation: { goBack, navigate }, route: { params } }) => {

    console.log(params, 'params')
    const dispatch = useDispatch();

    const { address, sort_address, latitude, longitude } = useSelector(state => ({
        address: state.AuthReducer.address,
        sort_address: state.AuthReducer.short_address,
        latitude: state.AuthReducer.latitude,
        longitude: state.AuthReducer.longitude
    }))



    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _goBack = () => {
        goBack()
    }

    const _navigateToHome = () => {
        _postRequest()
        // navigate('CustomerHome')
    }

    const checkLocationPermission=(callback)=> {
        check(
            Platform.select({
                ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
                android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            }),
        ).then((result) => {
            switch (result) {
                case RESULTS.DENIED:
                    request(
                        Platform.select({
                            ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
                            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                        })
                    ).then((result) => {
                        if (result === RESULTS.GRANTED) {
                            GetLocation.getCurrentPosition({
                                enableHighAccuracy: true,
                                timeout: 10000,
                            }).then(location => {
                                Logger.log("Location::", location);
                                dispatch(updateLoader(false))
                                callback(true, location)
                            }).catch(error => {
                                const { code, message } = error;
                                Logger.log(code, message);
                                callback(false, message)
                                dispatch(updateLoader(false))
                            })
                            
                        } else {
                            callback(false, 'Please give location permission')
                        }
                    });
                    break;
                case RESULTS.GRANTED:
                    GetLocation.getCurrentPosition({
                        enableHighAccuracy: true,
                        timeout: 10000,
                    }).then(location => {
                        Logger.log("Location::", location);
                        dispatch(updateLoader(false))
                        callback(true, location)
                    }).catch(error => {
                        const { code, message } = error;
                        Logger.log(code, message);
                        callback(false, message)
                        dispatch(updateLoader(false))
                    })
                    break;
                case RESULTS.BLOCKED:
                    callback(false, 'Please give location permission')
                    break;
            }
        }).catch((error) => {
            callback(false, 'Unavailable Permission')
        });
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _renderImageList = (data) => {
        return <ImageBackground
            source={{ uri: DOMAIN_URL + data.item.toString() }}
            style={{ height: 100, width: 100, marginRight: 20 }} />
    }

    const _postRequest = () => {

        const { OtherDetails, Quantity, TypeOfProduct, category_id, imagelist, job_type, need_to_done, sub_cat_name, sub_category_id, order_date, order_time, sub_job_type } = params;


        checkLocationPermission((status, message) => {
            if (status === false) {
                alert(message)
                return false;
            }
            
            let param = PostOrderParam(category_id, job_type === 1 ? 0 : sub_category_id, TypeOfProduct, OtherDetails, Quantity, job_type, need_to_done, order_date, order_time, sub_job_type, latitude, longitude, sort_address, address);
            param.latitude=message.latitude;
            param.longitude=message.longitude;

            imagelist.map((item, i) => {
                param[`image[${i}]`] = item.toString()
            })
            console.log("message=======>",param)
            
            dispatch(updateLoader(true))
            const apiClass = new APICallService(CREATE_ORDER, param)

            apiClass.callAPI()
                .then(res => {
                    console.log("Create order", res)
                    dispatch(updateLoader(false))
                    if (validateResponse(res)) {

                        RNToasty.Success({
                            title: res.message,
                            position: 'bottom',
                            fontFamily: Fonts.POPPINS_REGULAR,
                            withIcon: false
                        })
                        navigate('CustomerHome')
                    }
                })
                .catch(err => {

                    console.log(err, 'err========>')
                    dispatch(updateLoader(false))
                    validateResponse('error', err)

                })
        })


    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Confirm & Review'
            LeftIcon={BackArrow}
            onLeftClick={() => _goBack()}
        />
    }

    const renderMainView = () => {
        return <ScrollView
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}>
            <View style={MainContainer}>
                {renderImage()}
                {renderRequestTitle()}
                {renderServiceTitle()}
                {renderRequestName()}
                {renderSheduleView()}
                {renderQty()}
                {renderOtherDetailView()}
                {renderImageView()}
                {renderConfirmButton()}
            </View>
        </ScrollView>
    }

    const renderQty = () => {
        if (params.Quantity != '') {
            return <View>
                <Text style={[SheduleViewHeading, { marginTop: 10 }]}>Quantity</Text>
                <Text style={[ServiceTitle, { marginTop: 10 }]}>{params.Quantity}</Text>
            </View>
        }
        else {
            return null
        }
    }

    const renderImage = () => {
        return <View style={ImageView}>
            <ImageBackground
                resizeMode='contain'
                source={{ uri: params.image }}
                style={RequestImage}
            />
        </View>
    }

    const renderRequestTitle = () => {

        return <Text style={RequestTitle}>{params.name}</Text>

    }

    const renderServiceTitle = () => {

        return <View>
            <Text style={[SheduleViewHeading, { marginTop: 30 }]}>Description</Text>
            <Text style={[ServiceTitle, { marginTop: 10 }]}>{params.TypeOfProduct}</Text>
        </View>
    }

    const renderRequestName = () => {
        if (params.sub_cat_name === 'Select a Sub Category') {
            return null
        }
        else {
            return <View style={RequestNameView}>
                <Text style={SheduleViewHeading}>Sub Category</Text>
                <Text style={RequestNameTitle}>{params.sub_cat_name}</Text>
            </View>
        }
    }

    const renderSheduleView = () => {
        return <View style={[RequestNameView, { marginTop: 10 }]}>
            <Text style={SheduleViewHeading}>Schedule Appointment</Text>
            <Text style={RequestNameTitle}>{params.order_date + " " + params.order_time}</Text>
        </View>
    }

    const renderOtherDetailView = () => {
        return <View style={RequestNameView}>
            <Text style={SheduleViewHeading}>Other Details</Text>
            <Text style={RequestNameTitle}>{params.OtherDetails}</Text>
        </View>
    }

    const renderImageView = () => {
        return <View style={{ marginTop: 10 }}>
            <Text style={SheduleViewHeading}>Attached Images</Text>
            <FlatList
                horizontal
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                contentContainerStyle={{ marginTop: 10 }}
                data={params.imagelist}
                renderItem={(data) => {
                    return _renderImageList(data)
                }}
            />
        </View>
    }

    const renderConfirmButton = () => {
        return <CustomButton
            buttonAction={() => _navigateToHome()}
            btnStyle={ConfirmButton}
            title='Confirm'
            textColor={Colors.BLACK06}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}
            {renderMainView()}
        </View>
    )
}

export default CustomerRequestDetailConfirm;