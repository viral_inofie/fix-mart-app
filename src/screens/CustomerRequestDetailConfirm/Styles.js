//Global imports
import { StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainContainer:{
        padding: 20
    },

    RequestImage: {
        height: 80,
        width: 80,
        alignSelf: 'center'
    },

    ImageView: {
        height: 150,
        width: 150,
        backgroundColor: Colors.SERVICEIMAGE_BACK_COLOR,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },

    RequestTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.BLACK06,
        textAlign: 'center',
        marginTop: 10
    },

    ServiceTitle:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:16,
        color:Colors.BLACK06,
        marginTop:30
    },

    RequestNameView:{
        width:'100%',
        borderBottomWidth:1,
        borderColor:'#DDDFDD',
        paddingVertical:10,
        marginTop:10
    },

    RequestNameTitle:{
        fontSize:16,
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.PLACEHOLDER_COLOR,
        marginTop:10
    },

    ConfirmButton:{
        height:50,
        width:'100%',
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent:'center',
        alignItems: 'center',
        borderRadius:25,
        marginTop:40
    },

    SheduleViewHeading: {
        fontSize: 16,
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.BLACK06
    },
})