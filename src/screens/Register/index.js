//Global imports
import React, { Component, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TextInput
} from 'react-native';
import { height, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { CHECK_EMAIL_PHONE, EMAIL_REGEX, REGISTER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import APICallService from '../../Api/APICallService';
import { checkEmailPhoneParam, RegisterParam } from '../../Api/APIJson';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';



//Components imports
import LOGO from '../../../assets/images/logo.png';
import MsgIcon from '../../../assets/images/msg.png';
import LockIcon from '../../../assets/images/lock.png';
import EyeIcon from '../../../assets/images/closeeye.png';
import UserIcon from '../../../assets/images/user.png';
import PhoneIcon from '../../../assets/images/phone.png';
import Unchecked from '../../../assets/images/unchecked.png';
import TermCheck from '../../../assets/images/termcheck.png';
import CustomInput from '../../Components/TextField/index';
import CustomButton from '../../Components/Button/index';
import ErrorModal from '../../Components/ErrorModal/index';
import CustomTextField from '../../Components/TextField/index'
import OpenEye from '../../../assets/images/eyeopen.png'
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { Linking } from 'react-native';


const {
  ImageLogoStyle,
  InputField,
  LoginButton,
  DontAccountText,
  SignUpText,
  FieldIcon,
  InputFieldStyle,
  TermsText,
  InputFirstFieldStyle,
  TermsAndConditionView
} = Styles;

const Register = ({ navigation: { navigate, goBack }, route: { params } }) => {
  const dispatch = useDispatch();

  const [Data, SetData] = useState({
    FirstName: '',
    LastName: '',
    Email: '',
    Phone: '',
    TermCheck: false,
    Password: '',
    passwordFieldVisible: true
  })
  const [ErrorValidation, SetErrorValidation] = useState({
    Message: '',
    ShowErrorModel: false
  })


  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack();

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */
  const _onChangeTextFirstName = (text) => SetData({ ...Data, FirstName: text })

  const _onChangeTextLastName = (text) => SetData({ ...Data, LastName: text })

  const _emailOnChangeText = (text) => SetData({ ...Data, Email: text });

  const _mobileOnChangeText = (text) => SetData({ ...Data, Phone: text });

  const _doRegisterNewUser = () => {
    if (_Validate()) {
      if (params.type === 'customer') {
        _doRegister()
      }
      else {
        _doCheckEmailPhone()
        // navigate('ProviderRegistration', {
        //   first_name: Data.FirstName,
        //   last_name: Data.LastName,
        //   email: Data.Email,
        //   phone: Data.Phone,
        //   password: Data.Password,
        // })
      }
    }
  }

  const _doCheckEmailPhone = () => {

    dispatch(updateLoader(true));
    const { Email, Phone } = Data;

    const apiClass = new APICallService(CHECK_EMAIL_PHONE, checkEmailPhoneParam(Email, Phone));

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          navigate('ProviderRegistration', {
            first_name: Data.FirstName,
            last_name: Data.LastName,
            email: Data.Email,
            phone: Data.Phone,
            password: Data.Password,
          })
        }
        else {
          RNToasty.Error({
            title: res.message,
            position: 'bottom',
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        renderErrorToast('error', err);
        dispatch(updateLoader(false))
      })

  }

  const _Validate = () => {
    const { FirstName, LastName, Email, Phone, TermCheck, Password } = Data;

    if (FirstName == '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter first name', ShowErrorModel: true })
      return false
    }
    else if (LastName == '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter last name', ShowErrorModel: true })
      return false
    }
    else if (Email == '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter email address', ShowErrorModel: true })
      return false
    }
    else if (!EMAIL_REGEX.test(Email)) {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter valid email address', ShowErrorModel: true })
      return false
    }
    else if (Phone == '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please Enter Phone Number ', ShowErrorModel: true })
      return false
    }

    else if (Password === '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please Enter Password', ShowErrorModel: true })
      return false
    }
    else if (Password.length < 6) {
      SetErrorValidation({ ...ErrorValidation, Message: 'Password should be more than 6 characters', ShowErrorModel: true })
      return false
    }
    else if (!TermCheck) {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please check terms and condition', ShowErrorModel: true })
      return false
    }
    else {
      return true
    }

  }

  const _CloseValidationModel = () => {
    SetErrorValidation({ ...ErrorValidation, ShowErrorModel: false })
  }

  const _CheckCondition = () => SetData({ ...Data, TermCheck: !Data.TermCheck })

  const _doRegister = () => {
    dispatch(updateLoader(true))
    const { FirstName, LastName, Email, Phone, Password } = Data;

    const apiClass = new APICallService(REGISTER, RegisterParam(4, FirstName, LastName, Phone, Email, Password))

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          if (params.type == 'customer') {
            // goBack()
            // navigate('PaymentWebView', {
            //   url: res.url,
            // })
            navigate('Login')
          }
          RNToasty.Success({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false))
        renderErrorToast('error', err)
      })

  }

  const _onChangePasswordText = (text) => SetData({ ...Data, Password: text })

  const _onRightClickEyePress = () => SetData({ ...Data, passwordFieldVisible: !Data.passwordFieldVisible })

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */

  const renderLogoIcon = () => {
    return <ImageBackground
      source={LOGO}
      style={ImageLogoStyle}
    />
  }

  const renderFirstNameField = () => {
    return <CustomInput
      placeholder='First Name'
      leftImg={UserIcon}
      containerView={InputField}
      placeholderTextColor={Colors.PLACEHOLDER_COLOR}
      inputStyle={InputFirstFieldStyle}
      value={Data.FirstName}
      onChangeText={(text) => _onChangeTextFirstName(text)}
    />

  }

  const renderLastNameField = () => {
    return <CustomInput
      placeholder='Last Name'
      leftImg={UserIcon}
      placeholderTextColor={Colors.PLACEHOLDER_COLOR}
      containerView={[InputField, { marginTop: 20 }]}
      inputStyle={InputFirstFieldStyle}
      value={Data.LastName}
      onChangeText={(text) => _onChangeTextLastName(text)}
    />
  }

  const renderEmailField = () => {
    return <CustomInput
      placeholder='Email Address'
      value={Data.Email}
      containerView={[InputField, { marginTop: 20 }]}
      inputStyle={InputFirstFieldStyle}
      leftImg={MsgIcon}
      keyboardType='email-address'
      onChangeText={(text) => _emailOnChangeText(text)}
    />
  }

  const renderMobileField = () => {
    return <CustomInput
      placeholder='Phone Number'
      value={Data.Phone}
      containerView={[InputField, { marginTop: 20 }]}
      inputStyle={InputFirstFieldStyle}
      leftImg={PhoneIcon}
      keyboardType='numeric'
      onChangeText={(text) => _mobileOnChangeText(text)}
    />
  }

  const renderPasswordField = () => {
    return <CustomTextField
      containerView={[InputField, { marginTop: 20 }]}
      leftImg={LockIcon}
      placeholder='Password'
      value={Data.Password}
      secureTextEntry={Data.passwordFieldVisible}
      onChangeText={(text) => _onChangePasswordText(text)}
      rightImg={Data.passwordFieldVisible ? EyeIcon : OpenEye}
      RightImageClick={() => _onRightClickEyePress()}
    />
  }

  const renderCheckTermCondition = () => {
    return <View style={TermsAndConditionView}>
      <TouchableOpacity onPress={() => _CheckCondition()}>
        <ImageBackground
          source={Data.TermCheck ? TermCheck : Unchecked}
          resizeMode='contain'
          style={{ height: height(3), width: width(5) }}
        />
      </TouchableOpacity>

      <Text onPress={async () => {
        let url = "https://fix-mart.com/terms-and-condition";
        const supported = await Linking.canOpenURL(url);
        if (supported) {
          // Opening the link with some app, if the URL scheme is "http" the web link should be opened
          // by some browser in the mobile
          await Linking.openURL(url);
        } else {

        }
      }} style={TermsText}>By continuing you agree to our terms & conditions</Text>
    </View>

  }

  const renderRegisterButton = () => {
    return <CustomButton
      buttonAction={() => _doRegisterNewUser()}
      title={params.type === 'customer' ? 'Register' : 'Next'}
      btnStyle={LoginButton}
      textColor={Colors.BLACK}
    />
  }

  const renderSignInText = () => {
    return <Text style={DontAccountText}>
      Don't have an account? Please,
        <Text onPress={() => _navigateToBack()}
        style={SignUpText}> Sign In</Text>
    </Text>
  }


  const renderErrorModel = () => {
    return <ErrorModal
      message={ErrorValidation.Message}
      visible={ErrorValidation.ShowErrorModel}
      handleBack={() => _CloseValidationModel()}
    />
  }


  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={10}
      style={{ flex: 1 }}
      behavior={Platform.OS === 'ios' ? 'padding' : null}>
      <SafeAreaView

        style={{ flex: 1, backgroundColor: Colors.WHITE }}>
        <View style={{ flex: 1, }}>

          <ScrollView
            bounces={false}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            contentContainerStyle={{ paddingVertical: height(5), paddingHorizontal: width(10) }}>
            {renderLogoIcon()}
            {renderFirstNameField()}
            {renderLastNameField()}
            {renderEmailField()}
            {renderMobileField()}
            {renderPasswordField()}
            {renderCheckTermCondition()}
            {renderRegisterButton()}
            {renderSignInText()}
            {renderErrorModel()}
          </ScrollView>
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>

  )
}

export default Register;