//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, ScrollView, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { ACCEPTED_ORDER_DETAIL, PROVIDER_COMPLETE_ORDER, RIGHT_SCROLL_INDICATOR_INSENTS, UPDATE_ANSWER } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import ErrorModal from '../../Components/ErrorModal';
import { ProviderCompleteOrderParam, UpdateAnswerParam } from '../../Api/APIJson';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { FlatList } from 'react-native';

const {
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ConfirmButton,
    EstimateCostStyle
} = Styles;

const ProviderAcceptedOnDemandDetail = ({ navigation: { goBack, addListener, pop }, route: { params } }) => {

    const dispatch = useDispatch();
    const [Data, SetData] = useState({
        image: '',
        name: '',
        service: '',
        type_of_product: '',
        other_detail: '',
        price: '',
        address: '',
        customer_name: '',
        address: '',
        customer_mobile_no: '',
        estimate_cost: '',
        transport_charge: '',
        assessment_charge: '',
        arrival_time: '',
        visit_date: '',
        visit_time: '',
        total_cost: '',
        isCustomerPaid: '',
        job_id: '',
        isProviderCompleted: '',
        isQuatationUpdate: '',
        address: '',
        phone: ''
    })
    const [ErrorValidation, SetErrorValidation] = useState({
        Message: '',
        ShowErrorModel: false
    })

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getJobDetail();
        })
    }, []);

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getJobDetail = () => {
        const apiClass = new APICallService(ACCEPTED_ORDER_DETAIL, { job_id: params.job_id });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetData({
                        job_id: res.data.id,
                        image: res.data.image.length > 0 ? res.data.image : [],
                        name: res.data.category,
                        service: res.data.service,
                        type_of_product: res.data.type_product,
                        other_detail: res.data.other_details,
                        price: res.data.totalCost,
                        address: res.data.address,
                        estimate_cost: res.data.answer[0].answer,
                        transport_charge: res.data.answer[1].answer,
                        assessment_charge: res.data.answer[2].answer,
                        arrival_time: res.data.answer[3].answer,
                        visit_date: res.data.visit_date,
                        visit_time: res.data.visit_time,
                        customer_mobile_no: res.data.phone,
                        customer_name: res.data.customer_firstname + " " + res.data.customer_lastname,
                        isCustomerPaid: res.data.isCustomerPaid,
                        isProviderCompleted: res.data.isProviderCompleted,
                        isQuatationUpdate: res.data.isQuatationUpdate,
                        address: res.data.address,
                        phone: res.data.phone
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    const _doConfirm = () => {
        if (_validate()) {
            ConfirmJob();
        }
    }

    const _validate = () => {
        const { estimate_cost, transport_charge, assessment_charge } = Data

        if (estimate_cost === '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Estimate cost can\'t be null', ShowErrorModel: true })
            return false
        } else if (transport_charge === '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Transport Charge can\'t be null', ShowErrorModel: true })
            return false
        } else if (assessment_charge === '') {
            SetErrorValidation({ ...ErrorValidation, Message: 'Assessment Charge Charge can\'t be null', ShowErrorModel: true })
            return false
        } else {
            return true
        }
    }

    const _CloseValidationModel = () => {
        SetErrorValidation({ ...ErrorValidation, ShowErrorModel: false })
    }

    const ConfirmJob = () => {
        dispatch(updateLoader(true))
        let answerObj = {
            'answer[0][id]': 1,
            'answer[0][answer]': Data.estimate_cost,
            'answer[1][id]': 2,
            'answer[1][answer]': Data.transport_charge,
            'answer[2][id]': 3,
            'answer[2][answer]': Data.assessment_charge,
            'answer[3][id]': 4,
            'answer[3][answer]': Data.arrival_time,
        }
        const apiClass = new APICallService(UPDATE_ANSWER, UpdateAnswerParam(Data.job_id, answerObj));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    goBack();
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom',
                    })
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom',
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _doCompleteOrder = () => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(PROVIDER_COMPLETE_ORDER, ProviderCompleteOrderParam(Data.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    pop();
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                // renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Confirmed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <FlatList
            data={Data.image}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
                return <ImageBackground
                    source={{ uri: item.toString() }}
                    imageStyle={{ borderRadius: height(1.5) }}
                    style={[ImageStyle, { marginRight: width(5) }]}
                />
            }}
        />

    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(40) }}>
                {/* <Text style={PlumberDetailTitle}>Customer Name :</Text> */}
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(40), }}>
                {/* <Text style={PlumberDetailValue}>Russel</Text> */}
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_date).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 1,
            marginTop: 10.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderPaymentDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 1,
            // marginTop:10.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Payment Details</Text>
        </View>
    }

    const renderOrderHeading = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Issue :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.type_of_product}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.other_detail}</Text>
                </View>
            </View>



            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Address :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.address}</Text>
                </View>
            </View>

        </View>
    }



    const renderOrderDetail = () => {
        return <View style={PlumberDetailView}>
            {renderOrderHeading()}

            {/* {renderOrderValue()} */}

        </View>
    }

    const renderAcceptButton = () => {
        if (Data.isCustomerPaid) {
            if (Data.isProviderCompleted) {
                return null
            }
            else {

                return <CustomButton
                    title='Complete Order'
                    buttonAction={() => _doCompleteOrder()}
                    btnStyle={[ConfirmButton, {
                        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                        borderWidth: 0,
                        marginTop: 10
                    }]}
                />
            }
        }
        else {
            return null
        }

    }

    const renderDeclineButton = () => {
        if (Data.isCustomerPaid) {
            return null
        }
        else {
            if (Data.isQuatationUpdate) {
                return null
            }
            else {
                return <CustomButton
                    title='Confirm'
                    buttonAction={() => _doConfirm()}
                    btnStyle={[ConfirmButton, { marginTop: 20 }]}
                />
            }
        }
    }

    const renderPlaceSupplierButton = () => {
        return <CustomButton
            title='Place Order with Supplier'
            btnStyle={[ConfirmButton, { marginTop: 20 }]}
        />
    }

    const renderButtonView = () => {
        return <View>
            {renderDeclineButton()}
            {/* {renderPlaceSupplierButton()} */}
            {renderAcceptButton()}
        </View>
    }

    const renderPaymentDetail = () => {
        return <View style={PlumberDetailView}>
            {renderPaymentHeading()}

            {/* {renderPaymentValue()} */}

        </View>
    }

    const renderPaymentHeading = () => {
        return <View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Estimate cost :</Text>
                </View>
                <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                    <Text style={{ color: Colors.BLACK06, fontSize: totalSize(2), marginRight: width(2) }}>$</Text>
                    <View style={{ width: width(45) }}>
                        <TextInput
                            editable={Data.isCustomerPaid ? false : true}
                            defaultValue={Data.estimate_cost}
                            onChangeText={(text) => SetData({ ...Data, estimate_cost: text })}
                            keyboardType='numeric'
                            style={EstimateCostStyle}
                        />
                    </View>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Transport Charge :</Text>
                </View>
                <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                    <Text style={{ color: Colors.BLACK06, fontSize: totalSize(2), marginRight: width(2) }}>$</Text>
                    <View style={{ width: width(45) }}>
                        <TextInput
                            editable={Data.isCustomerPaid ? false : true}
                            keyboardType='numeric'
                            onChangeText={(text) => SetData({ ...Data, transport_charge: text })}
                            defaultValue={Data.transport_charge}
                            style={EstimateCostStyle}
                        />
                    </View>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
                </View>
                <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                    <Text style={{ color: Colors.BLACK06, fontSize: totalSize(2), marginRight: width(2) }}>$</Text>
                    <TextInput
                        editable={Data.isCustomerPaid ? false : true}
                        defaultValue={Data.assessment_charge}
                        onChangeText={(text) => SetData({ ...Data, assessment_charge: text })}
                        keyboardType='numeric'
                        style={EstimateCostStyle}
                    />
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Price :</Text>
                </View>
                <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.price}</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
                </View>
                <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.arrival_time}</Text>
            </View>

        </View>
    }

    const renderCustomerDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Customer Details</Text>
        </View>
    }

    const renderCustomerDetailView = () => {
        return <View style={PlumberDetailView}>
            {renderProfileHeading()}

        </View>
    }

    const renderProfileHeading = () => {
        return <View >
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Customer Name :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.customer_name}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Address :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.address}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Mobile Number :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.phone}</Text>
                </View>
            </View>

        </View>
    }




    return (
        <KeyboardAvoidingView
            style={{ flex: 1 }}
            keyboardVerticalOffset={10}
            behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

                {renderHeader()}

                <ScrollView
                    scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                    bounces={false}
                    contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                    {renderDemandImage()}

                    {renderDemandName()}

                    {renderPlumberDetailView()}

                    {renderServiceDetailTitle()}

                    {renderOrderDetail()}

                    {renderCustomerDetailTitle()}

                    {renderCustomerDetailView()}

                    {renderPaymentDetailTitle()}

                    {renderPaymentDetail()}


                    {renderButtonView()}


                </ScrollView>

                <ErrorModal
                    message={ErrorValidation.Message}
                    visible={ErrorValidation.ShowErrorModel}
                    handleBack={() => _CloseValidationModel()}
                />
            </View>
        </KeyboardAvoidingView>
    )
}

export default ProviderAcceptedOnDemandDetail;