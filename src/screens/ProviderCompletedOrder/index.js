//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import CustomHeader from '../../Components/CustomerHeader';
import ProviderCompletedOrderList from '../../Components/ProviderCompletedOrderList';
import { ANIMATION_DURATION, COMPLETED_ORDER_CUSTOMER } from '../../Helper/Constants';
import EmptyDataComponent from '../../Components/EmptyDataComponent';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';


const { TabView, OnDemand, OnDemandText } = Styles;

const ProviderCompletedOrder = ({ navigation: { navigate, addListener } }) => {
  const [DemandList, SetDemadList] = useState([])
  const [OffDemandList, SetOffDemandList] = useState([]);

  const [isOnDemand, SetIsOnDemand] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateLoader(true));

    addListener('focus', () => {
      getCompletedOrder()
    })
  }, [])
  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */
  const _navigateToHome = () => {
    navigate('ProviderJobRequestStack')
  }

  const _navigateToDetail = (type, job_id) => {
    navigate('ProviderCompletedOrderDetail', {
      type: type,
      job_id : job_id
    })
  }

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */
  const getCompletedOrder = () => {
    const apiClass = new APICallService(COMPLETED_ORDER_CUSTOMER, {});

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        
        if (validateResponse(res)) {
          SetOffDemandList(res.data.offDemand)
          SetDemadList(res.data.onDemand)
        }
        else {
          alert('hello')
          RNToasty.Error({
            title: res.message,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
            withIcon: false
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }


  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeaderView = () => {
    return <CustomHeader
      title='Completed Offers'
      onLeftClick={() => _navigateToHome()}
    />
  }

  const renderTabView = () => {
    return <View style={TabView}>
      <TouchableOpacity
        onPress={() => SetIsOnDemand(true)}
        style={[OnDemand, {
          backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
          borderWidth: isOnDemand ? 0 : 1
        }]}>
        <Text style={OnDemandText}>On Demand</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => SetIsOnDemand(false)}
        style={[OnDemand, {
          marginLeft: width(5),
          backgroundColor: !isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
          borderWidth: !isOnDemand ? 0 : 1
        }]}>
        <Text style={OnDemandText}>Off Demand</Text>
      </TouchableOpacity>
    </View>
  }

  const renderItemOrderList = (item, index) => {
    if (isOnDemand) {
      return <ProviderCompletedOrderList
        item={item}
        time={ANIMATION_DURATION * index}
        navigateToDetail={(job_id) => _navigateToDetail('ondemand', job_id)}
      />
    } else {
      return <ProviderCompletedOrderList
        item={item}
        time={ANIMATION_DURATION * index}
        navigateToDetail={(job_id) => _navigateToDetail('offdemand', job_id)}
      />
    }

  }

  const renderOrderList = () => {
    return <FlatList
      ListEmptyComponent={() => {
        return <EmptyDataComponent />
      }}
      showsVerticalScrollIndicator={false}
      bounces={false}
      data={isOnDemand ? DemandList : OffDemandList}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index }) => renderItemOrderList(item, index)}
    />
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeaderView()}

      {renderTabView()}

      {renderOrderList()}

    </View>
  )
}

export default ProviderCompletedOrder