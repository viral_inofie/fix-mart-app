//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import moment from 'moment';
//File imports
import Colors from '../../Helper/Colors';
import { RIGHT_SCROLL_INDICATOR_INSENTS, TRANSACTION_DETAILS } from '../../Helper/Constants';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import Styles from './Styles';
import { useDispatch } from 'react-redux';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    rowView,
    TransactionHeading,
    ValueView
} = Styles;

const CustomerTransactionHistoryDetail = ({ navigation: { goBack }, route: { params } }) => {

    const [TransactionData, SetTransactionData] = useState([])

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true))
        getTransactionDetail();
    }, [])


    /*  
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => {
        goBack()
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getTransactionDetail = () => {
        const apiClass = new APICallService(TRANSACTION_DETAILS, { id: params.id })
        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetTransactionData(res.data)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeaderView = () => {
        return <CustomHeader
            title='Transaction Details'
            onLeftClick={() => _navigateToBack()}
        />
    }


    const renderDetail = () => {
        return <View>
            <View style={rowView}>
                <View style={{ width: '34%', }}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={TransactionHeading}>Receipt :</Text>
                </View>

                <View style={ValueView}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={[TransactionHeading, {
                            color: Colors.PLACEHOLDER_COLOR
                        }]}>{TransactionData.txn_id}</Text>
                </View>
            </View>

            <View style={rowView}>
                <View style={{ width: '34%', }}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={TransactionHeading}>Paid :</Text>
                </View>

                <View style={ValueView}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={[TransactionHeading, {
                            color: Colors.PLACEHOLDER_COLOR
                        }]}>{TransactionData.paid}</Text>
                </View>
            </View>

            <View style={rowView}>
                <View style={{ width: '34%', }}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={TransactionHeading}>Paid To :</Text>
                </View>

                <View style={ValueView}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={[TransactionHeading, {
                            color: Colors.PLACEHOLDER_COLOR
                        }]}>{TransactionData.toUser}</Text>
                </View>
            </View>

            <View style={rowView}>
                <View style={{ width: '34%', }}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={TransactionHeading}>Date :</Text>
                </View>

                <View style={ValueView}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={[TransactionHeading, {
                            color: Colors.PLACEHOLDER_COLOR
                        }]}>{moment(TransactionData.created_at).format('DD MMM, YYYY')}</Text>
                </View>
            </View>

            {
                TransactionData.mode != ""
                    ?
                    <View style={rowView}>
                        <View style={{ width: '34%', }}>
                            <Text
                                numberOfLines={1}
                                ellipsizeMode='tail'
                                style={TransactionHeading}>Mode :</Text>
                        </View>

                        <View style={ValueView}>
                            <Text
                                numberOfLines={1}
                                ellipsizeMode='tail'
                                style={[TransactionHeading, {
                                    color: Colors.PLACEHOLDER_COLOR
                                }]}>{TransactionData.mode}</Text>
                        </View>
                    </View>
                    :
                    null
            }


            <View style={rowView}>
                <View style={{ width: '34%', }}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={TransactionHeading}>Category :</Text>
                </View>

                <View style={ValueView}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={[TransactionHeading, {
                            color: Colors.PLACEHOLDER_COLOR
                        }]}>{TransactionData.category}</Text>
                </View>
            </View>

            {
                TransactionData.sub_category != null
                    ?
                    <View style={rowView}>
                        <View style={{ width: '34%', }}>
                            <Text
                                numberOfLines={1}
                                ellipsizeMode='tail'
                                style={TransactionHeading}>Sub Category :</Text>
                        </View>

                        <View style={ValueView}>
                            <Text
                                numberOfLines={1}
                                ellipsizeMode='tail'
                                style={[TransactionHeading, {
                                    color: Colors.PLACEHOLDER_COLOR
                                }]}>{TransactionData.sub_category}</Text>
                        </View>
                    </View>
                    :
                    null
            }



            <View style={rowView}>
                <View style={{ width: '34%', }}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={TransactionHeading}>Status :</Text>
                </View>

                <View style={ValueView}>
                    <Text
                        numberOfLines={1}
                        ellipsizeMode='tail'
                        style={[TransactionHeading, {
                            color: Colors.PLACEHOLDER_COLOR
                        }]}>{TransactionData.status}</Text>
                </View>
            </View>
        </View>

    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeaderView()}

            {renderDetail()}
        </View>
    )
}

export default CustomerTransactionHistoryDetail;