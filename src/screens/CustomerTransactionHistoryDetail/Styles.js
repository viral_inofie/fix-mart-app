//Global imports
import { Dimensions, StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

const WIDTH = Dimensions.get('window').width;


export default StyleSheet.create({
    rowView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: (WIDTH - 40),

        paddingVertical: 10,
        borderBottomWidth: 0.5,
        alignSelf: 'center',
        borderColor: Colors.PRIMARY_BORDER_COLOR
    },

    TransactionHeading: {
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.BLACK06,
        fontSize: 14
    },

    ValueView: { width: '64%', marginLeft: '1%', },
})