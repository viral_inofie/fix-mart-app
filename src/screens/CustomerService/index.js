//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList } from 'react-native';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, GET_SERVICE } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import DrawerIcon from '../../../assets/images/Menu.png';
import CustomerServiceList from '../../Components/CustomerServiceList';
import Colors from '../../Helper/Colors';
import { RNToasty } from 'react-native-toasty';
import { width } from 'react-native-dimension';
import BackArrow from '../../../assets/images/backarrow.png';

const { MainContainer } = Styles;

const CustomerService = ({ navigation: { navigate, toggleDrawer, goBack }, route: { params } }) => {
    console.log(params, 'service')
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        getService()
    }, [])

    const [ServiceList, SetServiceList] = useState([]);

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _toggleDrawer = () => {
        goBack()
    }

    const _navigateToCategory = (category_id) => {
        if (params.job_type === 1) {
            navigate('CustomerSubCategory', { job_type: params.job_type, category_id: category_id, sub_job_type: 1, address: params.address })
        }
        else {
            navigate('CustomerCategory', { job_type: params.job_type, category_id, address: params.address })
        }
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getService = () => {
        const apiClass = new APICallService(GET_SERVICE, {})

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    SetServiceList(res.data)
                    dispatch(updateLoader(false));
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Select your Desired Service'
            LeftIcon={BackArrow}
            onLeftClick={() => _toggleDrawer()}
        />
    }

    const renderServiceList = () => {
        return <View style={MainContainer}>
            {renderServiceFlatList()}
        </View>
    }

    const renderServiceFlatList = () => {
        return <FlatList
            numColumns={3}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            data={ServiceList}
            contentContainerStyle={{ paddingHorizontal: 10 }}
            renderItem={({ item, index }) => {
                return renderItemService(item, index)
            }}
            keyExtractor={(item, index) => index.toString()}
        />
    }

    const renderItemService = (item, index) => {
        return <CustomerServiceList
            _navigateToCategory={(service_id) => {
                if (service_id === 1) {
                    _navigateToCategory(service_id)
                }
            }}
            item={item}
            time={100 * index}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}
            {renderServiceList()}
        </View>
    )
}

export default CustomerService;