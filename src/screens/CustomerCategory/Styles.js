//Global imports
import { StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';

export default StyleSheet.create({
    MainContainer:{
        flex: 1,
        backgroundColor: Colors.WHITE,
        paddingVertical: 20,
        paddingHorizontal:10
    },

    MainView:{ flex: 1, backgroundColor: Colors.DRAWER_BACKGROUND_COLOR }
})