import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    HeaderView: {
        height: height(7),
        paddingHorizontal: width(5),
        width: '100%',

        borderBottomWidth: 1,
        borderColor: Colors.GRAY,
        flexDirection: 'row',
        alignItems: 'center'
    },

    HeaderTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(2.5),
        color: Colors.BLACK06,
        marginLeft: width(5)
    },

    ImageStyle: {
        height: height(20),
        width: width(90),
    },

    Title: {
        fontSize: totalSize(2.2),
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.BLACK06,
        textAlign:'center',
        marginTop:height(2)
    },

    PlumberDetailTitle:{
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: totalSize(1.5),
        color: Colors.BLACK05,
    },

    PlumberDetailValue:{
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.5),
        color: '#B7B6B6',
    },

    PlumberDetailView:{
        width: width(80),
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginTop:height(3)
    },

    ServiceDetailStyle:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(1.9),
        color:Colors.BLACK06
    },

    ViewButtonStyle:{
        height:height(5),
        width:width(20),
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent:'center',
        alignItems:'center',
        marginTop:height(0.5)
    },

    ViewText:{
        fontSize:totalSize(1.8),
        color:Colors.BLACK06,
        fontFamily:Fonts.POPPINS_REGULAR,
    },

    StarView:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:height(1)
    },

    ConfirmButton:{
        height:height(7),
        width:width(80),
        alignItems:'center',
        alignSelf:'center',
        justifyContent:'center',
        borderWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        borderRadius:height(3.5)
    },

    ButtonText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(1.8),
        color:Colors.BLACK06
    },

    ModelMainContainer:{
        height:height(40),
        width:'100%',
        backgroundColor:Colors.WHITE,
        overflow:'scroll'
    },

    ModelHeader:{
        height:height(8),
        paddingHorizontal:width(5),
        borderBottomWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row'
    },

    ModelTitle:{
        fontSize:totalSize(2),
        fontFamily:Fonts.POPPINS_MEDIUM,
        color:Colors.BLACK
    },

    ReviewText:{
        fontFamily:Fonts.POPPINS_LIGHT,
        fontSize:totalSize(1.6),
        color:Colors.BLACK
    }
})