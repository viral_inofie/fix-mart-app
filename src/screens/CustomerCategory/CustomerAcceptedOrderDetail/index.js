//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, ScrollView, StatusBar, FlatList, Alert } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import BraintreeDropIn from 'react-native-braintree-dropin-ui';
import stripe from 'tipsi-stripe';




//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import {
    ACCEPTED_ORDER_DETAIL,
    BRAINTREE_PAY,
    CUSTOMER_ACCEPTED_CANCEL,
    CUSTOMER_COMPLETE_ORDER,
    CUSTOMER_PAY_NOW,
    GET_BRAINTREE_TOKEN,
    HEIGHT,
    RIGHT_SCROLL_INDICATOR_INSENTS,
    PUBLISHABLE_KEY,
    SECRET_KEY,
    ANDROID_MODE,
    STRIPE_PAY
} from '../../Helper/Constants';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { AcceptedOrderDetailParam, CustomerAccpetedOrderCancelParam, CustomerCompletedJobParam, CustomerPayNowParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { AirbnbRating } from 'react-native-ratings';
import EmptyDataComponent from '../../Components/EmptyDataComponent';
import Fonts from '../../Helper/Fonts';
import { RNToasty } from 'react-native-toasty';

const {
    HeaderView,
    HeaderTitle,
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ViewButtonStyle,
    ViewText,
    StarView,
    ConfirmButton,
    ButtonText,
    ModelMainContainer,
    ModelHeader,
    ModelTitle,
    ReviewText
} = Styles;

stripe.setOptions({
    publishableKey: PUBLISHABLE_KEY,
    merchantId: SECRET_KEY,
    androidPayMode: ANDROID_MODE,
})



const CustomerAcceptedOrderDetail = ({ navigation: { goBack, navigate, addListener }, route: { params } }) => {

    const [ReviewModel, SetReviewModel] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));

        // addListener('focus', () => {
        getOrderDetail();
        // })
    }, [])

    const [Data, SetData] = useState({

        product_image: '',
        product_name: '',
        plumber_name: '',
        scheduledDate: '',
        scheduleTime: '',
        service: '',
        status: '',
        subCategory: '',
        price: '',
        otherDetail: '',
        rating: '',
        review: '',
        isCustomerPaid: '',
        isJobStart: '',
        isProviderCompleted: '',
        isCustomerConfirmCompleted: '',
        repair_cost: '',
        transport_charge: '',
        assessment_charge: '',
        arrival_time: '',
        total_amount: '',
        sub_job_type: '',
        job_id: ''
    })

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);

    const _navigateToReview = () => {
        navigate('CustomerReview')
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(ACCEPTED_ORDER_DETAIL, AcceptedOrderDetailParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    SetData({
                        product_image: res.data.image,
                        product_name: res.data.name,
                        plumber_name: res.data.service_provicer_firstname + " " + res.data.service_provicer_lastname,
                        scheduledDate: res.data.visit_date,
                        scheduleTime: res.data.visit_time,
                        service: res.data.service,
                        status: '',
                        subCategory: res.data.sub_category,
                        price: res.data.price,
                        otherDetail: res.data.description,
                        rating: res.data.rating,
                        review: res.data.reviews,
                        isCustomerPaid: res.data.isCustomerPaid,
                        isJobStart: res.data.isJobStart,
                        isProviderCompleted: res.data.isProviderCompleted,
                        isCustomerConfirmCompleted: res.data.isCustomerConfirmCompleted,
                        repair_cost: res.data.answer.length > 0 ? res.data.answer[0].answer : '',
                        transport_charge: res.data.answer.length > 0 ? res.data.answer[1].answer : '',
                        assessment_charge: res.data.answer.length > 0 ? res.data.answer[2].answer : '',
                        arrival_time: res.data.answer.length > 0 ? res.data.answer[3].answer : '',
                        total_amount: res.data.totalCost,
                        sub_job_type: res.data.sub_job_type,
                        job_id: res.data.id
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    const _doCancelOrder = (job_id, status) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(CUSTOMER_ACCEPTED_CANCEL, CustomerAccpetedOrderCancelParam(job_id, status, 'confirm'))

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _doPayNow = (token, type) => {
        dispatch(updateLoader(true));

        const apiClass = new APICallService(CUSTOMER_PAY_NOW, CustomerPayNowParam(params.job_id, type === 'transport_charge' ? Data.transport_charge : Data.price, 1, token, type === 'transport_charge' ? {} : { isTransportation: 1 }));

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message.toString(),
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    if (type === 'transport_charge') {
                        _doCancelOrder(Data.job_id, Data.status)
                    }
                    else {
                        getOrderDetail()
                    }
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                // renderErrorToast('error', err)
            })
    }


    const BrainTreePay = (token, type) => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(BRAINTREE_PAY, { amount: type === 'transport_charge' ? Data.transport_charge : Data.price, paymentToken: token });

        apiClass.callAPI()
            .then(res => {

                if (validateResponse(res)) {
                    _doPayNow(res.data.txn_id, type)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const BrainTreePaymentGateway = (Token, type) => {
        // alert(Token)
        dispatch(updateLoader(true))
        BraintreeDropIn.show({
            clientToken: Token,
            merchantIdentifier: 'tbgv6k8nk99gwhfp',
            // googlePayMerchantId: 'googlePayMerchantId',
            countryCode: 'SG',    //apple pay setting
            currencyCode: 'USD',   //apple pay setting
            merchantName: 'Your Merchant Name for Apple Pay',
            orderTotal: Data.price,
            googlePay: false,
            applePay: false,
            vaultManager: true,
            cardDisabled: false,
            darkTheme: true,
        })
            .then(result => {
                console.log(result, 'result');
                dispatch(updateLoader(false));
                // _doPayNow(result.nonce)
                BrainTreePay(result.nonce, type)
            })
            .catch((error) => {
                if (error.code === 'USER_CANCELLATION') {
                    dispatch(updateLoader(false))
                    console.log(error)
                    // update your UI to handle cancellation
                } else {
                    console.log(error)
                    dispatch(updateLoader(false))
                    // update your UI to handle other errors
                }
            });
    }

    const _getBraintreeToken = (type) => {

        // SetPayNowModel(false);

        dispatch(updateLoader(true))

        const apiClass = new APICallService(GET_BRAINTREE_TOKEN, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    // _doPayNow(res.data.token);
                    BrainTreePaymentGateway(res.data.token, type)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const _doCompleteOrder = () => {
        dispatch(updateLoader(true));

        const apiClass = new APICallService(CUSTOMER_COMPLETE_ORDER, CustomerCompletedJobParam(params.job_id, 1));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doDisputeOrder = () => {
        dispatch(updateLoader(true));

        const apiClass = new APICallService(CUSTOMER_COMPLETE_ORDER, CustomerCompletedJobParam(params.job_id, 0));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const StripePay = (token, type) => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(STRIPE_PAY, { amount: type === 'transport_charge' ? Data.transport_charge : Data.price, token: token });

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'stripe pay')
                if (validateResponse(res)) {
                    _doPayNow(res.data.txn_id, type)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const generateStripeToken = async (type) => {
        const token = await stripe.paymentRequestWithCardForm()
        console.log(token, 'stripe token')
        StripePay(token.tokenId, type)
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Confirmed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <FlatList
            data={Data.product_image}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
                return <ImageBackground
                    source={{ uri: item.toString() }}
                    imageStyle={{ borderRadius: height(1.5) }}
                    style={[ImageStyle, { marginRight: width(5) }]}
                />
            }}
        />

    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.product_name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(35) }}>
                <Text style={PlumberDetailTitle}>Service provider :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Date & time :</Text>
            </View>

            <View style={{ width: width(45), }}>
                <Text style={PlumberDetailValue}>{Data.plumber_name}</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.scheduledDate).format('DD MMM, YYYY') + " " + renderTime(Data.scheduleTime)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            {
                Data.subCategory != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Sub Category :</Text>
                        </View>
                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{Data.subCategory}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.otherDetail}</Text>
                </View>
            </View>

            {
                Data.sub_job_type === 3
                    ?
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                            <View style={{ width: width(35) }}>
                                <Text style={PlumberDetailTitle}>Product Ratings :</Text>
                            </View>
                            <View style={{ width: width(45) }}>
                                {renderStarView()}
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                            <View style={{ width: width(35) }}>
                                <Text style={PlumberDetailTitle}>Plumber Reviews :</Text>
                            </View>
                            <View style={{ width: width(45) }}>
                                {renderViewButton()}
                            </View>
                        </View>
                    </View>
                    :
                    null
            }

        </View>
    }

    const renderStarView = () => {
        return <View style={[StarView, { marginTop: 0 }]}>
            <AirbnbRating
                selectedColor={Colors.YELLOW}
                count={5}
                showRating={false}
                defaultRating={Data.rating.substring(0, 3)}
                reviewColor={Colors.PRIMARY_DARK}
                size={10}
                isDisabled
                onFinishRating={(val) => { }} />
            <Text style={PlumberDetailValue}> / {Data.rating.substring(3, 5)}</Text>
        </View>
    }

    const renderViewButton = () => {
        return <CustomButton
            title='View'
            textStyle={{ fontSize: 13 }}
            textColor={Colors.BLACK}
            buttonAction={() => _openModel()}
            btnStyle={ViewButtonStyle}
        />
    }

    const renderServiceValue = () => {
        return <View style={{ width: width(45), }}>
            {
                Data.subCategory != null
                    ?
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.subCategory}</Text>
                    :
                    null
            }
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.otherDetail}</Text>
        </View>
    }


    const renderServiceDetail = () => {
        return <View style={PlumberDetailView}>
            {renderServiceHeading()}

            {/* {renderServiceValue()} */}
        </View>
    }

    const renderPayButton = () => {
        if (Data.isCustomerPaid) {
            return null
        }
        else {

            return <CustomButton
                title='Pay Now'
                buttonAction={() => generateStripeToken('order_pay')}
                btnStyle={[ConfirmButton, {
                    backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: 0
                }]}
            />
        }
    }

    const renderReviewButton = () => {
        if (!Data.isProviderCompleted) {
            return null
        }
        else {
            if (Data.isCustomerConfirmCompleted) {
                return null
            }
            else {
                return <CustomButton
                    buttonAction={() => _doCompleteOrder()}
                    title='Complete'
                    btnStyle={[ConfirmButton, { marginTop: 10 }]}
                />
            }
        }
    }

    const renderDisputeButton = () => {
        if (!Data.isProviderCompleted) {
            return null
        }
        else {
            if (Data.isCustomerConfirmCompleted) {
                return null
            }
            else {
                return <CustomButton
                    buttonAction={() => _doDisputeOrder()}
                    title='Dispute'
                    btnStyle={[ConfirmButton, { marginTop: 10 }]}
                />
            }
        }
    }

    const renderCancelButton = () => {
        if (Data.isCustomerPaid) {
            return null
        }
        else {
            return <CustomButton
                buttonAction={() => {
                    if (Data.sub_job_type === 3) {
                        Alert.alert(
                            "Cancel Job",
                            "Are you sure you want to cancel the Job?",
                            [
                                {
                                    text: "Cancel",
                                    onPress: () => console.log("Cancel Pressed"),
                                    style: "cancel"
                                },
                                { text: "OK", onPress: () => _doCancelOrder(Data.job_id, Data.status) }
                            ],
                            { cancelable: false }
                        );
                    }
                    else {
                        Alert.alert(
                            "Cancel Order",
                            "You will be charged for provider transportation cost ?",
                            [
                                {
                                    text: "Cancel",
                                    onPress: () => console.log("Cancel Pressed"),
                                    style: "cancel"
                                },
                                { text: "OK", onPress: () => generateStripeToken('transport_charge') }
                            ],
                            { cancelable: false }
                        )
                    }
                }}
                title='Cancel'
                btnStyle={[ConfirmButton, { marginTop: 10 }]}
            />
        }
    }

    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>
            {renderPayButton()}

            {renderReviewButton()}

            {renderDisputeButton()}

            {renderCancelButton()}

        </View>
    }

    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <Text style={ModelTitle}>Plumber Reviews</Text>

                        <TouchableOpacity onPress={() => SetReviewModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>

                        <FlatList
                            data={Data.review}
                            keyExtractor={(item, index) => index.toString()}
                            ListEmptyComponent={() => {
                                return <View style={{ height: HEIGHT - 650, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: Fonts.POPPINS_REGULAR, color: Colors.LIGHT_GRAY, fontSize: 16 }}>No Review Found</Text>
                                </View>
                            }}
                            renderItem={({ item, index }) => {
                                return <Text style={ReviewText}>{`${index + 1}.${item.comment}`}</Text>
                            }}
                        />
                    </View>

                </View>
            </View>
        </Modal>
    }

    const renderPaymentDetailView = () => {
        if (Data.sub_job_type === 3) {
            return null
        }
        else {
            return <View style={PlumberDetailView}>
                {renderPaymentsHeading()}

            </View>
        }
    }

    const renderPaymentsHeading = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Repair cost :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.repair_cost}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Transport Charge :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.assessment_charge}</Text>
                </View>
            </View>

            {/* <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.arrival_time}</Text>
                </View>
            </View> */}

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Total amount :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.total_amount}</Text>
                </View>
            </View>

        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                bounces={false}
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {renderPaymentDetailView()}

                {renderButtonView()}


            </ScrollView>

            {renderModel()}

        </View>
    )
}

export default CustomerAcceptedOrderDetail;