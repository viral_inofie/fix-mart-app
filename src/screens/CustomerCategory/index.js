//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, FlatList } from 'react-native';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import { GET_CATEGORY, INSTALLATION, REPAIRING, RIGHT_SCROLL_INDICATOR_INSENTS, SUPPLY_AND_INSTALLTION } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';

//Component imports
import DrawerIcon from '../../../assets/images/Menu.png';
import CustomHeader from '../../Components/CustomerHeader/index';
import Colors from '../../Helper/Colors';
import Repairing from '../../../assets/images/repair.png';
import Installation from '../../../assets/images/install.png';
import CustomerCategoryList from '../../Components/CustomerCategoryList';
import SI from '../../../assets/images/si.png';
import BackArrow from '../../../assets/images/backarrow.png';


import { renderErrorToast, validateResponse } from '../../Helper/Helper';

const {
    MainContainer,
    MainView
} = Styles;

const CustomerCategory = ({ navigation: { navigate, toggleDrawer , goBack}, route: { params } }) => {

    const dispatch = useDispatch();

    useEffect(() => {
        // dispatch(updateLoader(true))
        // getCategoryList()
    }, [])

    const [CategoryData, SetCategoryData] = useState([
        { id: REPAIRING, name: 'Repairing', image: Repairing },
        { id: INSTALLATION, name: 'Installation', image: Installation },
        { id: SUPPLY_AND_INSTALLTION, name: 'Supply And Installation', image: SI },
    ])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
   
    const _toggleDrawer = () => {
        goBack()
    }

    const _navigateToSubCategory = (sub_job_type) => {
        navigate('CustomerSubCategory', {
            job_type: params.job_type,
            sub_job_type: sub_job_type,
            // service_id: params.service_id,
            category_id:params.category_id,
            address : params.address
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _renderCategoryList = (item, index) => {
        return <CustomerCategoryList
            item={item}
            time={100 * index}
            _navigateToRequestDetail={(sub_job_type) => _navigateToSubCategory(sub_job_type)}
        />
    }

    const getCategoryList = () => {
        const apiClass = new APICallService(GET_CATEGORY, {})

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    dispatch(updateLoader(false))
                    SetCategoryData(res.data)
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title={'Type of Services'}
            LeftIcon={BackArrow}
            onLeftClick={() => _toggleDrawer()}
        />
    }

    const renderCategoryList = () => {
        return <FlatList
            numColumns={3}
            scrollIndicatorInsets={{ right: 0 }}
            data={CategoryData}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => _renderCategoryList(item, index)}
        />
    }

    return (
        <View style={MainView}>
            {renderHeader()}
            <View style={MainContainer}>
                {renderCategoryList()}
            </View>
        </View>
    )
}

export default CustomerCategory;