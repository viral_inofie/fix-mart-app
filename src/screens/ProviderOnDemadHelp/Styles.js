//Global imports
import { StyleSheet } from 'react-native';
import { height, width } from 'react-native-dimension';



//File imports
import Colors from '../../Helper/Colors';
import { WIDTH } from '../../Helper/Constants';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    PreViewStyle:{
        width: (WIDTH - 10)*0.65,
        paddingVertical:15,
        backgroundColor:Colors.CHAT_INPUT_BACKGROUND_COLOR,
        borderRadius:10,
        justifyContent:'center',
        marginBottom:20,
        paddingHorizontal:15
    },

    ValueText:{
        fontSize:12,
        color:Colors.BLACK06,
        fontFamily:Fonts.POPPINS_REGULAR
    },

    PriceTextField:{
        width: (WIDTH - 10)*0.65,
        height:50,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        borderRadius:10,
        paddingHorizontal:10,
        alignSelf:'flex-end',
        marginBottom:20,
        fontSize:12,
        color:Colors.BLACK06,
        fontFamily:Fonts.POPPINS_REGULAR
    },

    dateField : {
        width: (WIDTH - 10)*0.65,
        height:50,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        borderRadius:10,
        paddingHorizontal:10,
        alignSelf:'flex-end',
        justifyContent:'center',
        // alignItems:'center'
    },

    dateText:{
        fontSize:12,
       
        fontFamily:Fonts.POPPINS_REGULAR
    },

    SubmitBtnStyle:{
        height: 45,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        width:width(70),
        alignSelf:'center',
        borderRadius:30,
        marginTop:20
    },

    SubmitSuccessView:{
        paddingVertical:15,
        width:'100%',
        backgroundColor:'#CBF7CB'
    },

    SubmitSuccessText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:14,
        color:Colors.BLACK06,
        textAlign:'center'
    }
})