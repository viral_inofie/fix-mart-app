//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, TextInput, KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker"
import moment from 'moment';


//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';

//Component imports
import CustomerHeader from '../../Components/CustomerHeader/index';
import { height, width } from 'react-native-dimension';
import { GET_QUESTIONS, RIGHT_SCROLL_INDICATOR_INSENTS, SET_ANSWER } from '../../Helper/Constants';
import CustomButton from '../../Components/Button';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { setAnswer } from '../../Api/APIJson';


const {
    PreViewStyle,
    ValueText,
    PriceTextField,
    SubmitBtnStyle,
    SubmitSuccessView,
    SubmitSuccessText,
    dateField,
    dateText
} = Styles;


const ProviderOnDemandHelp = ({ navigation: { navigate, goBack, addListener }, route: { params } }) => {

    const [HelpList, SetHelpList] = useState([
        { key: '0', value: 'What is cost estimate ?', type: 'pre', price: '' },
        { key: '1', value: '', type: 'select', },
    ])
    const [SubmitView, SetSubmitView] = useState(false);
    const [QuestionArray, SetQuestionArray] = useState([]);
    const [isSubmitShow, SetIsSubmitShow] = useState(false);
    const [isDateModalVisible, SetisDateModalVisible] = useState(false)
    const [currentIndex, setCurrentIndex] = useState([]);
    const dispatch = useDispatch();

    const _navigateToBack = () => {
        goBack()
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _onChangeTextText = (text, key) => {
        let NewArray = HelpList.map((item, i) => {
            if (item.key === key) {
                item.value = text
                return item
            }
            return item
        })
        SetHelpList(NewArray)
    }

    const _AddValueToArray = (index) => {

        setCurrentIndex([...currentIndex, ...[index]]);
        currentIndex.push(index)

        if (index === 1) {
            let NewArray = [
                { key: '2', value: 'What is the transport charge ?', type: 'pre', },
                { key: '3', value: '', type: 'select', },
            ]
            SetHelpList([...HelpList, ...NewArray]);
        }
        else if (index === 3) {
            let NewArray = [
                { key: '4', value: 'What is the assessment charge ?', type: 'pre', },
                { key: '5', value: '', type: 'select', },
            ]
            SetHelpList([...HelpList, ...NewArray]);
        }
        else if (index === 5) {

            let NewArray = [
                { key: '6', value: 'At what time you will arrive ?', type: 'pre', },
                { key: '7', value: '', type: 'date', },
            ]
            SetHelpList([...HelpList, ...NewArray]);
        }
    }

    const SetQuestion = () => {
        dispatch(updateLoader(true));

        let newArray = [];

        HelpList.map((item, i) => {
            let obj = {}
            if ((item.key / 2) > 0 || i === 0) {
                obj['price'] = item.value
            }

            let arr = [obj];
            newArray = [...newArray, ...arr]
        })

        let finalArray = [
            { id: 1, que: HelpList[0].value, price: newArray[1].price },
            { id: 2, que: HelpList[2].value, price: newArray[3].price },
            { id: 3, que: HelpList[4].value, price: newArray[5].price },
            { id: 4, que: HelpList[6].value, price: newArray[7].price }
        ]

        let answersObj = {}

        finalArray.map((item, i) => {
            answersObj[`answers[${i}][id]`] = item.id,
                answersObj[`answers[${i}][answer]`] = item.price
        })
        console.log(answersObj, 'answersObj')

        const apiClass = new APICallService(SET_ANSWER, setAnswer(params.job_id, answersObj))

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetSubmitView(true);
                    setTimeout(() => {
                        navigate('ProviderJobRequest')
                    }, 1000)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })

    }


    const _handleDatePicked = (val) => {
        let selectedDate = moment(val).format('HH:mm A');

        const newArr = HelpList.map((item, i) => {
            if (item.key === '7') {
                item.value = selectedDate
                return item
            }
            return item
        })

        SetHelpList(newArr);
        _hideDateTimePicker();
        SetIsSubmitShow(true)

    }


    const _hideDateTimePicker = () => {
        SetisDateModalVisible(false)
    }

    const _validateIndex = (index) => {
        let valid = true;

        currentIndex.map((item,i) => {
            if(item === index){
                valid = false;
            }
            else{
                valid = true
            }
        })

        return valid
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomerHeader
            title={'Help'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const _renderHelpListItem = (item, index) => {
        if (item.type === 'pre') {
            return <View style={PreViewStyle}>
                <Text style={ValueText}>{item.value}</Text>
            </View>
        }
        else if (item.type === 'date') {
            return <TouchableOpacity onPress={() => SetisDateModalVisible(true)} style={dateField}>
                <Text style={[dateText, { color: item.value === '' ? Colors.PLACEHOLDER_COLOR : Colors.BLACK06, }]}>{item.value === '' ? 'Select Time' : item.value}</Text>
            </TouchableOpacity>
        }
        else {
            return <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-end' }}>
                <Text style={{ fontSize: 25, marginBottom: height(2), marginRight: width(2) }}>$</Text>
                <TextInput
                    onSubmitEditing={() => {
                        if (_validateIndex(index)) {
                            _AddValueToArray(index)
                        }
                    }}
                    style={PriceTextField}
                    keyboardType='number-pad'
                    returnKeyType='done'
                    value={item.value}
                    onChangeText={(text) => _onChangeTextText(text, item.key)}
                />
            </View>
        }
    }

    const renderHelpList = () => {
        return <FlatList
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            data={HelpList}
            contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: 20 }}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => _renderHelpListItem(item, index)}
        />
    }

    const renderSubmitButton = () => {
        if (SubmitView) {
            return <View style={SubmitSuccessView}>
                <Text style={SubmitSuccessText}>Thank you,</Text>
                <Text style={SubmitSuccessText}>We will notify you on confirmation</Text>
            </View>
        }
        else {
            if (isSubmitShow) {
                return <CustomButton
                    buttonAction={() => SetQuestion(true)}
                    title='Submit'
                    btnStyle={SubmitBtnStyle}
                />
            }
            else {
                null
            }

        }

    }



    return (

        <KeyboardAvoidingView
            keyboardVerticalOffset={10}
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}
            <ScrollView scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}>
                {renderHelpList()}
            </ScrollView>
            {renderSubmitButton()}

            <DateTimePicker
                isVisible={isDateModalVisible}
                value={new Date()}
                mode={'time'}
                
                onConfirm={(date) => _handleDatePicked(date)}
                onCancel={() => _hideDateTimePicker()}
            />
        </KeyboardAvoidingView>
    )
}

export default ProviderOnDemandHelp;