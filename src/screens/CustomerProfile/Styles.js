//Global imports
import { StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    ProfileImage: {
        height: 120,
        width: 120,
        alignSelf: 'center'
    },

    ProfileNameStyle:{
        fontSize:16,
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.BLACK,
        textAlign:'center',
        marginTop:10
    },

    DetailView:{
        height:45,
        width:'100%',
        borderBottomWidth:1,
        borderColor:Colors.INDEX_GREY,
        justifyContent:'center',
        marginTop:20
    },

    DetailText:{
        fontSize:14,
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.PLACEHOLDER_COLOR
    }
})