//Global imports
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    ImageBackground
} from 'react-native';
import { useDispatch } from 'react-redux';

//File imports
import Colors from '../../Helper/Colors';
import { RIGHT_SCROLL_INDICATOR_INSENTS, USER_PROFILE } from '../../Helper/Constants';
import Styles from './Styles';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';


//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import ProfileIcon from '../../../assets/images/profile.png';

const {
    ProfileImage,
    ProfileNameStyle,
    DetailView,
    DetailText
} = Styles;

const CustomerProfile = ({ navigation: { toggleDrawer, navigate, addListener }, route: { params: { user_type } } }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getProfileData()
        })
    }, [])

    const [profileData, SetProfileData] = useState({
        user_type:user_type,
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        address: '',
        profile_image: ''
    });
    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    console.log(user_type,'user_type')

    const _navigateToBack = () => {
        if (user_type === 'supplier') {
            navigate('SupplierHomeStack');
            return true;
        }
        if (user_type === 'provider') {
            navigate('ProviderJobRequestStack')
        }
        else {
            navigate('Home')
        }
    }

    const _navigateToEditProfile = () => {
        navigate('CustomerEditProfile', { profileData })
    }

    /*
    .##........#######...######...####..######.
    .##.......##.....##.##....##...##..##....##
    .##.......##.....##.##.........##..##......
    .##.......##.....##.##...####..##..##......
    .##.......##.....##.##....##...##..##......
    .##.......##.....##.##....##...##..##....##
    .########..#######...######...####..######.
    */

    const getProfileData = () => {

        const apiClass = new APICallService(USER_PROFILE, {})

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {

                    dispatch(updateLoader(false));
                    SetProfileData({
                        ...profileData,
                        firstName: res.data.firstname,
                        lastName: res.data.lastname,
                        phone: res.data.phone,
                        email: res.data.email,
                        address: res.data.address,
                        profile_image: res.data.profile_image
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeaderView = () => {
        return <CustomHeader
            title={'My Profile'}
            isRight
            onRightClick={() => _navigateToEditProfile()}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDetailView = () => {
        return <ScrollView
            bounces={false}
            contentContainerStyle={{ padding: 20 }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}>
            {renderProfileImage()}
            {renderProfileName()}
            {renderFirstName()}
            {renderLastName()}
            {renderEmail()}
            {renderPhone()}
            {/* {renderAddress()} */}
        </ScrollView>
    }

    const renderProfileImage = () => {
        return <ImageBackground
            source={{ uri: profileData.profile_image }}
            style={ProfileImage}
            imageStyle={{ borderRadius: 60 }}
        />
    }

    const renderProfileName = () => {
        return <Text style={ProfileNameStyle}>{profileData.firstName + ' ' + profileData.lastName}</Text>
    }

    const renderFirstName = () => {
        return <View style={[DetailView, { marginTop: 30 }]}>
            <Text style={DetailText}>{profileData.firstName}</Text>
        </View>
    }

    const renderLastName = () => {
        return <View style={DetailView}>
            <Text style={DetailText}>{profileData.lastName}</Text>
        </View>
    }

    const renderEmail = () => {
        return <View style={DetailView}>
            <Text style={DetailText}>{profileData.email}</Text>
        </View>
    }

    const renderPhone = () => {
        return <View style={DetailView}>
            <Text style={DetailText}>{profileData.phone}</Text>
        </View>
    }

    const renderAddress = () => {
        return <View style={DetailView}>
            <Text style={DetailText}>{profileData.address}</Text>
        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeaderView()}
            {renderDetailView()}
        </View>
    )
}

export default CustomerProfile;