//Global imports
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    ScrollView,
    ImageBackground,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { height, width, totalSize } from 'react-native-dimension';
import moment from "moment";

//File imports
import { CUSTOMER_MY_REQUEST_DETAIL, PENDING_ORDER_DECLINE, PROVIDER_DECLINE_REQUEST, RIGHT_SCROLL_INDICATOR_INSENTS, SUPPLIER_DECLINED_ORDER } from '../../Helper/Constants';


//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import BackArrow from '../../../assets/images/backarrow.png';
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import Category1 from '../../../assets/images/subcategory5.png';
import Installation from '../../../assets/images/installation.png';
import CustomButton from '../../Components/Button/index';
import ProfileIcon from '../../../assets/images/profile.png';
import AttachImage from '../../../assets/images/attach_image.png';
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { JobDetailParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

const {
    MainContainer,
    RequestImage,
    ImageView,
    RequestTitle,
    ServiceTitle,
    RequestNameView,
    RequestNameTitle,
    ConfirmButton,
    PlumberDetailView,
    PlumberDetailValue,
    PlumberDetailTitle,
    ServiceDetailStyle,
    renderDetailView,
    SheduleViewHeading,
    BottomView,
    ProductText,
    ButtonView,
    OnDemandText,
    OnDemand
} = Styles;

const options = {
    title: 'Select Avatar',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const ProvideJobRequestDetail = ({ navigation: { goBack, navigate, addListener }, route: { params } }) => {

    const [Data, SetData] = useState({
        job_id: '',
        customer_name: '',
        service: '',
        sub_category: '',
        other_detail: '',
        quantity: '',
        AttachmentImage: '',
        address: '',
        customer_image: '',
        sub_job_type: '',
        type_product: '',
        visit_date: "",
        visit_time: ""
    })

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getJobDetail();
        })
    }, [])


    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _goBack = () => {
        goBack()
    }

    const _navigateToHome = () => {
        navigate('CustomerHome')
    }

    const _navigaToProviderHelp = () => {
        navigate('ProviderOnDemandHelp', {
            job_id: Data.job_id
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _renderImageList = (data) => {
        return <ImageBackground
            source={Installation}
            style={{ height: 100, width: 100, marginRight: 20 }} />
    }

    const _renderAttachedImage = (data) => {
        return <ImageBackground
            source={{ uri: data.item }}
            style={{ height: 100, width: 100, marginRight: 20 }}
        />
    }

    const getJobDetail = () => {
        const apiClass = new APICallService(CUSTOMER_MY_REQUEST_DETAIL, JobDetailParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    SetData({
                        ...Data,
                        job_id: res.data.id,
                        customer_name: res.data.customerFirstname + " " + res.data.customerLastname,
                        service: res.data.service,
                        sub_category: res.data.sub_category,
                        other_detail: res.data.other_details,
                        quantity: res.data.quantity,
                        address: res.data.sort_address,
                        AttachmentImage: res.data.image,
                        customer_image: res.data.customerProfile,
                        sub_job_type: res.data.sub_job_type,
                        type_product: res.data.type_product,
                        visit_date: res.data.visit_date,
                        visit_time: res.data.visit_time
                    })
                    dispatch(updateLoader(false))

                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doDeclinJob = () => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(PROVIDER_DECLINE_REQUEST, { job_id: Data.job_id });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    goBack()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='New Job Request'
            LeftIcon={BackArrow}
            onLeftClick={() => _goBack()}
        />
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(35) }}>
                <Text style={PlumberDetailTitle}>Customer Name :</Text>
            </View>

            <View style={{ width: width(45), }}>
                <Text style={PlumberDetailValue}>{Data.customer_name}</Text>
            </View>
        </View>
    }

    const renderServiceHeading = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            {
                Data.sub_category != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                        <View style={{ width: width(35) }}>
                            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text>
                        </View>

                        <View style={{ width: width(45) }}>
                            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.sub_category}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_date).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Description :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.type_product}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Address :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.address}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.other_detail}</Text>
                </View>
            </View>

        </View>
    }

    const renderServiceValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>{Data.service}</Text>
            {
                Data.sub_category != null
                    ?
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.sub_category}</Text>
                    :
                    null
            }
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.type_product}</Text>

            {/* <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.quantity}</Text> */}
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderAttachImageView = () => {
        return <View style={{ paddingHorizontal: width(5), marginTop: 20 }}>
            {renderAttachImageList()}
        </View>
    }

    const renderAttachImageList = () => {
        return <FlatList
            data={Data.AttachmentImage}
            horizontal
            scrollIndicatorInsets={{ bottom: 0 }}
            contentContainerStyle={{ marginTop: 20 }}
            renderItem={(data) => _renderAttachedImage(data)}
        // ListFooterComponent={() => _renderAttachImageFooter()}
        />
    }

    const rendeBottomView = () => {
        if (params.type === 'ondemand') {
            return null
        }
        else {
            if (Data.sub_job_type === 3) {
                return <TouchableOpacity
                    onPress={() => navigate('ProviderListOfProducts', {
                        job_id: params.job_id,
                    })}
                    style={BottomView}>
                    <Text style={ProductText}>SEARCH FOR PRODUCTS</Text>
                </TouchableOpacity>
            }
            else {
                return null
            }

        }
    }

    const renderButtonView = () => {
        if (params.type === 'ondemand') {
            return <View style={{ marginTop: 40 }}>
                <CustomButton
                    title='Accept'
                    textStyle={OnDemandText}
                    buttonAction={() => _navigaToProviderHelp()}
                    btnStyle={OnDemand}
                />

                <CustomButton
                    title='Decline'
                    buttonAction={() => _doDeclinJob()}
                    textStyle={OnDemandText}
                    btnStyle={OnDemand}
                />
            </View>
        }
        else {
            if (Data.sub_job_type === 3) {
                return null
            }
            else {
                return <View style={{ marginTop: 20 }}>
                    <CustomButton
                        title='Accept'
                        textStyle={OnDemandText}
                        buttonAction={() => _navigaToProviderHelp()}
                        btnStyle={[OnDemand, , { backgroundColor: Colors.APP_PRIMARY, borderWidth: 0 }]}
                    />

                    <CustomButton
                        title='Decline'
                        buttonAction={() => _doDeclinJob()}
                        textStyle={OnDemandText}
                        btnStyle={OnDemand}
                    />
                </View>
            }
        }
    }

    const renderMainView = () => {
        return <ScrollView
            contentContainerStyle={{ padding: 20, paddingBottom: 60 }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}>

            {renderImage()}
            {/* {renderRequestTitle()} */}
            {renderPlumberDetailView()}
            {renderServiceDetailTitle()}

            <View style={renderDetailView}>
                {renderServiceHeading()}
                {/* {renderServiceValue()} */}
            </View>
            {renderAttachImageView()}
            {renderButtonView()}
        </ScrollView>
    }

    const renderImage = () => {
        return <ImageBackground
            imageStyle={{ borderRadius: 50 }}
            source={{ uri: Data.customer_image }}
            style={RequestImage}
        />
    }

    const renderRequestTitle = () => {
        return <Text style={RequestTitle}>John Demo</Text>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}

            {renderMainView()}

            {rendeBottomView()}
        </View>
    )
}

export default ProvideJobRequestDetail;