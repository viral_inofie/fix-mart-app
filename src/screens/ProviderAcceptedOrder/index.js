//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { ACCEPTED_ORDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions'
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import CustomHeader from '../../Components/CustomerHeader';
import ProviderAcceptedOrderList from '../../Components/ProviderCompletedOrderList';
import ProviderAcceptedOnDemandList from '../../Components/ProviderAcceptedOnDemandList';
import EmptyDataComponent from '../../Components/EmptyDataComponent';


const { TabView, OnDemand, OnDemandText } = Styles;

const ProviderAcceptedOrder = ({ navigation: { navigate, addListener } }) => {
  const [DemandList, SetDemadList] = useState([]);

  const [isOnDemand, SetIsOnDemand] = useState(true);
  const [OffDemandList, SetOffDemandList] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateLoader(true))
    addListener('focus', () => {
      getAcceptedOrder()
    })
  }, [])
  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */
  const _navigateToHome = () => {
    navigate('ProviderJobRequestStack')
  }

  const _navigateToDetail = (job_id) => {
    navigate('ProviderAcceptedOrderDetail', { job_id })
  }

  const _navigateToOnDemandDetail = (job_id) => {
    navigate('ProviderAcceptedOnDemandDetail', { job_id })
  }


  const getAcceptedOrder = () => {
    const apiClass = new APICallService(ACCEPTED_ORDER, {});

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));

        if (validateResponse(res)) {
          SetOffDemandList(res.data.offDemand);
          SetDemadList(res.data.onDemand)
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }


  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeaderView = () => {
    return <CustomHeader
      title='Confirmed Offers'
      onLeftClick={() => _navigateToHome()}
    />
  }

  const renderTabView = () => {
    return <View style={TabView}>
      <TouchableOpacity
        onPress={() => SetIsOnDemand(true)}
        style={[OnDemand, {
          backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
          borderWidth: isOnDemand ? 0 : 1
        }]}>
        <Text style={OnDemandText}>On Demand</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => SetIsOnDemand(false)}
        style={[OnDemand, {
          marginLeft: width(5),
          backgroundColor: !isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
          borderWidth: !isOnDemand ? 0 : 1
        }]}>
        <Text style={OnDemandText}>Off Demand</Text>
      </TouchableOpacity>
    </View>
  }

  const renderItemOrderList = (item, index) => {
    if (isOnDemand) {
      return <ProviderAcceptedOnDemandList
        item={item}
        time={100 * index}
        navigateToDetail={(job_id) => _navigateToOnDemandDetail(job_id)}
      />
    }
    else {
      return <ProviderAcceptedOrderList
        item={item}
        time={100 * index}
        navigateToDetail={(job_id) => _navigateToDetail(job_id)}
      />
    }
  }

  const renderOrderList = () => {
    return <FlatList
      ListEmptyComponent={() => {
        return <EmptyDataComponent />
      }}
      scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
      bounces={false}
      data={isOnDemand ? DemandList : OffDemandList}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index }) => renderItemOrderList(item, index)}
    />
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeaderView()}

      {renderTabView()}

      {renderOrderList()}

    </View>
  )
}

export default ProviderAcceptedOrder