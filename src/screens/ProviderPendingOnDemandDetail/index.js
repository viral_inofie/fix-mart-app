//Global imports
import React, { useEffect, useState } from 'react';
import {
  View, Text, TouchableOpacity, SafeAreaView,
  ImageBackground, ScrollView, TextInput, FlatList
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DateTimePicker from "@react-native-community/datetimepicker"
import moment from 'moment'

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { CUSTOMER_ONDEMAND_JOB_DETAILS, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import styles from '../ProviderRegistration/styles';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import Fonts from '../../Helper/Fonts';

const {
  HeaderView, HeaderTitle, ImageStyle, Title, PlumberDetailTitle,
  PlumberDetailValue, PlumberDetailView, ServiceDetailStyle,
  ViewButtonStyle, ViewText, StarView, ConfirmButton, ButtonText,
  ModelMainContainer, ModelHeader, ModelTitle, ReviewText,
  radioBtnImg, radiBtnImgCont, textInput, moneyTextInput,
  MainWhiteView,
  ModalHeaderView,
  HeadingText,
  RowView,
  RadioView,
  PaymentNameStyle,
  EnableRadio,
  PayNowButton,
  AdjustmentDateField
} = Styles;

const ProviderPendingOnDemandDetail = ({ navigation: { goBack, navigate, addListener }, route: { params } }) => {

  const [PayNowModel, SetPayNowModel] = useState(false)
  const [ReviewModel, SetReviewModel] = useState(false);
  const [PayLaterModel, SetPayLaterModel] = useState(false)
  const [mode, setMode] = useState('date')
  const [DoYouWantVisit, SetDoYouWantVisit] = useState(true)
  const [CommentHere, SetCommentHere] = useState('(Optional) Comment here...')
  const [Money, SetMoney] = useState('')
  const [NextInvestmentDate, SetNextInvestmentDate] = useState(new Date())
  const [isDateModalVisible, SetisDateModalVisible] = useState(false)
  const dispatch = useDispatch();
  const [Data, SetData] = useState({
    image: '',
    name: '',
    service: '',
    other_detail: '',
    price: '',
    address: '',
    customer_name: '',
    visit_date: '',
    visit_time: '',
    total_cost: '',
    job_id: '',
    cost_estimate: "",
    transport_charge: '',
    assesment_charge: '',
    arrival_time: ''
  })


  const [PaymentMode, SetPaymentMode] = useState([
    { key: 0, name: 'Debit Card', isSelect: false },
    { key: 1, name: 'Credit Card', isSelect: false },
    { key: 2, name: 'Paypal', isSelect: false },
    { key: 3, name: 'Paybalance', isSelect: false },

  ])


  useEffect(() => {
    dispatch(updateLoader(true));
    addListener('focus', () => {
      getJobDetail();
    })
  }, []);

  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack();

  const _openModel = () => SetReviewModel(true);

  const _handleDatePicked = (val) => {
    let selectedDate = moment(val).format('MMM DD, YYYY')
    // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
    NextInvestmentDate(`${selectedDate}`)
    SetisDateModalVisible(false)
  }

  const _hideDateTimePicker = () => {
    SetisDateModalVisible(false)
  }

  const _closeModal = () => {
    SetPayNowModel(false)
  }

  const _navigateToHelp = () => {
    navigate('ProviderOnDemandHelp')
  }

  /*
     .##........#######...######...####..######...######.
     .##.......##.....##.##....##...##..##....##.##....##
     .##.......##.....##.##.........##..##.......##......
     .##.......##.....##.##...####..##..##........######.
     .##.......##.....##.##....##...##..##.............##
     .##.......##.....##.##....##...##..##....##.##....##
     .########..#######...######...####..######...######.
     */

  const _SelectPaymentMethod = (d) => {
    console.log(d, 'item')
    let NewArray = PaymentMode.map((item, i) => {
      item.isSelect = false;
      if (item.key === d.key) {
        item.isSelect = true
        return item
      }
      return item
    })
    SetPaymentMode(NewArray)
  }


  const getJobDetail = () => {
    const apiClass = new APICallService(CUSTOMER_ONDEMAND_JOB_DETAILS, { pending_job_id: params.job_id });

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          SetData({
            job_id: res.data.id,
            image: res.data.image,
            name: res.data.category,
            service: res.data.service,
            other_detail: res.data.description,
            price: res.data.price,
            visit_date: res.data.visit_date,
            visit_time: res.data.visit_time,
            customer_name: res.data.customer_firstname + " " + res.data.customer_lastname,
            total_cost: res.data.totalCost,
            cost_estimate: res.data.answer.length > 0 ? res.data.answer[0].answer : '',
            transport_charge: res.data.answer.length > 0 ? res.data.answer[1].answer : '',
            assesment_charge: res.data.answer.length > 0 ? res.data.answer[2].answer : '',
            arrival_time: res.data.answer.length > 0 ? res.data.answer[3].answer : '',
          })
        }
      })
      .catch(err => {
        console.log(err, 'errr')
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const renderTime = (Time) => {
    let Hours = Time.substring(0, 2);

    let Minutes = Time.substring(3, 5);

    let isAmPm = Hours >= 12 ? 'PM' : 'AM'

    return `${Hours} : ${Minutes} ${isAmPm}`
  }

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeader = () => {
    return <CustomHeader
      title={'Sent Offers'}
      onLeftClick={() => _navigateToBack()}
    />
  }

  const renderDemandImage = () => {
    return <ImageBackground
      source={{ uri: Data.image }}
      imageStyle={{ borderRadius: height(1.5) }}
      style={ImageStyle}
    />
  }

  const renderDemandName = () => {
    return <Text style={Title}>{Data.name}</Text>
  }

  const renderPlumberDetailView = () => {
    return <View style={PlumberDetailView}>
      <View style={{ width: width(35) }}>
        <Text style={PlumberDetailTitle}>Customer Name :</Text>
        <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Shedule Date & time :</Text>
      </View>

      <View style={{ width: width(45), }}>
        <Text style={PlumberDetailValue}>{Data.customer_name}</Text>
        <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_date).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
      </View>
    </View>
  }

  const renderServiceDetailTitle = () => {
    return <View style={[PlumberDetailView, {
      marginTop: height(3),
      height: height(4),
      borderBottomWidth: 0.5,
      borderColor: '#CCCCCC'
    }]}>
      <Text style={ServiceDetailStyle}>Service Details</Text>
    </View>
  }

  const renderServiceHeading = () => {
    return <View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Service :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.service}</Text>
        </View>
      </View>


      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Other Details :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.other_detail}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Estimated cost :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.cost_estimate}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Transportation Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.assesment_charge}</Text>
        </View>
      </View>

      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.arrival_time}</Text>
        </View>
      </View>


      <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Price :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.total_cost}</Text>
        </View>
      </View>

    </View>
  }


  const renderServiceDetail = () => {
    return <View style={PlumberDetailView}>
      {renderServiceHeading()}

    </View>
  }

  const renderPayNowButton = () => {
    return <CustomButton
      title='Accept'
      buttonAction={() => _navigateToHelp()}
      btnStyle={[ConfirmButton, {
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        borderWidth: 0
      }]}
    />
  }

  const renderPayLaterButton = () => {
    return <CustomButton
      title='Decline'
      btnStyle={[ConfirmButton, { marginTop: 10 }]}
      buttonAction={() => SetPayLaterModel(true)}
    />
  }


  const renderButtonView = () => {
    return <View style={{ marginTop: height(5) }}>
      {renderPayNowButton()}

      {renderPayLaterButton()}

      {/* {renderCancelButton()} */}
    </View>
  }

  const renderModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetReviewModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
            <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
          </View>
        </View>
      </View>
    </Modal>
  }

  const renderPayLaterModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={PayLaterModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <View style={{ flexDirection: 'row', }}>
              <Text style={ModelTitle}>How much money</Text>
              <TextInput
                style={moneyTextInput}
                value={Money}
                onChangeText={(text) => SetMoney(text)}
              />
            </View>

            <TouchableOpacity onPress={() => SetPayLaterModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>Next Installment date</Text>

            <TouchableOpacity activeOpacity={1} onPress={() => SetNextInvestmentDate(true)}>
              <TextInput
                style={[moneyTextInput, { width: 200 }]}
                value={NextInvestmentDate}
                onChangeText={(text) => SetNextInvestmentDate(text)}
              />
            </TouchableOpacity>

            <CustomButton
              title='Submit'
              btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0,
                height: 40,
                marginTop: 15
              }]}
              buttonAction={() => SetPayLaterModel(false)}
            />
          </View>
        </View>
      </View>
    </Modal>
  }


  const renderPayNowModel = () => {
    return <Modal style={{ marginHorizontal: 0 }} isVisible={PayNowModel}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <View style={MainWhiteView}>

          <View style={ModalHeaderView}>
            <Text style={HeadingText}>Payment Mode</Text>

            <TouchableOpacity onPress={() => _closeModal()}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          {renderPaymentModalList()}

          {renderPayNowButton()}
        </View>
      </View>
    </Modal>
  }


  const renderPaymentModalList = () => {
    return <FlatList
      contentContainerStyle={{ paddingHorizontal: width(5), paddingTop: height(2) }}
      data={PaymentMode}
      keyExtractor={(item, index) => index.toString()}
      renderItem={(data) => _renderListPayment(data)}
    />
  }

  // const renderPayNowButton = () => {
  //   return <CustomButton
  //       buttonAction={() => SetPayNowModel(true)}
  //       title='Pay Now'
  //       btnStyle={PayNowButton}
  //   />
  // }

  const _renderListPayment = (data) => {
    return <TouchableOpacity
      onPress={() => _SelectPaymentMethod(data.item)}
      style={RowView}>
      <View style={RadioView}>
        {
          data.item.isSelect
            ?
            <View style={EnableRadio}>
            </View>
            :
            null
        }
      </View>

      <Text style={PaymentNameStyle}>{data.item.name}</Text>
    </TouchableOpacity>
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeader()}

      <ScrollView
        scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
        bounces={false}
        contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
        {renderDemandImage()}

        {renderDemandName()}

        {renderPlumberDetailView()}

        {renderServiceDetailTitle()}

        {renderServiceDetail()}

        {/* {renderButtonView()} */}

        {
          isDateModalVisible
            ?
            <DateTimePicker
              value={NextInvestmentDate}
              mode={mode}
              onConfirm={(val) => _handleDatePicked(val)}
              onCancel={() => _hideDateTimePicker()}
            />
            :
            null
        }

      </ScrollView>

      {renderModel()}
      {renderPayLaterModel()}
      {renderPayNowModel()}

    </View>
  )
}

export default ProviderPendingOnDemandDetail