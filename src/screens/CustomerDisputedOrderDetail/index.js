//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, ScrollView, StatusBar } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, COMPLETED_ORDER_DETAIL, CUSTOMER_COMPLETE_ORDER } from '../../Helper/Constants';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { CustomerCompletedJobParam } from '../../Api/APIJson';

const {
    HeaderView,
    HeaderTitle,
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ViewButtonStyle,
    ViewText,
    StarView,
    ConfirmButton,
    ButtonText,
    ModelMainContainer,
    ModelHeader,
    ModelTitle,
    ReviewText
} = Styles;

const CustomerDisputeOrderDetail = ({ navigation: { goBack, navigate, addListener }, route: { params } }) => {

    const [ReviewModel, SetReviewModel] = useState(false);

    const dispatch = useDispatch();

    const [Data, setData] = useState({
        image: '',
        name: '',
        plumber_name: '',
        visit_data: '',
        visit_time: '',
        service: '',
        subCategory: '',
        price: '',
        other_details: '',
        estimate_cost: '',
        transport_cost: '',
        assesment_cost: '',
        arrival_time: '',
        sub_job_type: '',
        isDisputeNoteSubmited: ""
    })

    useEffect(() => {
        dispatch(updateLoader(true))
        addListener('focus', () => {
            getJobDetails();
        })
    }, []);

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);

    const _navigateToNote = () => {
        navigate('CustomerSubmitNoteDispute', { job_id: params.job_id })
    }

    const _navigateToReview = () => {
        navigate('CustomerReview')
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getJobDetails = () => {
        const apiClass = new APICallService(COMPLETED_ORDER_DETAIL, { job_id: params.job_id });

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    setData({
                        ...Data,
                        image: res.data.single_image,
                        name: res.data.name,
                        plumber_name: res.data.service_provicer_firstname + " " + res.data.service_provicer_lastname,
                        visit_data: res.data.visit_date,
                        visit_time: res.data.visit_time,
                        service: res.data.service,
                        subCategory: res.data.sub_category,
                        price: res.data.totalCost,
                        other_details: res.data.other_details,
                        estimate_cost: res.data.answer.length > 0 ? res.data.answer[0].answer : '',
                        transport_cost: res.data.answer.length > 0 ? res.data.answer[1].answer : '',
                        assesment_cost: res.data.answer.length > 0 ? res.data.answer[2].answer : '',
                        arrival_time: res.data.answer.length > 0 ? res.data.answer[3].answer : '',
                        sub_job_type: res.data.sub_job_type,
                        isDisputeNoteSubmited: res.data.isDisputeNoteSubmited
                    })
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }




    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Disputed Orders'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: Data.image }}
            imageStyle={{ borderRadius: height(1.5) }}
            style={ImageStyle}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(35) }}>
                <Text style={PlumberDetailTitle}>Plumber Name :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Date & time :</Text>
            </View>

            <View style={{ width: width(45), }}>
                <Text style={PlumberDetailValue}>{Data.plumber_name}</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.visit_data).format('DD MMM, YYYY') + " " + renderTime(Data.visit_time)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View style={{ width: width(35) }}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            {
                Data.subCategory != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Sub Category :</Text>
                        </View>
                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{Data.subCategory}</Text>
                        </View>
                    </View>
                    :
                    null
            }



            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.other_details}</Text>
                </View>
            </View>

            {
                Data.sub_job_type === 3
                    ?
                    null
                    :
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                            <View style={{ width: width(35) }}>
                                <Text style={PlumberDetailTitle}>Estimate Cost</Text>
                            </View>
                            <View style={{ width: width(45) }}>
                                <Text style={PlumberDetailValue}>$ {Data.estimate_cost}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                            <View style={{ width: width(35) }}>
                                <Text style={PlumberDetailTitle}>Transport Charge :</Text>
                            </View>
                            <View style={{ width: width(45) }}>
                                <Text style={PlumberDetailValue}>$ {Data.transport_cost}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                            <View style={{ width: width(35) }}>
                                <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
                            </View>
                            <View style={{ width: width(45) }}>
                                <Text style={PlumberDetailValue}>$ {Data.assesment_cost}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                            <View style={{ width: width(35) }}>
                                <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
                            </View>
                            <View style={{ width: width(45) }}>
                                <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.arrival_time}</Text>
                            </View>
                        </View>
                    </View>
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Price :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.price}</Text>
                </View>
            </View>

        </View>
    }

    const renderStarView = () => {
        return <View style={StarView}>
            <Foundation name='star' size={totalSize(2)} color={Colors.INDEX_GREY} />
            <Foundation name='star' size={totalSize(2)} color={Colors.INDEX_GREY} style={{ marginLeft: width(1) }} />
            <Foundation name='star' size={totalSize(2)} color={Colors.INDEX_GREY} style={{ marginLeft: width(1) }} />
            <Foundation name='star' size={totalSize(2)} color={Colors.INDEX_GREY} style={{ marginLeft: width(1) }} />
            <Foundation name='star' size={totalSize(2)} color={Colors.INDEX_GREY} style={{ marginLeft: width(1) }} />
            <Text style={PlumberDetailValue}> / 3.8</Text>
        </View>
    }

    const renderViewButton = () => {
        return <CustomButton
            title='View'
            textStyle={{ fontSize: 13 }}
            textColor={Colors.BLACK}
            buttonAction={() => _openModel()}
            btnStyle={ViewButtonStyle}
        />
    }


    const renderServiceDetail = () => {
        return <View style={PlumberDetailView}>
            {renderServiceHeading()}
        </View>
    }

    const renderPayButton = () => {
        if (Data.isDisputeNoteSubmited) {
            return null
        }
        else {
            return <CustomButton
                buttonAction={() => _navigateToNote()}
                title='Submit Note'
                btnStyle={[ConfirmButton, {
                    backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: 0
                }]}
            />
        }
    }

    const renderReviewButton = () => {
        return <CustomButton
            buttonAction={() => _navigateToReview()}
            title='Complete'
            btnStyle={[ConfirmButton, { marginTop: 10 }]}
        />
    }

    const renderDisputeButton = () => {
        return <CustomButton
            buttonAction={() => _navigateToReview()}
            title='Dispute'
            btnStyle={[ConfirmButton, { marginTop: 10 }]}
        />
    }



    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>
            {renderPayButton()}

            {/* {renderReviewButton()} */}

            {/* {renderDisputeButton()} */}
        </View>
    }

    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <Text style={ModelTitle}>Plumber Reviews</Text>

                        <TouchableOpacity onPress={() => SetReviewModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                    </View>
                </View>
            </View>
        </Modal>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                bounces={false}
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {renderButtonView()}


            </ScrollView>

            {renderModel()}

        </View>
    )
}

export default CustomerDisputeOrderDetail;