//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { GET_PRODUCT, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import DemandsList from '../../Components/DemandList';
import CustomHeader from '../../Components/CustomerHeader';
import ListOfProductList from '../../Components/ListOfProductList';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { getProduct } from 'react-native-device-info';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { JobDetailParam } from '../../Api/APIJson';
import EmptyDataComponent from '../../Components/EmptyDataComponent';


const { TabView, OnDemand, OnDemandText } = Styles;

const ProviderListOfProducts = ({ navigation: { navigate, goBack }, route: { params } }) => {
    const [DemandList, SetDemadList] = useState([])

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        getProduct();
    }, [])


    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToBack = () => {
        goBack()
    }

    const _navigateToDetail = (product_id, product_name) => {
        navigate('ProvideProductDetail', {
            product_id: product_id,
            job_id: params.job_id,
            product_name: product_name
        })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getProduct = () => {
        const apiClass = new APICallService(GET_PRODUCT, JobDetailParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetDemadList(res.data)
                }

            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='List of Products'
            onLeftClick={() => _navigateToBack()}
        />
    }


    const renderItemDemandList = (item, index) => {
        return <ListOfProductList
            item={item}
            time={100 * index}
            navigateToDetail={(product_id, product_name) => _navigateToDetail(product_id, product_name)}
        />
    }

    const renderDemandList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={DemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}


            {renderDemandList()}

        </View>
    )
}

export default ProviderListOfProducts;