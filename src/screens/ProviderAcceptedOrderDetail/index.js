//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, ScrollView, TextInput, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import {
    ACCEPTED_ORDER_DETAIL,
    PLACE_ORDER_WITH_SUPPLIER,
    PROVIDER_COMPLETE_ORDER,
    RIGHT_SCROLL_INDICATOR_INSENTS,
    START_SERVICE,
    UPDATE_ANSWER
} from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { AcceptedOrderDetailParam, PlaceOrderWithSupplierParam, ProviderCompleteOrderParam, StartServiceParam, UpdateAnswerParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ConfirmButton,
    EstimateCostStyle
} = Styles;

const ProviderAcceptedOrderDetail = ({ navigation: { goBack, addListener, pop }, route: { params } }) => {
    const dispatch = useDispatch();

    const [Data, SetData] = useState({
        job_id: '',
        product_image: '',
        product_name: '',
        customer_name: '',
        scheduledDate: '',
        scheduleTime: '',
        service: '',
        subCategory: '',
        typeOfProduct: '',
        qty: '',
        price: '',
        otherDetail: '',
        address: '',
        isCustomerPaid: '',
        isJobStart: '',
        isProviderCompleted: '',
        isProductOrdered: '',
        isProductVerified: '',
        estimate_cost: '',
        transport_charge: '',
        assessment_charge: '',
        arrival_time: '',
        sub_job_type: '',
        isQuatationUpdate: ''

    })

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getOrderDetail()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(ACCEPTED_ORDER_DETAIL, AcceptedOrderDetailParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetData({
                        job_id: res.data.id,
                        product_image: res.data.image,
                        product_name: res.data.name,
                        customer_name: res.data.customer_firstname + " " + res.data.customer_lastname,
                        scheduledDate: res.data.visit_date,
                        scheduleTime: res.data.visit_time,
                        service: res.data.service,
                        subCategory: res.data.sub_category,
                        typeOfProduct: res.data.type_product,
                        qty: res.data.quantity,
                        price: res.data.price,
                        otherDetail: res.data.description,
                        address: res.data.address,
                        isCustomerPaid: res.data.isCustomerPaid,
                        isJobStart: res.data.isJobStart,
                        isProviderCompleted: res.data.isProviderCompleted,
                        isProductOrdered: res.data.isProductOrdered,
                        isProductVerified: res.data.isProductVerified,
                        estimate_cost: res.data.answer.length > 0 ? res.data.answer[0].answer : '',
                        transport_charge: res.data.answer.length > 0 ? res.data.answer[1].answer : '',
                        assessment_charge: res.data.answer.length > 0 ? res.data.answer[2].answer : '',
                        arrival_time: res.data.answer.length > 0 ? res.data.answer[3].answer : '',
                        sub_job_type: res.data.sub_job_type,
                        isQuatationUpdate: res.data.isQuatationUpdate
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err)
                dispatch(updateLoader(false))
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    const _doOrderToSupplier = () => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(PLACE_ORDER_WITH_SUPPLIER, PlaceOrderWithSupplierParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doJobStart = () => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(START_SERVICE, StartServiceParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    getOrderDetail()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _doCompleteOrder = () => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(PROVIDER_COMPLETE_ORDER, ProviderCompleteOrderParam(params.job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    pop();
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const submitAnswer = () => {
        dispatch(updateLoader(true))
        let answerObj = {
            'answer[0][id]': 1,
            'answer[0][answer]': Data.estimate_cost,
            'answer[1][id]': 2,
            'answer[1][answer]': Data.transport_charge,
            'answer[2][id]': 3,
            'answer[2][answer]': Data.assessment_charge,
            'answer[3][id]': 4,
            'answer[3][answer]': Data.arrival_time,
        }
        const apiClass = new APICallService(UPDATE_ANSWER, UpdateAnswerParam(Data.job_id, answerObj));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    goBack();
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom',
                    })
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom',
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Confirmed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <FlatList
            data={Data.product_image}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
                return <ImageBackground
                    source={{ uri: item.toString() }}
                    imageStyle={{ borderRadius: height(1.5) }}
                    style={[ImageStyle, { marginRight: width(5) }]}
                />
            }}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.product_name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(40) }}>
                <Text style={PlumberDetailTitle}>Customer Name :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(40), }}>
                <Text style={PlumberDetailValue}>{Data.customer_name}</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.scheduledDate).format('DD MMM, YYYY') + " " + renderTime(Data.scheduleTime)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 1,
            marginTop: 10.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderPaymentDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0,
            marginTop: 10.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Payment Details</Text>
        </View>
    }

    const renderOrderHeading = () => {
        return <View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            {
                Data.subCategory != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Sub Category :</Text>
                        </View>

                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{Data.subCategory}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            {/* <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Type Of Product :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.typeOfProduct}</Text>
                </View>
            </View> */}

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.otherDetail}</Text>
                </View>
            </View>

            {
                Data.sub_job_type === 3
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Quantity :</Text>
                        </View>

                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{Data.qty}</Text>
                        </View>
                    </View>
                    :
                    null
            }



            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Address :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.address}</Text>
                </View>
            </View>

        </View>
    }

    const renderOrderValue = () => {
        return <View>
            {
                Data.subCategory != null
                    ?
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.subCategory}</Text>
                    :
                    null
            }
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.typeOfProduct}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.otherDetail}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.qty}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.address}</Text>
        </View>
    }


    const renderOrderDetail = () => {
        return <View style={PlumberDetailView}>
            {renderOrderHeading()}

            {/* {renderOrderValue()} */}

        </View>
    }

    const renderAcceptButton = () => {
        if (!Data.isJobStart) {
            return null
        }
        else {
            if (Data.isProviderCompleted) {
                return null
            }
            else {
                return <CustomButton
                    title='Complete Order'
                    buttonAction={() => _doCompleteOrder()}
                    btnStyle={[ConfirmButton, {
                        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                        borderWidth: 0,
                        marginTop: 10
                    }]}
                />
            }
        }
    }

    const renderDeclineButton = () => {
        if (!Data.isProductVerified) {
            return null
        }
        else {

            if (!Data.isJobStart) {
                return <CustomButton
                    buttonAction={() => _doJobStart()}
                    title='Start Service'
                    btnStyle={[ConfirmButton, { marginTop: 20 }]}
                />

            }
            else {
                return null
            }


        }
    }

    const renderPlaceSupplierButton = () => {
        if (!Data.isCustomerPaid) {
            return null
        }
        else {
            if (Data.isProductOrdered) {
                return null
            }
            else {
                return <CustomButton
                    buttonAction={() => _doOrderToSupplier()}
                    title='Place Order with Supplier'
                    btnStyle={[ConfirmButton, { marginTop: 20 }]}
                />
            }

        }
    }

    const renderButtonView = () => {
        if (Data.sub_job_type === 3) {
            return <View style={{ marginTop: 20 }}>
                {renderDeclineButton()}
                {renderPlaceSupplierButton()}
                {renderAcceptButton()}
            </View>
        }
        else {
            return null
        }

    }

    const renderPaymentDetail = () => {
        return <View style={PlumberDetailView}>
            {renderPaymentHeading()}

            {renderPaymentValue()}

        </View>
    }

    const renderPaymentHeading = () => {
        return <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Price :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Status :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Paid by Plumber :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Remaining :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Due Date :</Text>
        </View>
    }

    const renderPaymentValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>$ 500</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>Pending</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>John Deo</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ 300</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>12th October, 2020</Text>

        </View>
    }

    const renderAmountDetail = () => {
        if (Data.sub_job_type === 3) {
            return null
        }
        else {
            return <View style={PlumberDetailView}>
                {renderPaymentHeadingView()}

                {/* {renderPaymentValueView()} */}

            </View>
        }
    }

    const renderPaymentHeadingView = () => {
        return <View style={{ width: width(35) }}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Estimate cost :</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ marginRight: width(2), fontSize: totalSize(2), color: Colors.BLACK06 }}>$</Text>
                    <View style={{ width: width(45) }}>
                        <TextInput
                            editable={Data.isCustomerPaid ? false : true}
                            defaultValue={Data.estimate_cost}
                            onChangeText={(text) => SetData({ ...Data, estimate_cost: text })}
                            keyboardType='numeric'
                            style={EstimateCostStyle}
                        />
                    </View>
                </View>

            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(5) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Transport Charge :</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ marginRight: width(2), fontSize: totalSize(2), color: Colors.BLACK06 }}>$</Text>
                    <View style={{ width: width(45) }}>
                        <TextInput
                            editable={Data.isCustomerPaid ? false : true}
                            keyboardType='numeric'
                            onChangeText={(text) => SetData({ ...Data, transport_charge: text })}
                            defaultValue={Data.transport_charge}
                            style={EstimateCostStyle}
                        />
                    </View>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(5) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Assessment Charge :</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ marginRight: width(2), fontSize: totalSize(2), color: Colors.BLACK06 }}>$</Text>
                    <View style={{ width: width(45) }}>
                        <TextInput
                            editable={Data.isCustomerPaid ? false : true}
                            defaultValue={Data.assessment_charge}
                            onChangeText={(text) => SetData({ ...Data, assessment_charge: text })}
                            keyboardType='numeric'
                            style={EstimateCostStyle}
                        />
                    </View>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Price :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>$ {Data.price}</Text>
                </View>
            </View>

            {/* <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>New Arrival Time :</Text>
                </View>
                <View style={{ width: width(45) }}>
                    <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.arrival_time}</Text>
                </View>
            </View> */}


        </View>
    }

    const renderPaymentValueView = () => {
        return <View>
            <View style={{ width: width(45), }}>









            </View>


        </View>
    }

    const renderSubmitButton = () => {
        if (Data.sub_job_type === 3) {
            return null
        }
        else {
            if (Data.isCustomerPaid) {
                return null
            }
            else {
                if (Data.isQuatationUpdate) {
                    return null
                }
                else {
                    return <CustomButton
                        buttonAction={() => submitAnswer()}
                        title='Confirm'
                        btnStyle={[ConfirmButton, { marginTop: 20, backgroundColor: Colors.APP_PRIMARY, borderWidth: 0 }]}
                    />
                }
            }
        }
    }

    const renderCompleteButton = () => {
        if (Data.sub_job_type === 3) {
            return null
        }
        else {
            if (Data.isCustomerPaid) {
                if (Data.isProviderCompleted) {
                    return null
                }
                else {
                    return <CustomButton
                        buttonAction={() => _doCompleteOrder()}
                        title='Complete Order'
                        btnStyle={[ConfirmButton, { marginTop: 20 }]}
                    />
                }
            }
            else {
                return null
            }
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                bounces={false}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderOrderDetail()}

                {renderAmountDetail()}

                {renderSubmitButton()}

                {renderCompleteButton()}

                {/* {renderPaymentDetailTitle()}

                {renderPaymentDetail()} */}


                {renderButtonView()}


            </ScrollView>



        </View>
    )
}

export default ProviderAcceptedOrderDetail;