//Global imports
import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import MenuIcon from '../../../assets/images/Menu.png';
import CustomInput from '../../Components/TextField/index';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { CUSTOMER_COMPLETE_ORDER, DISPUTE_SENT_NOTE } from '../../Helper/Constants';
import { CustomerDisputedJobParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';

const {
    MainContainer,
    InputField,
    sendButton
} = Styles;

const CustomerSubmitNoteDispute = ({ navigation: { navigate, toggleDrawer, goBack }, route : { params } }) => {
    const [Report, SetReport] = useState('');
    const dispatch = useDispatch();
    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToHome = () => {
        goBack()
    }


    const _onChangeTextReport = (text) => SetReport(text);

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _doCompleteOrder = () => {
        
        dispatch(updateLoader(true));

        const apiClass = new APICallService(DISPUTE_SENT_NOTE, CustomerDisputedJobParam(params.job_id,  Report));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    goBack()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Disputed Orders'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderReportField = () => {
        return <CustomInput
            multiline
            inputStyle={InputField}
            placeholder='Type here...'
            maxLength={256}
            textAlignVertical='top'
            value={Report}
            viewStyle={{ paddingHorizontal: 0 }}
            onChangeText={(text) => _onChangeTextReport(text)}
        />
    }

    const renderSendButton = () => {
        return <CustomButton
            btnStyle={sendButton}
            title='Submit'
            buttonAction={() => {
                if (Report != '') {
                    _doCompleteOrder()
                }
            }}
            textColor={Colors.BLACK06}
        />

    }

    const renderMainContainer = () => {
        return <View style={MainContainer}>
            {renderReportField()}

            {renderSendButton()}
        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}

            {renderMainContainer()}
        </View>
    )
}

export default CustomerSubmitNoteDispute;