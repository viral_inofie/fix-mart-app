//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, ScrollView } from 'react-native';
import { height, width } from 'react-native-dimension';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { RIGHT_SCROLL_INDICATOR_INSENTS, SUPPLIER_ACCEPTED_ORDER_DETAIL, SUPPLIER_PRODUCT_DELIVER } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { ProductDeliverOrderParam, SupplierOrderAcceptedParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

const {
    ImageStyle,
    Title,
    PlumberDetailTitle,
    PlumberDetailValue,
    PlumberDetailView,
    ServiceDetailStyle,
    ConfirmButton,
} = Styles;

const SupplierAcceptedOrderDetail = ({ navigation: { goBack, addListener }, route: { params } }) => {

    const dispatch = useDispatch();

    const [Data, SetData] = useState({
        product_image: '',
        product_name: '',
        plumber_name: '',
        scheduleDate: '',
        scheduleTime: '',
        service: '',
        sub_category: '',
        price: '',
        qty: '',
        otherDetail: '',
        paymentPrice: '',
        paymentStatus: '',
        dueAmount: '',
        dueDate: '',
        status: '',
        isProductDelivered: '',
        isProviderPaid: ''
    })

    useEffect(() => {
        dispatch(updateLoader(true))
        addListener('focus', () => {
            getOrderDetail()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(SUPPLIER_ACCEPTED_ORDER_DETAIL, SupplierOrderAcceptedParam(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetData({
                        ...Data,
                        product_image: res.data.image,
                        product_name: res.data.name,
                        plumber_name: res.data.provider_name,
                        scheduleDate: res.data.visit_date,
                        scheduleTime: res.data.visit_time,
                        service: res.data.service,
                        sub_category: res.data.sub_category,
                        price: res.data.price,
                        qty: res.data.quantity,
                        otherDetail: res.data.other_details,

                        status: res.data.orderStatus,
                        paymentPrice: res.data.paid,
                        paymentStatus: res.data.orderStatus,
                        dueAmount: res.data.remaning_price,
                        dueDate: res.data.dueDate,
                        isProductDelivered: res.data.isProductDelivered,
                        isProviderPaid: res.data.isProviderPaid
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    const _doProductDeliver = () => {
        dispatch(updateLoader(true))

        const apiClass = new APICallService(SUPPLIER_PRODUCT_DELIVER, ProductDeliverOrderParam(params.order_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    getOrderDetail();
                }
            })
            .catch(err => {
                renderErrorToast('error', err)
                dispatch(updateLoader(false))
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Confirmed Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: Data.product_image }}
            imageStyle={{ borderRadius: height(1.5) }}
            style={ImageStyle}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.product_name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(40) }}>
                <Text style={PlumberDetailTitle}>Plumber Name :</Text>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(40), }}>
                <Text style={PlumberDetailValue}>{Data.plumber_name}</Text>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.scheduleDate).format('DD MMM, YYYY') + " " + renderTime(Data.scheduleTime)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderPaymentDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Payment Details</Text>
        </View>
    }

    const renderOrderHeading = () => {
        return <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Service :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Price :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Quantity :</Text>

            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
        </View>
    }

    const renderOrderValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>{Data.service}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.sub_category}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.qty}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.otherDetail}</Text>
        </View>
    }


    const renderOrderDetail = () => {
        return <View style={PlumberDetailView}>
            {renderOrderHeading()}

            {renderOrderValue()}

        </View>
    }

    const renderAcceptButton = () => {
        if (!Data.isProviderPaid) {
            return null
        }
        else {
            if (Data.isProductDelivered) {
                return null
            }
            else {
                return <CustomButton
                    title='Product Delivered'
                    buttonAction={() => _doProductDeliver()}
                    btnStyle={[ConfirmButton, {
                        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                        borderWidth: 0
                    }]}
                />
            }
        }
    }

    const renderDeclineButton = () => {
        return <CustomButton
            title='Decline'
            btnStyle={[ConfirmButton, { marginTop: 20 }]}
        />
    }

    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>
            {renderAcceptButton()}

            {/* {renderDeclineButton()} */}
        </View>
    }

    const renderPaymentDetail = () => {
        return <View style={PlumberDetailView}>
            {renderPaymentHeading()}

            {renderPaymentValue()}

        </View>
    }

    const renderPaymentHeading = () => {
        return <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Price :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Status :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Paid by Plumber :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Remaining :</Text>
            {
                Data.dueDate != ''
                    ?
                    <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Due Date :</Text>
                    :
                    null
            }
        </View>
    }

    const renderPaymentValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>$ {Data.paymentPrice}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.status}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.plumber_name}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.dueAmount}</Text>
            {
                Data.dueDate != ''
                    ?
                    <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.dueDate).format('DD MMM, YYYY')}</Text>
                    :
                    null
            }

        </View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                bounces={false}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderOrderDetail()}

                {renderPaymentDetailTitle()}

                {renderPaymentDetail()}


                {renderButtonView()}


            </ScrollView>



        </View>
    )
}

export default SupplierAcceptedOrderDetail;