//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { PENDING_ORDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import CustomHeader from '../../Components/CustomerHeader';
import ProviderPendingOrderList from '../../Components/ProviderPendingOrderList';
import ProviderPendingOnDemandList from '../../Components/ProviderPendingOnDemandList';
import EmptyDataComponent from '../../Components/EmptyDataComponent/index';


const { TabView, OnDemand, OnDemandText } = Styles;

const ProviderPedingOrder = ({ navigation: { navigate, addListener } }) => {
  const [DemandList, SetDemadList] = useState([])

  const [isOnDemand, SetIsOnDemand] = useState(true);
  const [OffDemandList, SetOffDemandList] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateLoader(true))

    addListener('focus', () => {
      getPendingOrder()
    })

  }, [])

  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */
  const _navigateToHome = () => {
    navigate('ProviderJobRequestStack')
  }

  const _navigateToDetail = (job_id) => {
    navigate('ProviderPendingOrderDetail', { job_id })
  }


  const _navigateToOnDemandDetail = (job_id) => {
    navigate('ProviderPendingOnDemandDetail', { job_id })
  }

  const _onPressAccept = () => {
    navigate('ProviderOnDemandHelp')
  }

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */

  const getPendingOrder = () => {
    const apiClass = new APICallService(PENDING_ORDER, {});

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          SetOffDemandList(res.data.offDemand);
          SetDemadList(res.data.onDemand)
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })

  }



  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeaderView = () => {
    return <CustomHeader
      title='Sent Offers'
      onLeftClick={() => _navigateToHome()}
    />
  }

  const renderTabView = () => {
    return <View style={TabView}>
      <TouchableOpacity
        onPress={() => SetIsOnDemand(!isOnDemand)}
        style={[OnDemand, {
          backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
          borderWidth: !isOnDemand ? 1 : 0
        }]}>
        <Text style={OnDemandText}>On Demand</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => SetIsOnDemand(!isOnDemand)}
        style={[OnDemand, {
          marginLeft: width(5),
          backgroundColor: !isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
          borderWidth: isOnDemand ? 1 : 0
        }]}>
        <Text style={OnDemandText}>Off Demand</Text>
      </TouchableOpacity>
    </View>
  }

  const renderItemDemandList = (item, index) => {
    if (isOnDemand) {
      return <ProviderPendingOnDemandList
        item={item}
        _onPressAccept={() => _onPressAccept()}
        time={100 * index}
        navigateToDetail={(job_id) => _navigateToOnDemandDetail(job_id)}
      />
    }
    else {
      return <ProviderPendingOrderList
        item={item}
        time={100 * index}
        navigateToDetail={(job_id) => _navigateToDetail(job_id)}
      />
    }
  }

  const renderDemandList = () => {
    return <FlatList
      ListEmptyComponent={() => {
        return <EmptyDataComponent />
      }}
      scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
      bounces={false}
      data={isOnDemand ? DemandList : OffDemandList}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index }) => renderItemDemandList(item, index)}
    />
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeaderView()}

      {renderTabView()}

      {renderDemandList()}

    </View>
  )
}

export default ProviderPedingOrder