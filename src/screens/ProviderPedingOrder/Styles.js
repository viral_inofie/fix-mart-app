//Global imports
import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
  HeaderView: {
    height: height(7),
    paddingHorizontal: width(5),
    width: '100%',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: Colors.GRAY
  },

  TabView: {
    paddingVertical: height(3),
    paddingHorizontal: width(5),
    flexDirection: 'row',
    alignItems: 'center',
  },

  OnDemand: {
    height: height(6),
    width: width(42.5),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: Colors.PRIMARY_BORDER_COLOR
  },

  OnDemandText: {
    fontFamily: Fonts.POPPINS_REGULAR,
    fontSize: totalSize(2),
    color: Colors.BLACK06
  }
})