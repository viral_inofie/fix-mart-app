//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import CloseIcon from 'react-native-vector-icons/AntDesign';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { PENDING_ORDER_SUBPROVIDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import CustomHeader from '../../Components/CustomerHeader';
import SupplierSubPendingOrderList from '../../Components/SupplierSubPendingOrderList';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import EmptyDataComponent from '../../Components/EmptyDataComponent';

const {
    TabView,
    OnDemand,
    OnDemandText,
    MainWhiteView,
    ModalHeaderView,
    HeadingText,
    RowView,
    RadioView,
    PaymentNameStyle,
    EnableRadio,
    PayNowButton
} = Styles;

const SupplierSubPendingOrder = ({ navigation: { navigate, addListener } }) => {

    const [PayNowModel, SetPayNowModel] = useState(false)
    const [DemandList, SetDemadList] = useState([
        {
            key: '0',
            image: Demand1,
            category: 'Plumbing Installation',
            supplier_name: 'John Demo',
            customer_name: 'Mark Desai',
            date: '03th Sep 2020, 03:00 Pm',
        },
        {
            key: '1',
            image: Demand2,
            category: 'Plumbing Installation',
            supplier_name: 'John Demo',
            customer_name: 'Mark Desai',
            date: '03th Sep 2020, 03:00 Pm',
        },
        {
            key: '3',
            image: Demand3,
            category: 'Plumbing Installation',
            supplier_name: 'John Demo',
            customer_name: 'Mark Desai',
            date: '03th Sep 2020, 03:00 Pm',
        },
        {
            key: '4',
            image: Demand4,
            category: 'Plumbing Installation',
            supplier_name: 'John Demo',
            customer_name: 'Mark Desai',
            date: '03th Sep 2020, 03:00 Pm',
        }
    ])

    const [PaymentMode, SetPaymentMode] = useState([
        { key: 0, name: 'Debit Card', isSelect: false },
        { key: 1, name: 'Credit Card', isSelect: false },
        { key: 2, name: 'Paypal', isSelect: false },
        { key: 3, name: 'Paybalance', isSelect: false },
    ])
    const [isOnDemand, SetIsOnDemand] = useState(true);
    const [OffDemandList, SetOffDemandList] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getOrder()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('ProviderJobRequestStack')
    }

    const _navigateToDetail = (order_id) => {
        navigate('SupplierSubPendingOrderDetail', { order_id })
    }

    const _closeModal = () => {
        SetPayNowModel(false)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _SelectPaymentMethod = (d) => {
        console.log(d, 'item')
        let NewArray = PaymentMode.map((item, i) => {
            item.isSelect = false;
            if (item.key === d.key) {
                item.isSelect = true
                return item
            }
            return item
        })
        SetPaymentMode(NewArray)
    }

    const getOrder = () => {
        const apiClass = new APICallService(PENDING_ORDER_SUBPROVIDER, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetOffDemandList(res.data)
                }
            })
            .catch(err => {
                renderErrorToast('error', err)
                dispatch(updateLoader(false))
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Sent Offers'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: isOnDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemDemandList = (item, index) => {
        return <SupplierSubPendingOrderList
            item={item}
            time={100 * index}
            _onPayNow={() => SetPayNowModel(true)}
            _noPayLater={() => { }}
            navigateToDetail={(order_id) => _navigateToDetail(order_id)}
        />
    }

    const renderDemandList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={OffDemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
        />
    }

    const _renderListPayment = (data) => {
        return <TouchableOpacity
            onPress={() => _SelectPaymentMethod(data.item)}
            style={RowView}>
            <View style={RadioView}>
                {
                    data.item.isSelect
                        ?
                        <View style={EnableRadio}>
                        </View>
                        :
                        null
                }
            </View>

            <Text style={PaymentNameStyle}>{data.item.name}</Text>
        </TouchableOpacity>
    }

    const renderPaymentModalList = () => {
        return <FlatList
            contentContainerStyle={{ paddingHorizontal: width(5), paddingTop: height(2) }}
            data={PaymentMode}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(data) => _renderListPayment(data)}
        />
    }

    const renderPayNowModel = () => {
        return <Modal style={{ marginHorizontal: 0 }} isVisible={PayNowModel}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={MainWhiteView}>

                    <View style={ModalHeaderView}>
                        <Text style={HeadingText}>Payment Mode</Text>

                        <TouchableOpacity onPress={() => _closeModal()}>
                            <CloseIcon name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    {renderPaymentModalList()}

                    {renderPayNowButton()}
                </View>
            </View>
        </Modal>
    }

    const renderPayNowButton = () => {
        return <CustomButton
            buttonAction={() => { }}
            title='Pay Now'
            btnStyle={PayNowButton}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {/* {renderTabView()} */}

            {renderDemandList()}

            {renderPayNowModel()}
        </View>
    )
}

export default SupplierSubPendingOrder;