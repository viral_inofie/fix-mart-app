//Global imports
import React, { useEffect, useState } from 'react';
import {
  View, Text, TouchableOpacity, SafeAreaView,
  ImageBackground, ScrollView, TextInput, FlatList
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import {
  CUSTOMER_ONDEMAND_JOB_DETAILS,
  HEIGHT,
  RIGHT_SCROLL_INDICATOR_INSENTS,
  PENDING_ORDER_CONFIRM,
  PENDING_ORDER_DECLINE
} from '../../Helper/Constants';

//Component imports
import ProfileIcon from '../../../assets/images/profile.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import { AirbnbRating } from 'react-native-ratings';


const {
  PlumberDetailTitle,
  PlumberDetailValue,
  PlumberDetailView,
  ViewButtonStyle,
  StarView,
  ConfirmButton,
  ModelMainContainer,
  ModelHeader,
  ModelTitle,
  ReviewText,
  radioBtnImg,
  radiBtnImgCont,
  textInput,
  CertiImage,
  RowView
} = Styles;

const CustomerPendingOnDemandDetail = ({ navigation: { goBack, navigate, addListener }, route: { params } }) => {

  const [ReviewModel, SetReviewModel] = useState(false);
  const [DeclineOfferModel, SetDeclineOfferModel] = useState(false)
  const [DoYouWantVisit, SetDoYouWantVisit] = useState(true)
  const [CommentHere, SetCommentHere] = useState('(Optional) Comment here...')
  const [CertificateView, SetCertificateView] = useState(false);
  const [Data, SetData] = useState({
    job_id: '',
    service: '',
    costEstimate: '',
    transport_charge: '',
    assessment_charge: '',
    timeOfarrival: '',
    ratings: '',
    reviews: '',
    certificates: ''
  })

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateLoader(true));
    addListener('focus', () => {
      getJobDetail()
    })
  }, [])

  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack();

  const _openModel = () => SetReviewModel(true);

  const _openCertificateView = () => {
    SetCertificateView(true)
  }

  const _navigateToDetailCerti = (image) => {
    navigate('CustomerCertificateOnDemand', { image })
  }

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */

  const getJobDetail = () => {
    const apiClass = new APICallService(CUSTOMER_ONDEMAND_JOB_DETAILS, { pending_job_id: params.job_id });

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));
        if (validateResponse(res)) {
          SetData({
            ...Data,
            job_id: res.data.id,
            service: res.data.service,
            costEstimate: res.data.answer[0].answer,
            transport_charge: res.data.answer[1].answer,
            assessment_charge: res.data.answer[2].answer,
            timeOfarrival: res.data.answer[3].answer,
            ratings: res.data.rating,
            reviews: res.data.reviews,
            certificates: res.data.document
          })
        }
        else {
          RNToasty.Error({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const _acceptJob = () => {

    dispatch(updateLoader(true));

    const apiClass = new APICallService(PENDING_ORDER_CONFIRM, { pending_job_id: Data.job_id });

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          goBack();
          RNToasty.Success({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
        }
        else {
          RNToasty.Error({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  const _declineJob = () => {
    dispatch(updateLoader(true));

    const apiClass = new APICallService(PENDING_ORDER_DECLINE, { pending_job_id: Data.job_id });

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false))
        if (validateResponse(res)) {
          goBack();
          RNToasty.Success({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
        }
        else {
          RNToasty.Error({
            title: res.message,
            withIcon: false,
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderHeader = () => {
    return <CustomHeader
      isWithProfile
      title={params.provider_name}
      ProfileIcon={{ uri: params.profile_image }}
      onLeftClick={() => _navigateToBack()}
    />
  }

  const renderServiceHeading = () => {
    return <View>
      <View style={RowView}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Service :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>{Data.service}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Cost estimate :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.costEstimate}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Transport charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.transport_charge}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Assessment charge :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={PlumberDetailValue}>$ {Data.assessment_charge}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Time of arrival :</Text>
        </View>
        <View style={{ width: width(45) }}>
          <Text style={[PlumberDetailValue, { fontFamily: Fonts.POPPINS_BOLD, color: Colors.BLACK }]}>{Data.timeOfarrival}</Text>
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Ratings :</Text>
        </View>
        <View style={{ width: width(45) }}>
          {renderStarView()}
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Reviews :</Text>
        </View>
        <View style={{ width: width(45) }}>
          {renderReviewViewButton()}
        </View>
      </View>

      <View style={[RowView, { marginTop: height(1) }]}>
        <View style={{ width: width(35) }}>
          <Text style={PlumberDetailTitle}>Certificates :</Text>
        </View>
        <View style={{ width: width(45) }}>
          {renderCertificateViewButton()}
        </View>
      </View>
    </View>
  }

  const renderStarView = () => {
    return <View style={StarView}>
      <AirbnbRating
        selectedColor={Colors.YELLOW}
        count={5}
        showRating={false}
        defaultRating={Data.ratings.substring(0, 3)}
        reviewColor={Colors.PRIMARY_DARK}
        size={10}
        isDisabled
        onFinishRating={(val) => { }} />
      <Text style={PlumberDetailValue}> / {Data.ratings.substring(4, 5)}</Text>
    </View>
  }

  const renderViewButton = () => {
    return <CustomButton
      title='View'
      textStyle={{ fontSize: 12 }}
      textColor={Colors.BLACK}
      buttonAction={() => _openCertificateView()}
      btnStyle={ViewButtonStyle}
    />
  }

  const renderCertificateView = () => {
    if (CertificateView) {
      return <FlatList
        numColumns={2}
        contentContainerStyle={{ marginTop: 20 }}
        data={Data.certificates}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => {
          return <TouchableOpacity onPress={() => _navigateToDetailCerti(item.toString())}>
            <ImageBackground
              key={item.key}
              source={{ uri: item.toString() }}
              style={CertiImage}
            />
          </TouchableOpacity>
        }}
      />
    }
    else {
      return null
    }
  }

  const renderServiceValue = () => {
    return <View style={{ width: width(45), }}>
      <Text style={PlumberDetailValue}>{Data.service}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.costEstimate}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.transport_charge}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.assessment_charge}</Text>
      <Text style={[PlumberDetailValue, { marginTop: height(3) }]}>{Data.timeOfarrival}</Text>

      {renderStarView()}
      {/* <Text numberOfLines={2} style={[PlumberDetailValue, { marginTop: height(1) }]}>Very good service and nice experience</Text> */}
      {renderReviewViewButton()}

      {renderViewButton()}


    </View>
  }

  const renderReviewViewButton = () => {
    return <CustomButton
      title='View'
      textStyle={{ fontSize: 12 }}
      textColor={Colors.BLACK}
      buttonAction={() => SetReviewModel(!ReviewModel)}
      btnStyle={ViewButtonStyle}
    />
  }

  const renderCertificateViewButton = () => {
    return <CustomButton
      title='View'
      textStyle={{ fontSize: 12 }}
      textColor={Colors.BLACK}
      buttonAction={() => SetCertificateView(!CertificateView)}
      btnStyle={ViewButtonStyle}
    />
  }


  const renderServiceDetail = () => {
    return <View style={[PlumberDetailView, { marginTop: 0 }]}>
      {renderServiceHeading()}

      {/* {renderServiceValue()} */}
    </View>
  }

  const renderConfirmButton = () => {
    return <CustomButton
      title='Accept'
      buttonAction={() => _acceptJob()}
      btnStyle={[ConfirmButton, {
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        borderWidth: 0
      }]}
    />
  }

  const renderDeclineButton = () => {
    return <CustomButton
      title='Decline'
      btnStyle={[ConfirmButton, { marginTop: 10 }]}
      buttonAction={() => _declineJob(true)}
    />
  }

  const renderCancelButton = () => {
    return <CustomButton
      title='Cancel Order'
      btnStyle={[ConfirmButton, { marginTop: 10 }]}
    />
  }

  const renderButtonView = () => {
    return <View>
      {renderConfirmButton()}

      {renderDeclineButton()}

      {/* {renderCancelButton()} */}
    </View>
  }

  const renderModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetReviewModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>

            <FlatList
              data={Data.reviews}
              keyExtractor={(item, index) => index.toString()}
              ListEmptyComponent={() => {
                return <View style={{ height: HEIGHT - 650, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: Fonts.POPPINS_REGULAR, color: Colors.LIGHT_GRAY, fontSize: 16 }}>No Review Found</Text>
                </View>
              }}
              renderItem={({ item, index }) => {
                return <Text style={ReviewText}>{`${index + 1}.${item.comment}`}</Text>
              }}
            />
          </View>

        </View>
      </View>
    </Modal>
  }

  const renderDeclineModel = () => {
    return <Modal style={{ margin: 0 }} isVisible={DeclineOfferModel}>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <View style={ModelMainContainer}>
          <View style={ModelHeader}>
            <Text style={ModelTitle}>Plumber Reviews</Text>

            <TouchableOpacity onPress={() => SetDeclineOfferModel(false)}>
              <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
            <Text style={ReviewText}>Do you want to visit at different time and location?</Text>
            <View style={{ flexDirection: 'row', marginTop: 8 }}>
              <TouchableOpacity style={{ flexDirection: 'row', marginRight: 20, alignItems: 'center', justifyContent: 'center', }} onPress={() => SetDoYouWantVisit(true)}>
                <View style={radiBtnImgCont}>
                  {DoYouWantVisit && <View style={radioBtnImg} />}
                </View>
                <Text style={ReviewText}>Yes</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }} onPress={() => SetDoYouWantVisit(false)}>
                <View style={radiBtnImgCont}>
                  {!DoYouWantVisit && <View style={radioBtnImg} />}
                </View>
                <Text style={ReviewText}>No</Text>
              </TouchableOpacity>
            </View>

            <TextInput
              style={textInput}
              multiline
              value={CommentHere}
              onChangeText={(text) => SetCommentHere(text)}
            />
            <CustomButton
              title='Submit'
              btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0,
                height: 40,
                marginTop: 15
              }]}
              buttonAction={() => SetDeclineOfferModel(false)}
            />
          </View>
        </View>
      </View>
    </Modal>
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

      {renderHeader()}

      <ScrollView
        scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
        bounces={false}
        contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>




        {renderServiceDetail()}
        {renderCertificateView()}

        {renderButtonView()}


      </ScrollView>

      {renderModel()}
      {renderDeclineModel()}

    </View>
  )
}

export default CustomerPendingOnDemandDetail;