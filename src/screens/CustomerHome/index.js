//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, Platform, PermissionsAndroid } from 'react-native';
import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';
import GetLocation from 'react-native-get-location';
import {RNToasty} from 'react-native-toasty';


//File imports
import Styles from './Styles';

//Component imports
import CustomHeader from '../../Components/CustomerHeader/index';
import DrawerIcon from '../../../assets/images/Menu.png';
import HomeBg from '../../../assets/images/HomeBg.png';
import CustomInput from '../../Components/TextField/index';
import CustomButton from '../../Components/Button/index';
import BlackPin from '../../../assets/images/blackpin.png';
import Colors from '../../Helper/Colors';
import Logger from '../../Helper/Logger';
import { useDispatch, useSelector } from 'react-redux';
import { UPDATE_LOCATION } from '../../ReduxStore/Types';
import { UpdateAddress, UpdateLocation, UpdateShortAddress } from '../../ReduxStore/Actions/AuthActions';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import Fonts from '../../Helper/Fonts';

const {
    MainView,
    BackGroundImage,
    Container,
    InputContainer,
    BottomView,
    OnDemandButton,
    LocationInput,
    addressText
} = Styles;

const CustomerHome = ({ navigation: { navigate, toggleDrawer, addListener }, route: { params } }) => {
    const [isOnDemand, SetIsOnDemand] = useState(false)
    const [offDemand, SetOffDemand] = useState(false)
    let [LatLong, SetLatLong] = useState({
        longitude: '',
        latitude: '',
    });

    const dispatch = useDispatch();

    const { address } = useSelector(state => ({
        address: state.AuthReducer.address,
    }))

    useEffect(() => {
        // dispatch(updateLoader(true))
        checkLocationPermission();
    }, [])


    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _toggleDrawer = () => {
        toggleDrawer()
    }

    const _navigateToService = (job_type) => {
        navigate('CustomerService', { job_type : job_type, address : address })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    function checkLocationPermission() {
        check(
            Platform.select({
                ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
                android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            }),
        ).then((result) => {
            console.log("location check===>",result,RESULTS.DENIED)
            switch (result) {
                case RESULTS.DENIED:
                    request(
                        Platform.select({
                            ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
                            android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                        })
                    ).then((result) => {
                        if (result === RESULTS.GRANTED) {
                            GetLocation.getCurrentPosition({
                                enableHighAccuracy: true,
                                timeout: 10000,
                            }).then(location => {
                                Logger.log("Location::", location);
                                dispatch(updateLoader(false))
                                dispatch(UpdateLocation({ latitude: location.latitude, longitude: location.longitude }))
                                onSelectRegion({ latitude: location.latitude, longitude: location.longitude })
                            }).catch(error => {
                                const { code, message } = error;
                                Logger.log(code, message);
                                dispatch(updateLoader(false))
                                if (code === 'UNAVAILABLE')
                                checkLocationPermission();
                                
                            })
                        } else {
                            dispatch(updateLoader(false))
                            alert('Please give location permission');
                        }
                    });
                    break;
                case RESULTS.GRANTED:
                    GetLocation.getCurrentPosition({
                        enableHighAccuracy: true,
                        timeout: 10000,
                    })
                        .then(location => {
                            Logger.log("Location::", location);
                            dispatch(updateLoader(false))
                            dispatch(UpdateLocation({ latitude: location.latitude, longitude: location.longitude }))
                            onSelectRegion({ latitude: location.latitude, longitude: location.longitude })
                        })
                        .catch(error => {
                            const { code, message } = error;
                            Logger.log(code, message);
                            if (code === 'UNAVAILABLE')
                                dispatch(updateLoader(false))
                            alert('Please enable location on device');
                        })
                    break;
                case RESULTS.BLOCKED:
                    dispatch(updateLoader(false))
                    alert('Please give location permission');
                    break;
                default:
                    dispatch(updateLoader(false))
                    alert('Please give location permission');
                    break;
            }
        }).catch((error) => {
            dispatch(updateLoader(false))
            console.log("location check===>error",result)
            Logger.log("Error ::", error);
        });
    }

    const onSelectRegion = async (data) => {
        let { latitude, longitude } = data;

        let key = "AIzaSyDOVCWIQrQbXmfaIpEhCxGNKlMqPpqELeo";
console.log("onSelectRegion====>",key)
        fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${key}`)
            .then((resp) => resp.json())
            .then(async (dt) => {
                const {
                    results
                } = dt;
                console.log(dt, 'address')
                if (results.length > 0) {
                    dispatch(UpdateAddress(results[0].formatted_address))
                    dispatch(UpdateShortAddress(results[3].formatted_address))
                    // setAddress(results[0].formatted_address)
                    dispatch(updateLoader(false))
                    RNToasty.Success({
                        title : 'Address saved successfully...!',
                        withIcon : false,
                        position : 'bottom',
                        fontFamily : Fonts.POPPINS_REGULAR
                    })
                }
            })
            .catch((error) => {
                dispatch(updateLoader(false))
                console.log('err', error)
            });
    }



    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title='Fixmart'
            LeftIcon={null}
            onRightClick={() => navigate('Notification')}
            onLeftClick={() => _toggleDrawer()}
        />
    }

    const renderMainView = () => {
        return <View style={Container}>
            <View style={MainView}>
                {renderLocationField()}
                {renderBackGroundImage()}
                {renderBottomView()}
            </View>
        </View>
    }

    const renderBackGroundImage = () => {
        return <ImageBackground
            source={HomeBg}
            resizeMode='contain'
            style={BackGroundImage}
        />
    }

    const renderLocationField = () => {
        return <TouchableOpacity
            onPress={() => navigate('SearchAddress')}
            style={InputContainer}>
            <ImageBackground source={BlackPin} resizeMode='contain' style={{ height: 18, width: 18 }} />

            <Text style={addressText} numberOfLines={1}>{address}</Text>
        </TouchableOpacity>
    }

    const renderBottomView = () => {
        return <View style={BottomView}>
            {renderButton()}
        </View>
    }

    const renderButton = () => {
        return <View>
            <View style={{
                flex:1,justifyContent:'center',alignItems:'center',marginBottom:5
            }}>
                <Text style={{
                    color:Colors.SUCCESS,
                    fontSize:20,
                    backgroundColor: 'transparent',
                    borderWidth: 0,
                    
                    }}>Posted a Job Request</Text>
            </View>
            <CustomButton
                title='On Demand'
                buttonAction={() => {
                    SetIsOnDemand(true)
                    SetOffDemand(false)
                    _navigateToService(1)
                }}
                btnStyle={[OnDemandButton, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}
            />

            <CustomButton
                title='Off Demand'
                buttonAction={() => {
                    SetOffDemand(true)
                    SetIsOnDemand(false)
                    _navigateToService(2)
                }}
                btnStyle={[OnDemandButton, {
                    marginBottom: 0,
                    backgroundColor: !offDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: !offDemand ? 1 : 0
                }]}
            />
        </View>
    }

    return (
        <View style={{ flex: 1 }}>
            {renderHeader()}

            {renderMainView()}
        </View>
    )
}

export default CustomerHome;