import { StyleSheet } from 'react-native';
import {height, totalSize, width} from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    ImageLogoStyle:{
        height:totalSize(19),
        width:totalSize(16),
        alignSelf:'center'
    },

    InputField:{
        height:height(7),
        width:width(80),
        borderRadius:height(3.5),
        borderWidth:1,
        borderColor:Colors.INDEX_GREY,
        // paddingHorizontal:width(5),
        flexDirection:'row',
        alignItems:'center',
        marginTop:height(8),
    },

    FieldIcon:{
        height: height(3), width: width(5)
    },

    InputFieldStyle:{
        height:'100%',
        width:width(60),
        // marginLeft:width(),
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(1.8),
        color:Colors.BLACK,
        // backgroundColor:'red'

    },

    ForgotPasswordText:{
        fontSize:totalSize(1.5),
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.LIGHT_GRAY
    },

    LoginButton:{
        height:height(7),
        width:width(80),
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.APP_PRIMARY,
        borderRadius:height(3.5),
        marginTop:height(10)
    },

    LoginTextStyle:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(1.8),
        color:Colors.BLACK
    },

    DontAccountText:{
        marginTop:height(10), 
        textAlign:'center',
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(1.5),
        color:Colors.BLACK
    },

    SignUpText:{
        fontSize:totalSize(1.5),
        color:Colors.APP_PRIMARY,
        fontFamily:Fonts.POPPINS_REGULAR
    },

    ForgotPasswordClick:{ alignSelf: 'flex-end', marginTop: height(1) }
    
})