// Global imports
import React, { useState } from 'react'
import {
  View, Text, TouchableOpacity, ImageBackground,
  SafeAreaView, KeyboardAvoidingView, Platform, ScrollView,
} from 'react-native'
import { height, width } from 'react-native-dimension'
import { useDispatch } from 'react-redux'
import { RNToasty } from 'react-native-toasty'
import AsyncStorage from '@react-native-async-storage/async-storage'


// File imports
import Colors from '../../Helper/Colors'
import Styles from './Styles'
import { ACCESS_TOKEN, CUSTOMER_ROLE, SERVICE_PROVIDER, EMAIL_REGEX, LOGIN, RIGHT_SCROLL_INDICATOR_INSENTS, SUPPLIER, PREV_DEVICE_FCM_TOKEN } from '../../Helper/Constants'
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { LoginParam } from '../../Api/APIJson'
import APICallService from '../../Api/APICallService'
import { renderErrorToast, validateResponse } from '../../Helper/Helper'
import { UpdateLoginData } from '../../ReduxStore/Actions/AuthActions'

//Component import
import LOGO from '../../../assets/images/logo.png'
import MsgIcon from '../../../assets/images/msg.png'
import LockIcon from '../../../assets/images/lock.png'
import EyeIcon from '../../../assets/images/closeeye.png'
import OpenEye from '../../../assets/images/eyeopen.png'
import CustomTextField from '../../Components/TextField/index'
import CustomButton from '../../Components/Button/index'
import ErrorModal from '../../Components/ErrorModal/index'
import log from '../../Helper/log'
import Logger from '../../Helper/Logger'
import Loader from '../../Components/Loader'
import Fonts from '../../Helper/Fonts'

const {
  ImageLogoStyle,
  InputField,
  ForgotPasswordText,
  LoginButton,
  LoginTextStyle,
  DontAccountText,
  SignUpText,
  FieldIcon,
  InputFieldStyle,
  ForgotPasswordClick
} = Styles

const Login = ({ navigation: { navigate, replace }, route: { params } }) => {
  Logger.log('params => ', params.type)
  const [Data, SetData] = useState({
    Email: '',
    Password: '',
    passwordFieldVisible: true
  })

  const [ErrorValidation, SetErrorValidation] = useState({
    Message: '',
    ShowErrorModel: false
  })
  const dispatch = useDispatch();

  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToForgotPassword = () => {
    navigate('ForgotPassword')
  }

  const _navigateToRegister = () => {
    navigate('Register', {
      type: params.type
    })
  }

  const _navigateToApp = () => {
    if (_Validate()) {
      APICall()
    }
  }

  const _navigateForward = (res) => {
    if (res.data.role === CUSTOMER_ROLE) {
      replace('BottomTab')
    }

    if (res.data.role === SERVICE_PROVIDER) {
      replace('CustomerDrawer', {
        type: 'provider'
      })
    }

    if (res.data.role === SUPPLIER) {
      replace('CustomerDrawer', {
        type: 'supplier'
      })
    }
  }



  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */

  const _onChangeEmailText = (text) => SetData({ ...Data, Email: text })

  const _onChangePasswordText = (text) => SetData({ ...Data, Password: text })

  const _onRightClickEyePress = () => SetData({ ...Data, passwordFieldVisible: !Data.passwordFieldVisible })

  const _Validate = () => {
    const { Email, Password } = Data

    if (Email == '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter email address', ShowErrorModel: true })
      return false
    } else if (!EMAIL_REGEX.test(Email)) {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter valid email address', ShowErrorModel: true })
      return false
    } else if (Password == '') {
      SetErrorValidation({ ...ErrorValidation, Message: 'Please enter password', ShowErrorModel: true })
      return false
    } else if (Password.length < 6) {
      SetErrorValidation({ ...ErrorValidation, Message: 'Password should contain minimum 6 character', ShowErrorModel: true })
      return false
    } else {
      return true
    }
  }

  const _CloseValidationModel = () => {
    SetErrorValidation({ ...ErrorValidation, ShowErrorModel: false })
  }

  const APICall = async () => {

    const { Email, Password } = Data;
    const fcm_token = await AsyncStorage.getItem(PREV_DEVICE_FCM_TOKEN)

    dispatch(updateLoader(true));

    const apiClass = new APICallService(LOGIN, LoginParam(Email, Password, fcm_token))

    apiClass.callAPI()
      .then(async (res) => {
        if (res.code === 200) {
          console.log("loginsuccess",res)
          if (res.success) {
            AsyncStorage.setItem(ACCESS_TOKEN, res.token.access_token, (err) => {
              if (err === null) {
                dispatch(updateLoader(false));
                dispatch(UpdateLoginData(res.data));
                RNToasty.Success({
                  title: res.message,
                  position: 'bottom',
                  fontFamily: Fonts.POPPINS_REGULAR
                })
                _navigateForward(res);
              }
              else {
                dispatch(updateLoader(false));
                SetErrorValidation({ Message: 'Something went wrong...!', ShowErrorModel: true })
              }
            })
          }
          else{
            navigate('PaymentWebView', { url: res.url })
            dispatch(updateLoader(false));
          }
        }
        else {
          dispatch(updateLoader(false));
          RNToasty.Error({
            title: res.message,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
          // SetErrorValidation({ Message: res.message, ShowErrorModel: true });
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast()
      })
  }


  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########..######.
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##....##....##
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##....##......
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##.....######.
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##..........##
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##....##....##
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##.....######.
  */

  const renderEmailField = () => {
    return <CustomTextField
      containerView={InputField}
      leftImg={MsgIcon}
      keyboardType={'email-address'}
      placeholder='Email address'
      value={Data.Email}
      onChangeText={(text) => _onChangeEmailText(text)}
    />
  }

  const renderPasswordField = () => {
    return <CustomTextField
      containerView={[InputField, { marginTop: 20 }]}
      leftImg={LockIcon}
      placeholder='Password'
      value={Data.Password}
      secureTextEntry={Data.passwordFieldVisible}
      onChangeText={(text) => _onChangePasswordText(text)}
      rightImg={Data.passwordFieldVisible ? EyeIcon : OpenEye}
      RightImageClick={() => _onRightClickEyePress()}
    />
  }

  const renderForgotPasswordText = () => {
    return <TouchableOpacity
      onPress={() => _navigateToForgotPassword()}
      style={ForgotPasswordClick}>
      <Text style={ForgotPasswordText}>Forgot your password?</Text>
    </TouchableOpacity>
  }

  const renderLoginButton = () => {
    return <CustomButton
      title={'Login'}
      buttonAction={() => _navigateToApp()}
      btnStyle={LoginButton}
    />
  }

  const renderSignUpText = () => {
    return <Text style={DontAccountText}>
      Don't have an account? Please,
            <Text onPress={() => _navigateToRegister()}
        style={SignUpText}> Sign Up</Text>
    </Text>
  }

  const renderLogo = () => {
    return <ImageBackground
      source={LOGO}
      style={ImageLogoStyle}
    />
  }


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.WHITE }}>
      {/* <Loader /> */}
      <View style={{ flex: 1, }}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 20}
          behavior={Platform.OS === 'ios' ? 'padding' : null}>

          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={true}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            contentContainerStyle={{ paddingVertical: height(5), paddingHorizontal: width(10) }}>

            {renderLogo()}
            {renderEmailField()}
            {renderPasswordField()}
            {renderForgotPasswordText()}
            {renderLoginButton()}
            {renderSignUpText()}

          </ScrollView>

        </KeyboardAvoidingView>

      </View>

      <ErrorModal
        message={ErrorValidation.Message}
        visible={ErrorValidation.ShowErrorModel}
        handleBack={() => _CloseValidationModel()}
      />

    </SafeAreaView>
  )
}

export default Login