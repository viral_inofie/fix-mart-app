//Global imports
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    ScrollView,
    ImageBackground,
    FlatList,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { height, width, totalSize } from 'react-native-dimension';
import ImagePicker from 'react-native-image-picker';
import { useDispatch } from 'react-redux';

//File imports
import { PRODUCT_DETAIL, RIGHT_SCROLL_INDICATOR_INSENTS, SENT_TO_CUSTOMER } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { ProductDetailParam, ProductSentToCustomerParam } from '../../Api/APIJson';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import BackArrow from '../../../assets/images/backarrow.png';
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import Category1 from '../../../assets/images/subcategory5.png';
import Installation from '../../../assets/images/installation.png';
import CustomButton from '../../Components/Button/index';
import ProfileIcon from '../../../assets/images/profile.png';
import AttachImage from '../../../assets/images/attach_image.png';
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import Fonts from '../../Helper/Fonts';
import { RNToasty } from 'react-native-toasty';


const {
    MainContainer,
    RequestImage,
    ImageView,
    RequestTitle,
    ServiceTitle,
    RequestNameView,
    RequestNameTitle,
    ConfirmButton,
    PlumberDetailView,
    PlumberDetailValue,
    PlumberDetailTitle,
    ServiceDetailStyle,
    renderDetailView,
    SheduleViewHeading,
    BottomView,
    ProductText,
    PaginationText,
    BottomButtonStyle
} = Styles;

const options = {
    title: 'Select Avatar',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const ProvideProductDetail = ({ navigation: { goBack, navigate, pop }, route: { params } }) => {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true))
        getProductDetail()
    }, [])

    const [Data, SetData] = useState({
        supplier_id: '',
        supplier_name: '',
        service: '',
        sub_category: '',
        other_detail: '',
        supplier_price: '',
        product_image: '',
        provider_price: ''
    })


    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _goBack = () => {
        goBack()
    }

    const _navigateToHome = () => {
        navigate('CustomerHome')
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _renderImageList = (data) => {
        return <ImageBackground
            source={Installation}
            style={{ height: 100, width: 100, marginRight: 20 }} />
    }

    const _renderAttachedImage = (item, index) => {
        return <View style={{ alignItems: 'center' }}>
            <ImageBackground
                source={{ uri: item.toString() }}
                imageStyle={{ borderRadius: 5 }}
                style={{ height: 100, width: 260, marginRight: 20 }}
            />
            <Text style={PaginationText}>{index + 1 + "/" + Data.product_image.length}</Text>
        </View>
    }

    const _renderAttachImageFooter = () => {
        return <TouchableOpacity onPress={() => _uploadRequestImage()} >
            <ImageBackground source={AttachImage} resizeMode='contain' style={{ height: 100, width: 100 }} />
        </TouchableOpacity>
    }

    const _uploadRequestImage = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // this.setState({
                //     avatarSource: source,
                // });
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let ImageObj = {
                    key: AttachImageList.length,
                    ImageUri: response.uri,
                    ImageFileName: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    ImageType: response.type
                }
                let New_Image_Array = [ImageObj];
                SetAttachImageList([...AttachImageList, ...New_Image_Array])
                console.log(AttachImageList)
            }
        });
    }

    const getProductDetail = () => {

        const apiClass = new APICallService(PRODUCT_DETAIL, ProductDetailParam(params.product_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))

                if (validateResponse(res)) {
                    SetData({
                        ...Data,
                        supplier_id: res.data.supplier_id,
                        supplier_name: res.data.suplierfirstname + " " + res.data.suplierlastname,
                        sub_category: res.data.subcategory,
                        other_detail: res.data.description,
                        supplier_price: res.data.price,
                        service: res.data.service,
                        product_image: res.data.image
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _sentToCustomer = () => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(SENT_TO_CUSTOMER, ProductSentToCustomerParam(params.job_id, params.product_id, Data.provider_price))

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetData({ ...Data, provider_price: '' })
                    navigate('ProviderJobRequest');
                    RNToasty.Success({
                        title: res.message,
                        position: 'absolute',
                        fontFamily: Fonts.POPPINS_REGULAR
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err)
                dispatch(updateLoader(false))
            })
    }



    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title={params.product_name}
            LeftIcon={BackArrow}
            onLeftClick={() => _goBack()}
        />
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View>
                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                    <View style={{ width: width(35) }}>
                        <Text style={PlumberDetailTitle}>Supplier Name :</Text>
                    </View>

                    <View style={{ width: width(45) }}>
                        <Text style={PlumberDetailValue}>{Data.supplier_name}</Text>
                    </View>
                </View>
            </View>
        </View>
    }


    const renderServiceHeading = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Service :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.service}</Text>
                </View>
            </View>

            {
                Data.sub_category != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(35) }}>
                            <Text style={PlumberDetailTitle}>Sub Category :</Text>
                        </View>

                        <View style={{ width: width(45) }}>
                            <Text style={PlumberDetailValue}>{Data.sub_category}</Text>
                        </View>
                    </View>
                    :
                    null
            }
{/* 
            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Other Details :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>{Data.other_detail}</Text>
                </View>
            </View> */}

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Supplier price :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <Text style={PlumberDetailValue}>$ {Data.supplier_price}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(35) }}>
                    <Text style={PlumberDetailTitle}>Set Price :</Text>
                </View>

                <View style={{ width: width(45) }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                        <Text style={[PlumberDetailValue, { fontSize: 25 }]}>$</Text>
                        <TextInput
                            value={Data.provider_price}
                            keyboardType='numeric'
                            onChangeText={(text) => SetData({ ...Data, provider_price: text })}
                            style={{
                                height: 40, width: 100, marginLeft: width(2),
                                backgroundColor: Colors.INDEX_GREY,
                                paddingHorizontal: 10,
                                fontSize: 14, fontFamily: Fonts.POPPINS_REGULAR
                            }}
                        />
                    </View>
                </View>
            </View>

        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderAttachImageView = () => {
        return <View style={{ paddingHorizontal: width(5) }}>
            {renderAttachImageList()}
        </View>
    }

    const renderAttachImageList = () => {
        return <FlatList
            data={Data.product_image}
            horizontal
            contentContainerStyle={{ paddingBottom: 15 }}
            scrollIndicatorInsets={{ bottom: 0 }}
            renderItem={({ item, index }) => _renderAttachedImage(item, index)}
        // ListFooterComponent={() => _renderAttachImageFooter()}
        />
    }


    const renderMainView = () => {
        return <ScrollView
            contentContainerStyle={{ padding: 20, paddingBottom: 60 }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}>

            {renderAttachImageView()}

            {renderPlumberDetailView()}
            {renderServiceDetailTitle()}

            <View style={renderDetailView}>
                {renderServiceHeading()}
            </View>

            {renderButtonView()}

        </ScrollView>
    }

    const renderImage = () => {
        return <ImageBackground
            imageStyle={{ borderRadius: 50 }}
            resizeMode='contain'
            source={ProfileIcon}
            style={RequestImage}
        />
    }

    const renderRequestTitle = () => {
        return <Text style={RequestTitle}>John Demo</Text>
    }

    const renderButtonView = () => {
        return <View style={{ marginTop: 50 }}>
            <CustomButton
                btnStyle={BottomButtonStyle}
                buttonAction={() => navigate('CustomerMessages', { supplier_id: Data.supplier_id, supplier_name: Data.supplier_name })}
                title='Chat with supplier'
                textStyle={{ fontSize: 16 }}
            />

            <CustomButton
                btnStyle={[BottomButtonStyle, {
                    backgroundColor: 'transparent',
                    borderWidth: 1,
                    borderColor: Colors.PRIMARY_BORDER_COLOR
                }]}
                buttonAction={() => {
                    if (Data.provider_price != '') {
                        _sentToCustomer()
                    }
                }}
                title='Sent to customer'
                textStyle={{ fontSize: 16 }}
            />
        </View>
    }


    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}

            {renderMainView()}

        </View>
    )
}

export default ProvideProductDetail;