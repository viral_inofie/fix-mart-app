//Global imports
import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainContainer: {
        paddingHorizontal: 20,
        paddingTop: 20,
        flex: 1
    },

    RequestImage: {
        height: 100,
        width: 100,
        alignSelf: 'center'
    },

    ImageView: {
        height: 150,
        width: 150,
        backgroundColor: Colors.SERVICEIMAGE_BACK_COLOR,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },

    RequestTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.BLACK06,
        textAlign: 'center',
        marginTop: 10
    },

    ServiceTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.BLACK06,
        marginTop: 30
    },

    RequestNameView: {
        width: '100%',
        borderBottomWidth: 1,
        borderColor: '#DDDFDD',
        paddingVertical: 10,
        marginTop: 10
    },

    RequestNameTitle: {
        fontSize: 16,
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.PLACEHOLDER_COLOR
    },

    ConfirmButton: {
        height: 50,
        width: '100%',
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
        marginTop: 40
    },

    PlumberDetailTitle: {
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: totalSize(1.5),
        color: Colors.BLACK05,
    },

    PlumberDetailValue: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.5),
        color: '#B7B6B6',
    },

    PlumberDetailView: {
        width: width(80),
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginTop: height(3)
    },

    ServiceDetailStyle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1.9),
        color: Colors.BLACK06
    },

    renderDetailView: {
        width: width(80),
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginTop: height(3)
    },


    SheduleViewHeading: {
        fontSize: 16,
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.BLACK06
    },

    BottomView: {
        height: height(6),
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR
    },

    ProductText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 14,
        color: Colors.BLACK
    },

    PriceField: {
        height: 30, width: 100, marginLeft: width(2),
        backgroundColor: Colors.INDEX_GREY,
        paddingHorizontal: 10,
        fontSize: 14, fontFamily: Fonts.POPPINS_REGULAR
    },

    PaginationText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 12,
        color: Colors.BLACK06,
        marginTop:5,
        textAlign:'center'
    },

    BottomButtonStyle:{
        width:'90%',
        height:50,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:25,
        alignSelf:'center'
    }
})