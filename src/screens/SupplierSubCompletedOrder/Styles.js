//Global imports
import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';


//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    HeaderView: {
        height: height(7),
        paddingHorizontal: width(5),
        width:'100%',
        justifyContent:'center',
        borderBottomWidth:1,
        borderColor:Colors.GRAY
    },

    TabView:{
       paddingVertical:height(3),
       paddingHorizontal:width(5),
       flexDirection:'row',
       alignItems: 'center',
    },

    OnDemand:{
        height:height(6),
        width:width(42.5),
        justifyContent:'center',
        alignItems:'center',
        borderWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    OnDemandText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(2),
        color:Colors.BLACK06
    },

    MainWhiteView:{
        paddingBottom:height(2),
        width:'100%',
        backgroundColor:Colors.WHITE
    },
    ModalHeaderView:{
        height:height(7),
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal:width(5),
        borderBottomWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        justifyContent:'space-between'
    },

    HeadingText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:16,
        color:Colors.BLACK
    },

    RowView:{
        flexDirection:'row',
        alignItems:'center'
    },

    RadioView:{
        height:20,
        width:20,
        borderRadius:10,
        borderWidth:1,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        marginBottom:10,
        justifyContent:'center',
        alignItems:'center'
    },

    PaymentNameStyle:{
        fontSize:16,
        color:Colors.BLACK06,
        fontFamily:Fonts.POPPINS_REGULAR,
        marginLeft:width(5),
        marginBottom:height(1.5)
    },

    EnableRadio:{
        height:12,
        width:12,
        borderRadius:6,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR
    },

    PayNowButton:{
        height:50,
        width:width(70),
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'center',
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        borderRadius:25,
        marginTop:10
    }
})