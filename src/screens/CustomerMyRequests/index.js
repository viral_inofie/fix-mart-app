//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList,RefreshControl } from 'react-native';
import { width } from 'react-native-dimension';
import { RNToasty } from 'react-native-toasty';
import { useDispatch } from 'react-redux';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { GET_CUSTOMER_MY_REQUEST, PENDING_ORDER_CANCEL, PROVIDER_MY_REQUEST, RIGHT_SCROLL_INDICATOR_INSENTS, USER_OTHER_DETAILS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { PendingOrderCancelParam } from '../../Api/APIJson';


//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import MyRequestsList from '../../Components/MyRequestsList';
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import EmptyDataComponent from '../../Components/EmptyDataComponent';
import BackIcon from '../../../assets/images/backarrow.png';

const {
    TabView,
    OnDemand,
    OnDemandText
} = Styles;

const CustomerMyRequests = ({ navigation: { navigate, addListener, goBack }, route: { params: { user_type } } }) => {

    const dispatch = useDispatch();

    console.log(user_type, 'user_typeuser_type')


    const [isOnDemand, SetIsOnDemand] = useState(true);
    const [onDemandList, SetOnDemandList] = useState([]);
    const [offDemandList, SetOffDemandList] = useState([]);
    const [refreshing,setRefreshing] = useState(false)

    // useEffect(() => {
    //     dispatch(updateLoader(true))
    //     addListener('focus', () => {
    //         getRequestList()
    //     })
    // }, [])
    useEffect(() => {
        
        // addListener('focus', () => {
            
        // })
        const unsubscribe = addListener('focus', () => {
            // The screen is focused
            // Call any action
            dispatch(updateLoader(true))
            getRequestList()
          });
      
          // Return the function to unsubscribe from the event so it gets removed on unmount
          return unsubscribe;
      
    }, [navigate])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */


    const _navigateToHome = () => {
        if (user_type.type === 'provider') {
            navigate('ProviderJobRequestStack')
        }
        else {
            navigate('Home')
        }
    }

    const _navigateToDetail = (job_id) => {
        navigate('CustomerMyRequestsDetail', { job_id: job_id, user: user_type.type })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getRequestList = () => {
        setRefreshing(true)
        const apiClass = new APICallService(user_type.type === 'provider' ? PROVIDER_MY_REQUEST : GET_CUSTOMER_MY_REQUEST, {innewjob:0})

        apiClass.callAPI()
            .then(res => {
                console.log("customermyrequest",res.data)
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetOnDemandList(res.data.onDemand)
                    SetOffDemandList(res.data.offDemand)
                }
                setRefreshing(false)
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
                setRefreshing(false)
            })
    }

    const _doCancel = (job_id, status) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(PENDING_ORDER_CANCEL, PendingOrderCancelParam(job_id, status, 'myjob'))

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
                if (validateResponse(res)) {
                    dispatch(updateLoader(false))
                    getRequestList();
                    // RNToasty.Success({
                    //     title: res.message,
                    //     fontFamily: Fonts.POPPINS_REGULAR,
                    //     withIcon: false,
                    //     position: 'bottom'
                    // })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..########.##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.##.......###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.##.......####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.######...##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##.......##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##.......##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..########.##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Posted Job Request'
            LeftIcon={user_type === 'customer' ? BackIcon : BackIcon}
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: isOnDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderMyRequestList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent ErrorMessage={'No Jobs found'} />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={isOnDemand ? onDemandList : offDemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemRequestList(item, index)}
            refreshControl={<RefreshControl
                refreshing={refreshing}
                onRefresh={getRequestList} />}

        />
    }

    const renderItemRequestList = (item, index) => {
        return <MyRequestsList
            item={item}
            user_type={user_type}
            time={100 * index}
            _doCancel={(job_id, status) => _doCancel(job_id, status)}
            navigateToDetail={(job_id) => _navigateToDetail(job_id)}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}

            {renderTabView()}

            {renderMyRequestList()}
        </View>
    )
}


export default CustomerMyRequests;