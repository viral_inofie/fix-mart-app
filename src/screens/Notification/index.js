// GLobal imports
import React, { Component, useEffect, useState } from 'react'
import {
    View, TouchableOpacity, Text, FlatList,
    Image
} from 'react-native'
import * as Animatable from 'react-native-animatable'
import moment from 'moment';

// File imports
import styles from './styles'
import COLORS from '../../Helper/Colors'
import { FADE_IN_UP, NOTIFICATION } from '../../Helper/Constants'

// Component imports
import imgMenu from '../../../assets/images/Menu.png'
import BackArrow from '../../../assets/images/backarrow.png';

import Header from '@Header'
import Loader from '@Loader'
import CustomeHeader from '../../Components/CustomerHeader/index';
import EmptyDataComponent from '../../Components/EmptyDataComponent'
import { connect, useDispatch } from 'react-redux'
import { updateLoader } from '../../ReduxStore/Actions/CommonActions'
import { SHOW_LOADER } from '../../ReduxStore/Types'
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper'
import { RNToasty } from 'react-native-toasty'
import Fonts from '../../Helper/Fonts'
import NotificationList from '../../Components/NotificationList';


const Notification = (props) => {

    const { navigation: { addListener, goBack }, route : {params} } = props
    console.log(params,'notification params')
    const [notificationList, set_notificationList] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getNotification()
        })
    }, [])

    const getNotification = () => {
        const apiClass = new APICallService(NOTIFICATION, {});

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    set_notificationList(res.data)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    /*
   .##....##....###....##.....##.####..######......###....########.####..#######..##....##..######.
   .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##.##....##
   .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##.##......
   .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##..######.
   .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####.......##
   .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###.##....##
   .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##..######.
   */

    const _toggleDrawer = () => {
        goBack()
    }


    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */



    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########..######.
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##....##....##
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##....##......
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##.....######.
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##..........##
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##....##....##
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##.....######.
    */

    const renderMainView = () => {
        return (
            <View style={styles.container}>
                {renderHeader()}
                {renderFlatlist()}
            </View>
        )
    }


    const renderHeader = () => {
        return (
            <CustomeHeader
                LeftIcon={params.type === 'customer' ? null :BackArrow }
                title='Notifications'
                onLeftClick={params.UserType === 'customer'?() => _toggleDrawer() :()=> goBack()}
            />
        )
    }

    const renderLeftComponent = () => {
        return (
            <TouchableOpacity style={styles.headerBtn} onPress={this._toggleDrawer}>
                <Image source={imgMenu} style={styles.headerImage} />
            </TouchableOpacity>
        )
    }


    const renderFlatlist = () => {

        return (
            <FlatList
                style={styles.flatListStyle}
                contentContainerStyle={{ paddingBottom: 25 }}
                keyExtractor={(item, index) => index.toString()}
                data={notificationList}
                ItemSeparatorComponent={() => {
                    return <View style={styles.separator} />
                }}
                ListEmptyComponent={() => {
                    return <EmptyDataComponent />
                }}
                // ListFooterComponent={this.renderFlatListFooter}
                renderItem={({ item, index }) => renderInsuranceListCell(item, index)}
            />
        )
    }

    const renderInsuranceListCell = (item, index) => {
        return (
            <NotificationList time={index * 50} item={item} />
        )
    }


    return (
        <View style={{ flex: 1 }}>
            {renderMainView()}
        </View>
    )



}

export default Notification;
