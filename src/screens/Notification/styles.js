import { StyleSheet } from 'react-native'
import COLORS from '../../Helper/Colors'
import FONTS from '../../Helper/Fonts'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
  },
  headerText: {
    fontFamily: FONTS.POPPINS_MEDIUM,
    fontSize: 17,
  },
  headerImage: {
    width: 22,
    height: 22
  },
  headerBtn: {
    marginLeft: 7,
    padding: 5
  },
  flatListStyle: {
    flex: 1,
    marginTop: 20,
    paddingBottom: 5,
    paddingHorizontal: 20
  },
  cellContainer: {
    paddingHorizontal: 10,
    paddingVertical: 12,
    marginVertical: 10,
    flexDirection: 'row',
    backgroundColor: COLORS.WHITE,
    borderRadius: 5, shadowOffset: { width: 1, height: 1 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 3,
  },
  imgContainer: {
    height: 50,
    width: 50,
    padding: 10,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imgView: {
    backgroundColor: COLORS.APP_PRIMARY,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  name: {
    fontFamily: FONTS.POPPINS_MEDIUM,
    fontSize: 16,
    marginHorizontal: 12,
    alignSelf: 'center'
  },
  separator: {
    height: 1,
    marginLeft: 40,
    marginRight: 0,
    backgroundColor: COLORS.GRAY
  },
  cellUpperView: {
    paddingHorizontal: 8,
    flex: 3
  },
  textDesc: {
    fontFamily: FONTS.POPPINS_MEDIUM,
    fontSize: 16,
    margin: 2
  },
  text: {
    fontFamily: FONTS.POPPINS_REGULAR,
    color: COLORS.LIGHT_GRAY,
  }
})