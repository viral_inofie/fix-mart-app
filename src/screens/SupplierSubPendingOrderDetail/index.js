//Global imports
import React, { useEffect, useState } from 'react';
import {
    View, Text, TouchableOpacity, SafeAreaView,
    ImageBackground, ScrollView, TextInput
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import Foundation from 'react-native-vector-icons/Foundation';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DateTimePicker from "@react-native-community/datetimepicker"
import moment from 'moment'
import { useDispatch } from 'react-redux';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { PENDING_ORDER_DETAIL_SUBPROVIDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { PendingOrderDetailParamSubProvider } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import CustomHeader from '../../Components/CustomerHeader';
import CustomButton from '../../Components/Button/index';
import { AirbnbRating } from 'react-native-ratings';


const {
    HeaderView, HeaderTitle, ImageStyle, Title, PlumberDetailTitle,
    PlumberDetailValue, PlumberDetailView, ServiceDetailStyle,
    ViewButtonStyle, ViewText, StarView, ConfirmButton, ButtonText,
    ModelMainContainer, ModelHeader, ModelTitle, ReviewText,
    radioBtnImg, radiBtnImgCont, textInput, moneyTextInput
} = Styles;

const SupplierSubPendingOrderDetail = ({ navigation: { goBack, addListener }, route: { params } }) => {

    const [ReviewModel, SetReviewModel] = useState(false);
    const [PayLaterModel, SetPayLaterModel] = useState(false)
    const [mode, setMode] = useState('date')
    const [DoYouWantVisit, SetDoYouWantVisit] = useState(true)
    const [CommentHere, SetCommentHere] = useState('(Optional) Comment here...')
    const [Money, SetMoney] = useState('')
    const [NextInvestmentDate, SetNextInvestmentDate] = useState(new Date())
    const [isDateModalVisible, SetisDateModalVisible] = useState(false);
    const dispatch = useDispatch();

    const [Data, SetData] = useState({
        product_image: '',
        prodcut_name: '',
        service: '',
        sub_category: '',
        price: '',
        otherDetails: '',
        rating: '',
        scheduleDate: '',
        scheduleTime: ''
    })

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getOrderDetail()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => goBack();

    const _openModel = () => SetReviewModel(true);

    const _handleDatePicked = (val) => {
        console.log(val)
        let selectedDate = moment(val).format('MMM DD, YYYY')
        // this.setState({ purchasedDate: `${selectedDate}`, isModalVisible: false })
        SetNextInvestmentDate(`${selectedDate}`)
        SetisDateModalVisible(false)
    }

    const _hideDateTimePicker = () => {
        SetisDateModalVisible(false)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrderDetail = () => {
        const apiClass = new APICallService(PENDING_ORDER_DETAIL_SUBPROVIDER, PendingOrderDetailParamSubProvider(params.order_id))

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetData({
                        product_image: res.data.image,
                        prodcut_name: res.data.name,
                        scheduleDate: '',
                        scheduleTime: '',
                        service: res.data.service,
                        sub_category: res.data.sub_category,
                        price: res.data.price,
                        otherDetails: res.data.other_details,
                        rating: res.data.product_rating
                    })
                }
            })
            .catch(err => {
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? "PM" : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeader = () => {
        return <CustomHeader
            title={'Sent Offers'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: Data.product_image }}
            imageStyle={{ borderRadius: height(1.5) }}
            style={ImageStyle}
        />
    }

    const renderDemandName = () => {
        return <Text style={Title}>{Data.prodcut_name}</Text>
    }

    const renderPlumberDetailView = () => {
        return <View style={PlumberDetailView}>
            <View style={{ width: width(35) }}>
                <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Shedule Date & time :</Text>
            </View>

            <View style={{ width: width(45), }}>
                <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{moment(Data.scheduleDate).format('DD MMM, YYYY') + " " + renderTime(Data.scheduleTime)}</Text>
            </View>
        </View>
    }

    const renderServiceDetailTitle = () => {
        return <View style={[PlumberDetailView, {
            marginTop: height(3),
            height: height(4),
            borderBottomWidth: 0.5,
            borderColor: '#CCCCCC'
        }]}>
            <Text style={ServiceDetailStyle}>Service Details</Text>
        </View>
    }

    const renderServiceHeading = () => {
        return <View style={{ width: width(35) }}>
            <Text style={PlumberDetailTitle}>Service :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Sub Category :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Price :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Other Details :</Text>
            <Text style={[PlumberDetailTitle, { marginTop: height(1) }]}>Product Ratings :</Text>
        </View>
    }

    const renderStarView = () => {
        return <View style={StarView}>
            <AirbnbRating
                selectedColor={Colors.YELLOW}
                count={5}
                showRating={false}
                defaultRating={Data.rating.substring(0, 3)}
                reviewColor={Colors.PRIMARY_DARK}
                size={10}
                isDisabled
                onFinishRating={(val) => { }}
            />
            <Text style={PlumberDetailValue}>/ {Data.rating.substring(4, 6)}</Text>
        </View>
    }

    const renderViewButton = () => {
        return <CustomButton
            title='View'
            textStyle={{ fontSize: 12 }}
            textColor={Colors.BLACK}
            buttonAction={() => _openModel()}
            btnStyle={ViewButtonStyle}
        />
    }

    const renderServiceValue = () => {
        return <View style={{ width: width(45), }}>
            <Text style={PlumberDetailValue}>{Data.service}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.sub_category}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>$ {Data.price}</Text>
            <Text style={[PlumberDetailValue, { marginTop: height(1) }]}>{Data.otherDetails}</Text>

            {renderStarView()}

            {/* {renderViewButton()} */}

        </View>
    }


    const renderServiceDetail = () => {
        return <View style={PlumberDetailView}>
            {renderServiceHeading()}

            {renderServiceValue()}
        </View>
    }

    const renderPayNowButton = () => {
        return <CustomButton
            buttonAction={() => alert('hello')}
            title='Pay Now'
            btnStyle={[ConfirmButton, {
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                borderWidth: 0
            }]}
        />
    }

    const renderPayLaterButton = () => {
        return <CustomButton
            title='Pay Later'
            btnStyle={[ConfirmButton, { marginTop: 10 }]}
            buttonAction={() => SetPayLaterModel(true)}
        />
    }

    const renderCancelButton = () => {
        return <CustomButton
            title='Cancel Order'
            btnStyle={[ConfirmButton, { marginTop: 20 }]}
        />
    }

    const renderButtonView = () => {
        return <View style={{ marginTop: height(5) }}>
            {renderPayNowButton()}

            {renderPayLaterButton()}

            {/* {renderCancelButton()} */}
        </View>
    }

    const renderModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={ReviewModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <Text style={ModelTitle}>Plumber Reviews</Text>

                        <TouchableOpacity onPress={() => SetReviewModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>1. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>2. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                        <Text style={[ReviewText, { marginTop: height(2) }]}>3. mark mob was extremely polite and & genral and completed his work within time frame, great to work with him.</Text>
                    </View>
                </View>
            </View>
        </Modal>
    }

    const renderPayLaterModel = () => {
        return <Modal style={{ margin: 0 }} isVisible={PayLaterModel}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={ModelMainContainer}>
                    <View style={ModelHeader}>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={ModelTitle}>How much money</Text>
                            <TextInput
                                style={moneyTextInput}
                                value={Money}
                                onChangeText={(text) => SetMoney(text)}
                            />
                        </View>

                        <TouchableOpacity onPress={() => SetPayLaterModel(false)}>
                            <AntDesign name='close' size={totalSize(2.5)} color={Colors.BLACK} />
                        </TouchableOpacity>
                    </View>



                    <View style={{ paddingHorizontal: width(5), paddingVertical: height(2.5) }}>
                        <Text style={ReviewText}>Next Installment date</Text>

                        <TouchableOpacity activeOpacity={1}
                            style={{ height: 40, width: 100, backgroundColor: Colors.INDEX_GREY }}
                            // onPress={() => SetNextInvestmentDate(true)}
                            onPress={() =>
                                SetisDateModalVisible(!isDateModalVisible)
                            }
                        >
                            {/* <TextInput
                                editable={false}
                                style={[moneyTextInput, { width: 200 }]}
                                value={NextInvestmentDate}
                                onChangeText={(text) => SetNextInvestmentDate(text)}
                            /> */}
                        </TouchableOpacity>

                        {
                            isDateModalVisible
                            &&
                            (
                                <DateTimePicker
                                    value={NextInvestmentDate}
                                    mode={mode}
                                    onChange={(val) => _handleDatePicked(val)}
                                />
                            )

                        }

                        <CustomButton
                            title='Submit'
                            btnStyle={[ConfirmButton, {
                                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                                borderWidth: 0,
                                height: 40,
                                marginTop: 15
                            }]}
                            buttonAction={() => SetPayLaterModel(false)}
                        />
                    </View>

                </View>
            </View>
        </Modal>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeader()}

            <ScrollView
                scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
                bounces={false}
                contentContainerStyle={{ paddingHorizontal: width(5), paddingVertical: height(3) }}>
                {renderDemandImage()}

                {renderDemandName()}

                {renderPlumberDetailView()}

                {renderServiceDetailTitle()}

                {renderServiceDetail()}

                {/* {renderButtonView()} */}


            </ScrollView>

            {renderModel()}
            {renderPayLaterModel()}


        </View>
    )
}

export default SupplierSubPendingOrderDetail