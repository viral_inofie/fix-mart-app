//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, RefreshControl, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';
import { RNToasty } from 'react-native-toasty';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import { PENDING_ORDER, PENDING_ORDER_CANCEL, PENDING_ORDER_CONFIRM, PENDING_ORDER_DECLINE, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import Fonts from '../../Helper/Fonts';
import { PendingOrderCancelParam, PendingOrderDetailConfirm } from '../../Api/APIJson';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import DemandsList from '../../Components/DemandList';
import CustomHeader from '../../Components/CustomerHeader';
import CustomerOnDemandPendingList from '../../Components/CustomerOnDemandPendingList';
import CustomerOffDemandList from '../../Components/CustomerOffDemandList';
import EmptyDataComponent from '../../Components/EmptyDataComponent/index';
import { Alert } from 'react-native';

const { TabView, OnDemand, OnDemandText } = Styles;

const CustomerPendingOrder = ({ navigation: { navigate, addListener } }) => {
    const [DemandList, SetDemadList] = useState([])

    const [isOnDemand, SetIsOnDemand] = useState(true);
    const dispatch = useDispatch();
    const [OffDemandList, SetOffDemandList] = useState([]);
    const [refreshing,setRefreshing] = useState(false)

    useEffect(() => {
        
        // addListener('focus', () => {
            
        // })
        const unsubscribe = addListener('focus', () => {
            // The screen is focused
            // Call any action
            dispatch(updateLoader(true))
            getPendingOrder()
          });
      
          // Return the function to unsubscribe from the event so it gets removed on unmount
          return unsubscribe;
      
    }, [navigate])
    // useEffect(() => {

    //     dispatch(updateLoader(true))
    //     addListener('focus', () => {
    //         getPendingOrder()
    //     })

    // }, [])

    /*  
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('Home')
    }

    const _navigateToDetail = (job_id) => {
        navigate('CustomerPendingOrderDetail', { job_id })
    }

    const _navigateToOnDemandDetail = (job_id, service_provider_firstname, service_provider_lastname, profile_image) => {
        navigate('CustomerPendingOnDemandDetail', { job_id: job_id, provider_name: service_provider_firstname + " " + service_provider_lastname, profile_image })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getPendingOrder = () => {
        setRefreshing(true)
        const apiClass = new APICallService(PENDING_ORDER, {})

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false));
                setRefreshing(false)
                if (validateResponse(res)) {
                    SetOffDemandList(res.data.offDemand)
                    SetDemadList(res.data.onDemand)
                }
            })
            .catch(err => {
                setRefreshing(false)
                renderErrorToast('error', err);
                dispatch(updateLoader(false));
            })
    }

    const _doCancelOrder = (job_id, status) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(PENDING_ORDER_CANCEL, PendingOrderCancelParam(job_id, status));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    getPendingOrder();
                    RNToasty.Success({
                        title: res.message,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _doAccept = (job_id) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(PENDING_ORDER_CONFIRM, PendingOrderDetailConfirm(job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    getPendingOrder();
                    RNToasty.Success({
                        title: res.message,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })

    }

    const _doDecline = (job_id) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(PENDING_ORDER_DECLINE, PendingOrderDetailConfirm(job_id));

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR
                    })
                    getPendingOrder();
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Received Quotations'
            LeftIcon={null}
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: isOnDemand ? 0 : 1
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: !isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 0 : 1
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemDemandList = (item, index) => {
        if (isOnDemand) {
            return <CustomerOnDemandPendingList
                _navigateToDetail={(job_id, service_provider_firstname, service_provider_lastname, profile_image) => _navigateToOnDemandDetail(job_id, service_provider_firstname, service_provider_lastname, profile_image)}
                item={item}
                time={100 * index}
            />
        }
        else {
            return <CustomerOffDemandList
                item={item}
                _doDeclineOrder={(job_id) => _doDecline(job_id)}
                _doAccept={(job_id) => _doAccept(job_id)}
                _cancelOrder={(job_id, status) => {
                    Alert.alert(
                        "Cancel Job",
                        "Are you sure you want to cancel the Job?",
                        [
                            {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                            },
                            {
                                text: "OK",
                                onPress: () => { _doCancelOrder(job_id, status) }
                            }
                        ],
                        { cancelable: false }
                    )
                }}
                time={100 * index}
                navigateToDetail={(job_id) => _navigateToDetail(job_id)}
            />
        }
    }

    const renderDemandList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={isOnDemand ? DemandList : OffDemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemDemandList(item, index)}
            refreshControl={<RefreshControl
                refreshing={refreshing}
                onRefresh={getPendingOrder} />}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {renderTabView()}

            {renderDemandList()}

        </View>
    )
}

export default CustomerPendingOrder;