//global imports

import { StyleSheet } from 'react-native';

//File imports
import { WIDTH } from '../../Helper/Constants';
import Colors from '../../Helper/Colors';

export default StyleSheet.create({
    DividerView:{
        height:0.5,
        width:WIDTH - 40,
        alignSelf:'center',
        backgroundColor:Colors.PRIMARY_BORDER_COLOR
    }
})