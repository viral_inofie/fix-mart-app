//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { RNToasty } from 'react-native-toasty';
import { useDispatch } from 'react-redux';


//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles'
import Fonts from '../../Helper/Fonts';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import CustomHeader from '../../Components/CustomerHeader/index';
import MenuIcon from '../../../assets/images/Menu.png';
import CustomerConversationList from '../../Components/CustomerConversationList';
import { CHAT_CONVERSATION, RIGHT_SCROLL_INDICATOR_INSENTS, SOCKET_URL } from '../../Helper/Constants';
import { socket } from '../../../App';
import Logger from '../../Helper/Logger';
import EmptyDataComponent from '../../Components/EmptyDataComponent';

const {
    DividerView
}  = Styles;

const CustomerConversation = ({ navigation: { toggleDrawer , navigate, addListener} , route : { params : { user_type}}}) => {

    const [ConversationData, SetConversationData] = useState([])

    const disptach = useDispatch();

    useEffect(()=>{
        disptach(updateLoader(true));
        addListener('focus',()=> {
            getConversation();
        })
    },[])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToHome = () => {
        if(user_type === 'supplier'){
            navigate('SupplierHomeStack')
        }
        else if (user_type === 'provider'){
            navigate('ProviderJobRequest')
        }
        else{
            navigate('home')
        }
    }

    const _navigateToChat = (name, supplier_id) => {
        navigate('CustomerMessages',{
            supplier_id : supplier_id,
            supplier_name : name
        })
    }

    /*
    ..######...#######..##....##.##.....##.########.########...######.....###....########.####..#######..##....##
    .##....##.##.....##.###...##.##.....##.##.......##.....##.##....##...##.##......##.....##..##.....##.###...##
    .##.......##.....##.####..##.##.....##.##.......##.....##.##........##...##.....##.....##..##.....##.####..##
    .##.......##.....##.##.##.##.##.....##.######...########...######..##.....##....##.....##..##.....##.##.##.##
    .##.......##.....##.##..####..##...##..##.......##...##.........##.#########....##.....##..##.....##.##..####
    .##....##.##.....##.##...###...##.##...##.......##....##..##....##.##.....##....##.....##..##.....##.##...###
    ..######...#######..##....##....###....########.##.....##..######..##.....##....##....####..#######..##....##
    */

    const getConversation = () => {
        const apiClass = new APICallService(CHAT_CONVERSATION,{});

        apiClass.callAPI()
            .then(res => {
                disptach(updateLoader(false))
                if(validateResponse(res)){
                    SetConversationData(res.data)
                }
                else{
                    RNToasty.Error({
                        fontFamily : Fonts.POPPINS_REGULAR,
                        withIcon : false,
                        position : 'bottom',
                        title : res.message
                    })
                }
            })
            .catch(err => {
                disptach(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeader = () => {
        return <CustomHeader
            title='Messages'
            LeftIcon={user_type === 'customer' ? null : BackArrow}
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderConversation = () => {
        return <FlatList
            scrollIndicatorInsets={{right : RIGHT_SCROLL_INDICATOR_INSENTS}}
            data={ConversationData}
            ListEmptyComponent={()=>{
                return <EmptyDataComponent />
            }}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => _renderConversationList(item, index)}
        />
    }

    const _renderConversationList = (item, index) => {
        return <CustomerConversationList
            item={item}
            _navigateToChat={(name, supplier_id)=>_navigateToChat(name, supplier_id)}
            time={100 * index}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            {renderHeader()}

            {renderConversation()}
        </View>
    )
}

export default CustomerConversation;