//Global imports
import { StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainScrollView: {

    },

    ProfileContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 25,
        borderBottomWidth: 0.5,
        borderColor: Colors.PRIMARY_BORDER_COLOR
    },

    ProfileImage: {
        height: 80,
        width: 80
    },

    ProfileInnerView: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    UsernameText: {
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: 18,
        color: Colors.BLACK
    },

    UserMobile: {
        fontSize: 14,
        color: Colors.BLACK06,
        fontFamily: Fonts.POPPINS_REGULAR
    },

    PencilIcon: { width: 15, height: 15 },

    AccountTypeView: { height: 45, width: '100%', justifyContent: 'center', borderWidth:0.5, borderColor:Colors.PRIMARY_BORDER_COLOR, marginTop:15, paddingHorizontal:15}
})