import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import { useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

//File imports
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import APICallService from '../../Api/APICallService';


//Component file
import CustomHeader from '../../Components/CustomerHeader';
import Styles from './Styles';
import { ACCESS_TOKEN, LOGOUT, RIGHT_SCROLL_INDICATOR_INSENTS, USER_PROFILE } from '../../Helper/Constants';
import Pencil from '../../../assets/images/Edit.png';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';
import { RNToasty } from 'react-native-toasty';
import { Alert } from 'react-native';

const {
    MainScrollView,
    ProfileContainer,
    ProfileImage,
    ProfileInnerView,
    UsernameText,
    UserMobile,
    PencilIcon,
    AccountTypeView
} = Styles;

const More = ({ navigation: { addListener, navigate, replace } }) => {

    const dispatch = useDispatch();

    const [profileData, SetProfileData] = useState({
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        address: '',
        profile_image: ''
    });

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getProfileData()
        })
    }, [])

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getProfileData = () => {

        const apiClass = new APICallService(USER_PROFILE, {})

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {

                    dispatch(updateLoader(false));
                    SetProfileData({
                        ...profileData,
                        firstName: res.data.firstname,
                        lastName: res.data.lastname,
                        phone: res.data.phone,
                        email: res.data.email,
                        address: res.data.address,
                        profile_image: res.data.profile_image
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    const _logOutUser = () => {

        const apiClass = new APICallService(LOGOUT, {});

        apiClass.callAPI()
            .then(res => {
                if (validateResponse(res)) {
                    dispatch(updateLoader(false))
                    AsyncStorage.removeItem(ACCESS_TOKEN, (err) => {
                        console.log(err, "errrrrrrrrrr")
                        if (err === null) {
                            replace('selectUserType')
                        }
                        else {

                        }
                    })
                }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########..######.
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##....##....##
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##....##......
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##.....######.
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##..........##
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##....##....##
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##.....######.
    */

    const renderHeader = () => {
        return <CustomHeader
            title={'Account Details'}
            LeftIcon={null}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderProfileView = () => {
        return (
            <View style={ProfileContainer}>
                <View style={ProfileInnerView}>
                    <ImageBackground imageStyle={{ borderRadius: 40 }} source={{ uri: profileData.profile_image }} style={ProfileImage} />
                    <View style={{ marginLeft: 15 }}>
                        <Text style={UsernameText}>{profileData.firstName + " " + profileData.lastName}</Text>
                        <Text style={UserMobile}>{profileData.phone}</Text>
                    </View>
                </View>

                <TouchableOpacity onPress={() => navigate('CustomerEditProfile', {
                    profileData: {
                        firstName: profileData.firstName,
                        lastName: profileData.lastName,
                        email: profileData.email,
                        phone: profileData.phone,
                        address: profileData.address,
                        profile_image: profileData.profile_image
                    }
                })}>
                    <ImageBackground source={Pencil} style={PencilIcon} />
                </TouchableOpacity>

            </View>
        )
    }

    const renderAccountView = () => {
        return (
            <View style={{ padding: 25 }}>
                <Text style={UsernameText}>Manage Your Account</Text>

                <TouchableOpacity
                    onPress={() => navigate('TransactionStack', { type: 'customer' })}
                    style={[AccountTypeView, { marginTop: 25 }]}>
                    <Text style={[UsernameText, { fontFamily: Fonts.POPPINS_REGULAR, fontSize: 14 }]}>Transaction History</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigate('ChangePassword')}
                    style={AccountTypeView}>
                    <Text style={[UsernameText, { fontFamily: Fonts.POPPINS_REGULAR, fontSize: 14 }]}>Change Password</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigate('CustomerReportToAdmin', { type: 'customer' })}
                    style={AccountTypeView}>
                    <Text style={[UsernameText, { fontFamily: Fonts.POPPINS_REGULAR, fontSize: 14 }]}>Report To Admin</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => {
                    Alert.alert(
                        "Log out",
                        "Are you sure want to logout?",
                        [
                            {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                            },
                            { text: "OK", onPress: () => _logOutUser() }
                        ],
                        { cancelable: false }
                    );

                }} style={AccountTypeView}>
                    <Text style={[UsernameText, { fontFamily: Fonts.POPPINS_REGULAR, fontSize: 14 }]}>Logout</Text>
                </TouchableOpacity>
            </View>
        )
    }

    const renderMainView = () => {
        return <ScrollView
            bounces={false}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            style={MainScrollView}>
            {renderProfileView()}
            {renderAccountView()}
        </ScrollView>
    }


    return (
        <View style={{ backgroundColor: Colors.WHITE, flex: 1 }}>
            {renderHeader()}
            {renderMainView()}
        </View>
    )
}

export default More;