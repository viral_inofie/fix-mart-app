//Global imports
import { StyleSheet } from 'react-native'
import { width } from 'react-native-dimension'

//File imports
import Colors from '../../../Helper/Colors'
import Fonts from '../../../Helper/Fonts';

export default StyleSheet.create({
    main:{
        flex: 1
    },
    contaner:{
        marginHorizontal: 30,
        borderBottomWidth: 0.5,
    },
    ProfileImage: {
        height: 120,
        width: 120,
        alignSelf: 'center',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },

    CameraView: {
        height: 40,
        width: 40, justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,

    },

    InputFieldStyle:{
        height:45,
        borderBottomWidth:1,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        marginTop:20,
        paddingHorizontal:0

    },

    textFieldStyle:{
        paddingHorizontal:0

    },

    SubmitStyle:{
        height:50,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:25,
        marginTop:100
    },
    RelevantCertificate:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 25,
        marginHorizontal: 30,
        borderBottomColor: '#666',
        borderBottomWidth: 0.5,
    },
    RelevantCertificateText:{
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 16,
        color: Colors.LIGHT_GRAY
    },
    CertificateView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 25,
        marginHorizontal: 30,
        borderBottomColor: '#666',
        borderBottomWidth: 0.5,
    },

  Heading:{
    fontFamily:Fonts.POPPINS_REGULAR,
    fontSize:16,
    color:Colors.BLACK06,
    // marginLeft:width(7),
  },

  RepairingSelection:{
    height:40,
    paddingHorizontal:15,
    borderRadius:20,
    borderWidth:1,
    justifyContent:'center',
    alignItems:'center',
    marginRight:15,
    marginBottom:15,
    borderColor:Colors.PRIMARY_BORDER_COLOR
  },

  RepairingText:{
    fontFamily:Fonts.POPPINS_REGULAR,
    fontSize:14,
    color:Colors.PRIMARY_BORDER_COLOR
  },

  RepairingView:{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', paddingHorizontal:width(7) , marginTop:15},

  expandView:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    height:60,
    borderBottomWidth:1,
    borderColor:Colors.PLACEHOLDER_COLOR,
    width:width(85),
    alignSelf:'center',
    marginTop:10
  }
});