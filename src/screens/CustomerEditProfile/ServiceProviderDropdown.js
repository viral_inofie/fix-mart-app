// Global imports
import React, { Component } from 'react';
import { View, TextInput,Image } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown'

// File imports
import Styles from './Styles';
import COLORS from '../../Helper/Colors'
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { GET_SERVICE, OPTIONS, REGISTER } from '../../Helper/Constants';
import { BASE_URL, DOMAIN_URL, GET_PRODUCT_LIST, GET_SUBCATEGORY_BY_CATEGORY, GET_SUB_CATEGORY, GET_SUB_JOB_TYPE, RIGHT_SCROLL_INDICATOR_INSENTS, UPLOAD_CERTIFICATES, WIDTH } from '../../Helper/Constants'


// Component Import
import imgDownArrow from '../../../assets/images/downArrow.png'

const {
  textFieldStyle,
} = Styles;

export default class ServiceProviderDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
        serviceProvided: '',
        getServiceArray: [],
        serviceList: [],
        ServiceId: '',
        ProductList:[]
    };
  }

  componentDidMount(){
    const {serviceProvided,ServiceId} = this.props;
    this.setState({
      serviceProvided:serviceProvided,
      ServiceId:ServiceId
    })

      this.GetProduct()
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    const {serviceProvided,ServiceId} = nextProps;
    this.setState({
      serviceProvided:serviceProvided,
      ServiceId:ServiceId
    })
  }

  GetProduct() {
    const apiClass = new APICallService(GET_PRODUCT_LIST, {})

    apiClass.callAPI()
      .then(res => {
        if (validateResponse(res)) {
          let NewArray = res.data.map((item, i) => {
            item.isSelect = false;
            return item
          })
          // Logger.log('New array : => ', NewArray)
          this.setState({
            ProductList: NewArray
          }, () => {
            this.GetService()
          })
        }
      })
      .catch(err => {
        this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  GetService() {
    const apiClass = new APICallService(GET_SERVICE, {})
    apiClass.callAPI()
      .then(res => {
        // Logger.log('res : => ', res)

        if (validateResponse(res)) {

          let NewArray = res.data.map((item, i) => {
            return {
              value: item.service
            }
          })
          
          this.setState({
            getServiceArray: res.data,
            serviceList: NewArray,
            serviceProvided: res.data[0].service,
            ServiceId: res.data[0].id
          }, () => {
            // this.getRepairingCategory();
          })
        }
      })
      .catch(err => {
        // this.props.Loading(false);
        renderErrorToast('error', err)
      })
  }

  _setServiceId = (label) => {
    
    let data= this.state.getServiceArray.find(item=>item.service === label)
    if(data){
      this.setState({
        serviceProvided:label,
        ServiceId: data.id 
      })
      this.props.onPressServiceId(label,data.id)
    }else{
      this.setState({
        serviceProvided:label
      })
      this.props.onPressServiceId(label)
    }
}

  render() {
    
    return (
      <Dropdown
      rippleOpacity={0}
      data={this.state.serviceList}
      textColor={'#000'}
      baseColor={'#000'}
      value={this.state.serviceProvided}
      onChangeText={this._setServiceId}

      renderBase={() => {
          return (
              <View style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderBottomColor: '#666',
                  borderBottomWidth: 0.5,
                  marginHorizontal: 30,
                  marginTop: 30
              }}>
                  <TextInput
                      placeholderTextColor='#666'
                      style={textFieldStyle}
                      value={this.state.serviceProvided}
                      editable={false}
                      pointerEvents='none'
                  />
                  <Image source={imgDownArrow} style={{ width: 12, height: 12 }} />
              </View>
          );
      }}
  />
    );
  }
}
