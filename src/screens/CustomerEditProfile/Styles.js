//Global imports
import { StyleSheet } from 'react-native';


//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    ProfileImage: {
        height: 120,
        width: 120,
        alignSelf: 'center',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },

    CameraView: {
        height: 40,
        width: 40, justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,

    },

    InputFieldStyle:{
        height:45,
        borderBottomWidth:1,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        marginTop:20,
        paddingHorizontal:0

    },

    textFieldStyle:{
        paddingHorizontal:0

    },

    SubmitStyle:{
        height:50,
        backgroundColor:Colors.DRAWER_BACKGROUND_COLOR,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:25,
        marginTop:100
    },
    editProfileTabContainer:{
        paddingHorizontal: 25,
        paddingVertical: 4,
        flexDirection: 'row',
        marginTop: 12
    },
    editProfileTab:{
        flex: 1,
        height: 40,
        marginRight: 12,
        backgroundColor: Colors.APP_PRIMARY,
        alignItems: 'center',
        justifyContent: 'center',
    },
    editProfileTabText:{
        fontFamily: Fonts.POPPINS_REGULAR,
                        fontSize: 16
    }
})