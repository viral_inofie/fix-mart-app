//Global imports
import React, { Component, useState,useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    Platform,
    ScrollView,
    ImageBackground,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { useDispatch } from 'react-redux';

//File imports
import Colors from '../../Helper/Colors';
import { OPTIONS, PROFILE_UPDATE, RIGHT_SCROLL_INDICATOR_INSENTS} from '../../Helper/Constants';
import Styles from './Styles';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { UpdateProfileParam } from '../../Api/APIJson';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { UpdateLoginData } from '../../ReduxStore/Actions/AuthActions';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import CustomInput from '../../Components/TextField/index';
import CustomButton from '../../Components/Button/index';
import ImageUpload from '../../Components/TakePicture/index';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import PhotoUploadDialogue from '../../Components/PhotoUploadDialogue';
import { requestCameraPermission } from '../../Helper/Utills';
import ServiceProvider from './ServiceProvider/ServiceProvider';
import SupplierComponent from './Supplier/Supplier';


const {
    ProfileImage,
    CameraView,
    InputFieldStyle,
    textFieldStyle,
    SubmitStyle
} = Styles;

const CustomerEditProfile = ({ navigation: { goBack }, route: { params } }) => {

    const dispatch = useDispatch();
console.log("params.profileData",params.user_type)
    const [Profile_Image, SetProfile_Image] = useState(params.profileData.profile_image);
    const [Data, SetData] = useState({
        First_Name: params.profileData.firstName,
        Last_Name: params.profileData.lastName,
        Email: params.profileData.email,
        PhoneNumber: params.profileData.phone,
        Address: params.profileData.address,
        profile: params.profileData.profile_image
    })
    const [photoModalOpen, setPhotoModalOpen] = useState(false);
    const [selectedBtn, setSelectedBtn] = useState('main')
    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => {
        goBack();
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _uploadProfileImage = () => {
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                // this.setState({
                //     avatarSource: source,
                // });
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let image = {
                    name: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    type: response.type,
                    uri: response.uri

                }
                SetProfile_Image(image)
                SetData({ ...Data, profile: response.uri })
            }
        });
    }

    const _onChangeFirstName = (text) => SetData({ ...Data, First_Name: text });

    const _onChangeLastName = (text) => SetData({ ...Data, Last_Name: text });

    const _onChangeEmail = (text) => SetData({ ...Data, Email: text });

    const _onChangePhoneNumber = (text) => SetData({ ...Data, PhoneNumber: text });

    const _onChangeAddress = (text) => SetData({ ...Data, Address: text });

    const _updateProfile = () => {

        const { First_Name, Last_Name, Email, PhoneNumber, Address } = Data;
        dispatch(updateLoader(true))

        const apiClass = new APICallService(PROFILE_UPDATE, UpdateProfileParam(1,First_Name, Last_Name, PhoneNumber, Address, Email, Profile_Image))

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    dispatch(UpdateLoginData(res.data))
                    RNToasty.Success({
                        title: res.message,
                        position: 'bottom',
                        fontFamily: Fonts.POPPINS_REGULAR
                    })
                    _navigateToBack()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })

    }

    const _open_camera = () => {
        launchCamera(OPTIONS, (response) => {
            if (response.didCancel) {
                setPhotoModalOpen(false)
            }
            else {
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let image = {
                    name: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    type: response.type,
                    uri: response.uri
                }
                SetProfile_Image(image)
                SetData({ ...Data, profile: response.uri })
                setPhotoModalOpen(false)
            }
        });

    }

    const _open_gallery = () => {
        launchImageLibrary(OPTIONS, (response) => {
            if (response.didCancel) {
                setPhotoModalOpen(false)
            }
            else {
                let IosFileName = response.uri.substring(response.uri.toString().lastIndexOf('/') + 1, response.uri.toString().length);
                let image = {
                    name: Platform.OS === 'ios' ? IosFileName : response.fileName,
                    type: response.type,
                    uri: response.uri
                }
                SetProfile_Image(image)
                SetData({ ...Data, profile: response.uri })
                setPhotoModalOpen(false)
            }
        });

    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderHeaderView = () => {
        return <CustomHeader
            title={'Edit Profile'}
            onLeftClick={() => _navigateToBack()}
        />
    }

    const renderDetailView = () => {
        return <ScrollView
            contentContainerStyle={{ padding: 20 }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}>
            {renderProfileImage()}
            {renderFirstName()}
            {renderLastName()}
            {renderEmail()}
            {renderPhoneNumber()}
            {/* {renderAddress()} */}
            {renderSubmitButton()}
        </ScrollView>
    }

    const renderProfileImage = () => {
        return <ImageBackground
            source={{ uri: Data.profile }}
            style={ProfileImage}
            imageStyle={{ borderRadius: 60 }}>
                <ImageUpload 
                    isShow={false}
                    cropperCircleOverlay={true}
                    onPress={(ImageObj)=>{
                        SetProfile_Image(ImageObj)
                        SetData({ ...Data, profile: ImageObj.uri })
                    }}
                    style={CameraView}
                >
                    <Entypo name='camera' size={20} color={Colors.WHITE} />
                </ImageUpload>
            {/* {renderCameraIcon()} */}
        </ImageBackground>
    }

    const renderCameraIcon = () => {
        return <TouchableOpacity
            onPress={() => {
                if (requestCameraPermission()) {
                    setPhotoModalOpen(true)
                }
            }}
            activeOpacity={1}
            style={CameraView}>
            <Entypo name='camera' size={20} color={Colors.WHITE} />
        </TouchableOpacity>
    }

    const renderFirstName = () => {
        return <CustomInput
            containerView={[InputFieldStyle, { marginTop: 40 }]}
            inputStyle={textFieldStyle}
            placeholder='First Name'
            leftimgStyle={{ marginRight: 0 }}
            value={Data.First_Name}
            onChangeText={(text) => _onChangeFirstName(text)}
        />
    }

    const renderLastName = () => {
        return <CustomInput
            containerView={InputFieldStyle}
            inputStyle={textFieldStyle}
            placeholder='Last Name'
            leftimgStyle={{ marginRight: 0 }}
            value={Data.Last_Name}
            onChangeText={(text) => _onChangeLastName(text)}
        />
    }

    const renderEmail = () => {
        return <CustomInput
            containerView={InputFieldStyle}
            inputStyle={textFieldStyle}
            placeholder='Email Address'
            keyboardType='email-address'
            leftimgStyle={{ marginRight: 0 }}
            value={Data.Email}
            onChangeText={(text) => _onChangeEmail(text)}
        />
    }

    const renderPhoneNumber = () => {
        return <CustomInput
            containerView={InputFieldStyle}
            inputStyle={textFieldStyle}
            placeholder='Phone Number'
            keyboardType='numeric'
            leftimgStyle={{ marginRight: 0 }}
            value={Data.PhoneNumber}
            onChangeText={(text) => _onChangePhoneNumber(text)}
        />
    }

    // const renderAddress = () => {
    //     return <CustomInput
    //         containerView={[InputFieldStyle, { height: 100 }]}
    //         inputStyle={textFieldStyle}
    //         placeholder='Address'
    //         leftimgStyle={{ marginRight: 0 }}
    //         value={Data.Address}
    //         multiline
    //         maxLength={256}
    //         onChangeText={(text) => _onChangeAddress(text)}
    //     />
    // }

    const renderSubmitButton = () => {
        return <CustomButton
            btnStyle={SubmitStyle}
            title='Submit'
            buttonAction={() => _updateProfile()}
        />
    }

    const renderPhotoModal = () => (
        <PhotoUploadDialogue
            closeModel={() => setPhotoModalOpen(false)}
            openCamera={() => _open_camera()}
            openGallery={() => _open_gallery()}
            isShow={photoModalOpen}
        />
    )

    const renderHeaderButtons = () => {
        return (
            <View style={Styles.editProfileTabContainer}>
                <TouchableOpacity style={[Styles.editProfileTab, { backgroundColor: selectedBtn == 'main' ? Colors.APP_PRIMARY : Colors.WHITE }]} onPress={() => setSelectedBtn('main')}>
                    <Text style={Styles.editProfileTabText}>Profile</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[Styles.editProfileTab, { backgroundColor: selectedBtn == 'serviceProvider' ? Colors.APP_PRIMARY : Colors.WHITE }]} onPress={() => setSelectedBtn('serviceProvider')}>
                    <Text style={Styles.editProfileTabText}>{params.profileData.user_type==="provider"?"Service Provider":"Supplier"}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: Colors.WHITE }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
                {renderHeaderView()}
                
                {console.log("params.profileData.user_type",params.profileData.user_type)}
                {
                    params.profileData.user_type===undefined?
                    <React.Fragment>
                        {renderDetailView()}
                    </React.Fragment>
                    :
                    <React.Fragment>
                        {renderHeaderButtons()}
                        {
                            selectedBtn == 'main' ? 
                            renderDetailView() : 
                            (
                                params.profileData.user_type!==undefined?
                                <React.Fragment>
                                <ServiceProvider navigateToBack={_navigateToBack} user_type={params.profileData.user_type}/>
                                </React.Fragment>
                                :
                                <Text></Text>
                            )
                            
                        }
                    </React.Fragment>
                }
                
                {renderPhotoModal()}

                {/* {selectedBtn == 'main' ? renderPhotoModal() : renderServiceProviderPhotoModal()} */}
            </View>
        </KeyboardAvoidingView>
    )
}

export default CustomerEditProfile;