//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, RefreshControl, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import CustomHeader from '../../Components/CustomerHeader';
import CompletedOrderList from '../../Components/CompletedOrderList';
import { COMPLETED_ORDER_CUSTOMER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import EmptyDataComponent from '../../Components/EmptyDataComponent/index';

const { TabView, OnDemand, OnDemandText } = Styles;

const CustomerCompletedOrder = ({ navigation: { navigate, addListener } }) => {
    const [DemandList, SetDemadList] = useState([])

    const [isOnDemand, SetIsOnDemand] = useState(true);
    const [OffDemandList, SetOffDemandList] = useState([]);
    const [refreshing,setRefreshing] = useState(false)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));
        addListener('focus', () => {
            getOrder()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('Home')
    }

    const _navigateToDetail = (type, job_id) => {
        navigate('CustomerCompleteOrderDetail', {
            type: type,
            job_id: job_id
        })
    }


    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getOrder = () => {
        setRefreshing(true)
        const apiClass = new APICallService(COMPLETED_ORDER_CUSTOMER, {});

        apiClass.callAPI()
            .then(res => {
                setRefreshing(false)
                dispatch(updateLoader(false));
                if (validateResponse(res)) {
                    SetOffDemandList(res.data.offDemand)
                    SetDemadList(res.data.onDemand)
                }
            })
            .catch(err => {
                setRefreshing(false)
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Completed Offers'
            LeftIcon={null}
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: isOnDemand ? 0 : 1
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: !isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 0 : 1
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemOrderList = (item, index) => {
        if (isOnDemand) {
            return <CompletedOrderList
                item={item}
                onDemand={true}
                time={100 * index}
                navigateToDetail={(job_id) => _navigateToDetail('ondemand', job_id)}
            />
        }
        else {
            return <CompletedOrderList
                item={item}
                time={100 * index}
                navigateToDetail={(job_id) => _navigateToDetail('offdemand', job_id)}
            />
        }
    }

    const renderOrderList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={isOnDemand ? DemandList : OffDemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemOrderList(item, index)}
            refreshControl={<RefreshControl
                refreshing={refreshing}
                onRefresh={getOrder} />}
        />
    }

   

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {renderTabView()}

            {renderOrderList()}

         

        </View>
    )
}

export default CustomerCompletedOrder;