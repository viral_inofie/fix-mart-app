import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    HeaderView: {
        height: height(7),
        paddingHorizontal: width(5),
        width:'100%',
        borderBottomWidth:1,
        borderColor:Colors.GRAY,
        alignItems: 'center',
        flexDirection:'row'
    },

    TabView:{
       paddingVertical:height(3),
       paddingHorizontal:width(5),
       flexDirection:'row',
       alignItems: 'center',
    },

    OnDemand:{
        height:height(6),
        width:width(42.5),
        justifyContent:'center',
        alignItems:'center',
        borderWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    OnDemandText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(2),
        color:Colors.BLACK06
    },

    HeaderTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(2.5),
        color: Colors.BLACK06,
        marginLeft: width(5)
    },
})