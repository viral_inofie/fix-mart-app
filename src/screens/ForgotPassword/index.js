//Global imports
import React, { Component, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import Fonts from '../../Helper/Fonts';
import { EMAIL_REGEX, FORGOT_PASSWORD, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';


//Component import
import LOGO from '../../../assets/images/logo.png';
import BackArrow from '../../../assets/images/backarrow.png';
import PhoneIcon from '../../../assets/images/phone.png';
import CustomInput from '../../Components/TextField/index';
import CustomButton from '../../Components/Button/index';
import ErrorModal from '../../Components/ErrorModal/index';
import { RNToasty } from 'react-native-toasty';
import MsgIcon from '../../../assets/images/msg.png'

const {
  BackView,
  ImageLogoStyle,
  InputField,
  SubmitButton,
  ForgotPasswordTextStyle,
  FieldIcon,
} = Styles;

const ForgotPassword = ({ navigation: { navigate, goBack } }) => {
  const dispatch = useDispatch();

  const [Data, SetData] = useState({
    Phone: '',
  })

  const [ErrorValidation, SetErrorValidation] = useState({
    Message: '',
    ShowErrorModel: ''
  })


  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */

  const _navigateToBack = () => goBack()

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */

  const _onChangeMobileField = (text) => SetData({ ...Data, Phone: text });

  const _doForgetPassword = () => {
    if (_Validate()) {
      // goBack()
      APICall()
    }
  }

  const _Validate = () => {
    const { Phone } = Data;

    if (!EMAIL_REGEX.test(Phone)) {
      SetErrorValidation({ ...ErrorValidation, Message: 'Enter valid email', ShowErrorModel: true })
      return false
    } else {
      return true
    }
  }

  const _CloseValidationModel = () => {
    SetErrorValidation({ ...ErrorValidation, ShowErrorModel: false })
  }

  const APICall = () => {
    const { Phone } = Data;

    dispatch(updateLoader(true));

    const apiClass = new APICallService(FORGOT_PASSWORD, { email: Phone })

    apiClass.callAPI()
      .then(res => {
        dispatch(updateLoader(false));

        if (validateResponse(res)) {
          goBack();
          RNToasty.Success({
            title: res.message,
            withIcon: false,
            position: 'bottom',
            fontFamily: Fonts.POPPINS_REGULAR
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }

  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */

  const renderBackView = () => {
    return <View style={BackView}>
      <TouchableOpacity onPress={() => _navigateToBack()}>
        <ImageBackground
          resizeMode='contain'
          source={BackArrow}
          style={FieldIcon}
        />
      </TouchableOpacity>
    </View>
  }

  const renderLogoImage = () => {
    return <ImageBackground
      source={LOGO}
      style={ImageLogoStyle}
    />
  }

  const renderForgotPasswordText = () => {
    return <Text style={ForgotPasswordTextStyle}>Forgot Password</Text>
  }

  const renderMobileField = () => {
    return <CustomInput
      containerView={InputField}
      leftImg={MsgIcon}
      keyboardType='default'
      placeholder='Enter Email'
      returnKeyType='done'
      onChangeText={(text) => _onChangeMobileField(text)}
    />
  }

  const renderSubmitButton = () => {
    return <CustomButton
      title='Submit'
      buttonAction={() => _doForgetPassword()}
      btnStyle={SubmitButton}
      textColor={Colors.BLACK}
    />
  }

  const renderErrorModel = () => {
    return <ErrorModal
      message={ErrorValidation.Message}
      visible={ErrorValidation.ShowErrorModel}
      handleBack={() => _CloseValidationModel()}
    />
  }


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.WHITE }}>
      <View style={{ flex: 1, }}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 20}
          behavior={Platform.OS === 'ios' ? 'padding' : null}>

          <ScrollView
            bounces={false}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            contentContainerStyle={{ paddingVertical: height(5), paddingHorizontal: width(10) }}>

            {renderBackView()}
            {renderLogoImage()}
            {renderForgotPasswordText()}
            {renderMobileField()}
            {renderSubmitButton()}
            {renderErrorModel()}

          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    </SafeAreaView>
    
  )
}

export default ForgotPassword;