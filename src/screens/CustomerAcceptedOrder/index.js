//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, RefreshControl, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import { useDispatch } from 'react-redux';
import { RNToasty } from 'react-native-toasty';
import stripe from 'tipsi-stripe';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import {
    ACCEPTED_ORDER,
    CUSTOMER_ACCEPTED_CANCEL,
    RIGHT_SCROLL_INDICATOR_INSENTS,
    PUBLISHABLE_KEY,
    SECRET_KEY,
    ANDROID_MODE,
    STRIPE_PAY,
    CUSTOMER_PAY_NOW
} from '../../Helper/Constants';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions'
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper'
import Fonts from '../../Helper/Fonts';
import { CustomerAccpetedOrderCancelParam, CustomerPayNowParam } from '../../Api/APIJson';

//Component imports
import CustomHeader from '../../Components/CustomerHeader';
import AcceptedOrderList from '../../Components/AcceptedOrderList/index';
import CustomerAcceptedOnDemandList from '../../Components/CustomerAcceptedOnDemandList ';
import EmptyDataComponent from '../../Components/EmptyDataComponent';
import { Alert } from 'react-native';


const { TabView, OnDemand, OnDemandText } = Styles;


stripe.setOptions({
    publishableKey: PUBLISHABLE_KEY,
    merchantId: SECRET_KEY,
    androidPayMode: ANDROID_MODE,
})

const CustomerAcceptedOrder = ({ navigation: { navigate, addListener } }) => {
    const [DemandList, SetDemadList] = useState([])

    const [isOnDemand, SetIsOnDemand] = useState(true);

    const [OffDemandList, SetOffDemandList] = useState([]);
    const [refreshing,setRefreshing] = useState(false)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getAcceptedOrder()
        })
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('Home')
    }

    const _navigateToDetail = (job_id) => {
        navigate('CustomerAcceptedOrderDetail', { job_id })
    }

    const _navigateToOnDemandDetail = (job_id) => {
        navigate('CustomerAcceptedOnDemandDetail', { job_id })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getAcceptedOrder = () => {
        setRefreshing(true)
        const apiClass = new APICallService(ACCEPTED_ORDER, {})

        apiClass.callAPI()
            .then(res => {
                setRefreshing(false)
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    SetOffDemandList(res.data.offDemand)
                    SetDemadList(res.data.onDemand)
                }
            })
            .catch(err => {
                setRefreshing(false)
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _doCancelOrder = (job_id, status) => {

        dispatch(updateLoader(true))

        const apiClass = new APICallService(CUSTOMER_ACCEPTED_CANCEL, CustomerAccpetedOrderCancelParam(job_id, status, 'confirm'))

        apiClass.callAPI()
            .then(res => {
                dispatch(updateLoader(false))
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom',
                    })
                    getAcceptedOrder()
                }
            })
            .catch(err => {
                dispatch(updateLoader(false))
                renderErrorToast('error', err)
            })
    }

    const _doPayNow = (token, job_id, charge, status) => {
        dispatch(updateLoader(true));

        const apiClass = new APICallService(CUSTOMER_PAY_NOW, CustomerPayNowParam(job_id, charge, 0, token, { isTransportation: 1 }));

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'res')
               
                if (validateResponse(res)) {
                    RNToasty.Success({
                        title: res.message.toString(),
                        fontFamily: Fonts.POPPINS_REGULAR,
                        withIcon: false,
                        position: 'bottom'
                    })
                    _doCancelOrder(job_id, status)
                }
            })
            .catch(err => {
                console.log(err, 'err')
                dispatch(updateLoader(false));
                // renderErrorToast('error', err)
            })
    }

    const StripePay = (token, job_id, charge, status) => {
        dispatch(updateLoader(true))
        const apiClass = new APICallService(STRIPE_PAY, { amount: charge, token: token });

        apiClass.callAPI()
            .then(res => {
                console.log(res, 'stripe pay')
                if (validateResponse(res)) {
                    _doPayNow(res.data.txn_id, job_id, charge, status)
                }
                else {
                    RNToasty.Error({
                        title: res.message,
                        withIcon: false,
                        fontFamily: Fonts.POPPINS_REGULAR,
                        position: 'bottom'
                    })
                }
            })
            .catch(err => {
                console.log(err,'errrrrrrr')
                renderErrorToast('error', err);
                dispatch(updateLoader(false))
            })
    }

    const generateStripeToken = async (job_id, charge, status) => {
        const token = await stripe.paymentRequestWithCardForm()
        console.log(token, 'stripe token')
        StripePay(token.tokenId, job_id, charge, status)
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Accepted Offers'
            LeftIcon={null}
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity
                onPress={() => SetIsOnDemand(true)}
                style={[OnDemand, {
                    backgroundColor: isOnDemand ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                    borderWidth: !isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => SetIsOnDemand(false)}
                style={[OnDemand, {
                    marginLeft: width(5),
                    backgroundColor: isOnDemand ? 'transparent' : Colors.DRAWER_BACKGROUND_COLOR,
                    borderWidth: isOnDemand ? 1 : 0
                }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemOrderList = (item, index) => {
        if (isOnDemand) {
            return <CustomerAcceptedOnDemandList
                item={item}
                time={100 * index}
                _doCancelOrder={(job_id, charge, status) => {
                    Alert.alert(
                        "Cancel Job",
                        "Are you sure you want to cancel the Job?",
                        [
                            {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                            },
                            {
                                text: "OK",
                                onPress: () => {
                                    Alert.alert(
                                        "Cancel Order",
                                        "You will be charged for provider transportation cost ?",
                                        [
                                            {
                                                text: "Cancel",
                                                onPress: () => console.log("Cancel Pressed"),
                                                style: "cancel"
                                            },
                                            { text: "OK", onPress: () => generateStripeToken(job_id, charge, status) }
                                        ],
                                        { cancelable: false }
                                    )
                                }
                            }
                        ],
                        { cancelable: false }
                    );
                }}
                navigateToDetail={(job_id) => _navigateToOnDemandDetail(job_id)}
            />
        }
        else {
            return <AcceptedOrderList
                item={item}
                time={100 * index}
                _doCancelOrder={(job_id, charge, sub_job_type, status) => {
                    if (sub_job_type === 3) {
                        Alert.alert(
                            "Cancel Job",
                            "Are you sure you want to cancel the Job?",
                            [
                                {
                                    text: "Cancel",
                                    onPress: () => console.log("Cancel Pressed"),
                                    style: "cancel"
                                },
                                {
                                    text: "OK",
                                    onPress: () => { _doCancelOrder(job_id, status)}
                                }
                            ],
                            { cancelable: false }
                        );
                    }
                    else {
                        Alert.alert(
                            "Cancel Job",
                            "Are you sure you want to cancel the Job?",
                            [
                                {
                                    text: "Cancel",
                                    onPress: () => console.log("Cancel Pressed"),
                                    style: "cancel"
                                },
                                {
                                    text: "OK",
                                    onPress: () => {
                                        Alert.alert(
                                            "Cancel Order",
                                            "You will be charged for provider transportation cost ?",
                                            [
                                                {
                                                    text: "Cancel",
                                                    onPress: () => console.log("Cancel Pressed"),
                                                    style: "cancel"
                                                },
                                                { text: "OK", onPress: () => generateStripeToken(job_id, charge, status) }
                                            ],
                                            { cancelable: false }
                                        )
                                    }
                                }
                            ],
                            { cancelable: false }
                        );
                    }
                }}
                navigateToDetail={(job_id) => _navigateToDetail(job_id)}
            />
        }
    }

    const renderOrderList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={isOnDemand ? DemandList : OffDemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemOrderList(item, index)}
            refreshControl={<RefreshControl
                refreshing={refreshing}
                onRefresh={getAcceptedOrder} />}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {renderTabView()}

            {renderOrderList()}

        </View>
    )
}

export default CustomerAcceptedOrder;