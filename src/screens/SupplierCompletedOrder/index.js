//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, ImageBackground, FlatList } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';

//Component imports
import Demand1 from '../../../assets/images/demand1.png';
import Demand2 from '../../../assets/images/demand2.png';
import Demand3 from '../../../assets/images/demand3.png';
import Demand4 from '../../../assets/images/demand4.png';
import CustomHeader from '../../Components/CustomerHeader';
import CompletedOrderList from '../../Components/CompletedOrderList';
import { COMPLETE_ORDER_SUBPROVIDER, RIGHT_SCROLL_INDICATOR_INSENTS } from '../../Helper/Constants';
import SupplierCompletedOrderList from '../../Components/SupplierCompletedOrderList';
import { useDispatch } from 'react-redux';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { RNToasty } from 'react-native-toasty';
import Fonts from '../../Helper/Fonts';
import EmptyDataComponent from '../../Components/EmptyDataComponent';


const { TabView, OnDemand, OnDemandText } = Styles;

const SupplierCompletedOrder = ({ navigation: { navigate, addListener } }) => {
    const [DemandList, SetDemadList] = useState([])

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateLoader(true));

        addListener('focus', () => {
            getCompletedOrder();
        })
    }, [])




    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */
    const _navigateToHome = () => {
        navigate('SupplierHomeStack')
    }

    const _navigateToDetail = (order_id) => {
        navigate('SupplierCompletedOrderDetail', { order_id })
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const getCompletedOrder = () => {
        const apiClass = new APICallService(COMPLETE_ORDER_SUBPROVIDER, {});

        apiClass.callAPI()
            .then(res => {
                if (dispatch(updateLoader(false)))
                    if (validateResponse(res)) {
                        SetDemadList(res.data)
                    }
                    else {
                        RNToasty.Error({
                            title: res.message,
                            withIcon: false,
                            fontFamily: Fonts.POPPINS_REGULAR,
                            position: 'bottom'
                        })
                    }
            })
            .catch(err => {
                dispatch(updateLoader(false));
                renderErrorToast('error', err)
            })
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderHeaderView = () => {
        return <CustomHeader
            title='Completed Offers'
            onLeftClick={() => _navigateToHome()}
        />
    }

    const renderTabView = () => {
        return <View style={TabView}>
            <TouchableOpacity style={OnDemand}>
                <Text style={OnDemandText}>On Demand</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[OnDemand, { marginLeft: width(5), backgroundColor: Colors.DRAWER_BACKGROUND_COLOR, borderWidth: 0 }]}>
                <Text style={OnDemandText}>Off Demand</Text>
            </TouchableOpacity>
        </View>
    }

    const renderItemOrderList = (item, index) => {
        return <SupplierCompletedOrderList
            isSupplier
            item={item}
            time={100 * index}
            navigateToDetail={(order_id) => _navigateToDetail(order_id)}
        />
    }

    const renderOrderList = () => {
        return <FlatList
            ListEmptyComponent={() => {
                return <EmptyDataComponent />
            }}
            scrollIndicatorInsets={{ right: RIGHT_SCROLL_INDICATOR_INSENTS }}
            bounces={false}
            data={DemandList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => renderItemOrderList(item, index)}
        />
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>

            {renderHeaderView()}

            {/* {renderTabView()} */}

            {renderOrderList()}

        </View>
    )
}

export default SupplierCompletedOrder;