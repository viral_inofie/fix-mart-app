

export const LoginParam = (email, password, fcm_token) => {
    return { email, password, fcm_token }
}

export const UpdateProfileParam = (isprofile,firstname, lastname, phone, address, email, profile_image) => {
    return { isprofile,firstname, lastname, phone, address, email, profile_image }
}

export const RegisterParam = (user_type, firstname, lastname, phone, email, password) => {
    return { user_type, firstname, lastname, phone, email, password }
}

export const PostOrderParam = (category_id, sub_category_id, type_product, other_details, quantity, job_type, need_to_done, visit_date, visit_time, sub_job_type, latitude, longitude, sort_address, address) => {
    return { category_id, sub_category_id, type_product, other_details, quantity, job_type, need_to_done, visit_date, visit_time, sub_job_type, latitude, longitude, sort_address, address }
}

export const ProviderRegisterParam = (user_type, firstname, lastname, phone, company, email, password, service_id, document_file, CategoryIdParam, SubCategoryIdParam, ProductIdParam) => {
    let newObj = { user_type, firstname, lastname, phone, company, email, password, service_id, }
    return { ...newObj, ...document_file, ...CategoryIdParam, ...SubCategoryIdParam, ...ProductIdParam }
}

export const ProviderUpdateParam = (isprofile,user_type, company, service_id, document_file, CategoryIdParam, SubCategoryIdParam, ProductIdParam) => {
    let newObj = { isprofile,user_type, company, service_id, }
    return { ...newObj, ...document_file, ...CategoryIdParam, ...SubCategoryIdParam, ...ProductIdParam }
}

export const SupplierUpdateParam = (isprofile,user_type, comapanyName,  comapanyDetails, document_file) => {
    let newObj = { isprofile,user_type, company:comapanyName,  company_details:comapanyDetails }
    return { ...newObj, ...document_file }
}

export const SupplierRegisterParam = (user_type, firstname, lastname, phone, company,comapanyDetail, email, password, service_id, ProductIdParam) => {
    let newObj = { user_type, firstname, lastname, phone, company,company_details:comapanyDetail, email, password, service_id, }
    return { ...newObj, ...ProductIdParam }
}

export const JobDetailParam = (job_id) => {
    return { job_id }
}

export const ProductDetailParam = (product_id) => {
    return { product_id }
}

export const ProductSentToCustomerParam = (job_id, product_id, price) => {
    return { job_id, product_id, price }
}

export const PendingOrderDetailParam = (pending_job_id) => {
    return { pending_job_id }
}

export const PendingOrderDetailConfirm = (pending_job_id) => {
    return { pending_job_id }
}

export const PendingOrderCancelParam = (job_id, status, stag) => {
    return { job_id: job_id, status: status, stag: stag }
}

export const AcceptedOrderDetailParam = (job_id) => {
    return { job_id }
}

export const CustomerPayNowParam = (job_id, amount, paymentMethod, token, forTransportOnly) => {
    let obj = { job_id, amount, paymentMethod, token }
    return { ...obj, ...forTransportOnly }
}

export const PlaceOrderWithSupplierParam = (job_id) => {
    return { job_id }
}

export const PendingOrderDetailParamSubProvider = (order_id) => {
    return { order_id }
}

export const PendingOrderDetailParamSupplier = (order_id) => {
    return { order_id }
}

export const SupplierAcceptOrderParam = (order_id) => {
    return { order_id }
}

export const SupplierDeclinedOrderParam = (order_id) => {
    return { order_id }
}

export const ProductDeliverOrderParam = (order_id) => {
    return { order_id }
}

export const SupplierOrderAcceptedParam = (order_id) => {
    return { order_id }
}

export const SubProviderAcceptedOrderDetailParam = (order_id) => {
    return { order_id }
}

export const CustomerAccpetedOrderCancelParam = (job_id, status, stag) => {
    return { job_id: job_id, status: status, stag: stag }
}

export const PayNowProviderParam = (order_id, paymentType, paymentMethod, token, amount, due_date) => {
    return { order_id, paymentType, paymentMethod, token, amount, due_date }
}



export const ReviewProductParam = (order_id) => {
    return { order_id }
}

export const StartServiceParam = (job_id) => {
    return { job_id }
}

export const ProviderCompleteOrderParam = (job_id) => {
    return { job_id }
}

export const CustomerCompletedJobParam = (job_id, satisfied) => {
    return { job_id, satisfied }
}

export const CustomerDisputedJobParam = (job_id, note) => {
    return { job_id, note }
}

export const serviceParam = (service_id, type) => {
    return { service_id, type }
}

export const setAnswer = (job_id, answerObj) => {
    let newObj = { job_id }
    return { ...newObj, ...answerObj }
}

export const checkEmailPhoneParam = (email, phone) => {
    return { email, phone }
}

export const UpdateAnswerParam = (job_id, answerParam) => {
    let newObj = { job_id }
    return { ...newObj, ...answerParam }
}