import { Platform } from 'react-native'

export default {
  POPPINS_BOLD: Platform.OS === 'ios' ? 'Poppins-Bold' : 'Poppins-Bold',
  POPPINS_LIGHT: Platform.OS === 'ios' ? 'Poppins-Light' : 'Poppins-Light',
  POPPINS_REGULAR: Platform.OS === 'ios' ? 'Poppins-Regular' : 'Poppins-Regular',
  POPPINS_ITALIC: Platform.OS === 'ios' ? 'Poppins-Italic' : 'Poppins-Italic',
  POPPINS_MEDIUM: Platform.OS === 'ios' ? 'Poppins-Medium' : 'Poppins-Medium',

}