
const EnableLog = __DEV__;

const Logger = {
    error(message) {
        if (EnableLog)
            console.error(message);
            console.log(
                `%c Error`,
                `background:red;
                color:white;
                line-height: 30px;
                font-weight: bold;
                font-size: 12px;
                border: 1px clear;
                padding-right:5px;
                border-radius: 0px 15px 15px 0px;`,
                message
            );
    },
    info(message) {
        if (EnableLog)
            console.log(
                `%c Info`,
                `background:orange;
                color:white;
                line-height: 30px;
                font-weight: bold;
                font-size: 12px;
                border: 1px clear;
                padding-right:5px;
                border-radius: 0px 15px 15px 0px;`,
                message
            );
    },
    log(message, message2) {
        if (EnableLog)
            if (message2)
                console.log(
                    `%c ${message}`,
                    `background:white;
                    color:black;
                    line-height: 30px;
                    font-weight: bold;
                    font-size: 12px;
                    border: 1px clear;
                    padding-right:5px;
                    border-radius: 0px 15px 15px 0px;`,
                    message2
                );
            else
                // console.log(message);
                console.log(
                    `%c Error`,
                    `background:white;
                    color:black;
                    line-height: 30px;
                    font-weight: bold;
                    font-size: 12px;
                    border: 1px clear;
                    padding-right:5px;
                    border-radius: 0px 15px 15px 0px;`,
                    message
                );
    },
    warn(message) {
        if (EnableLog)
            console.log(
                `%c Warn`,
                `background:yellow;
                color:black;
                line-height: 30px;
                font-weight: bold;
                font-size: 12px;
                border: 1px clear;
                padding-right:5px;
                border-radius: 0px 15px 15px 0px;`,
                message
            );
    },
    trace(message) {
        if (EnableLog)
            console.trace(message);
    },
    debug(message) {
        if (EnableLog)
            console.debug(message);
    },
    table(message) {
        if (EnableLog)
            console.table(message);
    },
}
export default Logger;