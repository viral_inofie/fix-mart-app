import { Dimensions, Platform } from 'react-native'
import { CommonActions } from '@react-navigation/native'
import * as Utils from '../Helper/Utills'

// Header size accordingly platform
export const headerMarginTop = Platform.OS === 'ios' ? 0 : 0

export const WIDTH = Dimensions.get('window').width
export const HEIGHT = Dimensions.get('window').height

export const EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

// BASE URL
export const DOMAIN_URL = 'https://fix-mart.com/'
export const BASE_URL = DOMAIN_URL + 'api/'
export const SOCKET_URL = "https://fix-mart.com:2323"

// STATUS BAR TYPE
export const STATUS_BAR_TYPE = 'dark-content'

// SENTRY URL
export const SENTRY_URL = 'https://cd56732dafb54c4081f6dd90e7582a5d@o326473.ingest.sentry.io/5436899'

// LOTTIE_ANIMATION_FILE
export const LOTTIE_ANIMATION_FILE = require('../../loader1.json')

// HAPTIC FEEDBACK CONFIGs
export const HAPTIC_CONFIG = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: true
}
export const HAPTIC_FEEDBACK_FAILED = "notificationError"
export const HAPTIC_FEEDBACK_SUCCESS = "impactLight"

// ANIMATION
export const LIGHT_SPEED_IN = 'fadeInUp'
export const FADE_IN_DOWN = 'fadeInDown'
export const FADE_IN_UP = 'fadeInUp'
export const ANIMATION_DURATION = 1000

export const LIST_DELAY_TIME = 100;

//TOKEN
export const ACCESS_TOKEN = 'ACCESS_TOKEN'
export const USER_DATA = 'USER_DATA'

//SCROLVIEW CONSTANT 
export const RIGHT_SCROLL_INDICATOR_INSENTS = 1

//USER ROLE
export const CUSTOMER_ROLE = 4
export const ADMIN = 1
export const SUPPLIER = 2
export const SERVICE_PROVIDER = 3

export const PREV_DEVICE_FCM_TOKEN = 'PREV_DEVICE_FCM_TOKEN'

//Photo options

export const OPTIONS = {
  mediaType: 'photo',
  maxWidth: 100,
  maxHeight: 100,
  quality: 1,
}

//Api Endpoints


export const GET = "GET";
export const GET_URL_PARAMS = "GET_URL_PARAMS";
export const GET_ID_PARAMS = "GET_ID_PARAMS";

export const POST = "POST";
export const POST_RAW = "POST_RAW";
export const POST_FORM = "POST_FORM";
export const POST_URL_PARAMS = "POST_URL_PARAMS";

export const DELETE_ID_PARAMS = "DELETE_ID_PARAMS";
export const MULTI_PART = "MULTI_PART";


//Category

export const REPAIRING = 1;
export const INSTALLATION = 2;
export const SUPPLY_AND_INSTALLTION = 3;


export const LOGIN = 'login' + " " + POST_FORM;
export const USER_PROFILE = 'user' + " " + GET;
export const USER_OTHER_DETAILS = 'user/otherDetails' + " " + GET;
export const FORGOT_PASSWORD = 'forgot' + " " + POST_FORM;
export const GET_SERVICE = 'getServices' + " " + GET;
export const GET_CUSTOMER_MY_REQUEST = 'job/myJob' + " " + POST_FORM;
export const PROVIDER_MY_REQUEST = "job/providermyjob" + " " + POST_FORM;
export const GET_CATEGORY = 'getCategories' + " " + GET
export const GET_SUB_CATEGORY = 'getCategories' + " " + POST_FORM
export const GET_SUB_CATEGORY_REQUEST_PAGE = 'getSubCategories' + " " + POST_FORM
export const CUSTOMER_MY_REQUEST_DETAIL = 'job/details' + " " + POST_FORM
export const REPORT_TO_ADMIN = 'report' + " " + POST_FORM
export const PROFILE_UPDATE = 'user/update' + " " + POST_FORM
export const REGISTER = 'register' + " " + POST_FORM
export const CREATE_ORDER = 'job/post' + " " + POST_FORM
export const GET_PRODUCT = 'product/search' + " " + POST_FORM
export const PRODUCT_DETAIL = 'product/details' + " " + POST_FORM
export const SENT_TO_CUSTOMER = 'product/sendtocustomer' + " " + POST_FORM
export const PENDING_ORDER = 'job/pending' + " " + GET
export const ORDER_STATUS_COUNT = 'job/OrderstatusCount' + " " + GET
export const PENDING_ORDER_DETAIL = 'job/pending/details' + " " + POST_FORM
export const PENDING_ORDER_CONFIRM = 'job/pending/confirm' + " " + POST_FORM
export const PENDING_ORDER_DECLINE = 'job/pending/decline' + " " + POST_FORM
export const PENDING_ORDER_CANCEL = 'job/cancel' + " " + POST_FORM
export const POST_JOB_IMAGE_UPLOAD = 'job/image/upload' + " " + POST_FORM
export const ACCEPTED_ORDER = 'job/accepted' + " " + GET;
export const ACCEPTED_ORDER_DETAIL = 'job/accepted/details' + " " + POST_FORM
export const CUSTOMER_PAY_NOW = 'job/payment' + " " + POST_FORM
export const PLACE_ORDER_WITH_SUPPLIER = 'orderplace' + " " + POST_FORM
export const PENDING_ORDER_SUBPROVIDER = 'orders' + " " + GET
export const PENDING_ORDER_DETAIL_SUBPROVIDER = 'orders/details' + " " + POST_FORM
export const SUPPLIER_PENDING_ORDER = 'orders' + " " + GET
export const PENDING_ORDER_DETAIL_SUPPLIER = 'orders/details' + " " + POST_FORM
export const DO_ACCEPT_ORDER = 'orders/confirm' + " " + POST_FORM
export const SUPPLIER_ORDER_ACCEPTED = 'orders/accepted' + " " + GET
export const SUPPLIER_PRODUCT_DELIVER = 'orders/deliver' + " " + POST_FORM
export const SUPPLIER_ACCEPTED_ORDER_DETAIL = 'orders/details' + " " + POST_FORM
export const ACCEPTED_ORDER_DETAIL_SUBPROVIDER = 'orders/details' + " " + POST_FORM

export const CUSTOMER_ACCEPTED_CANCEL = 'job/cancel' + " " + POST_FORM
export const SUPPLIER_DECLINED_ORDER = 'orders/decline' + " " + POST_FORM

export const PAYNOW_PROVIDER = 'order/pay' + " " + POST_FORM
export const REVIEW_PRODUCT = 'orders/verify' + " " + POST_FORM
export const START_SERVICE = 'job/start' + " " + POST_FORM
export const PROVIDER_COMPLETE_ORDER = 'job/complete' + " " + POST_FORM
export const CUSTOMER_COMPLETE_ORDER = 'job/complete' + " " + POST_FORM
export const DISPUTE_SENT_NOTE = 'job/disputenote' + " " + POST_FORM

export const COMPLETED_ORDER_CUSTOMER = 'job/completed' + " " + GET;

export const GET_SUB_JOB_TYPE = 'getJobSubType' + " " + POST_FORM;
export const GET_SUBCATEGORY_BY_CATEGORY = 'SubCategorybycategory' + " " + POST_FORM;
export const GET_PRODUCT_LIST = 'getProducts' + " " + GET

export const UPLOAD_CERTIFICATES = 'user/image/upload' + " " + POST_FORM
export const GET_QUESTIONS = 'questions' + " " + GET
export const SET_ANSWER = 'answer' + " " + POST_FORM
export const CHECK_EMAIL_PHONE = 'checkEmailPhone' + " " + POST_FORM
export const CUSTOMER_ONDEMAND_JOB_DETAILS = 'job/pending/details' + " " + POST_FORM
export const UPDATE_ANSWER = 'updateAnswer' + " " + POST_FORM
export const COMPLETED_ORDER_DETAIL = 'job/completed/details' + " " + POST_FORM
export const CUSTOMER_REVIEW = 'review' + " " + POST_FORM
export const GET_BRAINTREE_TOKEN = 'braintree/token' + " " + GET
export const BRAINTREE_PAY = 'braintree/pay' + " " + POST_FORM

export const COMPLETE_ORDER_SUBPROVIDER = 'orders/completed' + " " + GET
export const COMPLETE_ORDER_DETAIL_SUBPROVIDER = 'orders/details' + " " + POST_FORM
export const TRANSACTIONS = "transactions" + " " + GET
export const TRANSACTION_DETAILS = 'transactions/details' + " " + POST_FORM
export const NOTIFICATION = 'notifications' + " " + GET
export const UPDATE_LATLONG = 'updateLatLong' + " " + POST_FORM
export const CHAT_CONVERSATION = 'chat/conversions' + " " + GET
export const RESET_PASSWORD = 'change-password' + " " + POST_FORM
export const PROVIDER_DECLINE_REQUEST = 'job/reject' + " " + POST_FORM
export const CUSTOMER_DISPUTE_ORDER = 'job/dispute' + " " + GET
export const STRIPE_PAY = 'stripe/pay' + " " + POST_FORM
export const LOGOUT = "logout" + " " + GET

//Stripe
export const PUBLISHABLE_KEY = 'pk_live_51HvgEGEAIUXRumzasbZ1qVxVHGxmRI1t10bOTGWUCGTEreA8cJELdP3Y4RPzMhU5OeXC6VoK6o7is8ao0m9gGFcM00elkA5vbe';
export const SECRET_KEY = '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';
export const ANDROID_MODE = 'production'


// GLOBAL LOGOUT METHOD
export const LOGOUT_METHOD = (navigation) => {
  Utils.isLogins(false)
  Utils.removeUserDetails()
  Utils.removeUserToken()
  navigation.dispatch(CommonActions.reset({ index: 1, routes: [{ name: 'Login' }] }))
}