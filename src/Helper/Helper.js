//GLOBAL IMPORTS
import { RNToasty } from "react-native-toasty";

//FILE IMPORTS
import Fonts from "./Fonts";

export function validateResponse(res) {
    if (res.code === 200 && res.success) {
        return true;
    }
    else if (res.code === 200 && res.status) {
        return true;
    }
    
    else {
        if (res.errorMessage)
            RNToasty.Error({
                title: res.errorMessage,
                fontFamily: Fonts.POPPINS_REGULAR,
                position: 'bottom',
                withIcon: false
            });
        else
            if (res.message)
                RNToasty.Error({
                    title: res.message,
                    fontFamily: Fonts.POPPINS_REGULAR,
                    position: 'bottom',
                    withIcon: false
                });
    }
}


export function renderErrorToast(type, message) {
    if (type === 'error') {
        RNToasty.Error({
            title: JSON.stringify(message),
            fontFamily: Fonts.POPPINS_REGULAR,
            position: 'bottom',
            withIcon: false
        });
    }
}

