const CommonStyles = {
    ShadowView: {
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 0.1,
    }
}

export default CommonStyles;