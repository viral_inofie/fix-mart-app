import AsyncStorage from '@react-native-async-storage/async-storage'
import log from '@log'
import { PermissionsAndroid, Platform } from 'react-native'
import { RNToasty } from 'react-native-toasty'
import Fonts from './Fonts'

// ISLOGIN
export const IsLogin = 'dispatchSystemLogin'
let isLogin = false

export const getLogin = async () => {
  isLogin = await AsyncStorage.getItem(IsLogin)
  return isLogin
}

export const isLogins = is => AsyncStorage.setItem(IsLogin, is.toString())

export const removeIsLogins = is => AsyncStorage.removeItem(IsLogin)

export const setIsUserLoggedIn = is => AsyncStorage.setItem(IsLogin, is.toString())

export const removeIsUserLoggedIn = is => AsyncStorage.removeItem(IsLogin)

// FIRST TIME
export const IsFirstTime = 'dispatchSystemLogin'
let isFirstTime = false

export const getIsFirstTime = async () => {
  isFirstTime = await AsyncStorage.getItem(IsFirstTime)
  return isFirstTime
}

export const isFirstTimes = is => AsyncStorage.setItem(IsFirstTime, is.toString())

export const removeIsFirstTime = is => AsyncStorage.removeItem(IsFirstTime)

export const setIsFirstTime = is => AsyncStorage.setItem(IsFirstTime, is.toString())


// NOTIFICATION DEVICE TOKEN
export const NotificationDeviceToken = 'NOTIFICATION_DEVICE_TOKEN'
let notificationDeviceToken = ''

export const getNotificationDeviceToken = async () => {
  notificationDeviceToken = await AsyncStorage.getItem(NotificationDeviceToken)
  return notificationDeviceToken
}

export const notificationDeviceTokens = is => AsyncStorage.setItem(NotificationDeviceToken, is.toString())

export const removeNotificationDeviceTokens = is => AsyncStorage.removeItem(NotificationDeviceToken)

export const setNotificationDeviceToken = is => AsyncStorage.setItem(NotificationDeviceToken, is.toString())

export const removeNotificationDeviceToken = is => AsyncStorage.removeItem(NotificationDeviceToken)

// USER TOKEN
export const USER_TOKEN = 'USER_TOKEN'
let USER_TOKEN_IS = ''

export const getUserToken = async () => {
  USER_TOKEN_IS = await AsyncStorage.getItem(USER_TOKEN)
  return USER_TOKEN_IS
}

export const setUserToken = token => {
  AsyncStorage.setItem(USER_TOKEN, token.toString())
  log.success('user token saved', token)
}

export const removeUserToken = is => AsyncStorage.setItem(USER_TOKEN, 'USER_TOKEN')

//USER_DETAILS

export const USER_DETAILS = 'USER_DETAILS'

export const getUserDetails = async () => {
  let USER_DETAIL_IS = await AsyncStorage.getItem(USER_DETAILS)
  let PARSED_DETAILS = USER_DETAIL_IS
  if (USER_DETAIL_IS != USER_DETAILS) PARSED_DETAILS = JSON.parse(USER_DETAIL_IS)
  return PARSED_DETAILS
}

export const setUserDetails = data => {
  AsyncStorage.setItem(USER_DETAILS, JSON.stringify(data))
  log.success('user details saved', data)
}

export const removeUserDetails = () => {
  AsyncStorage.setItem(USER_DETAILS, 'USER_DETAILS')
}


export const requestCameraPermission = async () => {
  if (Platform.OS === 'ios') {
    return true
  }
  else {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Fixmart Photo Permission",
          message:
            "Fixmart needs access to your camera " +
            "so you can take awesome pictures.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true
      } else {
        console.log("Camera permission denied");
        RNToasty.Error({
          title: "Camera permission denied",
          position: 'bottom',
          fontFamily: Fonts.POPPINS_REGULAR,
          withIcon: false
        })
        return false
      }
    } catch (err) {
      console.warn(err);
      RNToasty.Error({
        title: JSON.stringify(err),
        position: 'bottom',
        fontFamily: Fonts.POPPINS_REGULAR,
        withIcon: false
      })
      return false
    }
  }
}; 