//Global imports
import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    TabItem: { width: width(20), height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.WHITE },

    TabItemText: { fontSize: totalSize(1.3), color: Colors.BLACK06, fontFamily: Fonts.POPPINS_REGULAR, marginTop: 10 },

    TabIconStyle: { height: 20, width: 20 },

    FixmartIconStyle: { height: 40, width: 40 },

    HomeTabItem: {
        width: width(20),
        height: width(30),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        borderTopLeftRadius: width(10),
        borderTopRightRadius: width(10),
        paddingBottom: 20,
    },

    TabContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        height: width(20),
        backgroundColor: Colors.WHITE,
        elevation: 30,
        shadowColor: '#000',
        shadowOffset: { height: 1, width: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 30
    },

    OrderModal: {
        position: 'absolute',
        bottom: width(35),
        height: 130,
        width: width(50),
        right: width(10),
        // backgroundColor: Colors.WHITE,
        borderRadius: 10
    },

    OrderItem: {
        backgroundColor: Colors.WHITE,
        height: 40, width: '100%', paddingHorizontal: 20, justifyContent: 'center', borderBottomWidth: 1, borderColor: Colors.PRIMARY_BORDER_COLOR
    },

    OrderItemText: { fontFamily: Fonts.POPPINS_REGULAR, fontSize: 12, color: Colors.BLACK06 },

    btRadius: {
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },

    bbRadius: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },

    CarotStyle: {
        marginTop: -10,
        marginLeft: 10
    }
})