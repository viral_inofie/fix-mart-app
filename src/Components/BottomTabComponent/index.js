//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Keyboard } from 'react-native';
import { width } from 'react-native-dimension';
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

//File imports
import Styles from './Styles';
import { ORDER_STATUS_COUNT } from '../../Helper/Constants';
import APICallService from '../../Api/APICallService';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import Fixmart from '../../../assets/images/fixmartIcon.png';
import HomeIcon from '../../../assets/images/homeTab.png';
import MessageIcon from '../../../assets/images/messageTab.png';
import RequestIcon from '../../../assets/images/requestTab.png';
import MoreTcon from '../../../assets/images/moreTab.png';
import Colors from '../../Helper/Colors';

//Component imports
const {
    TabItem,
    TabItemText,
    TabIconStyle,
    FixmartIconStyle,
    HomeTabItem,
    TabContainer,
    OrderModal,
    OrderItem,
    OrderItemText,
    btRadius,
    bbRadius,
    CarotStyle
} = Styles;

const BottomTabComponent = ({ state, navigation: { navigate, popToTop,addListener } }) => {

    const [Route, SetRoute] = useState(state.routes);
    const [OrderModel, SetOrderModel] = useState(false);
    const [KeyboardOpen, SetKeyboardOpen] = useState(false);
    const [PendingCount,SetPendingCount] = useState(0);
    const [AcceptCount,SetAcceptCount] = useState(0);
    const [CompleteCount,SetCompleteCount] = useState(0);
    const [DispatchCount,SetDispatchCount] = useState(0);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            _keyboardDidShow,
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            _keyboardDidHide,
        )

        return () => {
            keyboardDidShowListener.remove();
            keyboardDidHideListener.remove();
        };
    }, []);

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const _keyboardDidShow = e => {
        SetKeyboardOpen(true)
    };

    const _keyboardDidHide = () => {
        SetKeyboardOpen(false)
    };

    const getOrderCount=()=>{
        SetOrderModel(true)
        // dispatch(updateLoader(true));
        
        const apiClass = new APICallService(ORDER_STATUS_COUNT, {})

        apiClass.callAPI()
            .then(res => {
                // dispatch(updateLoader(false));
                // setRefreshing(false)
                if (validateResponse(res)) {
                    const sources = res.data;
                    let accepted_count = sources.Acccept.onDemand + sources.Acccept.offDemand;
                    let completed_count = sources.Completed.onDemand + sources.Completed.offDemand;
                    let pending_count = sources.Pending.onDemand + sources.Pending.offDemand;
                    let dispute_count = sources.dispute.onDemand + sources.dispute.offDemand;
                    SetPendingCount(pending_count)
                    SetAcceptCount(accepted_count);
                    SetCompleteCount(completed_count);
                    SetDispatchCount(dispute_count)
                    console.log("count===>",sources);
                }
            })
            .catch(err => {
                console.log("count===>",err);
                // setRefreshing(false)
                // renderErrorToast('error', err);
                // dispatch(updateLoader(false));
            })
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########..######.
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##....##....##
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##....##......
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##.....######.
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##..........##
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##....##....##
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##.....######.
    */

    const renderImageIcon = (routeName) => {
        if (routeName === 'Home') {
            return (
                <ImageBackground source={HomeIcon} style={TabIconStyle} />
            )
        }
        else if (routeName === 'Job Request') {
            return (
                <ImageBackground resizeMode='contain' source={RequestIcon} style={TabIconStyle} />
            )
        }
        else if (routeName === 'Orders') {
            return (
                <ImageBackground resizeMode='contain' source={Fixmart} style={FixmartIconStyle} />
            )
        }
        else if (routeName === 'Notification') {
            return (
                <FontAwesome name='bell' size={20} color={Colors.BLACK} />
            )
        }
        else {
            return (
                <ImageBackground resizeMode='contain' source={MoreTcon} style={TabIconStyle} />
            )
        }

    }

    const renderModel = () => {
        return (
            <Modal
                backdropColor={'#A3A3A3'}
                onBackdropPress={() => SetOrderModel(false)}
                isVisible={OrderModel} style={{ margin: 0 }}>
                <View style={OrderModal}>
                    <TouchableOpacity
                        onPress={() => {
                            popToTop();
                            SetOrderModel(false);
                            navigate('CustomerPendingOrderStack')
                        }}
                        style={[OrderItem, btRadius]}>
                        <Text style={[OrderItemText]}>Received Quotations ({PendingCount})</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => {
                            popToTop();
                            SetOrderModel(false);
                            navigate('CustomerAcceptedOrderStack')
                        }}
                        style={OrderItem}>
                        <Text style={OrderItemText}>Accepted Offers ({AcceptCount})</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => {
                            popToTop();
                            SetOrderModel(false);
                            navigate('CustomerCompleteOrderStack')
                        }}
                        style={OrderItem}>
                        <Text style={OrderItemText}>Completed Jobs ({CompleteCount})</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => {
                            popToTop();
                            SetOrderModel(false);
                            navigate('CustomerDisputedOrderStack')
                        }}
                        style={[OrderItem, { borderBottomWidth: 0 }, bbRadius]}>
                        <Text style={OrderItemText}>Disputed Jobs ({DispatchCount})</Text>
                    </TouchableOpacity>

                    <AntDesign name='caretdown' size={20} color={Colors.WHITE} style={CarotStyle} />
                </View>
            </Modal>
        )
    }

    const renderTabView = () => {
        return (
            <View style={TabContainer}>
                {
                    Route.map((item, i) => {
                        let title=item.name;
                        if(item.name === 'Job Request'){
                            title='Posted Job Request';
                        }
                        if (item.name === 'CustomerPendingOrderStack' || item.name === 'CustomerAcceptedOrderStack' || item.name === 'CustomerCompleteOrderStack' || item.name === 'CustomerDisputedOrderStack') {
                            return null
                        }
                        else if (item.name === 'Orders') {
                            return <TouchableOpacity
                                key={item.name}
                                onPress={() => getOrderCount()}
                                style={HomeTabItem}>
                                {renderImageIcon(item.name)} 
                                <Text style={TabItemText}>{title}</Text>
                            </TouchableOpacity>
                        }
                        else {
                            return <TouchableOpacity
                                key={item.name}
                                onPress={() => {
                                    popToTop();
                                    navigate(item.name, { type: 'customer' })
                                }}
                                style={TabItem}>
                                {renderImageIcon(item.name)}
                                <Text style={TabItemText}>{title}</Text>
                            </TouchableOpacity>
                        }
                    })
                }
            </View>
        )
    }


    return (
        KeyboardOpen
            ?
            null
            :
            <View>
                {renderTabView()}

                {renderModel()}
            </View>
    )
}

export default BottomTabComponent;