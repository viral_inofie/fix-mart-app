//global imports
import { StyleSheet } from 'react-native';
import { width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainContainer:{
        flexDirection:'row',
        alignItems:'center',
        paddingVertical:20,
        borderBottomWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR,
        paddingHorizontal:width(5)
    },

    PlumberImage:{
        height:80,
        width:80,
    },

    TitleText:{
        fontSize:16,
        color:Colors.BLACK06,
        fontFamily:Fonts.POPPINS_REGULAR
    },

    RatingText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:12,
        color:Colors.PRIMARY_BORDER_COLOR
    }
})