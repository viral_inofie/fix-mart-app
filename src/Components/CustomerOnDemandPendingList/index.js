//Global imports
import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';

//Component imports
import ProfileIcon from '../../../assets/images/profile.png';
import { width } from 'react-native-dimension';

const {
    MainContainer,
    PlumberImage,
    TitleText,
    RatingText
} = Styles;

const CustomerOnDemandPendingList = ({ item: { profile_image, id, service_provider_firstname, service_provider_lastname, rating }, _navigateToDetail }) => {

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: profile_image }}
            imageStyle={{ borderRadius: 50 }}
            style={PlumberImage}
        />
    }

    const renderTitle = () => {
        return <View style={{ marginLeft: width(5) }}>
            <Text style={TitleText}>{service_provider_firstname + " " + service_provider_lastname}</Text>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                <AirbnbRating
                    selectedColor={Colors.YELLOW}
                    count={5}
                    showRating={false}
                    defaultRating={rating.substring(0, 1)}
                    reviewColor={Colors.YELLOW}
                    size={12}
                    isDisabled
                />

                <Text style={RatingText}> / {rating.substring(4, 5)}</Text>
            </View>
        </View>
    }

    return (
        <TouchableOpacity
            onPress={() => _navigateToDetail(id,service_provider_firstname, service_provider_lastname , profile_image)}
            style={MainContainer}>
            {renderImage()}
            {renderTitle()}

        </TouchableOpacity>
    )
}

export default CustomerOnDemandPendingList; 