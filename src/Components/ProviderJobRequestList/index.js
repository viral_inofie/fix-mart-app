//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const ProviderJobRequestList = ({ item, navigateToDetail, time }) => {
    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (job_id) => {
        navigateToDetail(job_id)
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.thumbnail }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View>
            <View style={DemandNameView}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Customer name :</Text>
                </View>
                <View style={{ width: width(25) }}>
                    <Text style={ValueText}>{item.customerFirstname + " " + item.customerLastname}</Text>
                </View>
            </View>

            <View style={DemandNameView}>
                <View style={{ width: width(25) }}>
                    <Text style={[TitleText, { marginTop: height(1) }]}>Service :</Text>
                </View>
                <View style={{ width: width(25) }}>
                    <Text style={[ValueText, { marginTop: height(1) }]}>{item.service}</Text>
                </View>
            </View>

            {
                item.sub_category != null
                    ?
                    <View style={DemandNameView}>
                        <View style={{ width: width(25) }}>
                            <Text style={[TitleText, { marginTop: height(1) }]}>Sub Category :</Text>
                        </View>

                        <View style={{ width: width(25) }}>
                            <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.sub_category}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            {/* <Text style={[TitleText, { marginTop: height(1) }]}>Scheduled Appointment :</Text> */}

            {/* <View style={{ width: width(25) }}>
                <Text numberOfLines={1} style={ValueText}>{item.customerFirstname + " " + item.customerLastname}</Text>
                {
                    item.sub_category != null
                        ?
                        :
                        null
                }
                {/* <Text numberOfLines={2} style={[ValueText, { marginTop: height(1) }]}>{item.Date}</Text> */}
        </View >
    }

    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    // const renderButtonView = () => {
    //     return <View style={ButtonView}>
    //         <CustomeButton
    //             title='Accept'
    //             textStyle={OnDemandText}
    //             btnStyle={[OnDemand,
    //                 {
    //                     backgroundColor: item.isConfirm ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
    //                     borderWidth: item.isConfirm ? 0 : 0.5
    //                 }]}
    //         />

    //         <CustomeButton
    //             title='Reject'
    //             textStyle={OnDemandText}
    //             btnStyle={[OnDemand, { marginLeft: width(5) }]}
    //         />
    //     </View>
    // }



    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id)}
            style={MainView}>
            {renderDetailView()}

            {/* {renderButtonView()} */}

        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default ProviderJobRequestList;