//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';
import moment from 'moment';
//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../../Components/Button/index';

const { MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText
} = Styles;

const CompletedOrderList = ({ item, navigateToDetail, time, isSupplier = false, onDemand }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => navigateToDetail(item.id)}
            style={MainView}>
            <View style={DemandDetailView}>
                {renderDemandImage()}

                {renderDemandDetailView()}
            </View>
            {/* {renderCompleteButton()} */}

        </TouchableOpacity>
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: item.single_image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandTitleView = () => {
        return <View style={{ width: width(25) }}>
            <Text style={TitleText}>Service Provider Name :</Text>
            <Text numberOfLines={1} style={[TitleText, [{ marginTop: height(1) }]]}>Service :</Text>
            {
                item.sub_category === null
                    ?
                    null
                    :
                    <Text style={[TitleText, { marginTop: height(1) }]}>Sub Category :</Text>
            }
            <Text style={[TitleText, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
        </View>
    }

    const renderDemandValueView = () => {
        return <View style={{ width: width(25) }}>
            <Text numberOfLines={1} style={ValueText}>{item.service_provicer_firstname + " " + item.service_provicer_lastname}</Text>
            <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(3) }]]}>{item.service}</Text>
            {
                item.sub_category === null
                    ?
                    null
                    :
                    <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.sub_category}</Text>
            }
            <Text numberOfLines={2} style={[ValueText, { marginTop: height(1) }]}>{moment(item.visit_date).format('DD MMM, YYYY') + " " + renderTime(item.visit_time)}</Text>
        </View>
    }

    const renderDemandDetailView = () => {
        return <View style={DemandNameView}>
            {renderDemandTitleView()}

            {renderDemandValueView()}
        </View>

    }

    const renderCompleteButton = () => {
        return <CustomeButton
            title='Complete Order'
            textStyle={{ fontSize: 16 }}
            btnStyle={[OnDemand, {
                width: width(60),
                alignSelf: 'center',
                marginTop: height(2)
            }]}
        />
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default CompletedOrderList;