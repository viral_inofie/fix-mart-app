import React, { useState, useEffect } from 'react';
import { Image, StyleSheet, Alert, TouchableOpacity, View, Platform, Dimensions, Text, Modal, TouchableWithoutFeedback } from 'react-native';
import { RNToasty } from 'react-native-toasty';
import ImagePicker from 'react-native-image-crop-picker';
import COLORS from '../../Helper/Colors'
import Fonts from '../../Helper/Fonts'

export default function TakePicture(props) {



    const [image, setImage] = useState(props.image ? props.image : null)
    const [visible, setVisible] = useState(false)
    const [message, setMessage] = useState('Choose Picture')

    useEffect(() => {
        console.log('Image', props.children)
    })

    const openImagePickerAsync = async () => {
        setVisible(true)
        // Alert.alert("Choose Picture", '',
        //     [
        //         { text: "Cancel", onPress: () => console.log("cancel dialog") },
        //         { text: "Camera", onPress: () => openCamera() },
        //         { text: "Gallery", onPress: () => openGallery() },

        //     ],
        //     {
        //         cancelable: true,
        //     }
        // )
    }

    const openCamera = async () => {
        try {

            ImagePicker.openCamera({
                mediaType: 'photo',
                width: 300,
                height: 400,
                cropping: true,
                cropperCircleOverlay: props.cropperCircleOverlay ? props.cropperCircleOverlay : false
            }).then(response => {
                console.log("opencamera===Image====Response==>", response);
                let IosFileName = response.path.substring(response.path.toString().lastIndexOf('/') + 1, response.path.toString().length);
                let image = {
                    name: IosFileName,
                    type: response.mime,
                    uri: Platform.OS === "android" ? response.path : response.path
                }
                if (props.isShow) {
                    setImage(Platform.OS === "android" ? response.uri : response.path)
                }
                props.onPress(image)
                setVisible(false)
            });
        } catch (err) {
            setVisible(false)
            RNToasty.Error({
                title: err.message,
                fontFamily: Fonts.POPPINS_REGULAR,
                position: 'bottom',
                withIcon: false
              })
        }

    }

    const openGallery = async () => {
        try {

            ImagePicker.openPicker({
                mediaType: 'photo',
                cropping: true,
                width: 300,
                height: 400,
                cropperCircleOverlay: props.cropperCircleOverlay ? props.cropperCircleOverlay : false
            }).then(response => {
                console.log("image=======>", response);
                let IosFileName = response.path.substring(response.path.toString().lastIndexOf('/') + 1, response.path.toString().length);
                let image = {
                    name: IosFileName,
                    type: response.mime,
                    uri: Platform.OS === "android" ? response.path : response.path
                }
                if (props.isShow) {
                    setImage(Platform.OS === "android" ? response.uri : response.path)
                }
                props.onPress(image)
                setVisible(false)
            });
        } catch (err) {
            setVisible(false)
            RNToasty.Error({
                title: err.message,
                fontFamily: Fonts.POPPINS_REGULAR,
                position: 'bottom',
                withIcon: false
              })
        }
    }

    // console.log('Result-------',props.image)
    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={openImagePickerAsync} style={props.style ? props.style : {}}>
                {image ? <Image source={{ uri: image }}
                    style={{
                        width: 100,
                        height: 100,
                        alignSelf: 'center'
                    }} /> : props.children}

            </TouchableOpacity>

            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
                supportedOrientations={['portrait', 'landscape']}
                onRequestClose={() => { setVisible(false) }}
            >
                <View style={styles.modelStyle1}>
                    <View style={styles.modelStyle2}>
                        <Text style={styles.msgText}
                        >
                            Choose Picture
                        </Text>
                        <View style={styles.btnContainer}>
                            <TouchableOpacity
                                style={[styles.btnStyle,{backgroundColor:COLORS.WHITE}]}
                                onPress={() => setVisible(false)}
                            ><Text style={styles.btnTitleStyle}>Cancel</Text></TouchableOpacity>

                            <TouchableOpacity
                                style={styles.btnStyle}
                                onPress={() => openCamera()}
                            ><Text style={styles.btnTitleStyle}>Camera</Text></TouchableOpacity>
                            <TouchableOpacity
                                style={styles.btnStyle}
                                onPress={() => openGallery()}
                            ><Text style={styles.btnTitleStyle}>Gallery</Text></TouchableOpacity>
                        </View>
                    </View>
                </View>

            </Modal>
        </View>
    );
}

const WIDTH = Dimensions.get('window').width - 40
const styles = {
    msgText: {
        fontSize: 18,
        color: COLORS.COLOR_BLACKSHADE,
        textAlign: 'center',
        fontFamily: Fonts.NUNITO_LIGHT
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20
    },
    btnTitleStyle: {
        color: COLORS.BLACK,
        fontFamily: Fonts.NUNITO_BOLD
    },
    btnStyle: {
        shadowOffset: { width: 2, height: 2, },
        borderRadius: 20,
        backgroundColor: COLORS.APP_PRIMARY,
        paddingHorizontal: 30,
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    modelStyle1: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00000080'
    },
    modelStyle2: {
        width: WIDTH,
        backgroundColor: COLORS.OFF_WHITE,
        borderRadius: 20,
        padding: 15
    },
    btnParentSection: {
        alignItems: 'center',
        marginTop: 10
    },
    btnSection: {
        width: 225,
        height: 50,
        backgroundColor: '#DCDCDC',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
        marginBottom: 10
    },
    btnText: {
        textAlign: 'center',
        color: 'gray',
        fontSize: 14,
        fontWeight: 'bold'
    }

};