//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, totalSize, width } from 'react-native-dimension';
import Foundation from 'react-native-vector-icons/Foundation';
import { Rating, AirbnbRating } from 'react-native-ratings';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const SupplierSubAcceptedOrderList = ({ item, navigateToDetail, time, _onPayNow ,_onPayLater }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (order_id) => {
        navigateToDetail(order_id)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM';

        let Minutes = Time.substring(3, 5);

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View style={DemandNameView}>
            <View style={{ width: width(25) }}>
                <Text style={TitleText}>Supplier Name :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Service :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Sub Category :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(25) }}>
                <Text numberOfLines={1} style={ValueText}>{item.firstname + " " + item.lastname}</Text>
                <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.service}</Text>
                <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.sub_category}</Text>
                <Text numberOfLines={2} style={[ValueText, { marginTop: height(1) }]}>{moment(item.visit_date).format('DD MMM, YYYY') + " " + renderTime(item.visit_time)}</Text>
            </View>
        </View>
    }


    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    const renderButtonView = () => {
        if (item.isProviderPaid) {
            return null
        }
        else {
            return <View style={ButtonView}>
                <CustomeButton
                    title='Pay Now'
                    buttonAction={()=>_onPayNow(item.price, item.id)}
                    textStyle={OnDemandText}
                    btnStyle={[OnDemand,
                        {
                            backgroundColor: item.isConfirm ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                            borderWidth: item.isConfirm ? 0 : 0.5
                        }]}
                />

                <CustomeButton
                    title='Pay Later'
                    buttonAction={()=>_onPayLater( item.id)}
                    textStyle={OnDemandText}
                    btnStyle={[OnDemand, { marginLeft: width(5) }]}
                />
            </View>
        }
    }

    const renderCancelButton = () => {
        if (item.isProviderPaid) {
            if (item.isProductVerified) {
                return <CustomeButton
                    title='Review Product'
                    textStyle={OnDemandText}
                    btnStyle={[OnDemand, { width: width(60), alignSelf: 'center', marginTop: height(2) }]}
                />
            }
            else {
                return null
            }
        }
        else {
            return null
        }
    }

    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id)}
            style={MainView}>
            {renderDetailView()}

            {renderButtonView()}

            {renderCancelButton()}
        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default SupplierSubAcceptedOrderList;