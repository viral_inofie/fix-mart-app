import {StyleSheet} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainView:{
        width:'100%',
        paddingHorizontal:width(5),
        paddingVertical:height(2.5),
        borderBottomWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    DemandDetailView:{
        flexDirection:'row',
        alignItems:'flex-start'
    },

    Demandimage:{
        height:height(15),
        width:width(35),
    },

    DemandNameView:{
        width:width(50),
        marginLeft:width(5),
        flexDirection:'row',
        alignItems:'flex-start'
    },

    TitleText:{
        fontSize:totalSize(1.3),
        fontFamily:Fonts.POPPINS_MEDIUM,
        color:Colors.BLACK05
    },

    ValueText:{
        fontSize:totalSize(1.3),
        fontFamily:Fonts.POPPINS_MEDIUM,
        color:Colors.LIGHT_GRAY_BLUE
    },

    ButtonView:{
        width:width(60),
        alignSelf:'center',
        flexDirection:'row',
        alignItems:'center',
        marginTop:height(3),
        
    },

    OnDemand:{
        height:height(6),
        width:width(27.5),
        justifyContent:'center',
        alignItems:'center',
        borderWidth:0.5,
        borderRadius:height(3),
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    OnDemandText:{
        fontFamily:Fonts.POPPINS_REGULAR,
        fontSize:totalSize(2),
        color:Colors.BLACK06
    }
})