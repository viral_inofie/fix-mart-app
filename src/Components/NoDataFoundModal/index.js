import React, { Component } from 'react'
import { View, Image, Text, Dimensions } from 'react-native'
import FONTS from '../../Helper/Fonts'
import COLORS from '../../Helper/Colors'

import imgNoDataFound from '../../../assets/images/noDataFound.png'

class NoDataFoundModal extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.imgStyle}
          source={imgNoDataFound}
          resizeMode='contain'
        />
        <Text style={styles.welcomeText}>Sorry, No data found!</Text>
      </View>
    )
  }
}

export default NoDataFoundModal

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -160,
    height: Dimensions.get('screen').height
  },
  imgStyle: {
    justifyContent: 'center',
    alignSelf: 'center',
    height: 212,
    width: '60%'
  },
  welcomeText: {
    marginTop: 0,
    color: 'rgb(172,176,182)',
    fontSize: 18,
    fontFamily: FONTS.NUNITO_BOLD,
    alignSelf: 'center',
  }
}