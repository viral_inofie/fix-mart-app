import React, { Component } from 'react'
import {
  Modal, View, SafeAreaView,
  TouchableOpacity, Text,
  TextInput, Image, FlatList
} from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import {
  BASE_URL, STATUS_BAR_TYPE, ICABBI_APP_KEY,
  HAPTIC_CONFIG, HAPTIC_FEEDBACK_ERROR, HAPTIC_FEEDBACK_SUCCESS
} from '../Helper/Constants'
import _ from 'lodash';
import COLORS from '../Helper/Colors'
import Fonts from '../Helper/Fonts'
import imgClose from '../../assets/images/close.png'
import imgMarkerPin from '../../assets/images/markerPin.png'
import log from '@log'
class GooglePlacePicker extends Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '', suggestionList: [] }
  }
  onChangeTextDelayed = (searchText) => {
    this.setState({ searchText })
    if (searchText && searchText.length > 2) {
      this.onChangeText(searchText)
    }
  }
  onChangeText = (searchText) => {
    console.log("text");
    var myHeaders = new Headers()
    myHeaders.append("Content-Type", "application/json")
    myHeaders.append("app-key", ICABBI_APP_KEY)

    var raw = JSON.stringify({ "address": searchText })

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      redirect: 'follow'
    }

    log.info('PLACES PARAMS ', raw)
    log.url(BASE_URL + '/users/login')

    fetch(`${BASE_URL}/address/search?string=${searchText}`, requestOptions)
      .then(response => response.text())
      .then(result => {
        let res = JSON.parse(result)
        log.success('PLACES SUCCESSFULLY', res)
        this.setState({ suggestionList: res.data.body.addresses })
        // ReactNativeHapticFeedback.trigger(HAPTIC_FEEDBACK_SUCCESS, HAPTIC_CONFIG)
        // this.setState({ isLoading: false }, () => {
        //   if (res.error) {
        //     this.props.navigation.navigate('UserDetails', { phone: phoneNumber })
        //   } else {
        //     Utils.setUserToken(res.data.token)
        //     Utils.isLogins(true)
        //     Utils.setUserDetails(res.data)
        //     this.props.navigation.dispatch(CommonActions.reset({ index: 1, routes: [{ name: 'Dashboard' }] }))
        //   }
        // })
      })
      .catch(error => {
        log.error('PLACES FAILED', error)
        //   ReactNativeHapticFeedback.trigger(HAPTIC_FEEDBACK_ERROR, HAPTIC_CONFIG)
        this.setState({ isLoading: false }, () => {
          this.setState({ message: error.message, showErrorModal: true })
        })
      })

  }
  render() {
    return (
      <Modal
        animationType="slide"
        transparent
        visible={this.props.close}
        onRequestClose={() => {
          this.props.onClose();
        }}
      >
        <View style={{ flex: 1, backgroundColor: COLORS.OFF_WHITE, }}>
          <SafeAreaView style={{ flex: 1, marginTop: 20 }}>
            <TouchableOpacity style={{ margin: 10, alignSelf: 'flex-end' }} onPress={() => this.props.onClose()}>
              <Text>Cancel</Text>
            </TouchableOpacity>
            <View style={{
              flexDirection: 'row',
              backgroundColor: 'white',
              margin: 12,
              borderWidth: 0.5,
              borderColor: '#ddd',
              borderRadius: 2
            }}>
              <TextInput
                style={{
                  fontFamily: Fonts.NUNITO_REGULAR,
                  flex: 1,
                  paddingHorizontal: 8
                }}
                autoFocus
                value={this.state.searchText}
                onChangeText={this.onChangeTextDelayed}
              />
              <TouchableOpacity style={{ margin: 10, alignSelf: 'flex-end' }} onPress={() => this.setState({ searchText: '' })}>
                <Image source={imgClose} style={{
                  height: 24, width: 24
                }} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.suggestionList}
                extraData={this.state}
                renderItem={({ item }) => <TouchableOpacity onPress={() => this.props._onGooglePlaceAPIPickerPressed(item)} style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderBottomWidth: 0.5, paddingVertical: 8, paddingHorizontal: 16
                }}>
                  <Image source={imgMarkerPin} style={{
                    height: 24, width: 24
                  }} />
                  <Text style={{
                    marginLeft: 8,
                    fontFamily: Fonts.NUNITO_REGULAR,
                    fontSize: 14
                  }}>{item.formatted}</Text>
                </TouchableOpacity>}
              />
            </View>
          </SafeAreaView>
        </View>
      </Modal>
    );
  }
}

export default GooglePlacePicker



// class GooglePlacePicker extends Component {
//   constructor(props) {
//     super(props);
//   }

//   render() {
//     return (
//       <Modal
//         animationType="slide"
//         transparent
//         visible={this.props.close}
//         onRequestClose={() => {
//           this.props.onClose();
//         }}
//       >
//         <View style={{ flex: 1, backgroundColor: COLORS.OFF_WHITE, }}>
//           <SafeAreaView style={{ flex: 1, marginTop: 20 }}>
//             <TouchableOpacity style={{ margin: 10, alignSelf: 'flex-end' }} onPress={() => this.props.onClose()}>
//               <Text>Cancel</Text>
//             </TouchableOpacity>
//             <GooglePlacesAutocomplete
//               placeholder='Enter Location'
//               minLength={2}
//               autoFocus={true}
//               returnKeyType={'default'}
//               fetchDetails={true}
//               query={{
//                 key: GOOGLE_PLACE_API_KEY,
//                 language: 'en',
//               }}
//               styles={{
//                 flex: 1,
//                 backgroundColor: 'red',
//                 textInputContainer: {
//                   backgroundColor: 'rgba(0,0,0,0)',
//                   borderTopWidth: 0,
//                   borderBottomWidth: 0,
//                 },
//                 textInput: {
//                   marginLeft: 0,
//                   marginRight: 0,
//                   height: 38,
//                   color: '#5d5d5d',
//                   fontSize: 16,
//                   backgroundColor: COLORS.OFF_WHITE,
//                 },
//                 predefinedPlacesDescription: {
//                   color: '#1faadb',
//                 },
//               }}
//               onPress={(data, details = null) => {
//                 this.props._onGooglePlaceAPIPickerPressed(details)
//               }}
//             />
//           </SafeAreaView>
//         </View>
//       </Modal>
//     );
//   }
// }