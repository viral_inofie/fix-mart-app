//Global imports
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Colors from '../../Helper/Colors';
import moment from 'moment';
import * as Animatable from 'react-native-animatable'
import Fonts from '../../Helper/Fonts';
import { FADE_IN_UP, NOTIFICATION } from '../../Helper/Constants'


const NotificationList = (props) => {

    const { item } = props

    return (
        <Animatable.View animation={FADE_IN_UP}>
            <View style={styles.cellContainer}>
                <View style={styles.cellUpperView}>
                    <Text style={styles.textDesc}>{item.title}</Text>
                    <Text style={styles.text}>{item.message}</Text>
                </View>
                <Text style={[styles.textDesc, { color: Colors.LIGHT_GRAY, fontSize: 12 }]}>{moment(item.created_at).format('DD MMM, YYYY')}</Text>
            </View>
        </Animatable.View>
    )
}

const styles = StyleSheet.create({
    cellContainer: {
        paddingHorizontal: 10,
        paddingVertical: 12,
        marginVertical: 10,
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        borderRadius: 5, shadowOffset: { width: 1, height: 1 },
        shadowColor: 'black',
        shadowOpacity: 0.2,
        elevation: 3,
      },
      imgContainer: {
        height: 50,
        width: 50,
        padding: 10,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
      },
      imgView: {
        backgroundColor: Colors.APP_PRIMARY,
        height: 50,
        width: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
      },
      name: {
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: 16,
        marginHorizontal: 12,
        alignSelf: 'center'
      },
      separator: {
        height: 1,
        marginLeft: 40,
        marginRight: 0,
        backgroundColor: Colors.GRAY
      },
      cellUpperView: {
        paddingHorizontal: 8,
        flex: 3
      },
      textDesc: {
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: 16,
        margin: 2
      },
      text: {
        fontFamily: Fonts.POPPINS_REGULAR,
        color: Colors.LIGHT_GRAY,
      }
})

export default NotificationList;