//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';
import { Rating, AirbnbRating } from 'react-native-ratings';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';

//Component imports
import CustomeButton from '../../Components/Button/index';

const { MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText
} = Styles;

const CustomerDisputedOrderList = ({ item, navigateToDetail, time, _navigateToNote }) => {

    const [IsLoading, SetIsLoading] = useState(true);
    const [refreshing,setRefreshing] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])


    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => navigateToDetail(item.id)}
            style={MainView}>
            <View style={DemandDetailView}>
                {renderDemandImage()}

                {renderDemandDetailView()}
            </View>
            {renderCancelButton()}

        </TouchableOpacity>
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: item.single_image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }


    const renderDemandTitleView = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Service Provider Name :</Text>
                </View>
                <View style={{ width: width(25) }}>
                    <Text numberOfLines={2} style={ValueText}>{item.service_provicer_firstname + " " + item.service_provicer_lastname}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Service :</Text>
                </View>
                <View style={{ width: width(25) }}>
                    <Text numberOfLines={1} style={ValueText}>{item.service}</Text>
                </View>
            </View>

            {
                item.sub_category != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(25) }}>
                            <Text style={TitleText}>Sub Category :</Text>
                        </View>
                        <View style={{ width: width(25) }}>
                            <Text numberOfLines={1} style={ValueText}>{item.sub_category}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Scheduled Appointment :</Text>
                </View>
                <View style={{ width: width(25) }}>
                    <Text numberOfLines={2} style={ValueText}>{moment(item.visit_date).format('DD MMM, YYYY') + " " + renderTime(item.visit_time)}</Text>
                </View>
            </View>

        </View>
    }

    const renderDemandValueView = () => {
        return <View style={{ width: width(25) }}>
            <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(1) }]]}>{item.service}</Text>
            <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>Wash basin</Text>
            <Text numberOfLines={2} style={[ValueText, { marginTop: height(1) }]}>{item.date}</Text>
        </View>
    }

    const renderDemandDetailView = () => {
        return <View style={DemandNameView}>
            {renderDemandTitleView()}

            {/* {renderDemandValueView()} */}
        </View>

    }

    const renderCancelButton = () => {
        if (item.isDisputeNoteSubmited) {
            return null
        }
        else {
            return <CustomeButton
                buttonAction={() => _navigateToNote(item.id)}
                title='Submit Note'
                textStyle={{ fontSize: 16 }}
                btnStyle={[OnDemand, {
                    width: width(60),
                    alignSelf: 'center',
                    marginTop: height(2)
                }]}
            />
        }
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={1000}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default CustomerDisputedOrderList;