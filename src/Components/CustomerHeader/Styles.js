//Global imports
import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    HeaderView: {
       
        paddingHorizontal: width(5),
        width: '100%',
        borderBottomWidth: 1,
        borderColor: Colors.GRAY,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    HeaderTitle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(2.5),
        color: Colors.BLACK06,
        marginLeft: width(5)
    }

})