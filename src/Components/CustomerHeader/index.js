//Global imports
import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Platform } from 'react-native';
import { height, width } from 'react-native-dimension';
import { hasNotch } from 'react-native-device-info';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

//File imports
import Styles from './Styles';
import BackArrow from '../../../assets/images/backarrow.png';
import Colors from '../../Helper/Colors';

//Component imports
import PencilIcon from '../../../assets/images/Edit.png';

const { HeaderView, HeaderTitle } = Styles;

const isDeviceNotch = hasNotch();

const CustomHeader = (props) => {
    const {
        onLeftClick = () => { },
        title,
        LeftIcon = BackArrow,
        isFilled = false,
        onRightClick = () => { },
        isRight = false,
        isWithProfile = false,
        ProfileIcon = '',
        isNotify = false
    } = props;

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToBack = () => {
        onLeftClick()
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderMainView = () => {
        return <View style={[HeaderView, {
            paddingTop: isDeviceNotch ? height(4) : Platform.OS === 'ios' ? height(3) : height(2),
            paddingBottom: height(2),
            backgroundColor: 'transparent'
        }]}>
            {renderTitleView()}
            {renderEditIconView()}
        </View>
    }

    const renderBackArrow = () => {
        if (LeftIcon === null) {
            return null
        }
        else {
            return <TouchableOpacity
                style={{ width: width(6) }}
                onPress={() => _navigateToBack()}>
                <ImageBackground
                    source={LeftIcon}
                    resizeMode='contain'
                    style={{ height: height(5), width: width(5) }}
                />

            </TouchableOpacity>
        }
    }

    const renderTitle = () => {
        if (isWithProfile) {
            return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <ImageBackground
                    source={ProfileIcon}
                    imageStyle={{ borderRadius: 20 }}
                    resizeMode='contain'
                    style={{ height: 40, width: 40, marginLeft: LeftIcon === null ? 0 : 10 }}
                />
                <Text style={[HeaderTitle, { marginLeft: LeftIcon === null ? 0 : 10 }]}>{title}</Text>
            </View>
        }
        else {
            return <Text numberOfLines={1} ellipsizeMode="tail" style={[HeaderTitle, { marginLeft: LeftIcon === null ? 0 : 10 }]}>{title}</Text>
        }
    }

    const renderTitleView = () => {
        return <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {renderBackArrow()}
            {renderTitle()}
        </View>
    }

    const renderEditIconView = () => {
        return !isRight ? null : <TouchableOpacity onPress={() => onRightClick()}>
            {
                isNotify
                    ?
                    <FontAwesome name='bell' size={20} color={Colors.BLACK} />
                    :
                    <ImageBackground
                        source={PencilIcon}
                        resizeMode='contain'
                        style={{ height: height(3.5), width: width(3.5) }}
                    />
            }
        </TouchableOpacity>
    }

    return (
        <>
            {renderMainView()}
        </>

    )
}

export default CustomHeader;