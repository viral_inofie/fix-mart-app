//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, width } from 'react-native-dimension';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';
import moment from 'moment';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const SupplierAcceptedOrderList = ({ item, navigateToDetail, time, _doProductDeliver }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (order_id) => {
        navigateToDetail(order_id)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }


    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View style={DemandNameView}>
            <View style={{ width: width(25) }}>
                <Text style={TitleText}>Service Provider Name :</Text>
                <Text numberOfLines={1} style={[TitleText, { marginTop: height(0.5) }]}>Service :</Text>
                <Text numberOfLines={1} style={[TitleText, { marginTop: height(1) }]}>Sub Category :</Text>
                <Text numberOfLines={2} style={[TitleText, { marginTop: height(1) }]}>Scheduled Appoinment :</Text>

            </View>

            <View style={{ width: width(25) }}>
                <Text numberOfLines={1} style={ValueText}>{item.firstname + " " + item.lastname}</Text>
                <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(2) }]]}>{item.service}</Text>
                <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(1) }]]}>{item.sub_category}</Text>
                <Text numberOfLines={2} style={[ValueText, [{ marginTop: height(1) }]]}>{moment(item.visit_date).format('DD MMM, YYYY') + " " + renderTime(item.visit_time)}</Text>

            </View>
        </View>
    }

    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    const renderButtonView = () => {
        if (!item.isProviderPaid) {
            return null
        }
        else {
            if (item.isProductDelivered) {
                return null
            }
            else {
                return <View style={ButtonView}>
                    <CustomeButton
                        buttonAction={() => _doProductDeliver(item.id)}
                        title='Product Delivered'
                        textStyle={OnDemandText}
                        btnStyle={[OnDemand,
                            {
                                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                                borderWidth: 0,
                                width: '100%'
                            }]}
                    />
                </View>
            }
        }
    }

    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id)}
            style={MainView}>
            {renderDetailView()}

            {renderButtonView()}


        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default SupplierAcceptedOrderList;