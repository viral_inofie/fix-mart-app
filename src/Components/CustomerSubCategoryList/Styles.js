//Global import
import { Dimensions, StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

const WIDTH = Dimensions.get('window').width;


export default StyleSheet.create({
    MainContainer: {
        width: (WIDTH - 80) / 3,
        height: (WIDTH - 80) / 3,
        elevation: 1,
        backgroundColor: Colors.WHITE,
        shadowOffset: { height: 1, width: 1 },
        shadowColor: '#000',
        shadowOpacity: 0.3,
        shadowRadius: 1,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 20
    },
    ImageStyle: {
        height: 70,
        width: 70
    },

    SubCategoryTitle: {
        fontSize: 12,
        fontFamily: Fonts.POPPINS_LIGHT,
        color: Colors.BLACK,
        textAlign: 'center',
        marginTop: 10,
        width: "100%",

    },

    textContainer: {
        width: (WIDTH - 80) / 3,
        alignItems: "center"
    }
})