//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';

//File imports
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

const { MainContainer, ImageStyle, SubCategoryTitle, textContainer } = Styles;


const CustomerSubCategoryList = ({ item: { image, name, id }, _navigateToRequestDetail, time }) => {
    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])
    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            resizeMode='contain'
            source={{ uri: image }}
            style={ImageStyle}
        />
    }

    const renderText = () => {
        return <Text style={SubCategoryTitle}>{name}</Text>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                <TouchableOpacity
                    onPress={() => _navigateToRequestDetail(image, name, id)}
                    style={{ marginBottom: 20, }}>
                    <View style={MainContainer}>
                        {renderImage()}
                    </View>
                    <View style={textContainer}>
                        {renderText()}
                    </View>
                </TouchableOpacity>
            </Animatable.View>
    )
}

export default CustomerSubCategoryList;