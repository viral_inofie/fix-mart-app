import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Colors from '../../Helper/Colors';
import { HEIGHT } from '../../Helper/Constants';
import Fonts from '../../Helper/Fonts';

const EmptyDataComponent = (props) => {
    const { ErrorMessage = 'No Jobs found' } = props;
    return (
        <View style={{ height: HEIGHT - 300, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontFamily: Fonts.POPPINS_REGULAR, color: Colors.LIGHT_GRAY, fontSize: 16 }}>{ErrorMessage}</Text>
        </View>
    )
}

export default EmptyDataComponent;