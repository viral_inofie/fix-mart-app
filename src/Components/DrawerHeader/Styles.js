import { StyleSheet } from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainContainer: {
        paddingVertical: height(3),
        borderBottomWidth: 0.5,
        borderColor: Colors.LIGHT_GRAY,
        paddingHorizontal: width(7.5)
    },

    ProfileImage: {
        height: height(7),
        width: height(7),
    },

    ProfileView: {
        flexDirection: 'row',
        marginTop: height(2),
        width: width(55),
        alignItems: 'center'
    },

    UserText: {
        fontFamily: Fonts.POPPINS_MEDIUM,
        fontSize: totalSize(1.6),
        color: Colors.DRAWER_USER_TEXT_COLOR
    },

    MobileText: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: totalSize(1),
        color: Colors.DRAWER_USER_TEXT_COLOR
    }
})