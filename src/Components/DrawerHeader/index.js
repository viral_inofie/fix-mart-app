//Global imports
import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';

//File imports
import Styles from './Styles';

//Component imports
import BackArrow from '../../../assets/images/backarrow.png';
import ProfileIcon from '../../../assets/images/profile.png';
import PencilIcon from '../../../assets/images/Edit.png';
import { useSelector } from 'react-redux';
import Logger from '../../Helper/Logger';

const {
    MainContainer,
    ProfileImage,
    ProfileView,
    UserText,
    MobileText
} = Styles;

const DrawerHeader = ({ CloseDrawer = () => { }, _navigateToProfile }) => {

    const { loginRes: { email, phone, firstname, lastname, profile_image } } = useSelector(state => state.AuthReducer)
    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderBackArrow = () => {
        return <TouchableOpacity onPress={() => CloseDrawer()}>
            <ImageBackground
                source={BackArrow}
                resizeMode='contain'
                style={{ height: height(5), width: width(5) }}
            />
        </TouchableOpacity>
    }

    const renderProfileIcon = () => {
        return <ImageBackground
            source={{uri : profile_image}}
            imageStyle={{ borderRadius: height(10) }}
            style={ProfileImage}
        />
    }

    const renderUserDetailView = () => {
        return <View style={{ width: width(35), marginLeft: width(3) }}>
            <Text numberOfLines={1} ellipsizeMode='tail' style={UserText}>{firstname + " " + lastname}</Text>
            <Text numberOfLines={1} ellipsizeMode='tail' style={MobileText}>{phone}</Text>
        </View>
    }

    const renderEditProfileIcon = () => {
        return <ImageBackground
            source={PencilIcon}
            resizeMode='contain'
            style={{
                height: height(3.5),
                width: width(3.5), marginLeft: width(2)
            }}
        />
    }



    const renderProfileView = () => {
        return <View style={ProfileView}>
            {renderProfileIcon()}

            {renderUserDetailView()}

            {renderEditProfileIcon()}
        </View>
    }

    return (
        <TouchableOpacity
            onPress={() => _navigateToProfile()}
            style={MainContainer}>
            {renderBackArrow()}

            {renderProfileView()}
        </TouchableOpacity>
    )
}

export default DrawerHeader;