//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';

//Component imports
import CustomeButton from '../../Components/Button/index';

const { MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText
} = Styles;

const AcceptedOrderList = ({ item, navigateToDetail, time }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])
    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => navigateToDetail()}
            style={MainView}>
            <View style={DemandDetailView}>
                {renderDemandImage()}

                {renderDemandDetailView()}
            </View>
            {/* {renderCancelButton()} */}

        </TouchableOpacity>
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={item.image}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }


    const renderDemandTitleView = () => {
        return <View style={{ width: width(25) }}>
            <Text style={TitleText}>Service :</Text>
            <Text style={[TitleText, [{ marginTop: height(1) }]]}>Plumber Name :</Text>
            <Text style={[TitleText, { marginTop: height(1) }]}>Date & Time :</Text>
            <Text style={[TitleText, { marginTop: height(1) }]}>Price :</Text>
            <Text style={[TitleText, { marginTop: height(1) }]}>Ratings :</Text>
        </View>
    }

    const renderDemandValueView = () => {
        return <View style={{ width: width(25) }}>
            <Text numberOfLines={1} style={ValueText}>{item.service}</Text>
            <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(1) }]]}>{item.plumber_name}</Text>
            <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.date}</Text>
            <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.Price}</Text>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: height(1) }}>
                    <AirbnbRating
                        selectedColor={Colors.YELLOW}
                        count={5}
                        showRating={false}
                        defaultRating={item.Ratings}
                        reviewColor={Colors.YELLOW}
                        size={12}
                        isDisabled
                        onFinishRating={(val) => { }}
                    />
                </View>
        </View>
    }

    const renderDemandDetailView = () => {
        return <View style={DemandNameView}>
            {renderDemandTitleView()}

            {renderDemandValueView()}
        </View>

    }

    const renderCancelButton = () => {
        return <CustomeButton
            title='Cancel Order'
            textStyle={{ fontSize: 16 }}
            btnStyle={[OnDemand, {
                width: width(60),
                alignSelf: 'center',
                marginTop: height(2)
            }]}
        />
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={1000}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default AcceptedOrderList;