//Global imports
import { Dimensions, StyleSheet } from 'react-native';


//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';


const WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
    MainContainer:{
        paddingVertical:20,
        borderBottomWidth:0.5,
        width:(WIDTH - 40) ,
        alignSelf:'center',
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    rowView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        width:(WIDTH - 40) ,
        alignSelf:'center'
    },

    TransactionHeading:{
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.BLACK06,
        fontSize:14
    },

    ValueView:{ width: '49%', marginLeft:'1%' },

    PriceView:{ width: '20%', marginLeft:'1%', alignItems:'flex-end' }
})