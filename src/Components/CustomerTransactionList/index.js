//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';
import moment from 'moment';

//File imports
import Styles from './Styles';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';
import { LIST_DELAY_TIME, WIDTH } from '../../Helper/Constants';

const {
    MainContainer,
    rowView,
    TransactionHeading,
    ValueView,
    PriceView
} = Styles;

const CustomerTransactionList = ({ item, time, navigateToDetail }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false);
        }, time)
    }, []);

    const _navigateToDetail = (id) => {
        navigateToDetail(id)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(11, 13);

        let Minutes = Time.substring(14, 16);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderDateTimeView = () => {
        return <View style={rowView}>
            <View style={{ width: '29%' }}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode='tail'
                    style={TransactionHeading}>Date & time :</Text>
            </View>

            <View style={ValueView}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode='tail'
                    style={[TransactionHeading, {
                        color: Colors.PLACEHOLDER_COLOR
                    }]}>{moment(item.created_at).format('DD MMM, YYYY HH::mm:ss')}</Text>
            </View>

            <View style={PriceView}>

            </View>
        </View>
    }

    const renderCategoryView = () => {
        return <View style={[rowView, { marginTop: 10 }]}>
            <View style={{ width: '29%' }}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode='tail'
                    style={TransactionHeading}>Service :</Text>
            </View>

            <View style={ValueView}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode='tail'
                    style={[TransactionHeading, {
                        color: Colors.PLACEHOLDER_COLOR
                    }]}>{item.service}</Text>
            </View>

            <View style={PriceView}>

            </View>
        </View>
    }

    const renderSubCategoryView = () => (
        <View style={{ justifyContent: 'space-between', flexDirection: 'row', width: WIDTH - 40,  marginTop: 10, }}>
            {
                item.sub_category != ""
                    ?
                    <View style={[rowView, { width : '75%', justifyContent:'flex-start' }]}>
                        <View>
                            <Text
                                numberOfLines={1}
                                ellipsizeMode='tail'
                                style={TransactionHeading}>Sub Category :</Text>
                        </View>

                        <View style={[ValueView,{width:'auto', marginLeft:width(2)}]}>
                            <Text
                                numberOfLines={1}
                                ellipsizeMode='tail'
                                style={[TransactionHeading, {
                                    color: Colors.PLACEHOLDER_COLOR
                                }]}>{item.sub_category}</Text>
                        </View>
                    </View>
                     :
                    <View></View>
            } 

            <View style={PriceView}>
                <Text
                    style={[TransactionHeading, {
                        fontFamily: Fonts.POPPINS_MEDIUM,
                        color: item.type === "Received" ? Colors.CREDIT_TRANSACTION_COLOR : Colors.RED
                    }]}>{item.type === "Received" ? "+" : "-"} ${item.paid}</Text>
            </View>
        </View >
    )

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                <TouchableOpacity
                    onPress={() => _navigateToDetail(item.id)}
                    style={MainContainer}>
                    {renderDateTimeView()}

                    {renderCategoryView()}

                    {renderSubCategoryView()}
                </TouchableOpacity>
            </Animatable.View>
    )
}

export default CustomerTransactionList;