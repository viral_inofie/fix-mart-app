//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, totalSize, width } from 'react-native-dimension';
import Foundation from 'react-native-vector-icons/Foundation';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView,
    StarView,
    PlumberDetailValue
} = Styles;

const ListOfProductList = ({ item, navigateToDetail, time }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (product_id, product_name) => {
        navigateToDetail(product_id, product_name)
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.image[0] }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {

        return <View style={{ marginLeft: width(5) }}>
            <Text numberOfLines={2} ellipsizeMode='tail'  style={[TitleText, { fontSize: 15, color: Colors.BLACK, width:width(50) }]}>{item.name}</Text>
            <View style={DemandNameView}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Category :</Text>
                    <Text numberOfLines={1} style={[TitleText, [{ marginTop: height(1) }]]}>Supplier name :</Text>
                    <Text style={[TitleText, { marginTop: height(1) }]}>Price :</Text>
                </View>

                <View style={{ width: width(25) }}>
                    <Text numberOfLines={1} style={ValueText}>{item.category}</Text>
                    <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(1) }]]}>{item.suplierfirstname + " " + item.suplierlastname}</Text>
                    <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.price}</Text>
                </View>
            </View>
        </View>
    }

    const renderStarView = () => {
        return <View style={StarView}>
            <AirbnbRating
                selectedColor={Colors.YELLOW}
                count={5}
                showRating={false}
                starContainerStyle={{ height: 10, width: '5%', alignSelf: 'flex-start', justifyContent: 'space-between', }}
                defaultRating={item.rating.substring(0,3)}
                reviewColor={Colors.PRIMARY_DARK}
                size={20}
                isDisabled
                onFinishRating={(val) => { }}
            />
            <Text style={PlumberDetailValue}>{`${item.rating}`}</Text>
        </View>
    }

    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}

            {renderDemandView()}
        </View>
    }


    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id, item.name)}
            style={MainView}>
            {renderDetailView()}
            {renderStarView()}
        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default ListOfProductList;