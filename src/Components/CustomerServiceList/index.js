//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';

//File imports
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

const { MainContainer, ImageStyle, SubCategoryTitle } = Styles;


const CustomerServiceList = ({ item: { image, service , id}, _navigateToCategory, time }) => {
    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])
    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            resizeMode='contain'
            source={{uri : image}}
            style={ImageStyle}
        />
    }

    const renderText = () => {
        return <Text numberOfLines={2} style={SubCategoryTitle}>{service}</Text>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                <TouchableOpacity
                    onPress={() => _navigateToCategory(id)}
                    style={{ marginBottom: 20 }}>
                    <View style={MainContainer}>
                        {renderImage()}
                    </View>
                    {renderText()}
                </TouchableOpacity>
            </Animatable.View>
    )
}

export default CustomerServiceList;