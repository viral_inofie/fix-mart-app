//Global import
import { Dimensions, StyleSheet } from 'react-native';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

const WIDTH = Dimensions.get('window').width;


export default StyleSheet.create({
    MainContainer : {
        width:(WIDTH - 80)/3,
        height : (WIDTH - 80)/3,
       
        marginHorizontal:10,
        elevation:1,
        backgroundColor:Colors.WHITE,
        shadowOffset:{height:1, width:1},
        shadowColor:'#000',
        shadowOpacity:0.3,
        shadowRadius:1,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        
    },
    ImageStyle:{
        height:60,
        width:60
    },

    SubCategoryTitle:{
        fontSize:12,
        width:100,
        alignSelf:'center',
        fontFamily:Fonts.POPPINS_LIGHT,
        color:Colors.BLACK,
        textAlign:'center',
        marginTop:10
    }
})