// Global imports
import React, { Component } from 'react'
import { TouchableOpacity, Text, Image, Platform } from 'react-native'

// File imports
import COLORS from '../../Helper/Colors'
import FONTS from '../../Helper/Fonts'

class CustomButton extends Component {
  render() {
    const {
      title,
      btnStyle,
      buttonAction,
      leftImg,
      rightImg,
      rightImageStyle,
      leftImageStyle,
      textColor,
      textStyle
    } = this.props;
    return (
      <TouchableOpacity {...this.props} style={[styles.btnStyle, btnStyle]} onPress={buttonAction ? buttonAction : () => { }}>
        {leftImg && <Image resizeMode='contain' source={leftImg} style={[styles.leftIcon, leftImageStyle]} />}
        <Text style={[styles.text, { color: textColor }, textStyle]}>{title}</Text>
        {rightImg && <Image resizeMode='contain' source={rightImg} style={[styles.rightIcon, rightImageStyle]} />}
      </TouchableOpacity>
    )
  }
}

export default CustomButton

const styles = {
  btnStyle: {
    marginHorizontal: 0,
    marginVertical: 10,
    paddingVertical: 10,
    // borderRadius: 50,
    // backgroundColor: COLORS.OFF_WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    // shadowOffset: { width: 2, height: 2 },
    // shadowColor: 'black',
    // shadowOpacity: 0.3,
    // elevation: 5,
    marginTop: 0,
    flexDirection: 'row',
  },
  text: {
    fontFamily: FONTS.POPPINS_REGULAR,
    fontSize: 18,
  },
  rightIcon: {
    height: 18,
    width: 18,
    justifyContent: 'center',
    marginLeft: 10,
  },
  leftIcon: {
    height: 18,
    width: 18,
    justifyContent: 'center',
    marginRight: 10,
  },
}