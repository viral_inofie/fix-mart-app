//Globals imports
import React, { Component, useState } from 'react';
import { View, Text, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';
import AsyncStorage from '@react-native-async-storage/async-storage';

//File imports
import Styles from './Styles';
import Fonts from '../../Helper/Fonts';
import ChevronDown from '../../../assets/images/chevdown.png';
import { ACCESS_TOKEN, LOGOUT } from '../../Helper/Constants';


//Component imports
import DrawerHeader from '../DrawerHeader';
import { Alert } from 'react-native';
import APICallService from '../../Api/APICallService';
import { updateLoader } from '../../ReduxStore/Actions/CommonActions';
import { renderErrorToast, validateResponse } from '../../Helper/Helper';
import { useDispatch } from 'react-redux';

const { DrawerItem, DrawerItemTextStyle, RowView } = Styles;

const CustomerDrawerComponent = ({ navigation: { toggleDrawer, navigate, replace }, state: { routes }, value }) => {
  const dispatch = useDispatch();

  const [DrawerRoute, SetDrawerRoute] = useState(routes);
  const [OrderView, SetOrderView] = useState(false);
  const [SupplierOrderView, SetSupplierOrderView] = useState(false);


  /*
  .##....##....###....##.....##.####..######......###....########.####..#######..##....##
  .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
  .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
  .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
  .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
  .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
  .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
  */
  const _toggleDrawer = () => {
    toggleDrawer()
  }

  const _onClickPendingOrder = () => {
    _toggleDrawer();
    value === 'provider' ? navigate('ProviderPedingOrder') : navigate('CustomerPendingOrder')
  }

  const _onClickAcceptedOrder = () => {
    _toggleDrawer();
    value === 'supplier' ? navigate('SupplierAcceptedOrder') : value === 'provider' ? navigate('ProviderAcceptedOrder') : navigate('CustomerAcceptedOrder');

  }

  const _onClickCompletedOrder = () => {
    _toggleDrawer();
    value === 'supplier' ? navigate('SupplierCompletedOrderStack') : value === 'provider' ? navigate('ProviderCompletedOrder') : navigate('CustomerCompletedOrder')
  }

  const _onClickRenderDrawerField = (item) => {
    _toggleDrawer();
    navigate(item)
  }

  const _navigateToProfile = () => {
    navigate('Profile')
  }

  const _onClickDisputedOrder = () => {
    _toggleDrawer();
    navigate('CustomerDisputedOrder')
  }

  const _navigateToSelecteUser = () => {
    _toggleDrawer();
    replace('selectUserType')
  }

  /*
  .##........#######...######...####..######...######.
  .##.......##.....##.##....##...##..##....##.##....##
  .##.......##.....##.##.........##..##.......##......
  .##.......##.....##.##...####..##..##........######.
  .##.......##.....##.##....##...##..##.............##
  .##.......##.....##.##....##...##..##....##.##....##
  .########..#######...######...####..######...######.
  */

  const toggleOrderView = () => SetOrderView(!OrderView);
  const toggleSupplierOrderView = () => SetSupplierOrderView(!SupplierOrderView);

  const _logOutUser = () => {

    const apiClass = new APICallService(LOGOUT, {});

    apiClass.callAPI()
      .then(res => {
        if (validateResponse(res)) {
          dispatch(updateLoader(false))
          AsyncStorage.removeItem(ACCESS_TOKEN, (err) => {
            console.log(err, "errrrrrrrrrr")
            if (err === null) {
              _navigateToSelecteUser()
            }
            else {

            }
          })
        }
      })
      .catch(err => {
        dispatch(updateLoader(false));
        renderErrorToast('error', err)
      })
  }



  /*
  ..######...#######..##.....##.########...#######..##....##.########.##....##.########
  .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
  .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
  .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
  .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
  .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
  ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
  */
  const renderDrawerHeader = () => {
    return <DrawerHeader
      _navigateToProfile={() => _navigateToProfile()}
      CloseDrawer={() => _toggleDrawer()}
    />
  }

  const renderChevronDown = () => {
    return <ImageBackground
      key='Order'
      source={ChevronDown}
      resizeMode='contain'
      style={{ height: height(3), width: width(3) }}
    />
  }

  const renderOrderField = () => {
    return <TouchableOpacity
      onPress={() => toggleOrderView()}
      style={[DrawerItem, RowView]}>
      <Text style={DrawerItemTextStyle}>Orders</Text>
      {renderChevronDown()}
    </TouchableOpacity>
  }

  const renderSupplierOrderField = () => {
    return <TouchableOpacity
      onPress={() => toggleSupplierOrderView()}
      style={[DrawerItem, RowView]}>
      <Text style={DrawerItemTextStyle}>Supplier Orders</Text>
      {renderChevronDown()}
    </TouchableOpacity>
  }

  const renderPendingOrder = () => {
    return <TouchableOpacity
      key='Sent'
      onPress={() => _onClickPendingOrder()}
      style={[DrawerItem, { height: height(4) }]}>
      <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Sent</Text>
    </TouchableOpacity>
  }

  const renderAcceptedOrder = () => {
    return <TouchableOpacity
      key='Accepted'
      onPress={() => _onClickAcceptedOrder()}
      style={[DrawerItem, { height: height(4) }]}>
      <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Confirmed</Text>
    </TouchableOpacity>
  }

  const renderCompletedOrder = () => {
    return <TouchableOpacity
      key={'Completed'}
      onPress={() => _onClickCompletedOrder()}
      style={[DrawerItem, { height: height(4) }]}>
      <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Completed</Text>
    </TouchableOpacity>
  }

  const renderDisputedOrder = () => {
    return <TouchableOpacity
      key={'Disputed'}
      onPress={() => _onClickDisputedOrder()}
      style={[DrawerItem, { height: height(4) }]}>
      <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Disputed</Text>
    </TouchableOpacity>
  }

  const renderSubOrderField = () => {
    if (OrderView) {
      return <View key='Order'>
        {value === 'supplier' ? null : renderPendingOrder()}
        {renderAcceptedOrder()}
        {value === 'supplier' || value === 'provider' ? null : renderDisputedOrder()}
        {renderCompletedOrder()}
      </View>
    }
    else return null
  }

  const renderSupplierSubOrderField = () => {
    if (SupplierOrderView) {
      return <View key='SupplierOrder'>
        {/* {renderPendingOrder()} */}
        <TouchableOpacity
          key='Pending'
          onPress={() => navigate('SupplierSubPendingOrderStack')}
          style={[DrawerItem, { height: height(4) }]}>
          <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Sent</Text>
        </TouchableOpacity>

        <TouchableOpacity
          key='Accepted'
          onPress={() => navigate('SupplierSubAcceptedOrderStack')}
          style={[DrawerItem, { height: height(4) }]}>
          <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Confirmed</Text>
        </TouchableOpacity>

        <TouchableOpacity
          key='Completed'
          onPress={() => navigate('SupplierSubCompletedOrderStack')}
          style={[DrawerItem, { height: height(4) }]}>
          <Text style={[DrawerItemTextStyle, { fontFamily: Fonts.POPPINS_LIGHT }]}>Completed</Text>
        </TouchableOpacity>

        {/* {renderAcceptedOrder()} */}
        {/* {renderCompletedOrder()} */}
      </View>
    }
    else return null
  }

  const renderLogOut = (item) => {
    return <TouchableOpacity
      key={item.name}
      onPress={() => {
        toggleDrawer();
        Alert.alert(
          "Log out",
          "Are you sure want to logout?",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => _logOutUser() }
          ],
          { cancelable: false }
        );

      }}
      style={DrawerItem}>
      <Text style={DrawerItemTextStyle}>{item}</Text>
    </TouchableOpacity>
  }

  const renderDrawerField = (item) => {
    return <TouchableOpacity
      key={item}
      onPress={() => _onClickRenderDrawerField(item)}
      style={DrawerItem}>
      <Text style={DrawerItemTextStyle}>{item}</Text>
    </TouchableOpacity>
  }

  const renderDrawerItem = () => {

    return <View>
      {
        DrawerRoute.map((item, i) => {
          if (item.name === 'Home' || item.name === 'CustomerPendingOrder' || item.name === 'CustomerAcceptedOrder' ||
            item.name === 'CustomerCompletedOrder' || item.name === 'ProviderCompletedOrder' || item.name === 'ProviderAcceptedOrder'
            || item.name === 'ProviderPedingOrder' || item.name === 'Profile' || item.name === 'SupplierHomeStack' || item.name === 'SupplierAcceptedOrder'
            || item.name === 'SupplierSubPendingOrderStack' || item.name === 'SupplierCompletedOrderStack'
            || item.name === 'ProviderJobRequestStack' || item.name === 'CustomerDisputedOrder' || item.name === 'SupplierSubAcceptedOrderStack' || item.name === 'SupplierSubCompletedOrderStack'
            || item.name === "ProviderListOfProducts" || item.name === "ProvideProductDetail" || item.name === "CustomerMessages" || item.name === "ProviderOnDemandHelp"
            || item.name === "ProviderAcceptedOrderDetail" || item.name === "ProviderJobRequestDetail" || item.name === "CustomerPendingOnDemandDetail" || item.name === "CustomerPendingOrderDetail" || item.name === "CustomerAcceptedOnDemandDetail"
            || item.name === "CustomerAcceptedOrderDetail" || item.name === "CustomerCompleteOrderDetail" || item.name === "ProviderCompletedOrderDetail" || item.name === "ProviderAcceptedOnDemandDetail" || item.name === "SupplierNewOrderDetail"
            || item.name === "SupplierAcceptedOrderDetail" || item.name === "ProviderJobRequest" || item.name === "SupplierSubPendingOrderDetail" || item.name === "SupplierCompletedOrderDetail" || item.name === "SupplierSubAcceptedOrderDetail"
          ) {
            return null
          }

          if (item.name === 'New Job Requests' && value === 'supplier') {
            return null
          }

          if (value !== 'customer') {
            if (item.name === 'Supplier Order') {
              if (value === 'supplier') {
                return null
              }
              else {
                return <View key={item.name}>
                  {renderSupplierOrderField()}
                  {renderSupplierSubOrderField()}
                </View>
              }
            }
          }

          if (item.name === 'Order') {
            return <View key={item.name}>
              {renderOrderField()}
              {renderSubOrderField()}
            </View>
          } else if (item.name === 'Logout') {
            return renderLogOut(item.name)
          } else {
            return renderDrawerField(item.name)
          }
        })
      }
    </View>
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        bounces={false}>

        {renderDrawerHeader()}
        {renderDrawerItem()}

      </ScrollView>
    </View>
  )
}

export default CustomerDrawerComponent;