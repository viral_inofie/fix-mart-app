import {StyleSheet} from 'react-native';
import { height, totalSize, width } from 'react-native-dimension';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    DrawerItem:{
        paddingHorizontal:width(7.5),
        height:height(7),
        justifyContent:'center',
    },

    DrawerItemTextStyle:{
        fontSize:totalSize(1.8),
        fontFamily:Fonts.POPPINS_MEDIUM,
        color:Colors.DRAWER_ITEM_TEXT_COLOR
    },

    RowView:{
        flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' 
    }
})