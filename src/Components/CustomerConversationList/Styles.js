//Global imports
import { Dimensions, StyleSheet } from 'react-native';
import Colors from '../../Helper/Colors';

//File imports
import Fonts from '../../Helper/Fonts';

const WIDTH = Dimensions.get('window').width;

export default StyleSheet.create({
    MainContainer: {
        flexDirection: 'row',
        width: WIDTH - 40,
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical:20,
        borderBottomWidth:0.5,
        borderColor:Colors.PRIMARY_BORDER_COLOR
    },

    ImageStyle:{
        width:(WIDTH - 40) * 0.15,
        height:(WIDTH - 40) * 0.15
    },

    TitleView:{
        height:(WIDTH - 40) * 0.15,
        justifyContent:'space-around',
        width:(WIDTH - 40) * 0.60,
        marginLeft:'5%',
       
    },

    TitleText:{
        fontSize:16,
        fontFamily:Fonts.POPPINS_REGULAR,
        color:Colors.BLACK06
    },

    DesText:{
        fontSize:12,
        fontFamily:Fonts.POPPINS_LIGHT,
        color:Colors.PLACEHOLDER_COLOR
    },

    DateView:{
        width:(WIDTH - 40) * 0.15,
        height:(WIDTH - 40) * 0.15,
        justifyContent:'space-around', 
        marginLeft:'5%',
       
    },

    DateText:{
        fontSize:12,
        fontFamily:Fonts.POPPINS_LIGHT,
        color:Colors.PLACEHOLDER_COLOR
    }
})  