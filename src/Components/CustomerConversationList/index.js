//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';

//File imports
import Styles from './Styles';
import { LIST_DELAY_TIME, WIDTH } from '../../Helper/Constants';

//component imports
import ProfileIcon from '../../../assets/images/profile.png';


const {
    MainContainer,
    ImageStyle,
    TitleView,
    TitleText,
    DesText,
    DateView,
    DateText
} = Styles;

const CustomerConversationList = ({ item, time , _navigateToChat}) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time);
    }, []);

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderMainContainer = () => {
        return <TouchableOpacity 
        onPress={()=>_navigateToChat(item.name, item.userId)}
        style={MainContainer}>
            {renderImage()}

            {renderTitleView()}

            {renderDateView()}
        </TouchableOpacity>
    }

    const renderImage = () => {
        return <ImageBackground
            source={{uri : item.image}}
            style={ImageStyle}
            imageStyle={{ borderRadius: (WIDTH - 40) * 0.19 }}
        />
    }

    const renderTitleView = () => {
        return <View style={TitleView}>
                <Text style={TitleText}>{item.name}</Text>
                <Text style={DesText} numberOfLines={1} ellipsizeMode='tail'>{item.message}</Text>
        </View>
    }

    const renderDateView = () => {
        return <View style={DateView}>
                <Text style={DateText}>{item.last_time}</Text>
                <Text></Text>
        </View>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
            duration={LIST_DELAY_TIME}
            animation={'fadeInUp'}>
                {renderMainContainer()}
            </Animatable.View>
    )
}

export default CustomerConversationList;