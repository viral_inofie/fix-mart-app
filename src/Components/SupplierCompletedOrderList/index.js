//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const { MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText
} = Styles;

const SupplierCompletedOrderList = ({ item, navigateToDetail, time, isSupplier = false }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])
    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => navigateToDetail(item.id)}
            style={MainView}>
            <View style={DemandDetailView}>
                {renderDemandImage()}

                {renderDemandDetailView()}
            </View>
            {/* {renderCompleteButton()} */}

        </TouchableOpacity>
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{uri : item.image}}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }


    const renderDemandTitleView = () => {
        return <View style={{ width: width(25) }}>
             <Text style={TitleText}>Service Provider Name :</Text>
                <Text numberOfLines={1} style={[TitleText,{marginTop:height(1)}]}>Service :</Text>
                <Text numberOfLines={1} style={[TitleText,{marginTop:height(1)}]}>Sub Category :</Text>
                <Text numberOfLines={2} style={[TitleText,{marginTop:height(1)}]}>Scheduled Appoinment :</Text>
        </View>
    }

    const renderDemandValueView = () => {
        return <View style={{ width: width(25) }}>
             <Text numberOfLines={1} style={ValueText}>John Deo</Text>
                <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(3) }]]}>Plumbing Installation</Text>
                <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(1) }]]}>Wash Basin</Text>
                <Text numberOfLines={2} style={[ValueText, [{ marginTop: height(1) }]]}>04th October 2020, 04:50 PM</Text>
        </View>
    }

    const renderDemandDetailView = () => {
        return <View style={DemandNameView}>
            {renderDemandTitleView()}

            {renderDemandValueView()}
        </View>

    }

    const renderCompleteButton = () => {
        return <CustomeButton
            title='Complete Order'
            textStyle={{ fontSize: 16 }}
            btnStyle={[OnDemand, {
                width: width(60),
                alignSelf: 'center',
                marginTop: height(2)
            }]}
        />
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default SupplierCompletedOrderList;