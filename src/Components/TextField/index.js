// Global imports
import React, { Component } from 'react'
import { Keyboard, TouchableOpacity } from 'react-native';
import { TextInput, View, Image } from 'react-native';

// File imports
import COLORS from '../../Helper/Colors'
import FONTS from '../../Helper/Fonts'

class CustomTextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      style: {},
    };
  }

  onFocus = () => {
    const state = { ...this.state };
    state.style = {
      backgroundColor: COLORS.WHITE,
      borderColor: COLORS.LIGHT_GRAY
    };

    this.setState(state);
  }

  onBlur = () => {
    const state = { ...this.state };
    state.style = {
      backgroundColor: COLORS.TEXT_FIELD_BG
    };

    this.setState(state);
  }

  render() {
    const {
      viewStyle,
      placeholderColor,
      autoCapitalize,
      value,
      secureTextEntry,
      maxLength,
      inputStyle,
      rightImg,
      leftImg,
      isFocused,
      editable,
      containerView,
      leftimgStyle,
      onChangeText,
      keyboardType,
      RightImageClick,
      multiline,
      onEndEditing = () => {}
    } = this.props;
    return (
      <View style={containerView ? containerView : styles.containerView}>
        <View style={[isFocused ? styles.isFocusedview : styles.view, viewStyle, this.state.style]}>
          {leftImg && <Image resizeMode='contain' source={leftImg} style={[styles.leftIcon, leftimgStyle]} />}
          <TextInput
            editable={editable}
            // onFocus={this.onFocus}
            // onBlur={this.onBlur}
            textAlignVertical={'top'}
            onEndEditing={()=>onEndEditing()}
            value={value}
            multiline={multiline ? multiline : false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            onChangeText={onChangeText ? (text) => onChangeText(text) : (text) => { }}
            selectionColor={COLORS.APP_PRIMARY}
            secureTextEntry={secureTextEntry ? secureTextEntry : false}
            placeholderTextColor={placeholderColor ? placeholderColor : COLORS.PLACEHOLDER_COLOR}
            autoCapitalize={autoCapitalize ? autoCapitalize : 'none'}
            maxLength={maxLength ? maxLength : 48}
            {...this.props}
            style={[styles.textFieldStyle, inputStyle]}
          />
          {rightImg &&
            <TouchableOpacity onPress={RightImageClick ? () => RightImageClick() : () => { }}>
              <Image
                resizeMode='contain'
                source={rightImg}
                style={styles.rightIcon}
              />
            </TouchableOpacity>}
        </View>
      </View>
    )
  }
}

export default CustomTextField

const styles = {
  containerView: {
    width: '100%',
    paddingVertical: 5,

  },
  textFieldStyle: {
    flex: 1,
    textAlign: 'left',
    fontFamily: FONTS.POPPINS_REGULAR,
    fontSize: 16,
    padding: 8,

    color: COLORS.BLACK,
  },
  view: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  isFocusedview: {
    backgroundColor: COLORS.TEXT_FIELD_BG,
    borderRadius: 50,
    borderWidth: 1.0,
    borderColor: COLORS.BLACK,
    marginHorizontal: 40,
    alignItems: 'center',
    flexDirection: 'row'
  },
  rightIcon: {
    height: 18,
    width: 18,
    justifyContent: 'center',
    marginLeft: 5,
    marginRight: 8,
  },
  leftIcon: {
    height: 18,
    width: 18,
    justifyContent: 'center',
    marginLeft: 12,
    marginRight: 5,
  },
}