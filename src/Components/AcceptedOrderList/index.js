//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { height, width } from 'react-native-dimension';
import * as Animatable from 'react-native-animatable';
import { Rating, AirbnbRating } from 'react-native-ratings';
import moment from 'moment';
//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';

//Component imports
import CustomeButton from '../../Components/Button/index';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

const { MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText
} = Styles;

const AcceptedOrderList = ({ item, navigateToDetail, _doCancelOrder, time }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */
    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => navigateToDetail(item.id)}
            style={MainView}>
            <View style={DemandDetailView}>
                {renderDemandImage()}

                {renderDemandDetailView()}
            </View>
            {renderCancelButton()}

        </TouchableOpacity>
    }

    const renderDemandImage = () => {
        return <ImageBackground
            source={{ uri: item.single_image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }


    const renderDemandTitleView = () => {
        return <View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Service Provider Name :</Text>
                </View>

                <View style={{ width: width(25) }}>
                    <Text style={ValueText}>{item.service_provicer_firstname + " " + item.service_provicer_lastname}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Service :</Text>
                </View>

                <View style={{ width: width(25) }}>
                    <Text style={ValueText}>{item.service}</Text>
                </View>
            </View>

            {
                item.subcategory != null
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(25) }}>
                            <Text style={TitleText}>Sub Category :</Text>
                        </View>

                        <View style={{ width: width(25) }}>
                            <Text style={ValueText}>{item.subcategory}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Scheduled Appointment :</Text>
                </View>

                <View style={{ width: width(25) }}>
                    <Text style={ValueText}>{moment(item.visit_date).format('DD MMM, YYYY') + " " + renderTime(item.visit_time)}</Text>
                </View>
            </View>
        </View>
    }


    const renderDemandDetailView = () => {
        return <View style={DemandNameView}>
            {renderDemandTitleView()}
        </View>

    }

    const renderCancelButton = () => {
        if (item.isCustomerPaid) {
            return null
        }
        else {
            return <CustomeButton
                buttonAction={() => _doCancelOrder(item.id, item.tranportation_charge, item.sub_job_type, item.status)}
                title='Cancel Order'
                textStyle={{ fontSize: 16 }}
                btnStyle={[OnDemand, {
                    width: width(60),
                    alignSelf: 'center',
                    marginTop: height(2)
                }]}
            />
        }
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default AcceptedOrderList;