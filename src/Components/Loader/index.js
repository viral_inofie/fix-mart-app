import React, { Component } from 'react'
import AnimatedLoader from "react-native-animated-loader"
import { connect } from 'react-redux'
import Colors from '../../Helper/Colors';
import { LOTTIE_ANIMATION_FILE } from '../../Helper/Constants'

class Loader extends Component {
  render() {
    const { isLoading } = this.props;
    return (
      <AnimatedLoader
        overlayColor={Colors.WHITE}
        visible={isLoading}
        source={LOTTIE_ANIMATION_FILE}
        animationStyle={styles.lottie}
        speed={1}
      />
    )
  }
}

const styles = {
  lottie: {
    width: 200,
    height: 200
  }
}

const mapStateToProps = (state) => {
  const { CommonReducer } = state;
  return {
    isLoading: CommonReducer.isLoading
  }
}
export default connect(mapStateToProps, null)(Loader);
