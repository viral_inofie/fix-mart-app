//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, totalSize, width } from 'react-native-dimension';
import Foundation from 'react-native-vector-icons/Foundation';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const DemandsList = ({ item, navigateToDetail, time }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = () => {
        navigateToDetail()
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={item.image}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View style={DemandNameView}>
            <View style={{ width: width(25) }}>
                <Text style={TitleText}>Service Provider Name :</Text>
                <Text numberOfLines={1} style={[TitleText, [{ marginTop: height(1) }]]}>Service :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Sub Category :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(25) }}>
                <Text numberOfLines={1} style={ValueText}>{item.plumber_name}</Text>
                <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(3) }]]}>{item.service}</Text>
                <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.date}</Text>
                <Text numberOfLines={2} style={[ValueText, { marginTop: height(1) }]}>Wash basin</Text>
            </View>
        </View>
    }


    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    const renderButtonView = () => {
        return <View style={ButtonView}>
            <CustomeButton
                title='Accept'
                textStyle={OnDemandText}
                btnStyle={[OnDemand,
                    {
                        backgroundColor: item.isConfirm ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                        borderWidth: item.isConfirm ? 0 : 0.5
                    }]}
            />

            <CustomeButton
                title='Decline'
                textStyle={OnDemandText}
                btnStyle={[OnDemand, { marginLeft: width(5) }]}
            />
        </View>
    }

    const renderCancelButton = () => {
        return <CustomeButton
            title='Cancel Order'
            textStyle={OnDemandText}
            btnStyle={[OnDemand, { width: width(60), alignSelf: 'center', marginTop: height(2) }]}
        />

    }

    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail()}
            style={MainView}>
            {renderDetailView()}

            {renderButtonView()}

            {renderCancelButton()}
        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default DemandsList;