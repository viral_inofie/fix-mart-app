//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, width } from 'react-native-dimension';
import moment from 'moment';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const MyRequestsList = ({ ondemand, item, navigateToDetail, time, _doCancel, user_type }) => {

    const [IsLoading, SetIsLoading] = useState(true);
    console.log(user_type,'lisssss')
    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (job_id) => {
        navigateToDetail(job_id)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let isAmPm = Hours >= 12 ? 'PM' : 'AM';

        let Minutes = Time.substring(3, 5);

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.thumbnail }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View style={DemandNameView}>
            <View>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: width(25) }}>
                        <Text style={TitleText}>Service :</Text>
                    </View>
                    <View style={{ width: width(25) }}>
                        <Text numberOfLines={1} style={ValueText}>{item.service}</Text>
                    </View>
                </View>


                {
                    item.sub_job_type === 3
                        ?

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: height(1) }}>
                            <View style={{ width: width(25) }}>
                                <Text style={TitleText}>Issue :</Text>
                            </View>
                            <View style={{ width: width(25) }}>
                                <Text numberOfLines={1} style={ValueText}>{item.type_product}</Text>
                            </View>
                        </View>
                        :
                        null
                }


                {
                    item.sub_category != null
                        ?
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: height(1) }}>
                            <View style={{ width: width(25) }}>
                                <Text style={TitleText}>Sub Category :</Text>
                            </View>
                            <View style={{ width: width(25) }}>
                                <Text numberOfLines={1} style={ValueText}>{item.sub_category}</Text>
                            </View>
                        </View>
                        :
                        null
                }

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: height(1) }}>
                    <View style={{ width: width(25) }}>
                        <Text style={TitleText}>Scheduled Appointment :</Text>
                    </View>
                    <View style={{ width: width(25) }}>
                        <Text style={ValueText}>{moment(item.visit_date).format('DD MMM, YYYY')}</Text>
                        <Text style={ValueText}>{renderTime(item.visit_time)}</Text>
                    </View>
                </View>
            </View>
        </View>
    }

    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    const renderCancelButton = () => {
        if (user_type.type === "provider") {
            return null
        }
        else {
            return <CustomeButton
                title='Cancel Order'
                buttonAction={() => _doCancel(item.id, item.status)}
                textStyle={OnDemandText}
                btnStyle={[OnDemand, {
                    width: width(60),
                    alignSelf: 'center',
                    marginTop: height(2),
                    borderRadius: 50,
                    borderWidth: 0,
                    backgroundColor: Colors.DRAWER_BACKGROUND_COLOR
                }]}
            />
        }
    }

    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id)}
            style={MainView}>
            {renderDetailView()}


            {renderCancelButton()}
        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default MyRequestsList;