//Global imports
import { StyleSheet } from 'react-native';
import { moderateScale } from 'react-native-size-matters';


//File imports
import { WIDTH } from '../../Helper/Constants';
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';

export default StyleSheet.create({
    MainView: {
        width: WIDTH,
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingHorizontal: 20,
        marginBottom: 20
    },

    ProfileImageStyle: {
        height: (WIDTH - 40) * 0.10,
        width: (WIDTH - 40) * 0.10
    },

    ChatTextView: {
        width: (WIDTH - 40) * 0.75,
        padding: 15,
        backgroundColor: Colors.INDEX_GREY,
        borderRadius: 15,
    },

    ChatTextStyle: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 14,
        color: Colors.BLACK06
    },

    TimeTextStyle: {
        fontFamily: Fonts.POPPINS_LIGHT,
        fontSize: 12,
        marginTop: 20,
        textAlign: 'right'
    },

    TriangleShape: {

        width: 0,
        height: 0,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: Colors.WHITE,
        borderRightColor: Colors.WHITE,
    },

    balloon: {
        width: (WIDTH - 40) * 0.76,
       
        paddingHorizontal: moderateScale(10, 2),
        paddingTop: moderateScale(5, 2),
        paddingBottom: moderateScale(7, 2),
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomRightRadius:10
     },
     arrowContainer: {
         position: 'absolute',
         top: 0,
         left: 0,
         right: 0,
         bottom: 0,
         zIndex: -1,
         flex: 1,
     },
     arrowLeftContainer: {
         justifyContent: 'flex-end',
         alignItems: 'flex-start',
     },

     arrowLeft: {
        left: moderateScale(-6, 0.5),
    },
    arrowRight: {
        right:-(WIDTH - 40) * 0.73
    }
})