
//Global imports
import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import { moderateScale } from 'react-native-size-matters';

//File imports
import Styles from './Styles';
import { WIDTH } from '../../Helper/Constants';

//Component imports
import ProfileIcon from '../../../assets/images/profile.png';
import Colors from '../../Helper/Colors';
import { TextInput } from 'react-native-gesture-handler';


const {
    MainView,
    ProfileImageStyle,
    ChatTextView,
    ChatTextStyle,
    TimeTextStyle,
    TriangleShape,
    balloon,
    arrowContainer,
    arrowLeftContainer,
    arrowLeft,
    arrowRight
} = Styles;

let LEFT_SVG_PATH = 'M38.484,17.5c0,8.75,1,13.5-6,17.5C51.484,35,52.484,17.5,38.484,17.5z';
let RIGHT_SVG_PATH = 'M48,35c-7-4-6-8.75-6-17.5C28,17.5,29,35,48,35z'

const CustomerChatComponent = ({ user_id,userProfile,opponentProfile, data: { item } }) => {

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderMainView = () => {
        return <View style={[MainView, { flexDirection: user_id === item.fromUserId ? 'row' : 'row-reverse' }]}>
            {renderProfileImage()}
            {renderChatTextView()}
        </View>
    }

    const renderProfileImage = () => {
        return <ImageBackground
            source={{uri : user_id === item.fromUserId ? userProfile  : opponentProfile}}
            imageStyle={{ borderRadius: (WIDTH - 40) * 0.15 }}
            style={[ProfileImageStyle, {
                // marginLeft: user_id === item.user_id ? 0 : (WIDTH - 40) * 0.05
            }]}
        />
    }

    const renderChatTextView = () => {
        return <View style={{ flexDirection: user_id != item.fromUserId ? 'row-reverse' : 'row', alignItems: 'flex-end', }}>
            {/* <View style={[TriangleShape, {
                transform: [{ rotate: '0deg' }],
                borderBottomColor: user_id != item.user_id ? Colors.DRAWER_BACKGROUND_COLOR : Colors.INDEX_GREY,
                borderLeftWidth: user_id != item.user_id ? 0 : 6,
                borderBottomWidth: user_id != item.user_id ? 10 : 10,
                borderRightWidth: user_id != item.user_id ? 10 : 0,


            }]}>

            </View>
            <View style={[ChatTextView, {
                borderBottomLeftRadius: user_id != item.user_id ? 10 : 0,
                borderBottomRightRadius: user_id != item.user_id ? 0 : 10,
                backgroundColor: user_id != item.user_id ? Colors.DRAWER_BACKGROUND_COLOR : Colors.INDEX_GREY
            }]}>
                <Text style={ChatTextStyle}>{item.msg}</Text>
                <Text style={[TimeTextStyle, { color: user_id != item.user_id ? Colors.WHITE : Colors.BLACK }]}>{item.time}</Text>
            </View> */}
            <View style={[balloon,
                {
                    backgroundColor: user_id != item.fromUserId ? Colors.DRAWER_BACKGROUND_COLOR : Colors.INDEX_GREY,
                    marginLeft: user_id === item.fromUserId ? (WIDTH - 40) * 0.02 : 0,
                    marginRight: user_id === item.fromUserId ? 0 : (WIDTH - 40) * 0.02,
                    borderBottomRightRadius: user_id === item.fromUserId ? 10 : 0,
                    borderBottomLeftRadius: user_id === item.fromUserId ? 0 : 10
                }]}>
                <Text style={{ paddingTop: 5, color: Colors.BLACK }}>{item.message}</Text>
                <Text style={{ paddingTop: 5, color: Colors.PRIMARY_BORDER_COLOR, textAlign: 'right' }}>{item.message_time}</Text>

                <View
                    style={[
                        arrowContainer,
                        arrowLeftContainer,
                    ]}>

                    <Svg style={user_id === item.fromUserId ? arrowLeft : arrowRight}
                        width={moderateScale(16.5, 0.6)}
                        height={moderateScale(18.5, 0.6)}
                        viewBox="32.484 16.5 15.515 17.5"
                        enable-background="new 32.485 17.5 15.515 17.5">
                        <Path
                            d={user_id === item.fromUserId ? LEFT_SVG_PATH : RIGHT_SVG_PATH}
                            fill={user_id != item.fromUserId ? Colors.DRAWER_BACKGROUND_COLOR : Colors.INDEX_GREY}
                            // fill={'red'}
                            x="0"
                            y="0"
                        />
                    </Svg>
                </View>
            </View>
        </View>
    }

    return (
        <View>
            {renderMainView()}
        </View>
    )
}

export default CustomerChatComponent;