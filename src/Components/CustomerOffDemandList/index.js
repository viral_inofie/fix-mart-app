//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, totalSize, width } from 'react-native-dimension';
import Foundation from 'react-native-vector-icons/Foundation';
import { Rating, AirbnbRating } from 'react-native-ratings';

//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';
import moment from 'moment';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const CustomerOffDemandList = ({ item, navigateToDetail, time, _doDeclineOrder, _cancelOrder, _doAccept }) => {

    const [IsLoading, SetIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (job_id) => {
        navigateToDetail(job_id)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (time) => {
        let hours = time.substring(0, 2);

        let minutes = time.substring(3, 5);

        let isAmPm = hours >= 12 ? 'PM' : 'AM'

        return `${hours} : ${minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.single_image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View style={{ width: width(50), marginLeft: width(5), }}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                <View style={{ width: width(25) }}>
                    <Text style={TitleText}>Service Provider Name :</Text>
                </View>
                <View style={{ width: width(25), marginLeft: width(1) }}>
                    <Text style={ValueText}>{item.service_provider_firstname + " " + item.service_provider_lastname}</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(25) }}>
                    <Text numberOfLines={1} style={TitleText}>Service :</Text>

                </View>
                <View style={{ width: width(24), marginLeft: width(1) }}>
                    <Text numberOfLines={1} style={ValueText}>{item.service}</Text>
                </View>
            </View>

            {
                item.subcategory
                    ?
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                        <View style={{ width: width(25) }}>
                            <Text numberOfLines={1} style={TitleText}>Sub Category :</Text>

                        </View>
                        <View style={{ width: width(24), marginLeft: width(1) }}>
                            <Text numberOfLines={1} style={ValueText}>{item.subcategory}</Text>
                        </View>
                    </View>
                    :
                    null
            }

            <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginTop: height(1) }}>
                <View style={{ width: width(25) }}>
                    <Text numberOfLines={2} style={TitleText}>Scheduled Appointment :</Text>

                </View>
                <View style={{ width: width(24), marginLeft: width(1) }}>
                    <Text numberOfLines={2} style={ValueText}>{moment(item.visit_date).format('DD MMMM, YYYY') + " " + renderTime(item.visit_time)}</Text>
                </View>
            </View>

        </View >
    }


    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    const renderButtonView = () => {
        return <View style={ButtonView}>
            <CustomeButton
                title='Accept'
                textStyle={OnDemandText}
                buttonAction={() => _doAccept(item.id)}
                btnStyle={[OnDemand,
                    {
                        backgroundColor: item.isConfirm ? Colors.DRAWER_BACKGROUND_COLOR : 'transparent',
                        borderWidth: item.isConfirm ? 0 : 0.5
                    }]}
            />

            <CustomeButton
                title='Decline'
                buttonAction={() => _doDeclineOrder(item.id)}
                textStyle={OnDemandText}
                btnStyle={[OnDemand, { marginLeft: width(5) }]}
            />
        </View>
    }

    const renderCancelButton = () => {
        return <CustomeButton
            buttonAction={() => _cancelOrder(item.id, item.status)}
            title='Cancel Order'
            textStyle={OnDemandText}
            btnStyle={[OnDemand, { width: width(60), alignSelf: 'center', marginTop: height(2) }]}
        />

    }

    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id)}
            style={MainView}>
            {renderDetailView()}

            {renderButtonView()}

            {renderCancelButton()}
        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default CustomerOffDemandList;