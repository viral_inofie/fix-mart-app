//Global imports
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { height, width } from 'react-native-dimension';
import moment from 'moment';
//File imports
import Colors from '../../Helper/Colors';
import Styles from './Styles';
import { LIST_DELAY_TIME } from '../../Helper/Constants';

//Component imports
import CustomeButton from '../Button/index';

const {
    MainView,
    DemandDetailView,
    Demandimage,
    OnDemand,
    OnDemandText,
    DemandNameView,
    TitleText,
    ValueText,
    ButtonView
} = Styles;

const SupplierSubPendingOrderList = ({ item, navigateToDetail, time, _onPayNow, _noPayLater }) => {

    const [IsLoading, SetIsLoading] = useState(true);
    const [visit_time, SetVisit_time] = useState('');

    useEffect(() => {
        setTimeout(() => {
            SetIsLoading(false)
        }, time)
    }, [])

    /*
    .##....##....###....##.....##.####..######......###....########.####..#######..##....##
    .###...##...##.##...##.....##..##..##....##....##.##......##.....##..##.....##.###...##
    .####..##..##...##..##.....##..##..##.........##...##.....##.....##..##.....##.####..##
    .##.##.##.##.....##.##.....##..##..##...####.##.....##....##.....##..##.....##.##.##.##
    .##..####.#########..##...##...##..##....##..#########....##.....##..##.....##.##..####
    .##...###.##.....##...##.##....##..##....##..##.....##....##.....##..##.....##.##...###
    .##....##.##.....##....###....####..######...##.....##....##....####..#######..##....##
    */

    const _navigateToDetail = (order_id) => {
        navigateToDetail(order_id)
    }

    /*
    .##........#######...######...####..######...######.
    .##.......##.....##.##....##...##..##....##.##....##
    .##.......##.....##.##.........##..##.......##......
    .##.......##.....##.##...####..##..##........######.
    .##.......##.....##.##....##...##..##.............##
    .##.......##.....##.##....##...##..##....##.##....##
    .########..#######...######...####..######...######.
    */

    const renderTime = (Time) => {
        let Hours = Time.substring(0, 2);

        let Minutes = Time.substring(3, 5);

        let isAmPm = Hours >= 12 ? "PM" : 'AM'

        return `${Hours} : ${Minutes} ${isAmPm}`
    }

    /*
    ..######...#######..##.....##.########...#######..##....##.########.##....##.########
    .##....##.##.....##.###...###.##.....##.##.....##.###...##.##.......###...##....##...
    .##.......##.....##.####.####.##.....##.##.....##.####..##.##.......####..##....##...
    .##.......##.....##.##.###.##.########..##.....##.##.##.##.######...##.##.##....##...
    .##.......##.....##.##.....##.##........##.....##.##..####.##.......##..####....##...
    .##....##.##.....##.##.....##.##........##.....##.##...###.##.......##...###....##...
    ..######...#######..##.....##.##.........#######..##....##.########.##....##....##...
    */

    const renderImage = () => {
        return <ImageBackground
            source={{ uri: item.image }}
            style={Demandimage}
            resizeMode='cover'
            imageStyle={{ borderRadius: height(1.5) }}
        />
    }

    const renderDemandView = () => {
        return <View style={DemandNameView}>
            <View style={{ width: width(25) }}>
                <Text style={TitleText}>Supplier name :</Text>
                <Text numberOfLines={1} style={[TitleText, [{ marginTop: height(1) }]]}>Service :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Sub Category :</Text>
                <Text style={[TitleText, { marginTop: height(1) }]}>Scheduled Appointment :</Text>
            </View>

            <View style={{ width: width(25) }}>
                <Text numberOfLines={1} style={ValueText}>{item.firstname + " " + item.lastname}</Text>
                <Text numberOfLines={1} style={[ValueText, [{ marginTop: height(1) }]]}>{item.service}</Text>
                <Text numberOfLines={1} style={[ValueText, { marginTop: height(1) }]}>{item.sub_category}</Text>
                <Text numberOfLines={2} style={[ValueText, { marginTop: height(1) }]}>{moment(item.visit_date).format('DD MMM, YYYY')}</Text>
            </View>
        </View>
    }

    const renderDetailView = () => {
        return <View style={DemandDetailView}>
            {renderImage()}
            {renderDemandView()}
        </View>
    }

    const renderPayNow = () => {
        return <CustomeButton
            buttonAction={() => _onPayNow()}
            title='Pay Now'
            textStyle={OnDemandText}
            btnStyle={[OnDemand, {
                width: width(60),
                alignSelf: 'center',
                backgroundColor: Colors.DRAWER_BACKGROUND_COLOR,
                marginTop: height(2)
            }]}
        />
    }

    const renderPayLater = () => {
        return <CustomeButton
            buttonAction={() => _noPayLater()}
            title='Pay Later'
            textStyle={OnDemandText}
            btnStyle={[OnDemand, { width: width(60), alignSelf: 'center', marginTop: height(1) }]}
        />

    }

    const renderMainView = () => {
        return <TouchableOpacity
            onPress={() => _navigateToDetail(item.id)}
            style={MainView}>
            {renderDetailView()}

            {/* {renderPayNow()} */}

            {/* {renderPayLater()} */}
        </TouchableOpacity>
    }

    return (
        IsLoading
            ?
            null
            :
            <Animatable.View
                duration={LIST_DELAY_TIME}
                animation={'fadeInUp'}>
                {renderMainView()}
            </Animatable.View>
    )
}

export default SupplierSubPendingOrderList;