//Global imports
import React, { useRef } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Modal from 'react-native-modal';

//File imports
import Colors from '../../Helper/Colors';
import Fonts from '../../Helper/Fonts';


const PhotoUploadDialogue = (props) => {


    const { isShow, closeModel, openCamera, openGallery } = props;
    return (
        <Modal  isVisible={isShow}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: Colors.WHITE }}>
                    <Text style={styles.heading}>Select Image</Text>

                    <TouchableOpacity onPress={()=>openCamera()}>
                        <Text style={styles.subTitle}>Take Photo</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>openGallery()}>
                        <Text style={[styles.subTitle, { paddingTop: 0 }]}>Open Gallery</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>closeModel()}>
                        <Text style={[styles.subTitle, { paddingTop: 0 }]}>Close</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    heading: {
        fontFamily: Fonts.POPPINS_REGULAR,
        fontSize: 20,
        color: Colors.BLACK,
        paddingHorizontal: 100,
        paddingVertical: 10
    },

    subTitle: {
        fontSize: 15,
        color: Colors.BLACK06,
        fontFamily: Fonts.POPPINS_REGULAR,
        padding: 15
    }
})

export default PhotoUploadDialogue;